//-----------------------------------------------------------------------------
/// \file
/// FUSE arguments class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FUSEPP_ARGUMENTS_HPP
#define OCTSYNC_FUSEPP_ARGUMENTS_HPP

//-----------------------------------------------------------------------------

#include "Fuse.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Fusepp
{

/// Argument collection.
class Arguments
{
public: /// \todo Private
    /// Internal arguments structure.
    fuse_args m_args;

public:
    /// Constructs empty argument collection.
    Arguments();

    /// Constructs argument collection initialized with application arguments.
    ///
    /// \param argc
    ///         Arguments count (as in main() function).
    /// \param argv
    ///         Array with argument values (as in main() function).
    Arguments(int argc, char** argv);

    /// Destroys the arguments collection.
    ~Arguments();

    /// Appends application arguments to this argument collection.
    ///
    /// \param argc
    ///         Arguments count (as in main() function).
    /// \param argv
    ///         Array with argument values (as in main() function).
    void appendArguments(int argc, char** argv);

    /// Appends single argument to this argument collection.
    ///
    /// \param argument
    ///         Argument to append.
    void appendArgument(const char* const argument);

    /// Inserts single argument to this argument collection.
    ///
    /// The argument is inserted at specified position. This is useful for adding
    /// options at the beginning of the array which must not come after the
    /// special '--' option.
    ///
    /// \param position
    ///         Position at which argument shall be added.
    /// \param argument
    ///         Argument to be inserted.
    void insertArgument(const int position, const char* const argument);

private:
    /// Initializes the arguments.
    ///
    /// Initializes the object as an empty list of arguments.
    void init();

};

}
}

//-----------------------------------------------------------------------------

#endif /* OCTSYNC_FUSEPP_ARGUMENTS_HPP */
