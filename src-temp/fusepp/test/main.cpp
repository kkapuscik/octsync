/*
 * main.cpp
 *
 *  Created on: Mar 3, 2013
 *      Author: krzysztof
 */

#include "../Fuse.hpp"
#include "../Arguments.hpp"
#include "../Options.hpp"

#include <cstdio>
using namespace std;

using OCTsync::Fusepp::Arguments;
using OCTsync::Fusepp::OptionSet;
using OCTsync::Fusepp::NumericOption;
using OCTsync::Fusepp::StringOption;
using OCTsync::Fusepp::BooleanOption;

struct fuse_operations examplefs_oper;

static int process_option(
        void *data,
        const char *arg,
        int key,
        struct fuse_args *outargs)
{
    printf("Option: data=%p, arg=%s, key=%d, outargs=%p\n", data, arg, key, outargs);

    switch (key)
    {
        default:
            // keep all unknown things
            return 1;
    }

    return 0;
}

class MyOptionSet : public OptionSet
{
public:
    NumericOption<int> m_bar;
    StringOption m_foo;
    NumericOption<unsigned int> m_end;
    BooleanOption m_verbose;

public:
    MyOptionSet() :
                    m_bar("--bar", "%d", 50),
                    m_foo("--foo"),
                    m_end("--end", "%u", 0),
                    m_verbose("--verbose")
    {
        addOption(&m_bar);
        addOption(&m_foo);
        addOption(&m_end);
        addOption(&m_verbose);
    }

protected:
    virtual void addOptions()
    {
    }

};

int main(
        int argc,
        char* argv[])
{
    Arguments arguments(argc, argv);

    arguments.appendArgument("-f");
    arguments.appendArgument("-s");
    arguments.appendArgument("bar=10");
    arguments.appendArgument("-bar=10");
    arguments.appendArgument("--bar=10");
    arguments.appendArgument("bar2=10");
    arguments.appendArgument("-bar2=10");
    arguments.appendArgument("--bar2=10");
    arguments.appendArgument("--foo=Zapaua");
    arguments.appendArgument("--end=-3");
    arguments.appendArgument("--verbose");
    arguments.appendArgument("--verbose=Zapaua");

#if 0
    /**
     * Key option.  In case of a match, the processing function will be
     * called with the specified key.
     */
#define FUSE_OPT_KEY(templ, key) { templ, -1U, key }

    /**
     * Last option.  An array of 'struct fuse_opt' must end with a NULL
     * template value
     */
#define FUSE_OPT_END { NULL, 0, 0 }
#endif

    MyOptionSet optionSet;

//    fuse_opt fuseOptions[] =
//    {
//            FUSE_OPT_KEY("bar=%d", 1),
//            FUSE_OPT_KEY("foo=%s", 2),
//            FUSE_OPT_KEY("end=%u", 3),
//            FUSE_OPT_KEY("--bar2=%d", 11),
//            FUSE_OPT_END
//        };

//    const struct fuse_opt opts[]

    printf("InArgs=%p\n", &arguments.m_args);

    if (optionSet.parse(arguments))
//    if (0 != fuse_opt_parse(&arguments.m_args, (void*) 0x12345678, fuseOptions, &process_option))
    {
        printf("Options parsing failed.\n");
        return 1;
    }

#if 0
    fuse_stat = fuse_main(args.argc, args.argv, &examplefs_oper, NULL);

    fuse_opt_free_args(&args);
#endif

    return 0;
}
