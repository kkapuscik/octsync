//-----------------------------------------------------------------------------
/// \file
/// FUSE options class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FUSEPP_OPTIONS_HPP
#define OCTSYNC_FUSEPP_OPTIONS_HPP

//-----------------------------------------------------------------------------

#include "Fuse.hpp"

#include <string>
#include <vector>
#include <cstring>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Fusepp
{

/// Result of the option processing.
enum OptionProcessingResult
{
    /// Error occurred during option parsing.
    OPTION_PROCESSING_PARSE_ERROR = -1,
    /// Option shall be discarded before passing to FUSE.
    OPTION_PROCESSING_DISCARD = 0,
    /// Option shall be kept and passed to FUSE.
    OPTION_PROCESSING_KEEP = 1
};

/// Option descriptor with parsed value.
///
/// Option class with the derived classes are used to describe options that
/// user may specify. Each option is described by name and a template. After
/// arguments parsing is done the options also has information if there was
/// a matching argument present in the input and contains value that was given
/// by the user.
class Option
{
private:
    /// Option name (e.g. --foo).
    const QString m_name;

    /// Option template in FUSE formate (e.g. --foo=%s).
    const QString m_template;

public:
    /// Constructs option with given name and template.
    ///
    /// \param name
    ///             Option name.
    /// \param templateString
    ///             Option template.
    Option(
            const QString& name,
            const QString& templateString) :
                    m_name(name),
                    m_template(templateString)
    {
        // nothing to do
    }

    /// Destroys the option.
    virtual ~Option()
    {
        // nothing to do
    }

    /// Returns option name.
    ///
    /// \return
    /// Option name as given to the constructor.
    const QString& getName() const
    {
        return m_name;
    }

    /// Returns option template.
    ///
    /// \return
    /// Option template as given to the constructor.
    const QString& getTemplate() const
    {
        return m_template;
    }

    /// Resets the option to default state.
    ///
    virtual void reset() = 0;

    /// Processes the argument that matched option template.
    ///
    /// \param argumentValue        Value of the argument to be processed.
    /// \param outputArguments      Current output argument list.
    ///
    /// \return     Result of the processing. See OptionProcessingResult for detailed description
    virtual OptionProcessingResult processArgument(
            const char* argumentValue,
            fuse_args* outputArguments) = 0;

};

/// Option class specialization with specified data type.
///
/// TypedOption class adds two elements to base Option class: information
/// that there was a match in the input arguments and the value that was parsed.
///
/// \tparam Type of the option value.
template<class DATA_TYPE>
class TypedOption : public Option
{
public:
    /// Option data type.
    typedef DATA_TYPE DataType;

protected:
    /// Flag indicating if there was a match with an input argument.
    bool m_matched;

    /// Value of the option parsed from input arguments.
    DataType m_value;

    /// Default option value in case there was no match.
    const DataType m_defaultValue;

public:
    /// Constructs option with given name, template and default value.
    ///
    /// \param name                 Option name.
    /// \param templateString       Option template.
    /// \param defaultValue         Default value used if there was no match.
    ///
    TypedOption(
            const QString& name,
            const QString& templateString,
            const DataType& defaultValue) :
                    Option(name, templateString),
                    m_matched(false),
                    m_value(defaultValue),
                    m_defaultValue(defaultValue)
    {
        // nothing to do
    }

    /// \copydoc Option::reset()
    ///
    /// Resets the state of an option clearing match information
    /// and setting the value to default given to constructor.
    virtual void reset()
    {
        m_matched = false;
        m_value = m_defaultValue;
    }

    /// Checks if there was a matching input argument.
    ///
    /// \retval true        if there was a matching input argument.
    /// \retval false       if there was no matching input arguments.
    bool isMatched() const
    {
        return m_matched;
    }

    /// Returns option value.
    ///
    /// \return             Parsed option value or default if there was no match.
    const DataType& getValue() const
    {
        return m_value;
    }

    /// Returns default value.
    ///
    /// \return             Default value as given to the constructor.
    const DataType& getDefaultValue() const
    {
        return m_defaultValue;
    }

};

/// Boolean option.
///
/// Specialization of Option class that represents boolean (yes/no) options.
class BooleanOption : public TypedOption<bool>
{
public:
    /// Constructs option with given name.
    ///
    /// \param name     Name of the option.
    BooleanOption(
            const QString& name) :
                    TypedOption(name, name, false)
    {
        // nothing to do
    }

    /// \copydoc Option::processArgument()
    virtual OptionProcessingResult processArgument(
            const char* argumentValue,
            fuse_args* outputArguments)
    {
        m_matched = true;
        m_value = true;

        return OPTION_PROCESSING_DISCARD;
    }

};

/// Option with a string value.
///
/// Specialization of Option class that has a string value.
class StringOption : public TypedOption<QString>
{
public:
    /// Constructs option with given name.
    ///
    /// \param name             Name of the option.
    /// \param defaultValue     Default value of the option.
    StringOption(
            const QString& name,
            const QString& defaultValue = QString()) :
                    TypedOption(name, name + "=" + "%s", defaultValue)
    {
        // nothing to do
    }

    /// \copydoc Option::processArgument()
    virtual OptionProcessingResult processArgument(
            const char* argumentValue,
            fuse_args* /*outputArguments*/)
    {
        if (!argumentValue)
        {
            return OPTION_PROCESSING_PARSE_ERROR;
        }

        size_t charsToSkip = getName().size() + 1;
        if (std::strlen(argumentValue) < charsToSkip)
        {
            return OPTION_PROCESSING_PARSE_ERROR;
        }

        argumentValue += charsToSkip;

        m_matched = true;
        m_value = argumentValue;

        printf("------------------- SSSSSSSSSSSSSTRING = %s\n", argumentValue);

        return OPTION_PROCESSING_DISCARD;
    }
};

/// Option with a numeric value.
///
/// Specialization of Option class that has a numeric (e.g. int) value.
template<class DATA_TYPE>
class NumericOption : public TypedOption<DATA_TYPE>
{
public:
    /// Type of the
    typedef DATA_TYPE NumericDataType;

    /// Option format string.
    ///
    /// The format string will be used to parse value using sscanf.
    QString m_formatString;

public:
    /// Constructs the option.
    ///
    /// \param name             Name of the option.
    /// \param formatString     Format string that will be used to parse the value in sscanf format.
    /// \param defaultValue     Default value of the option.
    NumericOption(
            const QString& name,
            const QString& formatString,
            const NumericDataType& defaultValue = 0) :
                    TypedOption<NumericDataType>(name, name + "=" + formatString, defaultValue),
                    m_formatString(formatString)
    {
        // nothing to do
    }

    /// \copydoc Option::processArgument()
    virtual OptionProcessingResult processArgument(
            const char* argumentValue,
            fuse_args* /*outputArguments*/)
    {
        if (!argumentValue)
        {
            return OPTION_PROCESSING_PARSE_ERROR;
        }

        size_t charsToSkip = this->getName().size() + 1;
        if (std::strlen(argumentValue) < charsToSkip)
        {
            return OPTION_PROCESSING_PARSE_ERROR;
        }

        argumentValue += charsToSkip;

        if (1 != sscanf(argumentValue, m_formatString.c_str(), &this->m_value))
        {
            return OPTION_PROCESSING_PARSE_ERROR;
        }

        this->m_matched = true;

        printf("------------------- NUMERICCCCC = %s\n", argumentValue);

        return OPTION_PROCESSING_DISCARD;
    }
};

/// Set of options.
///
/// Options set is a base class from which the user shall derive its own option parser.
///
/// Normally the operations user will need to do are:
/// - Create a class that is derived from OptionSet.
/// - Add separate field for each option using Option derived classes.
/// - Add the fields to OptionSet using addOption().
///
class OptionSet
{
private:
    /// Array of pointers to options.
    typedef std::vector<Option*> OptionArray;

    /// Array of fuse options.
    typedef std::vector<fuse_opt> FuseOptionArray;

private:
    /// Collection of defined options.
    OptionArray m_options;

    /// Collection of FUSE options (used during parsing).
    FuseOptionArray m_fuseOptionArray;

public:
    /// Constructs option set without any options.
    OptionSet()
    {
        // nothing to do
    }

    /// Destroys option set.
    virtual ~OptionSet()
    {
        // nothing to do
    }

    /// Parses the arguments using registered option descriptors.
    ///
    /// \param arguments        Collection of arguments to be parsed.
    ///
    /// \retval true            if arguments were parsed successfully.
    /// \retval false           if arguments parsing failed.
    bool parse(
            Arguments& arguments)
    {
        for (size_t i = 0; i < m_options.size(); ++i)
        {
            fuse_opt fuseOption;

            m_options[i]->reset();

            fuseOption.templ = m_options[i]->getTemplate().c_str();
            fuseOption.offset = -1;
            fuseOption.value = i;

            m_fuseOptionArray.push_back(fuseOption);
        }

        bool result = false;

        if (0
                == fuse_opt_parse(&arguments.m_args, this, m_fuseOptionArray.data(),
                        OptionSet::parseCallback))
        {
            result = true;
        }

        m_fuseOptionArray.clear();

        return result;
    }

protected:
    /// Adds option descriptor.
    ///
    /// \param option           Option descriptor to be added.
    void addOption(
            Option* option)
    {
        m_options.push_back(option);
    }

    /// Processes the argument.
    ///
    /// \param argument         Argument to be processed.
    /// \param key              Key of the option (option index).
    /// \param outputArguments  Current output argument list.
    ///
    /// \return     Result of the processing. See OptionProcessingResult for detailed description
    virtual OptionProcessingResult processArgument(
            const char* argument,
            int key,
            fuse_args* outputArguments)
    {
        OptionProcessingResult result = OPTION_PROCESSING_PARSE_ERROR;

        printf("processArgument(%s,%d,%p)\n", argument, key, outputArguments);

        if (key >= 0)
        {
            result = processMatchedOption(argument, key, outputArguments);
        }
        else if (key == FUSE_OPT_KEY_OPT)
        {
            result = processUnmatchedOption(argument, key, outputArguments);
        }
        else if (key == FUSE_OPT_KEY_NONOPT)
        {
            result = processUnmatchedNonOption(argument, key, outputArguments);
        }

        printf("processArgument(%s,%d,%p) -> %d\n", argument, key, outputArguments, result);

        return result;
    }

    /// Processes the argument when it matched an option.
    ///
    /// \param argument         Argument to be processed.
    /// \param key              Key of the option (option index).
    /// \param outputArguments  Current output argument list.
    ///
    /// \return     Result of the processing. See OptionProcessingResult for detailed description
    virtual OptionProcessingResult processMatchedOption(
            const char* argument,
            int key,
            fuse_args* outputArguments)
    {
        printf("processMatchedOption(%s,%d,%p)\n", argument, key, outputArguments);

        OptionProcessingResult result = OPTION_PROCESSING_PARSE_ERROR;

        if ((key >= 0) && (key < (int) m_options.size()))
        {
            Option* option = m_options[key];

            result = option->processArgument(argument, outputArguments);
        }

        return result;
    }

    /// Processes the argument when it did not matched an option.
    ///
    /// \param argument         Argument to be processed.
    /// \param key              Key of the option (option index).
    /// \param outputArguments  Current output argument list.
    ///
    /// \return     Result of the processing. See OptionProcessingResult for detailed description
    virtual OptionProcessingResult processUnmatchedOption(
            const char* argument,
            int key,
            fuse_args* outputArguments)
    {
        // by default we keep it for the fuse
        return OPTION_PROCESSING_KEEP;
    }

    /// Processes the argument that is not an option.
    ///
    /// \param argument         Argument to be processed.
    /// \param key              Key of the option (option index).
    /// \param outputArguments  Current output argument list.
    ///
    /// \return     Result of the processing. See OptionProcessingResult for detailed description
    virtual OptionProcessingResult processUnmatchedNonOption(
            const char* argument,
            int key,
            fuse_args* outputArguments)
    {
        // by default we keep it for the fuse
        return OPTION_PROCESSING_KEEP;
    }

private:
    /// FUSE options parsing callback.
    ///
    /// \param userData         Pointer to user-specified data.
    ///                         This class passes pointer to parser object as a data.
    /// \param argument         Argument to be processed.
    /// \param key              Key of the option (option index).
    /// \param outputArguments  Current output argument list.
    ///
    /// \return     Result of the processing. See OptionProcessingResult for detailed description
    static int parseCallback(
            void* userData,
            const char* arugment,
            int key,
            fuse_args* outputArguments)
    {
        OptionSet* optionSet = static_cast<OptionSet*>(userData);

        printf("parseCallback(%p,%s,%d,%p)\n", userData, arugment, key, outputArguments);

        return optionSet->processArgument(arugment, key, outputArguments);
    }

};

}
}

//-----------------------------------------------------------------------------

#endif /* OCTSYNC_FUSEPP_OPTIONS_HPP */
