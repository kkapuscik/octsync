//-----------------------------------------------------------------------------
/// \file
/// FUSE arguments class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#include "../Arguments.hpp"

#include <cassert>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Fusepp
{

Arguments::Arguments()
{
    init();
}

Arguments::Arguments(int argc, char** argv)
{
    init();
    appendArguments(argc, argv);
}

Arguments::~Arguments()
{
    fuse_opt_free_args(&m_args);
}

void Arguments::appendArguments(int argc, char** argv)
{
    if (argc > 0)
    {
        assert(argv);

        for (int i = 0; i < argc; ++i)
        {
            appendArgument(argv[i]);
        }
    }
}

void Arguments::appendArgument(const char* const argument)
{
    assert(argument);

    fuse_opt_add_arg(&m_args, argument);
}

void Arguments::insertArgument(const int position, const char* const argument)
{
    assert(argument);

    fuse_opt_insert_arg(&m_args, position, argument);
}

void Arguments::init()
{
    m_args.allocated = 0;
    m_args.argc = 0;
    m_args.argv = nullptr;
}

}
}

//-----------------------------------------------------------------------------

