//-----------------------------------------------------------------------------
/// \file
/// Wrapper and WrapperFactory classes implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#include "../Wrapper.hpp"

#include <cassert>
#include <cerrno>
#include <cstring>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Fusepp
{

/// Implementation of Fuse CPP wrapper.
class WrapperImpl : public Wrapper
{
public:
    /// Returns wrapper singleton.
    ///
    /// \return
    /// Reference to wrapper singleton.
    static WrapperImpl& getInstance();

private:
    /// \copydoc FileSystem::getattr
    static int getattr(
            const char* path,
            struct stat* statBuffer);

    /// \copydoc FileSystem::readlink
    static int readlink(
            const char* path,
            char* targetBuffer,
            size_t targetBufferSize);

    /// \copydoc FileSystem::mknod
    static int mknod(
            const char* path,
            mode_t mode,
            dev_t dev);

    /// \copydoc FileSystem::mkdir
    static int mkdir(
            const char* path,
            mode_t mode);

    /// \copydoc FileSystem::unlink
    static int unlink(
            const char* path);

    /// \copydoc FileSystem::rmdir
    static int rmdir(
            const char* path);

    /// \copydoc FileSystem::symlink
    static int symlink(
            const char* targetPath,
            const char* linkName);

    /// \copydoc FileSystem::rename
    static int rename(
            const char* oldPath,
            const char* newPath);

    /// \copydoc FileSystem::link
    static int link(
            const char* targetPath,
            const char* linkName);

    /// \copydoc FileSystem::chmod
    static int chmod(
            const char* path,
            mode_t mode);

    /// \copydoc FileSystem::chown
    static int chown(
            const char* path,
            uid_t userId,
            gid_t groupId);

    /// \copydoc FileSystem::truncate
    static int truncate(
            const char* path,
            off_t newSize);

    /// \copydoc FileSystem::open
    static int open(
            const char* path,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::read
    static int read(
            const char* path,
            char* buffer,
            size_t size,
            off_t offset,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::write
    static int write(
            const char* path,
            const char* buffer,
            size_t size,
            off_t offset,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::statfs
    static int statfs(
            const char* path,
            struct statvfs* vfsStatBuffer);

    /// \copydoc FileSystem::flush
    static int flush(
            const char* path,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::release
    static int release(
            const char* path,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::fsync
    static int fsync(
            const char* path,
            int isDataSync,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::setxattr
    static int setxattr(
            const char* path,
            const char* name,
            const char* value,
            size_t valueSize,
            int flags);

    /// \copydoc FileSystem::getxattr
    static int getxattr(
            const char* path,
            const char* name,
            char* valueBuffer,
            size_t valueBufferSize);

    /// \copydoc FileSystem::listxattr
    static int listxattr(
            const char* path,
            char* listBuffer,
            size_t listBufferSize);

    /// \copydoc FileSystem::removexattr
    static int removexattr(
            const char* path,
            const char* name);

    /// \copydoc FileSystem::opendir
    static int opendir(
            const char* path,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::readdir
    static int readdir(
            const char* path,
            void* buffer,
            fuse_fill_dir_t fillerFunc,
            off_t offset,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::releasedir
    static int releasedir(
            const char* path,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::fsyncdir
    static int fsyncdir(
            const char* path,
            int isDataSync,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::init
    static void* init(
            struct fuse_conn_info* connection);

    /// \copydoc FileSystem::destroy
    static void destroy(
            void* context);

    /// \copydoc FileSystem::access
    static int access(
            const char* path,
            int mode);

    /// \copydoc FileSystem::create
    static int create(
            const char* path,
            mode_t mode,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::ftruncate
    static int ftruncate(
            const char* path,
            off_t newSize,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::fgetattr
    static int fgetattr(
            const char* path,
            struct stat* statBuffer,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::lock
    static int lock(
            const char* path,
            struct fuse_file_info* fileInfo,
            int command,
            struct flock* lockParams);

    /// \copydoc FileSystem::utimens
    static int utimens(
            const char* path,
            const struct timespec times[2]);

    /// \copydoc FileSystem::bmap
    static int bmap(
            const char* path,
            size_t blockSize,
            uint64_t* index);

    /// \copydoc FileSystem::ioctl
    static int ioctl(
            const char* path,
            int cmd,
            void* arg,
            struct fuse_file_info* fileInfo,
            unsigned int flags,
            void* data);

    /// \copydoc FileSystem::poll
    static int poll(
            const char* path,
            struct fuse_file_info* fileInfo,
            struct fuse_pollhandle* pollHandle,
            unsigned* returnedEvents);

    /// \copydoc FileSystem::write_buf
    static int write_buf(
            const char* path,
            struct fuse_bufvec* buffer,
            off_t offset,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::read_buf
    static int read_buf(
            const char* path,
            struct fuse_bufvec** bufferPtr,
            size_t size,
            off_t offset,
            struct fuse_file_info* fileInfo);

    /// \copydoc FileSystem::flock
    static int flock(
            const char* path,
            struct fuse_file_info* fileInfo,
            int command);

private:
    /// File system called by the wrapper.
    FileSystem* m_fileSystem;

    /// Fuse operations structure.
    fuse_operations m_operations;

public:
    /// Constructs the wrapper.
    WrapperImpl();

    // overridden
    virtual void setFileSystem(
            FileSystem* const fileSystem);

    // overridden
    virtual void enableOption(
            Option option);

    // overridden
    virtual void disableOption(
            Option option);

    // overridden
    virtual void enableOperation(
            Operation operation);

    // overridden
    virtual void disableOperation(
            Operation operation);

    // overridden
    virtual fuse_operations* getOperations();

private:

    /// Resets the wrapper.
    ///
    /// Method disables all options and operations.
    void reset();

    /// Enables fuse file system operation.
    ///
    /// \param operation
    ///         Operation to be enabled.
    /// \param enabled
    ///         True if operation shall be enabled,
    ///         or false to disable the operation.
    void setOperationEnabled(
            Operation operation,
            bool enabled);

};

//-----------------------------------------------------------------------------

WrapperImpl& WrapperImpl::getInstance()
{
    static WrapperImpl theSingleton;

    return theSingleton;
}

int WrapperImpl::getattr(
        const char* path,
        struct stat* statBuffer)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->getattr(path, statBuffer) : ENOSYS;
}

int WrapperImpl::readlink(
        const char* path,
        char* targetBuffer,
        size_t targetBufferSize)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->readlink(path, targetBuffer, targetBufferSize) : ENOSYS;
}

int WrapperImpl::mknod(
        const char* path,
        mode_t mode,
        dev_t dev)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->mknod(path, mode, dev) : ENOSYS;
}

int WrapperImpl::mkdir(
        const char* path,
        mode_t mode)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->mkdir(path, mode) : ENOSYS;
}

int WrapperImpl::unlink(
        const char* path)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->unlink(path) : ENOSYS;
}

int WrapperImpl::rmdir(
        const char* path)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->rmdir(path) : ENOSYS;
}

int WrapperImpl::symlink(
        const char* targetPath,
        const char* linkName)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->symlink(targetPath, linkName) : ENOSYS;
}

int WrapperImpl::rename(
        const char* oldPath,
        const char* newPath)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->rename(oldPath, newPath) : ENOSYS;
}

int WrapperImpl::link(
        const char* targetPath,
        const char* linkName)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->link(targetPath, linkName) : ENOSYS;
}

int WrapperImpl::chmod(
        const char* path,
        mode_t mode)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->chmod(path, mode) : ENOSYS;
}

int WrapperImpl::chown(
        const char* path,
        uid_t userId,
        gid_t groupId)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->chown(path, userId, groupId) : ENOSYS;
}

int WrapperImpl::truncate(
        const char* path,
        off_t newSize)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->truncate(path, newSize) : ENOSYS;
}

int WrapperImpl::open(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->open(path, fileInfo) : ENOSYS;
}

int WrapperImpl::read(
        const char* path,
        char* buffer,
        size_t size,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->read(path, buffer, size, offset, fileInfo) : ENOSYS;
}

int WrapperImpl::write(
        const char* path,
        const char* buffer,
        size_t size,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->write(path, buffer, size, offset, fileInfo) : ENOSYS;
}

int WrapperImpl::statfs(
        const char* path,
        struct statvfs* vfsStatBuffer)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->statfs(path, vfsStatBuffer) : ENOSYS;
}

int WrapperImpl::flush(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->flush(path, fileInfo) : ENOSYS;
}

int WrapperImpl::release(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->release(path, fileInfo) : ENOSYS;
}

int WrapperImpl::fsync(
        const char* path,
        int isDataSync,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->fsync(path, isDataSync, fileInfo) : ENOSYS;
}

int WrapperImpl::setxattr(
        const char* path,
        const char* name,
        const char* value,
        size_t valueSize,
        int flags)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->setxattr(path, name, value, valueSize, flags) : ENOSYS;
}

int WrapperImpl::getxattr(
        const char* path,
        const char* name,
        char* valueBuffer,
        size_t valueBufferSize)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->getxattr(path, name, valueBuffer, valueBufferSize) : ENOSYS;
}

int WrapperImpl::listxattr(
        const char* path,
        char* listBuffer,
        size_t listBufferSize)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->listxattr(path, listBuffer, listBufferSize) : ENOSYS;
}

int WrapperImpl::removexattr(
        const char* path,
        const char* name)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->removexattr(path, name) : ENOSYS;
}

int WrapperImpl::opendir(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->opendir(path, fileInfo) : ENOSYS;
}

int WrapperImpl::readdir(
        const char* path,
        void* buffer,
        fuse_fill_dir_t fillerFunc,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->readdir(path, buffer, fillerFunc, offset, fileInfo) : ENOSYS;
}

int WrapperImpl::releasedir(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->releasedir(path, fileInfo) : ENOSYS;
}

int WrapperImpl::fsyncdir(
        const char* path,
        int isDataSync,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->fsyncdir(path, isDataSync, fileInfo) : ENOSYS;
}

void* WrapperImpl::init(
        struct fuse_conn_info* connection)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->init(connection) : NULL;
}

void WrapperImpl::destroy(
        void* context)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    if (fileSystem)
    {
        fileSystem->destroy(context);
    }
}

int WrapperImpl::access(
        const char* path,
        int mode)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->access(path, mode) : ENOSYS;
}

int WrapperImpl::create(
        const char* path,
        mode_t mode,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->create(path, mode, fileInfo) : ENOSYS;
}

int WrapperImpl::ftruncate(
        const char* path,
        off_t newSize,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->ftruncate(path, newSize, fileInfo) : ENOSYS;
}

int WrapperImpl::fgetattr(
        const char* path,
        struct stat* statBuffer,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->fgetattr(path, statBuffer, fileInfo) : ENOSYS;
}

int WrapperImpl::lock(
        const char* path,
        struct fuse_file_info* fileInfo,
        int command,
        struct flock* lockParams)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->lock(path, fileInfo, command, lockParams) : ENOSYS;
}

int WrapperImpl::utimens(
        const char* path,
        const struct timespec times[2])
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->utimens(path, times) : ENOSYS;
}

int WrapperImpl::bmap(
        const char* path,
        size_t blockSize,
        uint64_t* index)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->bmap(path, blockSize, index) : ENOSYS;
}

int WrapperImpl::ioctl(
        const char* path,
        int cmd,
        void* arg,
        struct fuse_file_info* fileInfo,
        unsigned int flags,
        void* data)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->ioctl(path, cmd, arg, fileInfo, flags, data) : ENOSYS;
}

int WrapperImpl::poll(
        const char* path,
        struct fuse_file_info* fileInfo,
        struct fuse_pollhandle* pollHandle,
        unsigned* returnedEvents)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->poll(path, fileInfo, pollHandle, returnedEvents) : ENOSYS;
}

int WrapperImpl::write_buf(
        const char* path,
        struct fuse_bufvec* buffer,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->write_buf(path, buffer, offset, fileInfo) : ENOSYS;
}

int WrapperImpl::read_buf(
        const char* path,
        struct fuse_bufvec** bufferPtr,
        size_t size,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->read_buf(path, bufferPtr, size, offset, fileInfo) : ENOSYS;
}

int WrapperImpl::flock(
        const char* path,
        struct fuse_file_info* fileInfo,
        int command)
{
    FileSystem* fileSystem = getInstance().m_fileSystem;
    return fileSystem ? fileSystem->flock(path, fileInfo, command) : ENOSYS;
}

WrapperImpl::WrapperImpl() :
                m_fileSystem(nullptr)
{
    reset();
}

void WrapperImpl::reset()
{
    std::memset(&m_fileSystem, 0, sizeof(m_fileSystem));
}

void WrapperImpl::setFileSystem(
        FileSystem* const fileSystem)
{
    m_fileSystem = fileSystem;
    reset();
}

void WrapperImpl::enableOption(
        Wrapper::Option option)
{
    switch (option)
    {
        case NULLPATH_OK:
            m_operations.flag_nullpath_ok = 1;
            break;

        case NOPATH:
            m_operations.flag_nopath = 1;
            break;

        case UTIME_OMIT_OK:
            m_operations.flag_utime_omit_ok = 1;
            break;

        default:
            assert(0);
            break;
    }
}

void WrapperImpl::disableOption(
        Wrapper::Option option)
{
    switch (option)
    {
        case NULLPATH_OK:
            m_operations.flag_nullpath_ok = 0;
            break;

        case NOPATH:
            m_operations.flag_nopath = 0;
            break;

        case UTIME_OMIT_OK:
            m_operations.flag_utime_omit_ok = 0;
            break;

        default:
            assert(0);
            break;
    }
}

void WrapperImpl::enableOperation(
        Wrapper::Operation operation)
{
    assert(m_fileSystem);

    setOperationEnabled(operation, true);
}

void WrapperImpl::disableOperation(
        Wrapper::Operation operation)
{
    setOperationEnabled(operation, false);
}

void WrapperImpl::setOperationEnabled(
        Operation operation,
        bool enabled)
{
    /// \cond DOXYGEN_DO_NOT_INCLUDE
    #define MAKE_CASE(CASENAME, OPNAME) \
        case CASENAME: \
            m_operations.OPNAME = enabled ? OPNAME : nullptr; \
            break;

    switch (operation)
    {
        MAKE_CASE(GETATTR, getattr);
        MAKE_CASE(READLINK, readlink);
        MAKE_CASE(MKNOD, mknod);
        MAKE_CASE(MKDIR, mkdir);
        MAKE_CASE(UNLINK, unlink);
        MAKE_CASE(RMDIR, rmdir);
        MAKE_CASE(SYMLINK, symlink);
        MAKE_CASE(RENAME, rename);
        MAKE_CASE(LINK, link);
        MAKE_CASE(CHMOD, chmod);
        MAKE_CASE(CHOWN, chown);
        MAKE_CASE(TRUNCATE, truncate);
        MAKE_CASE(OPEN, open);
        MAKE_CASE(READ, read);
        MAKE_CASE(WRITE, write);
        MAKE_CASE(STATFS, statfs);
        MAKE_CASE(FLUSH, flush);
        MAKE_CASE(RELEASE, release);
        MAKE_CASE(FSYNC, fsync);
        MAKE_CASE(SETXATTR, setxattr);
        MAKE_CASE(GETXATTR, getxattr);
        MAKE_CASE(LISTXATTR, listxattr);
        MAKE_CASE(REMOVEXATTR, removexattr);
        MAKE_CASE(OPENDIR, opendir);
        MAKE_CASE(READDIR, readdir);
        MAKE_CASE(RELEASEDIR, releasedir);
        MAKE_CASE(FSYNCDIR, fsyncdir);
        MAKE_CASE(INIT, init);
        MAKE_CASE(DESTROY, destroy);
        MAKE_CASE(ACCESS, access);
        MAKE_CASE(CREATE, create);
        MAKE_CASE(FTRUNCATE, ftruncate);
        MAKE_CASE(FGETATTR, fgetattr);
        MAKE_CASE(LOCK, lock);
        MAKE_CASE(UTIMENS, utimens);
        MAKE_CASE(BMAP, bmap);
        MAKE_CASE(IOCTL, ioctl);
        MAKE_CASE(POLL, poll);
        MAKE_CASE(WRITE_BUF, write_buf);
        MAKE_CASE(READ_BUF, read_buf);
        MAKE_CASE(FLOCK, flock);

        default:
            assert(0);
            break;
    }

    #undef MAKE_CASE
    /// \endcond
}

fuse_operations* WrapperImpl::getOperations()
{
    return &m_operations;
}

Wrapper& WrapperFactory::getWrapperSingleton()
{
    return WrapperImpl::getInstance();
}

}
}
