//-----------------------------------------------------------------------------
/// \file
/// FileSystem class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FUSEPP_FUSE_HPP
#define OCTSYNC_FUSEPP_FUSE_HPP

//-----------------------------------------------------------------------------

#undef  FUSE_USE_VERSION

/// The FUSE version required.
#define FUSE_USE_VERSION    26

#include <fuse/fuse.h>
#include <fuse/fuse_opt.h>

//-----------------------------------------------------------------------------

/// Fuse operations.
typedef struct fuse_operations fuse_operations;

/// Fuse arguments.
typedef struct fuse_args fuse_args;

/// Fuse operation context.
typedef struct fuse_context fuse_context;

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FUSEPP_FUSE_HPP*/
