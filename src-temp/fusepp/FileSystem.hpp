//-----------------------------------------------------------------------------
/// \file
/// FUSE file system class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FUSEPP_FILE_SYSTEM_HPP
#define OCTSYNC_FUSEPP_FILE_SYSTEM_HPP

//-----------------------------------------------------------------------------

#include "Fuse.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Fusepp
{

/**
 * The file system operations:
 *
 * Most of these should work very similarly to the well known UNIX
 * file system operations.  A major exception is that instead of
 * returning an error in 'errno', the operation should return the
 * negated error value (-errno) directly.
 *
 * All methods are optional, but some are essential for a useful
 * filesystem (e.g. getattr).  Open, flush, release, fsync, opendir,
 * releasedir, fsyncdir, access, create, ftruncate, fgetattr, lock,
 * init and destroy are special purpose methods, without which a full
 * featured filesystem can still be implemented.
 *
 * Almost all operations take a path which can be of any length.
 *
 * Changed in fuse 2.8.0 (regardless of API version)
 * Previously, paths were limited to a length of PATH_MAX.
 *
 * See http://fuse.sourceforge.net/wiki/ for more information.  There
 * is also a snapshot of the relevant wiki pages in the doc/ folder.
 */
class FileSystem
{
public:
    /// Destroys the filesystem.
    virtual ~FileSystem()
    {
        // nothing to do
    }

    /**
     * Gets file attributes.
     *
     * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
     * ignored.  The 'st_ino' field is ignored except if the 'use_ino'
     * mount option is given.
     *
     * \param path
     *          Path to object for which attributes are retrieved.
     * \param statBuffer
     *          Place to store the attributes.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int getattr(
            const char* path,
            struct stat* statBuffer) = 0;

    /**
     * Reads the target of a symbolic link
     *
     * The buffer should be filled with a null terminated string.  The
     * buffer size argument includes the space for the terminating
     * null character.  If the link name is too long to fit in the
     * buffer, it should be truncated.  The return value should be 0
     * for success.
     *
     * \param path
     *          Path to the link.
     * \param targetBuffer
     *          Buffer to store the link target name.
     * \param targetBufferSize
     *          Size of the buffer.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int readlink(
            const char* path,
            char* targetBuffer,
            size_t targetBufferSize) = 0;

    /**
     * Creates a file node.
     *
     * This is called for creation of all non-directory, non-symlink
     * nodes. If the file system defines a create() method, then for
     * regular files that will be called instead.
     *
     * \param path
     *          Path to the node to be created.
     * \param mode
     *          The mode argument specifies both the permissions to use
     *          and the type of node to be created.
     * \param dev
     *          If the file type is S_IFCHR or S_IFBLK then dev specifies the major and minor
     *          numbers of the newly created device special file (makedev(3) may be useful to
     *          build the value for dev); otherwise it is ignored.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int mknod(
            const char* path,
            mode_t mode,
            dev_t dev) = 0;

    /**
     * Creates a directory.
     *
     * Note that the mode argument may not have the type specification
     * bits set, i.e. S_ISDIR(mode) can be false.  To obtain the
     * correct directory type bits use  mode|S_IFDIR
     *
     * \param path
     *          Path to the directory to be created.
     * \param mode
     *          The argument mode specifies the permissions to use.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int mkdir(
            const char* path,
            mode_t mode) = 0;

    /**
     * Removes a file.
     *
     * \param path
     *          Path to the file to be removed.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int unlink(
            const char* path) = 0;

    /**
     * Removes a directory.
     *
     * \param path
     *          Path to the directory to be removed.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int rmdir(
            const char* path) = 0;

    /**
     * Creates a symbolic link.
     *
     * \param targetPath
     *          Path to the link target.
     * \param linkName
     *          Name of the link to create.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int symlink(
            const char* targetPath,
            const char* linkName) = 0;

    /**
     * Renames a file.
     *
     * \param oldPath
     *          Path to the file object to be renamed.
     * \param newPath
     *          New name (path) of the object.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int rename(
            const char* oldPath,
            const char* newPath) = 0;

    /**
     * Creates a hard link to a file.
     *
     * \param targetPath
     *          Path to the link target.
     * \param linkName
     *          Name of the link to create.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int link(
            const char* targetPath,
            const char* linkName) = 0;

    /**
     * Changes the permission bits of a file.
     *
     * \param path
     *          Path to the object to be modified.
     * \param mode
     *          OR-ed permission flag to set.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int chmod(
            const char* path,
            mode_t mode) = 0;

    /**
     * Changes the owner and group of a file.
     *
     * \param path
     *          Path to the object to be modified.
     * \param userId
     *          New owner user identifier.
     * \param groupId
     *          New group identifier.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int chown(
            const char* path,
            uid_t userId,
            gid_t groupId) = 0;

    /**
     * Changes the size of a file.
     *
     * If the file previously was larger than this size, the extra data is lost.
     * If the file previously was shorter, it is extended, and the extended part
     * reads as null bytes ('\0').
     *
     * \param path
     *          Path to the file to be modified.
     * \param newSize
     *          The new size of the file.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int truncate(
            const char* path,
            off_t newSize) = 0;

    /**
     * Opens file.
     *
     * No creation (O_CREAT, O_EXCL) and by default also no
     * truncation (O_TRUNC) flags will be passed to open(). If an
     * application specifies O_TRUNC, fuse first calls truncate()
     * and then open(). Only if 'atomic_o_trunc' has been
     * specified and kernel version is 2.6.24 or later, O_TRUNC is
     * passed on to open.
     *
     * Unless the 'default_permissions' mount option is given,
     * open should check if the operation is permitted for the
     * given flags. Optionally open may also return an arbitrary
     * file handle in the fuse_file_info structure, which will be
     * passed to all file operations.
     *
     * Changed in version 2.2
     *
     * \param path
     *          Path to the file to be opened.
     * \param fileInfo
     *          File information structure to be filled.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     * The file handle may be stored in the \p fileInfo structure
     */
    virtual int open(
            const char* path,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Reads data from an open file.
     *
     * Read should return exactly the number of bytes requested except
     * on EOF or error, otherwise the rest of the data will be
     * substituted with zeroes.  An exception to this is when the
     * 'direct_io' mount option is specified, in which case the return
     * value of the read system call will reflect the return value of
     * this operation.
     *
     * Changed in version 2.2
     *
     * \param path
     *          Path to the opened file.
     * \param buffer
     *          Buffer to store the data read.
     * \param size
     *          Number of bytes to read.
     * \param offset
     *          Offset from which the data shall be read.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Number of bytes read (see method description), zero on EOF
     * or error code (-errno) otherwise.
     *
     * \todo Verify the return value.
     */
    virtual int read(
            const char* path,
            char* buffer,
            size_t size,
            off_t offset,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Writes data to an open file
     *
     * Write should return exactly the number of bytes requested
     * except on error.  An exception to this is when the 'direct_io'
     * mount option is specified (see read operation).
     *
     * Changed in version 2.2
     * \param path
     *          Path to the opened file.
     * \param buffer
     *          Buffer with data to write.
     * \param size
     *          Number of bytes to write.
     * \param offset
     *          Offset at which the data shall be written.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Number of bytes written (see method description),
     * or error code (-errno) otherwise.
     *
     * \todo Verify the return value.
     */
    virtual int write(
            const char* path,
            const char* buffer,
            size_t size,
            off_t offset,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Gets file system statistics.
     *
     * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored.
     *
     * Replaced 'struct statfs' parameter with 'struct statvfs' in
     * version 2.5
     *
     * \param path
     *          Path to any file on the file system.
     * \param vfsStatBuffer
     *          Buffer to be filled with file system statistics.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int statfs(
            const char* path,
            struct statvfs* vfsStatBuffer) = 0;

    /**
     * Possibly flushes cached data.
     *
     * BIG NOTE: This is not equivalent to fsync().  It's not a
     * request to sync dirty data.
     *
     * Flush is called on each close() of a file descriptor.  So if a
     * file system wants to return write errors in close() and the file
     * has cached dirty data, this is a good place to write back data
     * and return any errors.  Since many applications ignore close()
     * errors this is not always useful.
     *
     * NOTE: The flush() method may be called more than once for each
     * open().  This happens if more than one file descriptor refers
     * to an opened file due to dup(), dup2() or fork() calls.  It is
     * not possible to determine if a flush is final, so each flush
     * should be treated equally.  Multiple write-flush sequences are
     * relatively rare, so this shouldn't be a problem.
     *
     * File systems shouldn't assume that flush will always be called
     * after some writes, or that if will be called at all.
     *
     * Changed in version 2.2
     *
     * \param path
     *          Path to file for which buffers shall be flushed.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int flush(
            const char* path,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Releases an open file
     *
     * Release is called when there are no more references to an open
     * file: all file descriptors are closed and all memory mappings
     * are unmapped.
     *
     * For every open() call there will be exactly one release() call
     * with the same flags and file descriptor.  It is possible to
     * have a file opened more than once, in which case only the last
     * release will mean, that no more reads/writes will happen on the
     * file.  The return value of release is ignored.
     *
     * Changed in version 2.2
     *
     * \param path
     *          Path to the opened file.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int release(
            const char* path,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Synchronizes file contents.
     *
     * If the \p isDataSync parameter is non-zero, then only the user data
     * should be flushed, not the meta data.
     *
     * Changed in version 2.2
     *
     * \param path
     *          Path to file for which buffers shall be flushed.
     * \param isDataSync
     *          Flag indicating that only user data should be flushed.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int fsync(
            const char* path,
            int isDataSync,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Sets extended attribute.
     *
     * \param path
     *          Path to the accessed object.
     * \param name
     *          Name of the attribute.
     * \param value
     *          Value of the attribute.
     * \param valueSize
     *          Size of the attribute value.
     * \param flags
     *          Flags.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     *
     * \todo Describe flags
     */
    virtual int setxattr(
            const char* path,
            const char* name,
            const char* value,
            size_t valueSize,
            int flags) = 0;

    /**
     * Gets extended attribute.
     *
     * \param path
     *          Path to the accessed object.
     * \param name
     *          Name of the attribute.
     * \param valueBuffer
     *          Buffer to store the value.
     * \param valueBufferSize
     *          Size of the buffer.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int getxattr(
            const char* path,
            const char* name,
            char* valueBuffer,
            size_t valueBufferSize) = 0;

    /**
     * Lists extended attributes.
     *
     * \param path
     *          Path to the accessed object.
     * \param listBuffer
     *          Buffer to store the attributes list.
     * \param listBufferSize
     *          Size of the buffer.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int listxattr(
            const char* path,
            char* listBuffer,
            size_t listBufferSize) = 0;

    /**
     * Removes extended attributes.
     *
     * \param path
     *          Path to the accessed object.
     * \param  name
     *          Name of the attribute.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int removexattr(
            const char* path,
            const char* name) = 0;

    /**
     * Opens a directory.
     *
     * Unless the 'default_permissions' mount option is given,
     * this method should check if opendir is permitted for this
     * directory. Optionally opendir may also return an arbitrary
     * file handle in the fuse_file_info structure, which will be
     * passed to readdir, closedir and fsyncdir.
     *
     * Introduced in version 2.3
     *
     * \param path
     *          Path to the directory to be opened.
     * \param fileInfo
     *          File info structure to be filled.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     * The file handle may be stored in the \p fileInfo structure
     */
    virtual int opendir(
            const char* path,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Reads directory.
     *
     * This supersedes the old getdir() interface.  New applications
     * should use this.
     *
     * The file system may choose between two modes of operation:
     *
     * 1) The readdir implementation ignores the offset parameter, and
     * passes zero to the filler function's offset.  The filler
     * function will not return '1' (unless an error happens), so the
     * whole directory is read in a single readdir operation.  This
     * works just like the old getdir() method.
     *
     * 2) The readdir implementation keeps track of the offsets of the
     * directory entries.  It uses the offset parameter and always
     * passes non-zero offset to the filler function.  When the buffer
     * is full (or an error happens) the filler function will return
     * '1'.
     *
     * Introduced in version 2.3
     *
     * \param path
     *          Path to the directory.
     * \param buffer
     *          Buffer to be passed to filler function.
     * \param fillerFunc
     *          Function for filling the buffer with directory listing.
     * \param offset
     *          Offset.
     * \param fileInfo
     *          Structure describing the directory.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     *
     * \todo Describe and explain the offset.
     */
    virtual int readdir(
            const char* path,
            void* buffer,
            fuse_fill_dir_t fillerFunc,
            off_t offset,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Releases directory.
     *
     * Introduced in version 2.3
     *
     * \param path
     *          Path to the directory.
     * \param fileInfo
     *          Structure describing the directory.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int releasedir(
            const char* path,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Synchronizes directory contents.
     *
     * If the datasync parameter is non-zero, then only the user data
     * should be flushed, not the meta data
     *
     * Introduced in version 2.3
     *
     * \param path
     *          Path to directory for which buffers shall be flushed.
     * \param isDataSync
     *          Flag indicating that only user data should be flushed.
     * \param fileInfo
     *          Structure describing the directory.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int fsyncdir(
            const char* path,
            int isDataSync,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Initializes file system.
     *
     * The return value will passed in the private_data field of
     * fuse_context to all file operations and as a parameter to the
     * destroy() method.
     *
     * Introduced in version 2.3
     * Changed in version 2.6
     *
     * \param connection
     *          Connection structure which provides the kernel options
     *          and allows requesting some features by the file system
     *          implementation.
     *
     * \return
     * File system context.
     */
    virtual void* init(
            struct fuse_conn_info* connection) = 0;

    /**
     * Clean up file system.
     *
     * Called on file system exit.
     *
     * Introduced in version 2.3
     *
     * \param context
     *          File system context returned by init().
     */
    virtual void destroy(
            void* context) = 0;

    /**
     * Checks file access permissions.
     *
     * This will be called for the access() system call.  If the
     * 'default_permissions' mount option is given, this method is not
     * called.
     *
     * This method is not called under Linux kernel versions 2.4.x
     *
     * Introduced in version 2.5
     *
     * \param path
     *          Path to the object being checked.
     * \param mode
     *          Accessibility checks to be performed (e.g. F_OK).
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int access(
            const char* path,
            int mode) = 0;

    /**
     * Creates and opens a file.
     *
     * If the file does not exist, first create it with the specified
     * mode, and then open it.
     *
     * If this method is not implemented or under Linux kernel
     * versions earlier than 2.6.15, the mknod() and open() methods
     * will be called instead.
     *
     * Introduced in version 2.5.
     *
     * \param path
     *          Path to a file to be created.
     * \param mode
     *          The mode argument specifies both the permissions to use
     *          and the type of node to be created.
     * \param fileInfo
     *          File information structure to be filled.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     * The file handle may be stored in the \p fileInfo structure
     */
    virtual int create(
            const char* path,
            mode_t mode,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Changes the size of an open file.
     *
     * This method is called instead of the truncate() method if the
     * truncation was invoked from an ftruncate() system call.
     *
     * If this method is not implemented or under Linux kernel
     * versions earlier than 2.6.15, the truncate() method will be
     * called instead.
     *
     * Introduced in version 2.5
     *
     * \param path
     *          Path to the file to be modified.
     * \param newSize
     *          The new size of the file.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int ftruncate(
            const char* path,
            off_t newSize,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Gets attributes from an open file.
     *
     * This method is called instead of the getattr() method if the
     * file information is available.
     *
     * Currently this is only called after the create() method if that
     * is implemented (see above).  Later it may be called for
     * invocations of fstat() too.
     *
     * Introduced in version 2.5
     *
     * \param path
     *          Path to object for which attributes are retrieved.
     * \param statBuffer
     *          Place to store the attributes.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int fgetattr(
            const char* path,
            struct stat* statBuffer,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Performs POSIX file locking operation.
     *
     * The cmd argument will be either F_GETLK, F_SETLK or F_SETLKW.
     *
     * For the meaning of fields in 'struct flock' see the man page
     * for fcntl(2).  The l_whence field will always be set to
     * SEEK_SET.
     *
     * For checking lock ownership, the 'fuse_file_info->owner'
     * argument must be used.
     *
     * For F_GETLK operation, the library will first check currently
     * held locks, and if a conflicting lock is found it will return
     * information without calling this method.  This ensures, that
     * for local locks the l_pid field is correctly filled in.  The
     * results may not be accurate in case of race conditions and in
     * the presence of hard links, but it's unlikely that an
     * application would rely on accurate GETLK results in these
     * cases.  If a conflicting lock is not found, this method will be
     * called, and the filesystem may fill out l_pid by a meaningful
     * value, or it may leave this field zero.
     *
     * For F_SETLK and F_SETLKW the l_pid field will be set to the pid
     * of the process performing the locking operation.
     *
     * Note: if this method is not implemented, the kernel will still
     * allow file locking to work locally.  Hence it is only
     * interesting for network filesystems and similar.
     *
     * Introduced in version 2.6
     *
     * \param path
     *          Path to the lock file.
     * \param fileInfo
     *          Structure describing the file.
     * \param command
     *          Locking operation to be performed.
     * \param lockParams
     *          Lock parameters (e.g. the range of bytes to lock).
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int lock(
            const char* path,
            struct fuse_file_info* fileInfo,
            int command,
            struct flock* lockParams) = 0;

    /**
     * Changes the access and modification times of a file with
     * nanosecond resolution.
     *
     * This supersedes the old utime() interface.  New applications
     * should use this.
     *
     * See the utimensat(2) man page for details.
     *
     * Introduced in version 2.6
     *
     * \param path
     *          The file to be modified.
     * \param times
     *          Access time (atime) and modification time (mtime).
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int utimens(
            const char* path,
            const struct timespec times[2]) = 0;

    /**
     * Maps block index within file to block index within device.
     *
     * Note: This makes sense only for block device backed filesystems
     * mounted with the 'blkdev' option
     *
     * Introduced in version 2.6
     *
     * \param path
     *          Path to a file.
     * \param blockSize
     *          Size of the block.
     * \param index
     *          Index.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     *
     * \todo Describe index.
     */
    virtual int bmap(
            const char* path,
            size_t blockSize,
            uint64_t* index) = 0;

    /**
     * Performs ioctl.
     *
     * Flags will have FUSE_IOCTL_COMPAT set for 32bit ioctls in
     * 64bit environment.  The size and direction of data is
     * determined by _IOC_*() decoding of cmd.  For _IOC_NONE,
     * data will be NULL, for _IOC_WRITE data is out area, for
     * _IOC_READ in area and if both are set in/out area.  In all
     * non-NULL cases, the area is of _IOC_SIZE(cmd) bytes.
     *
     * Introduced in version 2.8
     *
     * \param path
     *          Path to a file.
     * \param cmd
     *          Command code.
     * \param arg
     *          Command argument.
     * \param fileInfo
     *          Structure describing the file.
     * \param flags
     *          Operation flags.
     * \param data
     *          Operation data.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     *
     * \todo Describe arguments correctly.
     */
    virtual int ioctl(
            const char* path,
            int cmd,
            void* arg,
            struct fuse_file_info* fileInfo,
            unsigned int flags,
            void* data) = 0;

    /**
     * Polls for IO readiness events.
     *
     * Note: If pollHandle is non-NULL, the client should notify
     * when IO readiness events occur by calling
     * fuse_notify_poll() with the specified pollHandle.
     *
     * Regardless of the number of times poll with a non-NULL pollHandle
     * is received, single notification is enough to clear all.
     * Notifying more times incurs overhead but doesn't harm
     * correctness.
     *
     * The callee is responsible for destroying pollHandle with
     * fuse_pollhandle_destroy() when no longer in use.
     *
     * Introduced in version 2.8
     *
     * \param path
     *          Path of the file being checked for events.
     * \param fileInfo
     *          Structure describing the file.
     * \param pollHandle
     *          Poll operation handle.
     * \param returnedEvents
     *          Place to store returned events.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int poll(
            const char* path,
            struct fuse_file_info* fileInfo,
            struct fuse_pollhandle* pollHandle,
            unsigned* returnedEvents) = 0;

    /**
     * Writes contents of buffer to an open file
     *
     * Similar to the write() method, but data is supplied in a
     * generic buffer.  Use fuse_buf_copy() to transfer data to
     * the destination.
     *
     * Introduced in version 2.9
     *
     * \param path
     *          Path to the opened file.
     * \param buffer
     *          Buffer with data to write.
     * \param offset
     *          Offset at which the data shall be written.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int write_buf(
            const char* path,
            struct fuse_bufvec* buffer,
            off_t offset,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Stores data from an open file in a buffer
     *
     * Similar to the read() method, but data is stored and
     * returned in a generic buffer.
     *
     * No actual copying of data has to take place, the source
     * file descriptor may simply be stored in the buffer for
     * later data transfer.
     *
     * The buffer must be allocated dynamically and stored at the
     * location pointed to by bufp.  If the buffer contains memory
     * regions, they too must be allocated using malloc().  The
     * allocated memory will be freed by the caller.
     *
     * Introduced in version 2.9
     *
     * \param path
     *          Path to the opened file.
     * \param bufferPtr
     *          Pointer to place where pointer to allocated buffer shall be stored.
     * \param size
     *          Number of bytes to read.
     * \param offset
     *          Offset from which the data shall be read.
     * \param fileInfo
     *          Structure describing the file.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int read_buf(
            const char* path,
            struct fuse_bufvec** bufferPtr,
            size_t size,
            off_t offset,
            struct fuse_file_info* fileInfo) = 0;

    /**
     * Perform BSD file locking operation
     *
     * The op argument will be either LOCK_SH, LOCK_EX or LOCK_UN
     *
     * Nonblocking requests will be indicated by ORing LOCK_NB to
     * the above operations
     *
     * For more information see the flock(2) manual page.
     *
     * Additionally fi->owner will be set to a value unique to
     * this open file.  This same value will be supplied to
     * ->release() when the file is released.
     *
     * Note: if this method is not implemented, the kernel will still
     * allow file locking to work locally.  Hence it is only
     * interesting for network filesystems and similar.
     *
     * Introduced in version 2.9
     *
     * \param path
     *          Path to the lock file.
     * \param fileInfo
     *          Structure describing the file.
     * \param command
     *          Locking operation to be performed.
     *
     * \return
     * Zero on success, error code (-errno) otherwise.
     */
    virtual int flock(
            const char* path,
            struct fuse_file_info* fileInfo,
            int command) = 0;

protected:
    /// Returns operation context.
    ///
    /// The context is only valid for the duration of a filesystem
    /// operation, and thus must not be stored and used later.
    ///
    /// @return
    /// Context object.
    fuse_context* getContext()
    {
        return fuse_get_context();
    }

};

}
}

//-----------------------------------------------------------------------------

#endif /* OCTSYNC_FUSEPP_FILE_SYSTEM_HPP */
