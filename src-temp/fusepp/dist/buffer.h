/*
 * buffer.h
 *
 *  Created on: Feb 10, 2013
 *      Author: krzysztof
 */

#ifndef BUFFER_H_
#define BUFFER_H_

#include "defs.h"

#include <stddef.h>
#include <glib.h>
#include <errno.h>

struct buffer
{
    uint8_t *p;
    size_t len;
    size_t size;
};

static inline void buf_init(
        struct buffer *buf,
        size_t size)
{
    if (size)
    {
        buf->p = (uint8_t *) malloc(size);
        if (!buf->p)
        {
            fprintf(stderr, "sshfs: memory allocation failed\n");
            abort();
        }
    }
    else
        buf->p = NULL;
    buf->len = 0;
    buf->size = size;
}

static inline void buf_free(
        struct buffer *buf)
{
    free(buf->p);
}

static inline void buf_finish(
        struct buffer *buf)
{
    buf->len = buf->size;
}

static inline void buf_clear(
        struct buffer *buf)
{
    buf_free(buf);
    buf_init(buf, 0);
}

static void buf_resize(
        struct buffer *buf,
        size_t len)
{
    buf->size = (buf->len + len + 63) & ~31;
    buf->p = (uint8_t *) realloc(buf->p, buf->size);
    if (!buf->p)
    {
        fprintf(stderr, "sshfs: memory allocation failed\n");
        abort();
    }
}

static inline void buf_check_add(
        struct buffer *buf,
        size_t len)
{
    if (buf->len + len > buf->size)
        buf_resize(buf, len);
}

#define _buf_add_mem(b, d, l)           \
    buf_check_add(b, l);            \
    memcpy(b->p + b->len, d, l);        \
    b->len += l;

static inline void buf_add_mem(
        struct buffer *buf,
        const void *data,
        size_t len)
{
    _buf_add_mem(buf, data, len);
}

static inline void buf_add_buf(
        struct buffer *buf,
        const struct buffer *bufa)
{
    _buf_add_mem(buf, bufa->p, bufa->len);
}

static inline void buf_add_uint8(
        struct buffer *buf,
        uint8_t val)
{
    _buf_add_mem(buf, &val, 1);
}

static inline void buf_add_uint32(
        struct buffer *buf,
        uint32_t val)
{
    uint32_t nval = htonl(val);
    _buf_add_mem(buf, &nval, 4);
}

static inline void buf_add_uint64(
        struct buffer *buf,
        uint64_t val)
{
    buf_add_uint32(buf, val >> 32);
    buf_add_uint32(buf, val & 0xffffffff);
}

static inline void buf_add_data(
        struct buffer *buf,
        const struct buffer *data)
{
    buf_add_uint32(buf, data->len);
    buf_add_mem(buf, data->p, data->len);
}

static inline void buf_add_string(
        struct buffer *buf,
        const char *str)
{
    struct buffer data;
    data.p = (uint8_t *) str;
    data.len = strlen(str);
    buf_add_data(buf, &data);
}

static inline void buf_add_path(
        struct buffer *buf,
        const char *path,
        const char* base_path)
{
    char *realpath;

    if (base_path[0])
    {
        if (path[1])
        {
            if (base_path[strlen(base_path) - 1] != '/')
            {
                realpath = g_strdup_printf("%s/%s", base_path, path + 1);
            }
            else
            {
                realpath = g_strdup_printf("%s%s", base_path, path + 1);
            }
        }
        else
        {
            realpath = g_strdup(base_path);
        }
    }
    else
    {
        if (path[1])
            realpath = g_strdup(path + 1);
        else
            realpath = g_strdup(".");
    }
    buf_add_string(buf, realpath);
    g_free(realpath);
}

static int buf_check_get(
        struct buffer *buf,
        size_t len)
{
    if (buf->len + len > buf->size)
    {
        fprintf(stderr, "buffer too short\n");
        return -1;
    }
    else
        return 0;
}

static inline int buf_get_mem(
        struct buffer *buf,
        void *data,
        size_t len)
{
    if (buf_check_get(buf, len) == -1)
        return -1;
    memcpy(data, buf->p + buf->len, len);
    buf->len += len;
    return 0;
}

static inline int buf_get_uint8(
        struct buffer *buf,
        uint8_t *val)
{
    return buf_get_mem(buf, val, 1);
}

static inline int buf_get_uint32(
        struct buffer *buf,
        uint32_t *val)
{
    uint32_t nval;
    if (buf_get_mem(buf, &nval, 4) == -1)
        return -1;
    *val = ntohl(nval);
    return 0;
}

static inline int buf_get_uint64(
        struct buffer *buf,
        uint64_t *val)
{
    uint32_t val1;
    uint32_t val2;
    if (buf_get_uint32(buf, &val1) == -1 || buf_get_uint32(buf, &val2) == -1)
    {
        return -1;
    }
    *val = ((uint64_t) val1 << 32) + val2;
    return 0;
}

static inline int buf_get_data(
        struct buffer *buf,
        struct buffer *data)
{
    uint32_t len;
    if (buf_get_uint32(buf, &len) == -1 || len > buf->size - buf->len)
        return -1;
    buf_init(data, len + 1);
    data->size = len;
    if (buf_get_mem(buf, data->p, data->size) == -1)
    {
        buf_free(data);
        return -1;
    }
    return 0;
}

static inline int buf_get_string(
        struct buffer *buf,
        char **str)
{
    struct buffer data;
    if (buf_get_data(buf, &data) == -1)
        return -1;
    data.p[data.size] = '\0';
    *str = (char *) data.p;
    return 0;
}

extern int fix_ssh_uid_gid(uint32_t* uid, uint32_t* gid);

static int buf_get_attrs(
        struct buffer *buf,
        struct stat *stbuf,
        int *flagsp,
        unsigned blksize)
{
    uint32_t flags;
    uint64_t size = 0;
    uint32_t uid = 0;
    uint32_t gid = 0;
    uint32_t atime = 0;
    uint32_t mtime = 0;
    uint32_t mode = S_IFREG | 0777;

    if (buf_get_uint32(buf, &flags) == -1)
        return -EIO;
    if (flagsp)
        *flagsp = flags;
    if ((flags & SSH_FILEXFER_ATTR_SIZE) && buf_get_uint64(buf, &size) == -1)
        return -EIO;
    if ((flags & SSH_FILEXFER_ATTR_UIDGID)
            && (buf_get_uint32(buf, &uid) == -1 || buf_get_uint32(buf, &gid) == -1))
        return -EIO;
    if ((flags & SSH_FILEXFER_ATTR_PERMISSIONS) && buf_get_uint32(buf, &mode) == -1)
        return -EIO;
    if ((flags & SSH_FILEXFER_ATTR_ACMODTIME))
    {
        if (buf_get_uint32(buf, &atime) == -1 || buf_get_uint32(buf, &mtime) == -1)
            return -EIO;
    }
    if ((flags & SSH_FILEXFER_ATTR_EXTENDED))
    {
        uint32_t extcount;
        unsigned i;
        if (buf_get_uint32(buf, &extcount) == -1)
            return -EIO;
        for (i = 0; i < extcount; i++)
        {
            struct buffer tmp;
            if (buf_get_data(buf, &tmp) == -1)
                return -EIO;
            buf_free(&tmp);
            if (buf_get_data(buf, &tmp) == -1)
                return -EIO;
            buf_free(&tmp);
        }
    }

    if (fix_ssh_uid_gid(&uid, &gid))
    {
        return -EPERM;
    }

    memset(stbuf, 0, sizeof(struct stat));
    stbuf->st_mode = mode;
    stbuf->st_nlink = 1;
    stbuf->st_size = size;
    if (blksize)
    {
        stbuf->st_blksize = blksize;
        stbuf->st_blocks = ((size + blksize - 1) & ~((unsigned long long) blksize - 1))
                >> 9;
    }
    stbuf->st_uid = uid;
    stbuf->st_gid = gid;
    stbuf->st_atime = atime;
    stbuf->st_ctime = stbuf->st_mtime = mtime;
    return 0;
}

static int buf_get_statvfs(
        struct buffer *buf,
        struct statvfs *stbuf)
{
    uint64_t bsize;
    uint64_t frsize;
    uint64_t blocks;
    uint64_t bfree;
    uint64_t bavail;
    uint64_t files;
    uint64_t ffree;
    uint64_t favail;
    uint64_t fsid;
    uint64_t flag;
    uint64_t namemax;

    if (buf_get_uint64(buf, &bsize) == -1 || buf_get_uint64(buf, &frsize) == -1
            || buf_get_uint64(buf, &blocks) == -1 || buf_get_uint64(buf, &bfree) == -1
            || buf_get_uint64(buf, &bavail) == -1 || buf_get_uint64(buf, &files) == -1
            || buf_get_uint64(buf, &ffree) == -1 || buf_get_uint64(buf, &favail) == -1
            || buf_get_uint64(buf, &fsid) == -1 || buf_get_uint64(buf, &flag) == -1
            || buf_get_uint64(buf, &namemax) == -1)
    {
        return -1;
    }

    memset(stbuf, 0, sizeof(struct statvfs));
    stbuf->f_bsize = bsize;
    stbuf->f_frsize = frsize;
    stbuf->f_blocks = blocks;
    stbuf->f_bfree = bfree;
    stbuf->f_bavail = bavail;
    stbuf->f_files = files;
    stbuf->f_ffree = ffree;
    stbuf->f_favail = favail;
    stbuf->f_namemax = namemax;

    return 0;
}

static int buf_get_entries(
        struct buffer *buf,
        fuse_cache_dirh_t h,
        fuse_cache_dirfil_t filler,
        int follow_symlinks,
        int blksize)
{
    uint32_t count;
    unsigned i;

    if (buf_get_uint32(buf, &count) == -1)
        return -EIO;

    for (i = 0; i < count; i++)
    {
        int err = -1;
        char *name;
        char *longname;
        struct stat stbuf;
        if (buf_get_string(buf, &name) == -1)
            return -EIO;
        if (buf_get_string(buf, &longname) != -1)
        {
            free(longname);
            err = buf_get_attrs(buf, &stbuf, NULL, blksize);
            if (!err)
            {
                if (follow_symlinks && S_ISLNK(stbuf.st_mode))
                {
                    stbuf.st_mode = 0;
                }
                filler(h, name, &stbuf);
            }
        }
        free(name);
        if (err)
            return err;
    }
    return 0;
}

#endif /* BUFFER_H_ */
