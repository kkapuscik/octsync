//-----------------------------------------------------------------------------
/// \file
/// Wrapper and WrapperFactory classes definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FUSEPP_WRAPPER_HPP
#define OCTSYNC_FUSEPP_WRAPPER_HPP

//-----------------------------------------------------------------------------

#include "Fuse.hpp"
#include "FileSystem.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Fusepp
{

/// C++ wrapper of Fuse operation callbacks.
class Wrapper
{
public:
    /// Fuse file system options (flags).
    enum Option
    {
        NULLPATH_OK,
        NOPATH,
        UTIME_OMIT_OK,
    };

    /// Fuse file system operations.
    enum Operation
    {
        GETATTR,
        READLINK,
        // GETDIR,
        MKNOD,
        MKDIR,
        UNLINK,
        RMDIR,
        SYMLINK,
        RENAME,
        LINK,
        CHMOD,
        CHOWN,
        TRUNCATE,
        // UTIME,
        OPEN,
        READ,
        WRITE,
        STATFS,
        FLUSH,
        RELEASE,
        FSYNC,
        SETXATTR,
        GETXATTR,
        LISTXATTR,
        REMOVEXATTR,
        OPENDIR,
        READDIR,
        RELEASEDIR,
        FSYNCDIR,
        INIT,
        DESTROY,
        ACCESS,
        CREATE,
        FTRUNCATE,
        FGETATTR,
        LOCK,
        UTIMENS,
        BMAP,
        IOCTL,
        POLL,
        WRITE_BUF,
        READ_BUF,
        FLOCK,
    };

public:
    /// Constructs the wrapper.
    Wrapper()
    {
        // nothing to do
    }

    /// Destroys the wrapper
    ///
    virtual ~Wrapper()
    {
        // nothing to do
    }

    /// Sets file system to be used by the wrapper.
    ///
    /// This method must be called with non-nullptr file system before any
    /// call to enableOption() and enableOperation() is done.
    /// Calling to this function disables all previously set options and operations.
    ///
    /// \param fileSystem
    ///         The file system to be used (may be nullptr).
    virtual void setFileSystem(
            FileSystem* const fileSystem) = 0;

    /// Enables fuse file system option.
    ///
    /// \param option
    ///         Option to be enabled.
    virtual void enableOption(
            Option option) = 0;

    /// Disables fuse file system option.
    ///
    /// \param option
    ///         Option to be disabled.
    virtual void disableOption(
            Option option) = 0;

    /// Enables fuse file system operation.
    ///
    /// \param operation
    ///         Operation to be enabled.
    virtual void enableOperation(
            Operation operation) = 0;

    /// Disables fuse file system operation.
    ///
    /// \param operation
    ///         Operation to be disabled.
    virtual void disableOperation(
            Operation operation) = 0;

    /// Returns Fuse file system operation callbacks.
    ///
    /// \return
    /// Fuse file system operation callbacks structure.
    virtual fuse_operations* getOperations() = 0;

};

/// C++ wrapper factory.
class WrapperFactory
{
public:
    /// Returns wrapper singleton.
    ///
    /// \return
    /// Wrapper singleton.
    static Wrapper& getWrapperSingleton();

private:
    /// Private constructor - forbid creating instances.
    WrapperFactory();

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FUSEPP_WRAPPER_HPP*/
