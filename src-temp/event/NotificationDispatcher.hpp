//-----------------------------------------------------------------------------
/// \file
/// Notification dispatcher.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_EVENT_NOTIFICATION_DISPATCHER_HPP
#define OCTSYNC_EVENT_NOTIFICATION_DISPATCHER_HPP

//-----------------------------------------------------------------------------

#include "Notification.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Event
{

class NotificationQueue
{
public:


public:
    NotificationQueue()
    {
        // nothing to do
    }

    void dispatch(Notification::Ptr& notification);

    void addObserver(const NotificationAbstractObserver& observer);

    void removeObserver(const NotificationAbstractObserver& observer);

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_EVENT_NOTIFICATION_DISPATCHER_HPP*/
