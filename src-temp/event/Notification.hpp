//-----------------------------------------------------------------------------
/// \file
/// Base class for notifications.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_EVENT_NOTIFICATION_HPP
#define OCTSYNC_EVENT_NOTIFICATION_HPP

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Event
{

/// Base class for all notifications.
class Notification
{
public:
    typedef std::unique_ptr<Notification> Ptr;

public:
    /// Constructs the notification.
    Notification()
    {
        // nothing to do
    }

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_EVENT_NOTIFICATION_HPP*/
