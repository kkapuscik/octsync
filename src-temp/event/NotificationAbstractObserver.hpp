//-----------------------------------------------------------------------------
/// \file
/// Notification observer.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_EVENT_NOTIFICATION_OBSERVER_HPP
#define OCTSYNC_EVENT_NOTIFICATION_OBSERVER_HPP

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Event
{

class NotificationAbstractObserver
{
public:

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_EVENT_NOTIFICATION_OBSERVER_HPP*/
