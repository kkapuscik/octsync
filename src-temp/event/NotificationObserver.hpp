//-----------------------------------------------------------------------------
/// \file
/// Notification observer.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_EVENT_NOTIFICATION_OBSERVER_HPP
#define OCTSYNC_EVENT_NOTIFICATION_OBSERVER_HPP

//-----------------------------------------------------------------------------

#include "NotificationAbstractObserver.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Event
{

class NotificationObserver : public NotificationAbstractObserver
{
public:
    NotificationObserver()
    {
        // nothing to do
    }

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_EVENT_NOTIFICATION_OBSERVER_HPP*/
