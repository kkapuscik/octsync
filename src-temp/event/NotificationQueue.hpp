//-----------------------------------------------------------------------------
/// \file
/// Notification queue.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_EVENT_NOTIFICATION_QUEUE_HPP
#define OCTSYNC_EVENT_NOTIFICATION_QUEUE_HPP

//-----------------------------------------------------------------------------

#include "Notification.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Event
{

/// Notification queue.
class NotificationQueue
{
public:
    NotificationQueue()
    {
        // nothing to do
    }

    void enqueue(Notification::Ptr& notification);

    Notification::Ptr dequeue();

    Notification::Ptr waitAndDequeue();

    Notification::Ptr waitAndDequeue(const size_t waitMs);

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_EVENT_NOTIFICATION_QUEUE_HPP*/
