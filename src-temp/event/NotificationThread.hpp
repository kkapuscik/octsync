//-----------------------------------------------------------------------------
/// \file
/// NotificationThread - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_NotificationThread
#define INCLUDED_OCTsync_NotificationThread

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Event
{

// forward references in namespace
class NotificationThreadPrivate;

/// TODO: Class documentation
///
class NotificationThread
{
// Types (public, protected, private)
// (none)

// Static constants (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    std::unique_ptr<NotificationThreadPrivate> m_private;

// Methods (public, protected, private)
public:
    NotificationThread();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_NotificationThread*/
