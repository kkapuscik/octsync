//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

//#include "TestApp.hpp"
//
//#include <framework/DependencyDescriptor.hpp>
//#include <framework/Framework.hpp>
//#include <commons/OctException.hpp>
//#include <iostream>
#include <cstdlib>

//#include <fuse/fuse.h>
//
//using OctSync::Framework::Framework;
//using OctSync::Framework::DependencyDescriptor;
//using OctSync::Commons::OctException;
//using OctSync::AppTest::TestApp;

//-----------------------------------------------------------------------------

int main(
        int argc,
        char* argv[])
{


//    try
//    {
//        std::cout << "Test application execution started." << std::endl;
//
//        Framework framework(DependencyDescriptor::create<TestApp>(), argc, argv);
//
//        int appExitCode = framework.execute();
//
//        std::cout << "Test application execution finished." << std::endl;
//
//        return appExitCode;
//    }
//    catch (const OctException& octException)
//    {
//        std::cerr << "Exception catched: " << octException.getMessage() << std::endl;
//        return EXIT_FAILURE;
//    }
//    catch (...)
//    {
//        std::cerr << "Unknown exception catched" << std::endl;
//        return EXIT_FAILURE;
//    }
    return EXIT_FAILURE;
}
