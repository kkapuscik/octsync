/// FuseOpsWrapper.cpp
///
///  Created on: Feb 5, 2013
///      Author: krzysztof
///

#define FUSE_USE_VERSION    26
#include <fuse/fuse.h>

///
/// The file system operations:
///
/// Most of these should work very similarly to the well known UNIX
/// file system operations.  A major exception is that instead of
/// returning an error in 'errno', the operation should return the
/// negated error value (-errno) directly.
///
/// All methods are optional, but some are essential for a useful
/// file system (e.g. getattr).  Open, flush, release, fsync, opendir,
/// releasedir, fsyncdir, access, create, ftruncate, fgetattr, lock,
/// init and destroy are special purpose methods, without which a full
/// featured file system can still be implemented.
///
/// Almost all operations take a path which can be of any length.
///
/// Changed in fuse 2.8.0 (regardless of API version)
/// Previously, paths were limited to a length of PATH_MAX.
///
/// See http://fuse.sourceforge.net/wiki/ for more information.  There
/// is also a snapshot of the relevant wiki pages in the doc/ folder.
///

///
/// Get file attributes.
///
/// Similar to stat().  The 'st_dev' and 'st_blksize' fields are
/// ignored.  The 'st_ino' field is ignored except if the 'use_ino'
/// mount option is given.
///
static int FuseWrapperGetAttr(
        const char* path,
        struct stat* statBuffer)
{
    /// TODO
}

///
/// Read the target of a symbolic link
///
/// The buffer should be filled with a null terminated string.  The
/// buffer size argument includes the space for the terminating
/// null character.  If the linkname is too long to fit in the
/// buffer, it should be truncated.  The return value should be 0
/// for success.
///
static int FuseWrapperReadLink(
        const char* path,
        char* linkBuffer,
        size_t bufferSize)
{
    /// TODO
}

/// Deprecated, use readdir() instead
static int FuseWrapperGetDir(
        const char* path,
        fuse_dirh_t,
        fuse_dirfil_t)
{
    /// TODO
}

///
/// Create a file node
///
/// This is called for creation of all non-directory, non-symlink
/// nodes.  If the file system defines a create() method, then for
/// regular files that will be called instead.
///
static int FuseWrapperMkNod(
        const char* path,
        mode_t mode,
        dev_t dev)
{
    /// TODO
}

///
/// Create a directory
///
/// Note that the mode argument may not have the type specification
/// bits set, i.e. S_ISDIR(mode) can be false.  To obtain the
/// correct directory type bits use  mode|S_IFDIR
//////
static int FuseWrapperMkDir(
        const char* path,
        mode_t mode)
{
    /// TODO
}

///
/// Remove a file
///
static int FuseWrapperUnlink(
        const char* path)
{
    /// TODO
}

///
/// Remove a directory
///
static int FuseWrapperRmDir(
        const char* path)
{
    /// TODO
}

///
/// Create a symbolic link
///
static int FuseWrapperSymlink(
        const char* path,
        const char* link)
{
    /// TODO
}

///
/// Rename a file
///
static int FuseWrapperRename(
        const char* path,
        const char* newPath)
{
    /// TODO
}

///
/// Create a hard link to a file
///
static int FuseWrapperLink(
        const char* path,
        const char* newPath)
{
    /// TODO
}

///
/// Change the permission bits of a file
///
static int FuseWrapperChMod(
        const char* path,
        mode_t mode)
{
    /// TODO
}

///
/// Change the owner and group of a file
///
static int FuseWrapperChOwn(
        const char* path,
        uid_t uid,
        gid_t gid)
{
    /// TODO
}

///
/// Change the size of a file
///
static int FuseWrapperTruncate(
        const char* path,
        off_t newSize)
{
    /// TODO
}

///
/// Change the access and/or modification times of a file
///
/// Deprecated, use utimens() instead.
///
static int FuseWrapperUTime(
        const char* path,
        struct utimbuf* timeBuffer)
{
    /// TODO
}

///
/// File open operation
///
/// No creation (O_CREAT, O_EXCL) and by default also no
/// truncation (O_TRUNC) flags will be passed to open(). If an
/// application specifies O_TRUNC, fuse first calls truncate()
/// and then open(). Only if 'atomic_o_trunc' has been
/// specified and kernel version is 2.6.24 or later, O_TRUNC is
/// passed on to open.
///
/// Unless the 'default_permissions' mount option is given,
/// open should check if the operation is permitted for the
/// given flags. Optionally open may also return an arbitrary
/// filehandle in the fuse_file_info structure, which will be
/// passed to all file operations.
///
/// Changed in version 2.2
///
static int FuseWrapperOpen(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Read data from an open file
///
/// Read should return exactly the number of bytes requested except
/// on EOF or error, otherwise the rest of the data will be
/// substituted with zeroes.  An exception to this is when the
/// 'direct_io' mount option is specified, in which case the return
/// value of the read system call will reflect the return value of
/// this operation.
///
/// Changed in version 2.2
///
static int FuseWrapperRead(
        const char* path,
        char* buffer,
        size_t bufferSize,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Write data to an open file
///
/// Write should return exactly the number of bytes requested
/// except on error.  An exception to this is when the 'direct_io'
/// mount option is specified (see read operation).
///
/// Changed in version 2.2
///
static int FuseWrapperWrite(
        const char* path,
        const char* buffer,
        size_t bufferSize,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Get file system statistics
///
/// The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
///
/// Replaced 'struct statfs' parameter with 'struct statvfs' in
/// version 2.5
///
static int FuseWrapperStatFs(
        const char* path,
        struct statvfs* statInfo)
{
    /// TODO
}

///
/// Possibly flush cached data
///
/// BIG NOTE: This is not equivalent to fsync().  It's not a
/// request to sync dirty data.
///
/// Flush is called on each close() of a file descriptor.  So if a
/// file system wants to return write errors in close() and the file
/// has cached dirty data, this is a good place to write back data
/// and return any errors.  Since many applications ignore close()
/// errors this is not always useful.
///
/// NOTE: The flush() method may be called more than once for each
/// open().  This happens if more than one file descriptor refers
/// to an opened file due to dup(), dup2() or fork() calls.  It is
/// not possible to determine if a flush is final, so each flush
/// should be treated equally.  Multiple write-flush sequences are
/// relatively rare, so this shouldn't be a problem.
///
/// file systems shouldn't assume that flush will always be called
/// after some writes, or that if will be called at all.
///
/// Changed in version 2.2
///
static int FuseWrapperFlush(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Release an open file
///
/// Release is called when there are no more references to an open
/// file: all file descriptors are closed and all memory mappings
/// are unmapped.
///
/// For every open() call there will be exactly one release() call
/// with the same flags and file descriptor.  It is possible to
/// have a file opened more than once, in which case only the last
/// release will mean, that no more reads/writes will happen on the
/// file.  The return value of release is ignored.
///
/// Changed in version 2.2
///
static int FuseWrapperRelease(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Synchronize file contents
///
/// If the datasync parameter is non-zero, then only the user data
/// should be flushed, not the meta data.
///
/// Changed in version 2.2
///
static int FuseWrapperFSync(
        const char* path,
        int isDataSync,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Set extended attributes
///
static int FuseWrapperSetXAttr(
        const char* path,
        const char* name,
        const char* value,
        size_t valueSize,
        int flags)
{
    /// TODO
}

///
/// Get extended attributes
///
static int FuseWrapperGetXAttr(
        const char* path,
        const char* name,
        char* valueBuffer,
        size_t bufferSize)
{
    /// TODO
}

///
/// List extended attributes
///
static int FuseWrapperListXAttr(
        const char* path,
        char* listBuffer,
        size_t bufferSize)
{
    /// TODO
}

///
/// Remove extended attributes
///
static int FuseWrapperRemoveXAttr(
        const char* path,
        const char* name)
{
    /// TODO
}

///
/// Open directory
///
/// Unless the 'default_permissions' mount option is given,
/// this method should check if opendir is permitted for this
/// directory. Optionally opendir may also return an arbitrary
/// filehandle in the fuse_file_info structure, which will be
/// passed to readdir, closedir and fsyncdir.
///
/// Introduced in version 2.3
///
static int FuseWrapperOpenDir(
        const char* path,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Read directory
///
/// This supersedes the old getdir() interface.  New applications
/// should use this.
///
/// The file system may choose between two modes of operation:
///
/// 1) The readdir implementation ignores the offset parameter, and
/// passes zero to the filler function's offset.  The filler
/// function will not return '1' (unless an error happens), so the
/// whole directory is read in a single readdir operation.  This
/// works just like the old getdir() method.
///
/// 2) The readdir implementation keeps track of the offsets of the
/// directory entries.  It uses the offset parameter and always
/// passes non-zero offset to the filler function.  When the buffer
/// is full (or an error happens) the filler function will return
/// '1'.
///
/// Introduced in version 2.3
///
static int FuseWrapperReadDir(
        const char* path,
        void* buffer,
        fuse_fill_dir_t filler,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Release directory
///
/// Introduced in version 2.3
///
static int FuseWrapperReleaseDir(
        const char* path,
        struct fuse_file_info* pathInfo)
{
    /// TODO
}

///
/// Synchronize directory contents
///
/// If the datasync parameter is non-zero, then only the user data
/// should be flushed, not the meta data
///
/// Introduced in version 2.3
///
static int FuseWrapperFSyncDir(
        const char* path,
        int isDataSync,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Initialize file system
///
/// The return value will passed in the private_data field of
/// fuse_context to all file operations and as a parameter to the
/// destroy() method.
///
/// Introduced in version 2.3
/// Changed in version 2.6
///
static void* FuseWrapperInit(
        struct fuse_conn_info* connection)
{
    /// TODO
}

///
/// Clean up file system
///
/// Called on file system exit.
///
/// Introduced in version 2.3
///
static void FuseWrapperDestroy(
        void* fsPrivate)
{
    /// TODO
}

///
/// Check file access permissions
///
/// This will be called for the access() system call.  If the
/// 'default_permissions' mount option is given, this method is not
/// called.
///
/// This method is not called under Linux kernel versions 2.4.x
///
/// Introduced in version 2.5
///
static int FuseWrapperAccess(
        const char* path,
        int) // TODO
{
    /// TODO
}

///
/// Create and open a file
///
/// If the file does not exist, first create it with the specified
/// mode, and then open it.
///
/// If this method is not implemented or under Linux kernel
/// versions earlier than 2.6.15, the mknod() and open() methods
/// will be called instead.
///
/// Introduced in version 2.5
///
static int FuseWrapperCreate(
        const char* path,
        mode_t mode,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Change the size of an open file
///
/// This method is called instead of the truncate() method if the
/// truncation was invoked from an ftruncate() system call.
///
/// If this method is not implemented or under Linux kernel
/// versions earlier than 2.6.15, the truncate() method will be
/// called instead.
///
/// Introduced in version 2.5
///
static int FuseWrapperFTruncate(
        const char* file,
        off_t newSize,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Get attributes from an open file
///
/// This method is called instead of the getattr() method if the
/// file information is available.
///
/// Currently this is only called after the create() method if that
/// is implemented (see above).  Later it may be called for
/// invocations of fstat() too.
///
/// Introduced in version 2.5
///
static int FuseWrapperFGetAttr(
        const char* path,
        struct stat* statBuffer,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Perform POSIX file locking operation
///
/// The cmd argument will be either F_GETLK, F_SETLK or F_SETLKW.
///
/// For the meaning of fields in 'struct flock' see the man page
/// for fcntl(2).  The l_whence field will always be set to
/// SEEK_SET.
///
/// For checking lock ownership, the 'fuse_file_info->owner'
/// argument must be used.
///
/// For F_GETLK operation, the library will first check currently
/// held locks, and if a conflicting lock is found it will return
/// information without calling this method.  This ensures, that
/// for local locks the l_pid field is correctly filled in.  The
/// results may not be accurate in case of race conditions and in
/// the presence of hard links, but it's unlikely that an
/// application would rely on accurate GETLK results in these
/// cases.  If a conflicting lock is not found, this method will be
/// called, and the file system may fill out l_pid by a meaningful
/// value, or it may leave this field zero.
///
/// For F_SETLK and F_SETLKW the l_pid field will be set to the pid
/// of the process performing the locking operation.
///
/// Note: if this method is not implemented, the kernel will still
/// allow file locking to work locally.  Hence it is only
/// interesting for network file systems and similar.
///
/// Introduced in version 2.6
///
static int FuseWrapperLock(
        const char* path,
        struct fuse_file_info* fileInfo,
        int command,
        struct flock* lock)
{
    /// TODO
}

///
/// Change the access and modification times of a file with
/// nanosecond resolution
///
/// This supersedes the old utime() interface.  New applications
/// should use this.
///
/// See the utimensat(2) man page for details.
///
/// Introduced in version 2.6
///
static int FuseWrapperUTimeNs(
        const char* path,
        const struct timespec tv[2])
{
    /// TODO
}

///
/// Map block index within file to block index within device
///
/// Note: This makes sense only for block device backed file systems
/// mounted with the 'blkdev' option
///
/// Introduced in version 2.6
///
static int FuseWrapperBMap(
        const char* path,
        size_t blockSize,
        uint64_t* index)
{
    /// TODO
}

///
/// Ioctl
///
/// flags will have FUSE_IOCTL_COMPAT set for 32bit ioctls in
/// 64bit environment.  The size and direction of data is
/// determined by _IOC_*() decoding of cmd.  For _IOC_NONE,
/// data will be NULL, for _IOC_WRITE data is out area, for
/// _IOC_READ in area and if both are set in/out area.  In all
/// non-NULL cases, the area is of _IOC_SIZE(cmd) bytes.
///
/// Introduced in version 2.8
///
static int FuseWrapperIoCtl(
        const char* path,
        int command,
        void* argument,
        struct fuse_file_info* fileInfo,
        unsigned int flags,
        void* data)
{
    /// TODO
}

///
/// Poll for IO readiness events
///
/// Note: If ph is non-NULL, the client should notify
/// when IO readiness events occur by calling
/// fuse_notify_poll() with the specified ph.
///
/// Regardless of the number of times poll with a non-NULL ph
/// is received, single notification is enough to clear all.
/// Notifying more times incurs overhead but doesn't harm
/// correctness.
///
/// The callee is responsible for destroying ph with
/// fuse_pollhandle_destroy() when no longer in use.
///
/// Introduced in version 2.8
///
static int FuseWrapperPoll(
        const char* path,
        struct fuse_file_info* fileInfo,
        struct fuse_pollhandle* pollHandle,
        unsigned* reventsp) // TODO
{
    /// TODO
}

///
/// Write contents of buffer to an open file
///
/// Similar to the write() method, but data is supplied in a
/// generic buffer.  Use fuse_buf_copy() to transfer data to
/// the destination.
///
/// Introduced in version 2.9
///
static int FuseWrapperWriteBuf(
        const char* path,
        struct fuse_bufvec* buffer,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Store data from an open file in a buffer
///
/// Similar to the read() method, but data is stored and
/// returned in a generic buffer.
///
/// No actual copying of data has to take place, the source
/// file descriptor may simply be stored in the buffer for
/// later data transfer.
///
/// The buffer must be allocated dynamically and stored at the
/// location pointed to by bufp.  If the buffer contains memory
/// regions, they too must be allocated using malloc().  The
/// allocated memory will be freed by the caller.
///
/// Introduced in version 2.9
///
static int FuseWrapperReadBuf(
        const char* path,
        struct fuse_bufvec** bufferPtr,
        size_t size,
        off_t offset,
        struct fuse_file_info* fileInfo)
{
    /// TODO
}

///
/// Perform BSD file locking operation
///
/// The op argument will be either LOCK_SH, LOCK_EX or LOCK_UN
///
/// Nonblocking requests will be indicated by ORing LOCK_NB to
/// the above operations
///
/// For more information see the flock(2) manual page.
///
/// Additionally fi->owner will be set to a value unique to
/// this open file.  This same value will be supplied to
/// ->release() when the file is released.
///
/// Note: if this method is not implemented, the kernel will still
/// allow file locking to work locally.  Hence it is only
/// interesting for network file systems and similar.
///
/// Introduced in version 2.9
///
static int FuseWrapperFLock(
        const char* path,
        struct fuse_file_info* fileInfo,
        int operation)
{
    /// TODO
}

///
/// Flag indicating that the file system can accept a NULL path
/// as the first argument for the following operations:
///
/// read, write, flush, release, fsync, readdir, releasedir,
/// fsyncdir, ftruncate, fgetattr, lock, ioctl and poll
///
/// If this flag is set these operations continue to work on
/// unlinked files even if "-ohard_remove" option was specified.
///
const unsigned int FUSE_WRAPPER_FLAG_NULLPATH_OK = 0;

///
/// Flag indicating that the path need not be calculated for
/// the following operations:
///
/// read, write, flush, release, fsync, readdir, releasedir,
/// fsyncdir, ftruncate, fgetattr, lock, ioctl and poll
///
/// Closely related to flag_nullpath_ok, but if this flag is
/// set then the path will not be calculated even if the file
/// wasn't unlinked.  However the path can still be non-NULL if
/// it needs to be calculated for some other reason.
///
static const unsigned int FUSE_WRAPPER_FLAG_NOPATH = 0;

///
/// Flag indicating that the file system accepts special
/// UTIME_NOW and UTIME_OMIT values in its utimens operation.
///
static const unsigned int FUSE_WRAPPER_FLAG_UTIME_OMIT_OK = 0;

static fuse_operations fuseWrapperOperations;

void foo()
{
#if 0
    fuseWrapperOperations.getattr = FuseWrapperGetAttr;
    fuseWrapperOperations.readlink = FuseWrapperReadLink;
    fuseWrapperOperations.getdir = FuseWrapperGetDir;
    fuseWrapperOperations.mknod = FuseWrapperMkNod;
    fuseWrapperOperations.mkdir = FuseWrapperMkDir;
    fuseWrapperOperations.unlink = FuseWrapperUnlink;
    fuseWrapperOperations.rmdir = FuseWrapperRmDir;
    fuseWrapperOperations.symlink = FuseWrapperSymlink;
    fuseWrapperOperations.rename = FuseWrapperRename;
    fuseWrapperOperations.link = FuseWrapperLink;
    fuseWrapperOperations.chmod = FuseWrapperChMod;
    fuseWrapperOperations.chown = FuseWrapperChOwn;
    fuseWrapperOperations.truncate = FuseWrapperTruncate;
    fuseWrapperOperations.utime = FuseWrapperUTime;
    fuseWrapperOperations.open = FuseWrapperOpen;
    fuseWrapperOperations.read = FuseWrapperRead;
    fuseWrapperOperations.write = FuseWrapperWrite;
    fuseWrapperOperations.statfs = FuseWrapperStatFs;
    fuseWrapperOperations.flush = FuseWrapperFlush;
    fuseWrapperOperations.release = FuseWrapperRelease;
    fuseWrapperOperations.fsync = FuseWrapperFSync;
    fuseWrapperOperations.setxattr = FuseWrapperSetXAttr;
    fuseWrapperOperations.getxattr = FuseWrapperGetXAttr;
    fuseWrapperOperations.listxattr = FuseWrapperListXAttr;
    fuseWrapperOperations.removexattr = FuseWrapperRemoveXAttr;
    fuseWrapperOperations.opendir = FuseWrapperOpenDir;
    fuseWrapperOperations.readdir = FuseWrapperReadDir;
    fuseWrapperOperations.releasedir = FuseWrapperReleaseDir;
    fuseWrapperOperations.fsyncdir = FuseWrapperFSyncDir;
    fuseWrapperOperations.init = FuseWrapperInit;
    fuseWrapperOperations.destroy = FuseWrapperDestroy;
    fuseWrapperOperations.access = FuseWrapperAccess;
    fuseWrapperOperations.create = FuseWrapperCreate;
    fuseWrapperOperations.ftruncate = FuseWrapperFTruncate;
    fuseWrapperOperations.fgetattr = FuseWrapperFGetAttr;
    fuseWrapperOperations.lock = FuseWrapperLock;
    fuseWrapperOperations.utimens = FuseWrapperUTimeNs;
    fuseWrapperOperations.bmap = FuseWrapperBMap;
    fuseWrapperOperations.ioctl = FuseWrapperIoCtl;
    fuseWrapperOperations.poll = FuseWrapperPoll;
    fuseWrapperOperations.write_buf = FuseWrapperWriteBuf;
    fuseWrapperOperations.read_buf = FuseWrapperReadBuf;
    fuseWrapperOperations.flock = FuseWrapperFLock;
#endif

    fuseWrapperOperations.flag_nullpath_ok = FUSE_WRAPPER_FLAG_NULLPATH_OK;
    fuseWrapperOperations.flag_nopath = FUSE_WRAPPER_FLAG_NOPATH;
    fuseWrapperOperations.flag_utime_omit_ok = FUSE_WRAPPER_FLAG_UTIME_OMIT_OK;
    fuseWrapperOperations.flag_reserved = 0;
}
