// contents of fsplugin.h  version 2.0 (30.Jan.2009)

#include "wintypes.h"

// ids for FsGetFile
#define FS_FILE_OK                      0
#define FS_FILE_EXISTS                  1
#define FS_FILE_NOTFOUND                2
#define FS_FILE_READERROR               3
#define FS_FILE_WRITEERROR              4
#define FS_FILE_USERABORT               5
#define FS_FILE_NOTSUPPORTED            6
#define FS_FILE_EXISTSRESUMEALLOWED     7

#define FS_EXEC_OK          0
#define FS_EXEC_ERROR       1
#define FS_EXEC_YOURSELF    -1
#define FS_EXEC_SYMLINK     -2

#define FS_COPYFLAGS_OVERWRITE              1
#define FS_COPYFLAGS_RESUME                 2
#define FS_COPYFLAGS_MOVE                   4
#define FS_COPYFLAGS_EXISTS_SAMECASE        8
#define FS_COPYFLAGS_EXISTS_DIFFERENTCASE   16

// flags for tRequestProc
#define RT_Other                0
#define RT_UserName             1
#define RT_Password             2
#define RT_Account              3
#define RT_UserNameFirewall     4
#define RT_PasswordFirewall     5
#define RT_TargetDir            6
#define RT_URL                  7
#define RT_MsgOK                8
#define RT_MsgYesNo             9
#define RT_MsgOKCancel          10

// flags for tLogProc
#define MSGTYPE_CONNECT             1
#define MSGTYPE_DISCONNECT          2
#define MSGTYPE_DETAILS             3
#define MSGTYPE_TRANSFERCOMPLETE    4
#define MSGTYPE_CONNECTCOMPLETE     5
#define MSGTYPE_IMPORTANTERROR      6
#define MSGTYPE_OPERATIONCOMPLETE   7

// flags for FsStatusInfo
#define FS_STATUS_START     0
#define FS_STATUS_END       1

#define FS_STATUS_OP_LIST               1
#define FS_STATUS_OP_GET_SINGLE         2
#define FS_STATUS_OP_GET_MULTI          3
#define FS_STATUS_OP_PUT_SINGLE         4
#define FS_STATUS_OP_PUT_MULTI          5
#define FS_STATUS_OP_RENMOV_SINGLE      6
#define FS_STATUS_OP_RENMOV_MULTI       7
#define FS_STATUS_OP_DELETE             8
#define FS_STATUS_OP_ATTRIB             9
#define FS_STATUS_OP_MKDIR              10
#define FS_STATUS_OP_EXEC               11
#define FS_STATUS_OP_CALCSIZE           12
#define FS_STATUS_OP_SEARCH             13
#define FS_STATUS_OP_SEARCH_TEXT        14
#define FS_STATUS_OP_SYNC_SEARCH        15
#define FS_STATUS_OP_SYNC_GET           16
#define FS_STATUS_OP_SYNC_PUT           17
#define FS_STATUS_OP_SYNC_DELETE        18

#define FS_ICONFLAG_SMALL           1
#define FS_ICONFLAG_BACKGROUND      2

#define FS_ICON_USEDEFAULT          0
#define FS_ICON_EXTRACTED           1
#define FS_ICON_EXTRACTED_DESTROY   2
#define FS_ICON_DELAYED             3

#define FS_BITMAP_NONE                          0
#define FS_BITMAP_EXTRACTED                     1
#define FS_BITMAP_EXTRACT_YOURSELF              2
#define FS_BITMAP_EXTRACT_YOURSELF_ANDDELETE    3

#define FS_BITMAP_CACHE         256

#define FS_CRYPT_SAVE_PASSWORD          1
#define FS_CRYPT_LOAD_PASSWORD          2
#define FS_CRYPT_LOAD_PASSWORD_NO_UI    3   // Load password only if master password has already been entered!
#define FS_CRYPT_COPY_PASSWORD          4   // Copy encrypted password to new connection name
#define FS_CRYPT_MOVE_PASSWORD          5   // Move password when renaming a connection
#define FS_CRYPT_DELETE_PASSWORD        6   // Delete password

#define FS_CRYPTOPT_MASTERPASS_SET      1   // The user already has a master password defined

typedef struct
{
    DWORD SizeLow, SizeHigh;
    FILETIME LastWriteTime;
    int Attr;
} RemoteInfoStruct;

typedef struct
{
    int size;
    DWORD PluginInterfaceVersionLow;
    DWORD PluginInterfaceVersionHi;
    char DefaultIniName[MAX_PATH];
} FsDefaultParamStruct;

// callback functions
typedef int (__stdcall *tProgressProc)(
        int PluginNr,
        char* SourceName,
        char* TargetName,
        int PercentDone);

typedef int (__stdcall *tProgressProcW)(
        int PluginNr,
        WCHAR* SourceName,
        WCHAR* TargetName,
        int PercentDone);

typedef void (__stdcall *tLogProc)(
        int PluginNr,
        int MsgType,
        char* LogString);

typedef void (__stdcall *tLogProcW)(
        int PluginNr,
        int MsgType,
        WCHAR* LogString);

typedef BOOL (__stdcall *tRequestProc)(
        int PluginNr,
        int RequestType,
        char* CustomTitle,
        char* CustomText,
        char* ReturnedText,
        int maxlen);

typedef BOOL (__stdcall *tRequestProcW)(
        int PluginNr,
        int RequestType,
        WCHAR* CustomTitle,
        WCHAR* CustomText,
        WCHAR* ReturnedText,
        int maxlen);

typedef int (__stdcall *tCryptProc)(
        int PluginNr,
        int CryptoNr,
        int Mode,
        char* ConnectionName,
        char* Password,
        int maxlen);

typedef int (__stdcall *tCryptProcW)(
        int PluginNr,
        int CryptoNr,
        int Mode,
        WCHAR* ConnectionName,
        WCHAR* Password,
        int maxlen);

// Function prototypes

/**
 * FsInit is called when loading the plugin.
 * The passed values should be stored in the plugin for later use.
 *
 * Description of parameters:
 * PluginNr         Internal number this plugin was given in Total Commander. Has to be passed as the
 *                  first parameter in all callback functions so Totalcmd knows which plugin has sent the request.
 * pProgressProc    Pointer to the progress callback function.
 * pLogProc         Pointer to the logging function
 * pRequestProc     Pointer to the request text proc
 *
 * Return value:
 * The return value is currently unused. You should return 0 when successful.
 *
 * Remarks:
 * FsInit is NOT called when the user initially installs the plugin. Only FsGetDefRootName is
 * called in this case, and then the plugin DLL is unloaded again. The plugin DLL is loaded when the
 * user enters the plugin root in Network Neighborhood.
 */
int __stdcall FsInit(
        int PluginNr,
        tProgressProc pProgressProc,
        tLogProc pLogProc,
        tRequestProc pRequestProc);

int __stdcall FsInitW(
        int PluginNr,
        tProgressProcW pProgressProcW,
        tLogProcW pLogProcW,
        tRequestProcW pRequestProcW);

/**
 * FsFindFirst is called to retrieve the first file in a directory of the plugin's file system.
 *
 * Description of parameters:
 * Path         Full path to the directory for which the directory listing has to be retrieved.
 *              Important: no wildcards are passed to the plugin! All separators will be backslashes,
 *              so you will need to convert them to forward slashes if your file system uses them!
 *              As root, a single backslash is passed to the plugin. The root items appear in the
 *              plugin base directory retrieved by FsGetDefRootName at installation time. This default
 *              root name is NOT part of the path passed to the plugin!
 *              All subdirs are built from the directory names the plugin returns through FsFindFirst
 *              and FsFindNext, separated by single backslashes, e.g. \Some server\c:\subdir
 *
 * FindData     A standard WIN32_FIND_DATA struct as defined in the Windows SDK, which contains the
 *              file or directory details. Use the dwFileAttributes field set to FILE_ATTRIBUTE_DIRECTORY
 *              to distinguish files from directories. On Unix systems, you can | (or) the
 *              dwFileAttributes field with 0x80000000 and set the dwReserved0 parameter to the Unix
 *              file mode (permissions)..
 *
 * Return value:
 * Return INVALID_HANDLE_VALUE (==-1, not zero!) if an error occurs, or a number of your choice if not.
 *              It is recommended to pass a pointer to an internal structure as this handle, which stores
 *              the current state of the search. This will allow recursive directory searches needed for
 *              copying whole trees. This handle will be passed to FsFindNext() by the calling program.
 *
 * When an error occurs, call SetLastError() to set the reason of the error. Total Commander checks
 * for the following two errors:
 * 1. ERROR_NO_MORE_FILES: The directory exists, but it's empty (Totalcmd can open it, e.g. to copy files to it)
 * 2. Any other error: The directory does not exist, and Total Commander will not try to open it.
 *
 * Important notes:
 * 1. FsFindFirst may be called directly with a subdirectory of the plugin! You cannot rely on it
 *      being called with the root \ after it is loaded. Reason: Users may have saved a subdirectory
 *      to the plugin in the Ctrl+D directory hotlist in a previous session with the plugin.
 * 2. "Path" can be up to 259 (MAX_PATH-1) characters long when FsFindFirstW isn't implemented, and
 *      up to 1023 characters when FsFindFirstW is implemented (also in the ANSI version of the function)!
 *      This doesn't include the final 0 character.
 */
HANDLE __stdcall FsFindFirst(
        char* Path,
        WIN32_FIND_DATA *FindData);

HANDLE __stdcall FsFindFirstW(
        WCHAR* Path,
        WIN32_FIND_DATAW *FindData);

/**
 * FsFindNext is called to retrieve the next file in a directory of the plugin's file system.
 *
 * Description of parameters:
 * Hdl          The find handle returned by FsFindFirst.
 * FindData     A standard WIN32_FIND_DATA struct as defined in the Windows SDK, which contains
 *              the file or directory details. Use the dwFileAttributes field set to
 *              FILE_ATTRIBUTE_DIRECTORY to distinguish files from directories. On Unix systems, you
 *              can | (or) the dwFileAttributes field with 0x80000000 and set the dwReserved0 parameter
 *              to the Unix file mode (permissions).
 *
 * Return value:
 * Return FALSE if an error occurs or if there are no more files, and TRUE otherwise.
 * SetLastError() does not need to be called.
 */
BOOL __stdcall FsFindNext(
        HANDLE Hdl,
        WIN32_FIND_DATA *FindData);

BOOL __stdcall FsFindNextW(
        HANDLE Hdl,
        WIN32_FIND_DATAW *FindData);

/**
 * FsFindClose is called to end a FsFindFirst/FsFindNext loop, either after retrieving all files,
 * or when the user aborts it.
 *
 * Description of parameters:
 * Hdl      The find handle returned by FsFindFirst.
 *
 * Return value:
 * Currently unused, should return 0.
 */
int __stdcall FsFindClose(
        HANDLE Hdl);

/**
 * FsSetCryptCallback is called when loading the plugin.
 * The passed values should be stored in the plugin for later use.
 * This function is only needed if you want to use the secure password store in Total Commander.
 *
 * Description of parameters:
 * pCryptProc  Pointer to the crypto callback function. See CryptProc for a description of this function
 * CryptoNr    A parameter which needs to be passed to the callback function
 * Flags   Flags regarding the crypto connection. Currently only FS_CRYPTOPT_MASTERPASS_SET is defined. It is set when the user has defined a master password.
 *
 * Return value:
 * This function does not return any value.
 *
 * Remarks:
 * You can use this callback function to store passwords in Total Commander's secure password store.
 * The user will be asked for the master password automatically.
 */
void __stdcall FsSetCryptCallback(
        tCryptProc pCryptProc,
        int CryptoNr,
        int Flags);

void __stdcall FsSetCryptCallbackW(
        tCryptProcW pCryptProcW,
        int CryptoNr,
        int Flags);

BOOL __stdcall FsMkDir(
        char* Path);

BOOL __stdcall FsMkDirW(
        WCHAR* Path);

int __stdcall FsExecuteFile(
        HWND MainWin,
        char* RemoteName,
        char* Verb);

int __stdcall FsExecuteFileW(
        HWND MainWin,
        WCHAR* RemoteName,
        WCHAR* Verb);

int __stdcall FsRenMovFile(
        char* OldName,
        char* NewName,
        BOOL Move,
        BOOL OverWrite,
        RemoteInfoStruct* ri);

int __stdcall FsRenMovFileW(
        WCHAR* OldName,
        WCHAR* NewName,
        BOOL Move,
        BOOL OverWrite,
        RemoteInfoStruct* ri);

int __stdcall FsGetFile(
        char* RemoteName,
        char* LocalName,
        int CopyFlags,
        RemoteInfoStruct* ri);

int __stdcall FsGetFileW(
        WCHAR* RemoteName,
        WCHAR* LocalName,
        int CopyFlags,
        RemoteInfoStruct* ri);

int __stdcall FsPutFile(
        char* LocalName,
        char* RemoteName,
        int CopyFlags);

int __stdcall FsPutFileW(
        WCHAR* LocalName,
        WCHAR* RemoteName,
        int CopyFlags);

BOOL __stdcall FsDeleteFile(
        char* RemoteName);

BOOL __stdcall FsDeleteFileW(
        WCHAR* RemoteName);

BOOL __stdcall FsRemoveDir(
        char* RemoteName);

BOOL __stdcall FsRemoveDirW(
        WCHAR* RemoteName);

BOOL __stdcall FsDisconnect(
        char* DisconnectRoot);

BOOL __stdcall FsDisconnectW(
        WCHAR* DisconnectRoot);

BOOL __stdcall FsSetAttr(
        char* RemoteName,
        int NewAttr);

BOOL __stdcall FsSetAttrW(
        WCHAR* RemoteName,
        int NewAttr);

BOOL __stdcall FsSetTime(
        char* RemoteName,
        FILETIME *CreationTime,
        FILETIME *LastAccessTime,
        FILETIME *LastWriteTime);

BOOL __stdcall FsSetTimeW(
        WCHAR* RemoteName,
        FILETIME *CreationTime,
        FILETIME *LastAccessTime,
        FILETIME *LastWriteTime);

void __stdcall FsStatusInfo(
        char* RemoteDir,
        int InfoStartEnd,
        int InfoOperation);

void __stdcall FsStatusInfoW(
        WCHAR* RemoteDir,
        int InfoStartEnd,
        int InfoOperation);

void __stdcall FsGetDefRootName(
        char* DefRootName,
        int maxlen);

int __stdcall FsExtractCustomIcon(
        char* RemoteName,
        int ExtractFlags,
        HICON* TheIcon);

int __stdcall FsExtractCustomIconW(
        WCHAR* RemoteName,
        int ExtractFlags,
        HICON* TheIcon);

void __stdcall FsSetDefaultParams(
        FsDefaultParamStruct* dps);

int __stdcall FsGetPreviewBitmap(
        char* RemoteName,
        int width,
        int height,
        HBITMAP* ReturnedBitmap);

int __stdcall FsGetPreviewBitmapW(
        WCHAR* RemoteName,
        int width,
        int height,
        HBITMAP* ReturnedBitmap);

BOOL __stdcall FsLinksToLocalFiles(
        void);

BOOL __stdcall FsGetLocalName(
        char* RemoteName,
        int maxlen);

BOOL __stdcall FsGetLocalNameW(
        WCHAR* RemoteName,
        int maxlen);

// ************************** content plugin extension ****************************

//
#define ft_nomorefields     0

#define ft_numeric_32       1
#define ft_numeric_64       2
#define ft_numeric_floating 3
#define ft_date             4
#define ft_time             5
#define ft_boolean          6
#define ft_multiplechoice   7
#define ft_string           8
#define ft_fulltext         9
#define ft_datetime         10
#define ft_stringw          11 // Should only be returned by Unicode function

// for FsContentGetValue
#define ft_nosuchfield      -1  // error, invalid field number given
#define ft_fileerror        -2  // file i/o error
#define ft_fieldempty       -3  // field valid, but empty
#define ft_ondemand         -4  // field will be retrieved only when user presses <SPACEBAR>
#define ft_delayed           0  // field takes a long time to extract -> try again in background

// for FsContentSetValue
#define ft_setsuccess        0     // setting of the attribute succeeded

// for FsContentGetSupportedFieldFlags
#define contflags_edit              1
#define contflags_substsize         2
#define contflags_substdatetime     4
#define contflags_substdate         6
#define contflags_substtime         8
#define contflags_substattributes   10
#define contflags_substattributestr 12
#define contflags_substmask         14

// for FsContentSetValue
#define setflags_first_attribute    1     // First attribute of this file
#define setflags_last_attribute     2     // Last attribute of this file
#define setflags_only_date          4     // Only set the date of the datetime value!

#define CONTENT_DELAYIFSLOW         1  // ContentGetValue called in foreground

typedef struct
{
    int size;
    DWORD PluginInterfaceVersionLow;
    DWORD PluginInterfaceVersionHi;

    char DefaultIniName[MAX_PATH];
} ContentDefaultParamStruct;

typedef struct
{
    WORD wYear;
    WORD wMonth;
    WORD wDay;
} tdateformat, *pdateformat;

typedef struct
{
    WORD wHour;
    WORD wMinute;
    WORD wSecond;
} ttimeformat, *ptimeformat;

int __stdcall FsContentGetSupportedField(
        int FieldIndex,
        char* FieldName,
        char* Units,
        int maxlen);

int __stdcall FsContentGetValue(
        char* FileName,
        int FieldIndex,
        int UnitIndex,
        void* FieldValue,
        int maxlen,
        int flags);

int __stdcall FsContentGetValueW(
        WCHAR* FileName,
        int FieldIndex,
        int UnitIndex,
        void* FieldValue,
        int maxlen,
        int flags);

void __stdcall FsContentStopGetValue(
        char* FileName);

void __stdcall FsContentStopGetValueW(
        WCHAR* FileName);

int __stdcall FsContentGetDefaultSortOrder(
        int FieldIndex);

void __stdcall FsContentPluginUnloading(
        void);

int __stdcall FsContentGetSupportedFieldFlags(
        int FieldIndex);

int __stdcall FsContentSetValue(
        char* FileName,
        int FieldIndex,
        int UnitIndex,
        int FieldType,
        void* FieldValue,
        int flags);

int __stdcall FsContentSetValueW(
        WCHAR* FileName,
        int FieldIndex,
        int UnitIndex,
        int FieldType,
        void* FieldValue,
        int flags);

BOOL __stdcall FsContentGetDefaultView(
        char* ViewContents,
        char* ViewHeaders,
        char* ViewWidths,
        char* ViewOptions,
        int maxlen);

BOOL __stdcall FsContentGetDefaultViewW(
        WCHAR* ViewContents,
        WCHAR* ViewHeaders,
        WCHAR* ViewWidths,
        WCHAR* ViewOptions,
        int maxlen);

/*
 With Total Commander 7.5 (fs plugin interface 2.0), Unicode support has been added to all plugin
 types. In principle, you need to implement the same functions as for ANSI, with two differences:
 The function name is changed from FunctionName to FunctionNameW, and ANSI strings are changed to
 wide char names.

 Total Commander will call the Unicode functions on all NT-based systems (Windows NT, 2000, XP,
 Vista, 7) if they are present. If not, or on Windows 9x/ME, Total Commander will call the ANSI
 functions.

 IMPORTANT WARNING: When FsFindFirstW is implemented, any Path field can be up to 1023 characters
 long! Make sure that your plugin uses buffers of 1024 characters in both the ANSI and Unicode
 functions, or at least doesn't crash for such long strings.

 ==============

 The following functions of the file system plugin interface support Unicode:

 FsInitW
 FsFindFirstW
 FsFindNextW
 FsSetCryptCallbackW
 FsGetFileW
 FsPutFileW
 FsDeleteFileW
 FsRemoveDirW
 FsRenMovFileW
 FsMkDirW
 FsSetAttrW
 FsSetTimeW
 FsExecuteFileW
 FsDisconnectW
 FsStatusInfoW
 FsExtractCustomIconW
 FsGetPreviewBitmapW
 FsGetLocalNameW
 FsContentGetValueW
 FsContentStopGetValueW
 FsContentSetValueW
 FsContentGetDefaultViewW

 The following functions do not exist in a Unicode form and must be implemented as ANSI:

 FsFindClose
 FsGetDefRootName
 FsSetDefaultParams
 FsLinksToLocalFiles
 FsContentGetSupportedField - field names MUST be ANSI!
 FsContentGetSupportedFieldFlags
 FsContentGetDefaultSortOrder
 FsContentPluginUnloading

 ==============

 2. Functions

 a) Mandatory (must be implemented)
 FsInit
 FsFindFirst
 FsFindNext
 FsFindClose

 b) Optional (must NOT be implemented if unsupported!)
 FsSetCryptCallback
 FsGetDefRootName
 FsGetFile
 FsPutFile
 FsRenMovFile
 FsDeleteFile
 FsRemoveDir
 FsMkDir
 FsExecuteFile
 FsSetAttr
 FsSetTime
 FsDisconnect
 FsStatusInfo
 FsExtractCustomIcon
 FsSetDefaultParams
 FsGetPreviewBitmap

 For temporary panel plugins only:
 FsLinksToLocalFiles
 FsGetLocalName

 For file system plugins supporting custom columns:
 FsContentGetSupportedField
 FsContentGetValue
 FsContentStopGetValue
 FsContentGetDefaultSortOrder
 FsContentPluginUnloading
 FsContentGetSupportedFieldFlags
 FsContentSetValue
 FsContentGetDefaultView

 */
