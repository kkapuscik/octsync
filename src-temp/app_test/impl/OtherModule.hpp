//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_TESTAPP_OTHER_MODULE_HPP
#define OCTSYNC_TESTAPP_OTHER_MODULE_HPP

//-----------------------------------------------------------------------------

#include <framework/Module.hpp>

using OCTsync::Framework::Module;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppTest
{

/// Another test module.
class OtherModule : public Module
{
public:
    /// Constructs test module.
    ///
    /// \param instanceName
    ///     Name of the module instance.
    OtherModule(const QString& instanceName);

    /// Destroys the module.
    virtual ~OtherModule();

    /// Prinst some stupid text to standard output.
    void printStupidText();
};

}
}

//-----------------------------------------------------------------------------

#endif /* OCTSYNC_TESTAPP_OTHER_MODULE_HPP */
