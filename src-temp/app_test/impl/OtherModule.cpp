//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#include "OtherModule.hpp"

#include <iostream>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppTest
{

OtherModule::OtherModule(
        const QString& instanceName) :
                Module(instanceName)
{
    // nothing to do
}

OtherModule::~OtherModule()
{
    // nothing to do
}

void OtherModule::printStupidText()
{
    std::cout << "Other module is doing something..." << std::endl;
}

}
}
