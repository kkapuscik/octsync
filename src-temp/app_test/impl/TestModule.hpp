//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_APPTEST_TEST_MODULE_HPP
#define OCTSYNC_APPTEST_TEST_MODULE_HPP

//-----------------------------------------------------------------------------

#include <framework/Module.hpp>
#include <framework/Dependency.hpp>
#include <memory>

using OCTsync::Framework::Module;
using OCTsync::Framework::TypedDependency;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppTest
{

// forward
class TestModulePrivate;

/// Example test module.
class TestModule : public Module
{
private:
    /// Module private data.
    std::unique_ptr<TestModulePrivate> m_private;

public:
    /// Constructs the module.
    ///
    /// \param instanceName
    ///     Name of the module instance.
    TestModule(
            const QString& instanceName);

    /// Destroys the module.
    virtual ~TestModule();

    /// Prints some stupid test to standard output.
    void printStupidText();
};

}
}

//-----------------------------------------------------------------------------

#endif /* OCTSYNC_APPTEST_TEST_MODULE_HPP */
