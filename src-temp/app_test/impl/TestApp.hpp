//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_TESTAPP_TEST_APP_HPP
#define OCTSYNC_TESTAPP_TEST_APP_HPP

//-----------------------------------------------------------------------------

#include <framework/AppModule.hpp>
#include <memory>

using OCTsync::Framework::AppModule;
using OCTsync::Framework::AppArguments;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppTest
{

// forward
class TestAppPrivate;

/// Test application
class TestApp : public AppModule
{
protected:
    /// TestApp private data.
    std::unique_ptr<TestAppPrivate> m_private;

public:
    /// Constructs the application.
    ///
    /// \param instanceName
    ///     Name of the application module instance.
    TestApp(
            const QString& instanceName);

    /// Destroys the application
    virtual ~TestApp();

    // Overridden
    virtual int execute(
            const AppArguments& appArguments);
};

}
}

//-----------------------------------------------------------------------------

#endif /* OCTSYNC_TESTAPP_TEST_APP_HPP */
