//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#include "TestModule.hpp"
#include "OtherModule.hpp"

#include <iostream>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppTest
{

/// Test module private data.
class TestModulePrivate
{
public:
    /// Dependency - Other module.
    TypedDependency<OtherModule> m_otherModule;
};

TestModule::TestModule(
        const QString& instanceName) :
                Module(instanceName),
                m_private(new TestModulePrivate())
{
    addDependency(&m_private->m_otherModule);
}

TestModule::~TestModule()
{
    // nothing to do
}

void TestModule::printStupidText()
{
    std::cout << "Test module is doing something..." << std::endl;
    std::cout << "Trying to use other module" << std::endl;
    m_private->m_otherModule->printStupidText();
    std::cout << "Test module finished doing something..." << std::endl;
}

}
}
