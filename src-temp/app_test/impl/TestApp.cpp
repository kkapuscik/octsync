//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#include "TestApp.hpp"
#include "TestModule.hpp"
#include "OtherModule.hpp"

#include <cstdlib>
#include <iostream>

#include <framework/Dependency.hpp>

using OCTsync::Framework::Dependency;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppTest
{

/// TestApp private data.s
class TestAppPrivate
{
public:
    /// Dependency - Test module.
    TypedDependency<TestModule> m_testModule;
    /// Dependency - Other module
    TypedDependency<OtherModule> m_otherModule;
};

TestApp::TestApp(
        const QString& instanceName) :
                AppModule(instanceName),
                m_private(new TestAppPrivate())
{
    /// \todo It seems it could be enforced by using the dependency construction trick.
    ///       If we add "dependency set" object to dependency constructor and let
    ///       module implement it then we could force addition in dependency construction
    ///       The argument against is that we will need to define private class constructor
    ///       which will perform initialization and also it will look a bit tricky.
    ///       The other con is that it may lead to add object which is not constructed correctly
    ///       to the collection and if there will be an exception later the collection will contain
    ///       broken pointer. But normally such exception will lead to something even more fatal...
    addDependency(&m_private->m_otherModule);
    addDependency(&m_private->m_testModule);
}

TestApp::~TestApp()
{
    // nothing to do
}

int TestApp::execute(
        const AppArguments& appArguments)
{
    std::cout << "TestApp::execute()" << std::endl;

    m_private->m_testModule->printStupidText();
    m_private->m_otherModule->printStupidText();

    return EXIT_SUCCESS;
}

}
}
