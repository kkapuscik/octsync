//-----------------------------------------------------------------------------
/// \file
/// InvalidClassException class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_INVALID_CLASS_EXCEPTION_HPP
#define OCTSYNC_FRAMEWORK_INVALID_CLASS_EXCEPTION_HPP

//-----------------------------------------------------------------------------

#include "FrameworkException.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Exception thrown when circular dependency was detected.
///
/// \todo Add class info object to the exception.
class InvalidClassException : public FrameworkException
{
public:
    /// Constructs the exception
    ///
    /// \param message
    ///     Exception message.
    InvalidClassException(
            QString message) :
                    FrameworkException(message)
    {
        // nothing to do
    }

    /// Destroys the exception.
    virtual ~InvalidClassException()
    {
        // nothing to do
    }

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_INVALID_CLASS_EXCEPTION_HPP*/
