cmake_minimum_required(VERSION 2.8)

project(framework)

set(FRAMEWORK_SOURCES
	impl/Framework.cpp
	impl/FrameworkExecutor.cpp
)

set(FRAMEWORK_HEADERS
    namespace.hpp

    impl/DependencyGraph.hpp
    impl/DependencyGraphNode.hpp
    impl/FrameworkExecutor.hpp

    AppArguments.hpp
    AppModule.hpp
    CircularDependencyException.hpp
    Dependency.hpp
    DependencyDescriptor.hpp
    Framework.hpp
    FrameworkException.hpp
    InvalidClassException.hpp
    ModuleErrorException.hpp
    UninitializedDependencyException.hpp
    Module.hpp
)

# include_directories(${CMAKE_CURRENT_SOURCE_DIR})

add_library(framework STATIC ${FRAMEWORK_SOURCES} ${FRAMEWORK_HEADERS})
