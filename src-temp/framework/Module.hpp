//-----------------------------------------------------------------------------
/// \file
/// Module class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_MODULE_HPP
#define OCTSYNC_FRAMEWORK_MODULE_HPP

//-----------------------------------------------------------------------------

#include <string>
#include <vector>
#include <cassert>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

class Dependency;

/// Base class for all framework modules.
class Module
{
private:
    /// Name of the module instance.
    QString m_instanceName;

    /// Collection of module dependencies.
    std::vector<Dependency*> m_dependencies;

public:
    /// Destroys the module.
    virtual ~Module()
    {
        // nothing to do
    }

    /// Returns module instance name.
    ///
    /// \return
    /// Name of this module instance as given to constructor.
    const QString& getInstanceName() const
    {
        return m_instanceName;
    }

    /// Performs module initialization.
    ///
    /// Initialization stage shall be used to setup elements that are required
    /// to interact with other modules.
    ///
    /// During initialization other modules should not be called if possible.
    /// The interaction shall be minimized as other modules may not be fully
    /// initialized.
    ///
    /// \retval true        Operation succeeded.
    /// \retval false       Operation failed.
    virtual bool init()
    {
        // nothing to do, success
        return true;
    }

    /// Performs module startup.
    ///
    /// Startup stage shall be used to start the module core and begin normal
    /// module activities.
    ///
    /// After module is started it shall remain accessible until shutdown is requested.
    ///
    /// In started state module could communicate with all dependent modules.
    ///
    /// \retval true        Operation succeeded.
    /// \retval false       Operation failed.
    virtual bool start()
    {
        // nothing to do, success
        return true;
    }

    /// Performs module shutdown.
    ///
    /// Stop command is used to finish module activities. After stop is called module should no
    /// longer communicate with other modules and should prepare itself for being deinitialized.
    ///
    /// \retval true        Operation succeeded.
    /// \retval false       Operation failed.
    virtual bool stop()
    {
        // nothing to do, success
        return true;
    }

    /// Performs module deinitialization.
    ///
    /// Deinitialization shall clean up all elements created during init() and prepare the module
    /// for being destroyed.
    ///
    /// \retval true        Operation succeeded.
    /// \retval false       Operation failed.
    virtual bool deinit()
    {
        // nothing to do, success
        return true;
    }

protected:
    /// Constructs the module.
    ///
    /// \param instanceName
    ///         Name of the module instance.
    Module(
            const QString& instanceName) :
                    m_instanceName(instanceName)
    {
        // nothing to do
    }

    /// Adds module dependency.
    ///
    /// \note Given object is not copied and must remain valid as long as the module object exists.
    ///       Usually the dependency will be a member of the Module derived class.
    ///
    /// \param dependency
    ///         Dependency to be added.
    void addDependency(
            Dependency* dependency)
    {
        assert(dependency);

        m_dependencies.push_back(dependency);
    }

private:
    /// Returns number of registered dependencies.
    ///
    /// \return
    /// Number of dependencies added using addDependency().
    size_t getDependencyCount() const
    {
        return m_dependencies.size();
    }

    /// Returns selected dependency.
    ///
    /// \param index
    ///         Index of the requested dependency.
    ///
    /// \return
    /// Dependency object.
    Dependency* getDependency(
            size_t index) const
    {
        assert(index >= 0 && index < getDependencyCount());
        return m_dependencies.at(index);
    }

    friend class FrameworkExecutor;
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_MODULE_HPP*/
