//-----------------------------------------------------------------------------
/// \file
/// UninitializedDependencyException class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_UNINITIALIZED_DEPENDENCY_EXCEPTION_HPP
#define OCTSYNC_FRAMEWORK_UNINITIALIZED_DEPENDENCY_EXCEPTION_HPP

//-----------------------------------------------------------------------------

#include "FrameworkException.hpp"
#include "DependencyDescriptor.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Exception thrown when access to uninitialized dependency was detected.
class UninitializedDependencyException : public FrameworkException
{
private:
    /// Descriptor of the dependency for which problem was detected.
    DependencyDescriptor m_dependency;

public:
    /// Constructs the exception
    ///
    /// \param message
    ///     Exception message.
    /// \param dependency
    ///     The dependency for which problem was detected.
    UninitializedDependencyException(
            QString message,
            const DependencyDescriptor& dependency) :
                    FrameworkException(message),
                    m_dependency(dependency)
    {
        // nothing to do
    }

    /// Destroys the exception.
    virtual ~UninitializedDependencyException()
    {
        // nothing to do
    }

    /// Returns dependency for which problem was detected.
    ///
    /// \return
    /// Dependency descriptor.
    const DependencyDescriptor& getDescriptor() const
    {
        return m_dependency;
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_UNINITIALIZED_DEPENDENCY_EXCEPTION_HPP*/
