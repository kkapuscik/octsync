//-----------------------------------------------------------------------------
/// \file
/// ModuleErrorException class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_MODULE_ERROR_EXCEPTION_HPP
#define OCTSYNC_FRAMEWORK_MODULE_ERROR_EXCEPTION_HPP

//-----------------------------------------------------------------------------

#include "FrameworkException.hpp"
#include "DependencyDescriptor.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Exception thrown when module returned error when called by framework.
class ModuleErrorException : public FrameworkException
{
private:
    /// Descriptor of the module that produced error.
    DependencyDescriptor m_dependency;

public:
    /// Constructs the exception
    ///
    /// \param message
    ///     Exception message.
    /// \param dependency
    ///     The dependency for which problem was detected.
    ModuleErrorException(
            QString message,
            const DependencyDescriptor& dependency) :
                    FrameworkException(message),
                    m_dependency(dependency)
    {
        // nothing to do
    }

    /// Destroys the exception.
    virtual ~ModuleErrorException()
    {
        // nothing to do
    }

    /// Returns dependency for which problem was detected.
    ///
    /// \return
    /// Dependency descriptor.
    const DependencyDescriptor& getDescriptor() const
    {
        return m_dependency;
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_MODULE_ERROR_EXCEPTION_HPP*/
