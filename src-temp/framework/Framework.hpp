//-----------------------------------------------------------------------------
/// \file
/// Framework class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_FRAMEWORK_HPP
#define OCTSYNC_FRAMEWORK_FRAMEWORK_HPP

//-----------------------------------------------------------------------------

#include "Dependency.hpp"
#include "AppArguments.hpp"

#include <commons/class.hpp>
#include <string>
#include <memory>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Modules framework.
///
/// Framework is a modules manager. It handles modules creation, dependencies, startup
/// and shutdown process.
///
/// In normal case each application has one framework object and one main module - the
/// application module. Framework build the modules dependencies graph, validates it,
/// then creates required modules, starts them and shutdown when the application is done.
class Framework
{
OCT_CLASS_NO_COPY(Framework)

private:
    /// Dependency from application module.
    DependencyDescriptor m_appModuleDependency;

    /// Application arguments.
    AppArguments m_appArguments;

public:
    /// Constructs the framework
    ///
    /// \param appModuleDependency
    ///         Dependency from application module.
    /// \param argc
    ///         Argument count, same as passed to main() function.
    /// \param argv
    ///         Argument values, same as passed to main() function.
    Framework(
            const DependencyDescriptor& appModuleDependency,
            int argc,
            char* argv[]);

    /// Destroys the framework.
    ~Framework();

    /// Executes the framework.
    ///
    /// Application constructs application module, all dependencies and passes the
    /// execution to application module.
    ///
    /// \return
    /// Application exit code. Value returned by the application module execution code.
    ///
    /// \exception CircularDependencyException
    ///         Circular dependency was detected.
    int execute();

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_FRAMEWORK_HPP*/
