//-----------------------------------------------------------------------------
/// \file
/// FrameworkExecutor class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_FRAMEWORK_EXECUTOR_HPP
#define OCTSYNC_FRAMEWORK_FRAMEWORK_EXECUTOR_HPP

//-----------------------------------------------------------------------------

#include "DependencyGraph.hpp"
#include "../Dependency.hpp"
#include "../AppArguments.hpp"
#include "../FrameworkException.hpp"

#include <commons/class.hpp>

#include <cstdint>
#include <vector>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Executor of the framework.
///
/// Executor is the class that actually handles application execution.
/// Framework delegates the execute responsibility to this class to hide the implementation
/// details.
class FrameworkExecutor
{
OCT_CLASS_NO_COPY(FrameworkExecutor)

private:
    /// Type of pointer to dependency graph node.
    typedef std::shared_ptr<DependencyGraphNode> NodePtr;

    /// Type of array with pointers to dependency graph nodes.
    typedef std::vector<NodePtr> NodePtrArray;

    /// Context of the execution.
    struct ExecutionContext
    {
        /// Depth-first search step counter.
        int32_t m_dfsStepCounter;

        /// Graph of dependencies between modules.
        DependencyGraph m_dependencyGraph;

        /// Array with pointers to dependency graph nodes.
        NodePtrArray m_nodes;

        /// Constructs and initializes the context.
        ExecutionContext() :
                        m_dfsStepCounter(0)
        {
            // nothing to do
        }
    };

    /// Processing order comparator functor.
    class ProcessingEndComparator
    {
    public:
        /// Compares two node values.
        ///
        /// \param d1
        ///         First node value to be compared.
        /// \param d2
        ///         Second node value to be compared.
        ///
        /// \return
        /// True if first node value is before the second node value.
        bool operator()(
                const NodePtr& d1,
                const NodePtr& d2) const
        {
            return d1->getProcessingEndIndex() < d2->getProcessingEndIndex();
        }
    };

private:
    /// Dependency from application module.
    DependencyDescriptor m_appModuleDependency;

    /// Application arguments.
    AppArguments m_appArguments;

public:
    /// Constructs the framework
    ///
    /// \param appModuleDependency
    ///         Dependency from application module.
    /// \param appArguments
    ///         Arguments to be passed to application module.
    FrameworkExecutor(
            const DependencyDescriptor& appModuleDependency,
            const AppArguments& appArguments);

    /// Destroys the framework.
    ~FrameworkExecutor();

    /// Executes the framework.
    ///
    /// Application constructs application module, all dependencies and passes the
    /// execution to application module.
    ///
    /// \return
    /// Application exit code. Value returned by the application module execution code.
    ///
    /// \exception CircularDependencyException
    ///         Circular dependency was detected.
    int execute();

private:
    /// Processes given dependency.
    ///
    /// \note This method is recursive.
    ///
    /// \param context
    ///         Context holding current execution state.
    /// \param dependencyDescriptor
    ///         Descriptor of the dependency to be processed.
    ///
    /// \return
    /// Module created for given dependency. This could be a newly constructed object or
    /// a module constructed before for another dependency with same descriptor.
    ///
    /// \exception FrameworkException
    /// Thrown when there was an error during processing.
    Module* processDependency(
            ExecutionContext& context,
            const DependencyDescriptor& dependencyDescriptor)
                    throw (FrameworkException);

    /// Dumps dependency graph nodes to console.
    ///
    /// \param context
    ///         Context holding current execution state.
    void dumpNodes(
            ExecutionContext& context);

    /// Sorts dependency graph topologically.
    ///
    /// \param context
    ///         Context holding current execution state.
    void sortNodes(
            ExecutionContext& context);

    /// Initializes all application modules.
    ///
    /// Calls Module::init() on all application modules.
    ///
    /// \param context
    ///         Context holding current execution state.
    void initModules(
            ExecutionContext& context);

    /// Starts all application modules.
    ///
    /// Calls Module::start() on all application modules.
    ///
    /// \param context
    ///         Context holding current execution state.
    void startModules(
            ExecutionContext& context);

    /// Stops all application modules.
    ///
    /// Calls Module::stop() on all application modules.
    ///
    /// \param context
    ///         Context holding current execution state.
    void stopModules(
            ExecutionContext& context);

    /// Deinitializes all application modules.
    ///
    /// Calls Module::deinit() on all application modules.
    ///
    /// \param context
    ///         Context holding current execution state.
    void deinitModules(
            ExecutionContext& context);

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_FRAMEWORK_EXECUTOR_HPP*/
