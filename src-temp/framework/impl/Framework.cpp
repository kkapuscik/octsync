//-----------------------------------------------------------------------------
/// \file
/// Framework class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#include "../Framework.hpp"

#include "FrameworkExecutor.hpp"
#include "../Module.hpp"

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

Framework::Framework(
        const DependencyDescriptor& appModule,
        int argc,
        char* argv[]) :
                m_appModuleDependency(appModule),
                m_appArguments(argc, argv)
{
    // nothing to do
}

Framework::~Framework()
{
    // nothing to do
}

int Framework::execute()
{
    return FrameworkExecutor(m_appModuleDependency, m_appArguments).execute();
}

}
}
