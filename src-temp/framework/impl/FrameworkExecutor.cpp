//-----------------------------------------------------------------------------
/// \file
/// FrameworkExecutor class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#include "FrameworkExecutor.hpp"
#include "../AppModule.hpp"
#include "../CircularDependencyException.hpp"
#include "../InvalidClassException.hpp"
#include "../ModuleErrorException.hpp"

#include <commons/FatalException.hpp>

#include <cassert>
#include <algorithm>

using OCTsync::Commons::FatalException;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

FrameworkExecutor::FrameworkExecutor(
        const DependencyDescriptor& appModule,
        const AppArguments& appArguments) :
                m_appModuleDependency(appModule),
                m_appArguments(appArguments)
{
    // nothing to do
}

FrameworkExecutor::~FrameworkExecutor()
{
    // nothing to do
}

Module* FrameworkExecutor::processDependency(
        ExecutionContext& context,
        const DependencyDescriptor& dependencyDescriptor)
                throw (FrameworkException)
{
    NodePtr nodePtr = context.m_dependencyGraph.getNode(dependencyDescriptor);
    if (!nodePtr)
    {
        std::unique_ptr<Module> module(dependencyDescriptor.createModule());
        assert(module);

        nodePtr = NodePtr(new DependencyGraphNode(dependencyDescriptor, std::move(module)));

        context.m_dependencyGraph.addNode(nodePtr);
    }

    assert(nodePtr);

    switch (nodePtr->getState())
    {
        case DependencyGraphNode::NOT_PROCESSED:
        {
            nodePtr->markProcessingStarted(++context.m_dfsStepCounter);

            Module* module = nodePtr->getModule();
            assert(module);

            for (size_t i = 0; i < module->getDependencyCount(); ++i)
            {
                Dependency* dependency = module->getDependency(i);

                Module* dependencyModule = processDependency(context, dependency->getDescriptor());
                if (!dependencyModule)
                {
                    throw FatalException("Cannot create dependency module");
                }

                dependency->setModuleInstance(dependencyModule);
            }

            nodePtr->markProcessingFinished(++context.m_dfsStepCounter);
            break;
        }

        case DependencyGraphNode::CURRENTLY_PROCESSED:
            throw CircularDependencyException("Circular dependency detected", dependencyDescriptor);

        case DependencyGraphNode::PROCESSED:
            // already processed, nothing more to do
            break;

        default:
            throw FatalException("Unknown dependency node state");
    }

    return nodePtr->getModule();
}

int FrameworkExecutor::execute()
{
    ExecutionContext context;

    // process dependencies and build modules
    Module* appModule = processDependency(context, m_appModuleDependency);
    if (!appModule)
    {
        throw FatalException("Cannot create application module");
    }

    // check if it is really application module
    /// \todo handle this the other way, so cast is not needed
    AppModule* appModuleCasted = dynamic_cast<AppModule*>(appModule);
    if (! appModuleCasted)
    {
        throw InvalidClassException("Application module is not an AppModule");
    }

    dumpNodes(context);

    sortNodes(context);

    initModules(context);

    startModules(context);

    // execute application
    int result = appModuleCasted->execute(m_appArguments);

    stopModules(context);

    deinitModules(context);

    return result;
}

void FrameworkExecutor::dumpNodes(
        ExecutionContext& context)
{
    for (size_t i = 0; i < context.m_nodes.size(); ++i)
    {
        NodePtr& nodePtr = context.m_nodes[i];
        std::cout << "Node: " << typeid(*nodePtr->getModule()).name() << " --- "
                << nodePtr->getProcessingStartIndex() << "/" << nodePtr->getProcessingEndIndex()
                << std::endl;
    }
}

void FrameworkExecutor::sortNodes(
        ExecutionContext& context)
{
    std::sort(context.m_nodes.begin(), context.m_nodes.end(), ProcessingEndComparator());
}

void FrameworkExecutor::initModules(
        ExecutionContext& context)
{
    for (size_t i = 0; i < context.m_nodes.size(); ++i)
    {
        NodePtr& nodePtr = context.m_nodes[i];
        if (!nodePtr->getModule()->init())
        {
            throw ModuleErrorException("Cannot initialize module", nodePtr->getDescriptor());
        }
    }
}

void FrameworkExecutor::startModules(
        ExecutionContext& context)
{
    for (size_t i = 0; i < context.m_nodes.size(); ++i)
    {
        NodePtr& nodePtr = context.m_nodes[i];
        if (!nodePtr->getModule()->start())
        {
            throw ModuleErrorException("Cannot start module", nodePtr->getDescriptor());
        }
    }
}

void FrameworkExecutor::stopModules(
        ExecutionContext& context)
{
    for (size_t i = 0; i < context.m_nodes.size(); ++i)
    {
        NodePtr& nodePtr = context.m_nodes[i];
        if (!nodePtr->getModule()->stop())
        {
            throw ModuleErrorException("Cannot stop module", nodePtr->getDescriptor());
        }
    }
}

void FrameworkExecutor::deinitModules(
        ExecutionContext& context)
{
    for (size_t i = 0; i < context.m_nodes.size(); ++i)
    {
        NodePtr& nodePtr = context.m_nodes[i];
        if (!nodePtr->getModule()->deinit())
        {
            throw ModuleErrorException("Cannot deinitialize module", nodePtr->getDescriptor());
        }
    }
}

}
}
