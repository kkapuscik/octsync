//-----------------------------------------------------------------------------
/// \file
/// DependencyGraph class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_DEPENDENCY_GRAPH_HPP
#define OCTSYNC_FRAMEWORK_DEPENDENCY_GRAPH_HPP

//-----------------------------------------------------------------------------

#include "DependencyGraphNode.hpp"
#include "../Dependency.hpp"

#include <map>
#include <iostream>
#include <algorithm>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Dependency graph.
///
/// Single dependency graph node consists of node key (the module dependency) and
/// dependency node value which describes state of the node.
class DependencyGraph
{
private:
    /// Type of key of the graph map.
    typedef DependencyDescriptor GraphMapKey;
    /// Type of value of the graph map.
    typedef std::shared_ptr<DependencyGraphNode> GraphMapValue;

    /// Dependency descriptors comparator functor.
    class DescriptorComparator
    {
    public:
        /// Compares two dependencies.
        ///
        /// \param d1
        ///         First dependency to be compared.
        /// \param d2
        ///         Second dependency to be compared.
        ///
        /// \return
        /// True if first dependency is before the second dependency.
        bool operator()(
                const DependencyDescriptor& d1,
                const DependencyDescriptor& d2) const
        {
            return d1.before(d2);
        }
    };

    /// Type of map holding the graph nodes.
    typedef std::map<GraphMapKey, GraphMapValue, DescriptorComparator> GraphMap;

private:
    /// Map holding the graph nodes.
    GraphMap m_graphMap;

public:
    /// Constructs empty graph.
    DependencyGraph()
    {
        // nothing to do
    }

    /// Destroys the graph.
    ~DependencyGraph()
    {
        // nothing to do
    }

    /// Returns node for given descriptor.
    ///
    /// \param descriptor
    ///         Descriptor for which node is requested.
    ///
    /// \return
    /// Requested node if such node is in the graph or nullptr otherwise.
    std::shared_ptr<DependencyGraphNode> getNode(
            const DependencyDescriptor& descriptor) const
    {
        GraphMap::const_iterator depIter = m_graphMap.find(descriptor);
        if (depIter != m_graphMap.end())
        {
            return depIter->second;
        }
        else
        {
            return nullptr;
        }
    }

    /// Adds node to the graph.
    ///
    /// \param newNode
    ///         Node to be added to graph.
    void addNode(
            const std::shared_ptr<DependencyGraphNode>& newNode)
    {
        assert(newNode);
        assert(! getNode(newNode->getDescriptor()));

        m_graphMap.insert(std::make_pair(newNode->getDescriptor(), newNode));
    }

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_DEPENDENCY_GRAPH_HPP*/
