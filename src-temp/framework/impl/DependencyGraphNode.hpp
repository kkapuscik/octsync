//-----------------------------------------------------------------------------
/// \file
/// DependencyGraphNode class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_DEPENDENCY_GRAPH_NODE_HPP
#define OCTSYNC_FRAMEWORK_DEPENDENCY_GRAPH_NODE_HPP

//-----------------------------------------------------------------------------

#include "../Module.hpp"
#include "../DependencyDescriptor.hpp"

#include <string>
#include <cassert>
#include <memory>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Single node of the dependency graph.
class DependencyGraphNode
{
public:
    /// Node state.
    enum State
    {
        /// Node was not processed so far.
        NOT_PROCESSED,
        /// Node is currently being processed (on DFS stack).
        CURRENTLY_PROCESSED,
        /// Node was successfully processed.
        PROCESSED
    };

private:
    /// Dependency descriptor.
    DependencyDescriptor m_descriptor;

    /// Module instance.
    std::unique_ptr<Module> m_module;

    /// Node state.
    ///
    /// The state is used by DFS algorithm when resolving dependencies.
    State m_state;

    /// DFS processing entry index.
    int32_t m_processingStartIndex;

    /// DFS processing exit index.
    ///
    /// This value is used by topological sort to determine modules initialization order.
    int32_t m_processingEndIndex;

public:
    /// Constructs graph node.
    ///
    /// \param descriptor
    ///         Descriptor of the dependency this node is created for.
    /// \param module
    ///         Instance of the module constructed for this dependency.
    DependencyGraphNode(
            const DependencyDescriptor& descriptor,
            std::unique_ptr<Module> module) :
                    m_descriptor(descriptor),
                    m_module(std::move(module)),
                    m_state(NOT_PROCESSED),
                    m_processingStartIndex(-1),
                    m_processingEndIndex(-1)
    {
        assert(m_module);
    }

    /// Returns descriptor of the dependency.
    ///
    /// \return
    /// Descriptor of the dependency as given to constructor.
    const DependencyDescriptor getDescriptor() const
    {
        return m_descriptor;
    }

    /// Returns dependency module instance.
    ///
    /// \note This object MUST NOT be deleted.
    ///
    /// \return
    /// Module created for the dependency as given to constructor.
    Module* getModule() const
    {
        return m_module.get();
    }

    /// Returns current node state.
    ///
    /// \return
    /// DFS algorithm node state.
    State getState() const
    {
        return m_state;
    }

    /// Marks graph node as currently being processed.
    ///
    /// \param processingIndex
    ///         Processing index (step) in the DFS algorithm.
    void markProcessingStarted(
            int32_t processingIndex)
    {
        assert(m_state == NOT_PROCESSED);
        m_state = CURRENTLY_PROCESSED;
        m_processingStartIndex = processingIndex;
    }

    /// Marks graph node as fully processed.
    ///
    /// \param processingIndex
    ///         Processing index (step) in the DFS algorithm.
    void markProcessingFinished(
            int32_t processingIndex)
    {
        assert(m_state == CURRENTLY_PROCESSED);
        m_state = PROCESSED;
        m_processingEndIndex = processingIndex;
    }

    /// Returns processing start index.
    ///
    /// \return
    /// Index (step) of the DFS algorithm when node processing was started.
    int32_t getProcessingStartIndex() const
    {
        return m_processingStartIndex;
    }

    /// Returns processing end index.
    ///
    /// \return
    /// Index (step) of the DFS algorithm when node processing was finished.
    int32_t getProcessingEndIndex() const
    {
        return m_processingEndIndex;
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_DEPENDENCY_GRAPH_NODE_HPP*/
