//-----------------------------------------------------------------------------
/// \file
/// AppModule class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_APP_MODULE_HPP
#define OCTSYNC_FRAMEWORK_APP_MODULE_HPP

//-----------------------------------------------------------------------------

#include "Module.hpp"
#include <framework/AppArguments.hpp>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Application module.
///
/// Application module is the application main module. It behaves the same way
/// as any other module but has additional execute() method that is called by
/// framework and shall act as an application entry point. When exiting this method
/// will return control to framework and the application execution will be finished.
class AppModule : public Module
{
public:
    /// Constructs the module.
    ///
    /// \param instanceName
    ///         Name of the module instance.
    AppModule(
            const QString& instanceName) :
                    Module(instanceName)
    {
        // nothing to do
    }

    /// Destroys the module.
    virtual ~AppModule()
    {
        // nothing to do
    }

    /// Executes the application.
    ///
    /// This method is application entry point.
    ///
    /// \param appArguments
    ///         Application arguments (as passed to main() function).
    ///
    /// \return
    /// Application exit code.
    virtual int execute(
            const AppArguments& appArguments) = 0;

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_APP_MODULE_HPP*/
