//-----------------------------------------------------------------------------
/// \file
/// DependencyDescriptor class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_DEPENDENCY_DESCRIPTOR_HPP
#define OCTSYNC_FRAMEWORK_DEPENDENCY_DESCRIPTOR_HPP

//-----------------------------------------------------------------------------

#include <string>
#include <memory>
#include <cassert>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

// forwards
class Module;

/// Module dependency descriptor.
class DependencyDescriptor
{
private:
    /// Generic dependency handler.
    class DependencyHandler
    {
    private:
        /// Name of the module instance.
        QString m_instanceName;

    public:
        /// Destroys the handler.
        virtual ~DependencyHandler()
        {
            // nothing to do
        }

        /// Returns module instance name.
        ///
        /// \return
        /// Module instance name.
        const QString& getInstanceName() const
        {
            return m_instanceName;
        }

        /// Returns module type info.
        ///
        /// \return
        /// Module type info structure.
        virtual const std::type_info& getTypeInfo() const = 0;

        /// Creates module instance.
        ///
        /// \return
        /// Created module instance.
        virtual std::unique_ptr<Module> createModule() const = 0;

    protected:
        /// Constructs dependency handler.
        ///
        /// \param instanceName
        ///         Name of the module instance.
        DependencyHandler(
                const QString& instanceName) :
                        m_instanceName(instanceName)
        {
            // nothing to do
        }
    };

    /// Type-specific dependency handler.
    ///
    /// \param T
    ///         Module type.
    template<class T>
    class TypedDependencyHandler : public DependencyHandler
    {
    public:
        /// Constructs dependency handler.
        ///
        /// \param instanceName
        ///         Name of the module instance.
        TypedDependencyHandler(
                const QString& instanceName) :
                        DependencyHandler(instanceName)
        {
            // nothing to do
        }

        /// Destroys the handler.
        virtual ~TypedDependencyHandler()
        {
            // nothing tp do
        }

        // override
        virtual const std::type_info& getTypeInfo() const
        {
            return typeid(T);
        }

        // override
        virtual std::unique_ptr<Module> createModule() const
        {
            return std::unique_ptr<Module>(new T(getInstanceName()));
        }
    };

public:
    /// Constructs module dependency.
    ///
    /// \param instanceName
    ///         Name of the module instance.
    /// \tparam MODULE_TYPE
    ///         Type of the module.
    ///
    /// \return
    /// Created dependency object.
    template<class MODULE_TYPE>
    static DependencyDescriptor create(
            const QString& instanceName = "default")
    {
        return DependencyDescriptor(new TypedDependencyHandler<MODULE_TYPE>(instanceName));
    }

private:
    /// Handler of the dependency actions.
    std::shared_ptr<DependencyHandler> m_dependencyHandler;

public:
    /// Constructs module satisfying the dependency requirements.
    ///
    /// \return
    /// Created module.
    std::unique_ptr<Module> createModule() const
    {
        return m_dependencyHandler->createModule();
    }

    /// Checks if this dependency is 'before' the given one.
    ///
    /// The order is fully implementation dependent.
    ///
    /// \param other
    ///         The other descriptor to which this descriptor will be compared.
    ///
    /// \retval true    This dependency is 'before' (is less than) the given one.
    /// \return false   This dependency is 'not before' (is greater of equal to) the given one.
    bool before(
            const DependencyDescriptor& other) const
    {
        if (m_dependencyHandler->getTypeInfo().before(other.m_dependencyHandler->getTypeInfo()))
        {
            return true;
        }

        if (other.m_dependencyHandler->getTypeInfo().before(m_dependencyHandler->getTypeInfo()))
        {
            return false;
        }

        return m_dependencyHandler->getInstanceName() < other.m_dependencyHandler->getInstanceName();
    }

    /// Returns string description of this dependency.
    ///
    /// \return
    /// String with dependency description.
    QString getDescription() const
    {
        return QString("type=") + m_dependencyHandler->getTypeInfo().name() + " / instance="
                + m_dependencyHandler->getInstanceName();
    }

private:
    /// Constructs the dependency.
    ///
    /// \param handler
    ///         Dependency descriptor handler.
    ///         Most of the descriptor actions are delegated to this given handler.
    DependencyDescriptor(
            DependencyHandler* handler) :
                    m_dependencyHandler(handler)
    {
        assert(handler);
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_DEPENDENCY_DESCRIPTOR_HPP*/
