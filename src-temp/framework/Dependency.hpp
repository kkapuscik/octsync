//-----------------------------------------------------------------------------
/// \file
/// Dependency and TypedDependency classes declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_DEPENDENCY_HPP
#define OCTSYNC_FRAMEWORK_DEPENDENCY_HPP

//-----------------------------------------------------------------------------

#include "DependencyDescriptor.hpp"
#include "InvalidClassException.hpp"
#include "UninitializedDependencyException.hpp"

#include <cassert>
#include <memory>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Dependency object.
///
/// Dependency is an object that contains information about the relation (dependency descriptor)
/// and pointer to the instance of module fulfilling dependency requirements.
class Dependency
{
private:
    /// Descriptor of the dependency.
    DependencyDescriptor m_descriptor;

    /// Pointer to instance of the module.
    ///
    /// The instance of the module is not owned by the dependency object.
    /// If it is equal to nullptr then the instance is currently not available.
    Module* m_moduleInstance;

public:
    /// Destroys the dependency.
    virtual ~Dependency()
    {
        // nothing to do
    }

    /// Returns dependency descriptor.
    ///
    /// \return
    /// Descriptor as given to the constructor.
    const DependencyDescriptor& getDescriptor() const
    {
        return m_descriptor;
    }

    /// Returns module instance created for this dependency.
    ///
    /// \return
    /// Module instance created for this dependency.
    ///
    /// \exception UninitializedDependencyException
    /// Thrown when module was not (yet) created for this dependency.
    Module* getModuleInstance() const
            throw (UninitializedDependencyException)
    {
        if (!m_moduleInstance)
        {
            throw UninitializedDependencyException("Dependency was not initialized",
                    getDescriptor());
        }

        return m_moduleInstance;
    }

protected:
    /// Constructs the dependency.
    ///
    /// Initially there is no module instance assigned to dependency.
    ///
    /// \param descriptor
    ///         Dependency descriptor.
    Dependency(
            const DependencyDescriptor& descriptor) :
                    m_descriptor(descriptor),
                    m_moduleInstance(nullptr)
    {
        // nothing to do
    }

    /// Sets module instance created for this dependency.
    ///
    /// \param moduleInstance
    ///         Module instance to set.
    virtual void setModuleInstance(
            Module* moduleInstance)
    {
        m_moduleInstance = moduleInstance;
    }

    /// Framework Executor uses setModuleInstance().
    friend class FrameworkExecutor;
};

/// Dependency with specified module type.
///
/// \tparam MODULE_TYPE
///         Type of the module required by the dependency.
template<class MODULE_TYPE>
class TypedDependency : public Dependency
{
public:
    /// Type of the module required by the dependency.
    typedef MODULE_TYPE ModuleType;

public:
    /// Constructs the dependency.
    TypedDependency() :
                    Dependency(DependencyDescriptor::create<ModuleType>())
    {
        // nothing to do
    }

    /// Destroys the dependency.
    ///
    /// \param descriptor
    ///         Dependency descriptor.
    TypedDependency(
            const DependencyDescriptor& descriptor) :
                    Dependency(descriptor)
    {
        // nothing to do
    }

    /// Operator providing access to the module instance.
    ///
    /// \return
    /// Pointer to module instance.
    ModuleType* operator ->() const
    {
        // check the type
        ModuleType* typedInstance = dynamic_cast<ModuleType*>(getModuleInstance());
        if (typedInstance == nullptr)
        {
            throw InvalidClassException("Module instance class does not match dependency");
        }

        return typedInstance;
    }

protected:
    // overridden
    virtual void setModuleInstance(
            Module* moduleInstance)
    {
        // check the type
        if (moduleInstance)
        {
            ModuleType* typedInstance = dynamic_cast<ModuleType*>(moduleInstance);
            if (typedInstance == nullptr)
            {
                throw InvalidClassException("Module instance class does not match dependency");
            }
        }

        Dependency::setModuleInstance(moduleInstance);
    }

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_DEPENDENCY_HPP*/
