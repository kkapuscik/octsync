//-----------------------------------------------------------------------------
/// \file
/// AppArguments class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_APP_ARGUMENTS_HPP
#define OCTSYNC_FRAMEWORK_APP_ARGUMENTS_HPP

//-----------------------------------------------------------------------------

#include <string>
#include <vector>
#include <cassert>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Application arguments.
class AppArguments
{
public:
    /// Collection of the arguments.
    typedef std::vector<QString> ArgumentCollection;

private:
    /// The arguments collection.
    ArgumentCollection m_arguments;

public:
    /// Constructs the application arguments.
    ///
    /// \param argc
    ///         Argument count, same as passed to main() function.
    /// \param argv
    ///         Argument values, same as passed to main() function.
    AppArguments(
            int argc,
            char* argv[])
    {
        // nothing to do
    }

    /// Returns number of arguments.
    ///
    /// \return
    /// Number of arguments.
    size_t getCount() const
    {
        return m_arguments.size();
    }

    /// Returns value of the single argument.
    ///
    /// \param index
    ///         Index of the requested argument.
    ///
    /// \return
    /// Value of the argument.
    const QString& get(size_t index) const
    {
        assert(index >= 0 && index < getCount());

        return m_arguments[index];
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_APP_ARGUMENTS_HPP*/
