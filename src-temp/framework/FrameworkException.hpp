//-----------------------------------------------------------------------------
/// \file
/// FrameworkException class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_FRAMEWORK_FRAMEWORK_EXCEPTION_HPP
#define OCTSYNC_FRAMEWORK_FRAMEWORK_EXCEPTION_HPP

//-----------------------------------------------------------------------------

#include <commons/OctException.hpp>

#include <string>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Framework
{

/// Base class for framework exceptions
class FrameworkException : public Commons::OctException
{
public:
    /// Destroys the exception.
    virtual ~FrameworkException()
    {
        // nothing to do
    }

protected:
    /// Constructs the exception
    ///
    /// \param message
    ///     Exception message.
    FrameworkException(
            QString message) :
                    OctException(message)
    {
        // nothing to do
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_FRAMEWORK_FRAMEWORK_EXCEPTION_HPP*/
