#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T18:43:23
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = ldp
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    impl/Advertiser.cpp \
    impl/Client.cpp \
    impl/Device.cpp \
    impl/Message.cpp \
    impl/MessageBuilder.cpp \
    impl/MessageParser.cpp \
    impl/Monitor.cpp \
    impl/MulticastInterface.cpp \
    impl/Socket.cpp \
    impl/SocketSet.cpp

HEADERS += \
    Advertiser.hpp \
    Client.hpp \
    Device.hpp \
    Monitor.hpp \
    namespace.hpp \
    impl/Message.hpp \
    impl/MessageBuilder.hpp \
    impl/MessageParser.hpp \
    impl/MulticastInterface.hpp \
    impl/Socket.hpp \
    impl/SocketSet.hpp
unix {
    target.path = /usr/lib
    INSTALLS += target
}

OTHER_FILES += \
    CMakeLists.txt

INCLUDEPATH += $$PWD/..
