//-----------------------------------------------------------------------------
/// \file
/// MessageBuilder - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "MessageBuilder.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

bool MessageBuilder::build(
        const Message& message,
        QByteArray& buffer)
{
    if (!message.isValid())
    {
        return false;
    }

    appendMessageLine(message, buffer);
    appendDataLines(message, buffer);
    appendEmptyLine(buffer);

    return true;
}

void MessageBuilder::appendMessageLine(
        const Message& message,
        QByteArray& buffer)
{
    QString messageLine = QString("%1/%2.%3 %4\r\n").arg(message.getProtocolName()).arg(
            message.getProtocolVersionMajor()).arg(message.getProtocolVersionMinor()).arg(
            message.getMessageType());
    buffer.append(messageLine.toUtf8());
}

void MessageBuilder::appendDataLines(
        const Message& message,
        QByteArray& buffer)
{
    QList<QString> keys = message.getDataKeys();
    for (int i = 0; i < keys.size(); ++i)
    {
        QString dataLine = QString("%1=%2\r\n").arg(keys[i]).arg(message.getData(keys[i]));
        buffer.append(dataLine.toUtf8());
    }
}

void MessageBuilder::appendEmptyLine(
        QByteArray& buffer)
{
    buffer.append('\r');
    buffer.append('\n');
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
