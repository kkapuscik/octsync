//-----------------------------------------------------------------------------
/// \file
/// MessageBuilder - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_MessageBuilder
#define INCLUDED_OCTsync_MessageBuilder

//-----------------------------------------------------------------------------

// local includes
#include "Message.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class MessageBuilder
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    bool build(
            const Message& message,
            QByteArray& buffer);

private:
    void appendMessageLine(
            const Message& message,
            QByteArray& buffer);

    void appendDataLines(
            const Message& message,
            QByteArray& buffer);

    void appendEmptyLine(
            QByteArray& buffer);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_MessageBuilder*/
