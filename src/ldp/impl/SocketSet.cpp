//-----------------------------------------------------------------------------
/// \file
/// SocketSet - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SocketSet.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

SocketSet::SocketSet(
        QObject* parent) :
                QObject(parent),
                m_enabled(false)
{
    auto interfaces = MulticastInterface::getAllInterfaces();

    for (const MulticastInterface& iface : interfaces)
    {
        Socket* newSocket = new Socket(iface, this);
        if (newSocket && newSocket->init())
        {
            connect(newSocket, &Socket::messageReceived, this, &SocketSet::messageReceived);

            m_sockets.append(newSocket);
        }
        else
        {
            delete newSocket;
        }
    }
}

bool SocketSet::isEnabled() const
{
    return m_enabled;
}

void SocketSet::enable()
{
    if (!m_enabled)
    {
        for (auto socket : m_sockets)
        {
            socket->enable();
        }
        m_enabled = true;
    }
}

void SocketSet::disable()
{
    if (m_enabled)
    {
        for (auto socket : m_sockets)
        {
            socket->disable();
        }
        m_enabled = false;
    }
}

int SocketSet::getSocketCount() const
{
    return m_sockets.size();
}

Socket& SocketSet::getSocket(
        int index) const
{
    return *m_sockets.at(index);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
