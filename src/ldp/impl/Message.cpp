//-----------------------------------------------------------------------------
/// \file
/// Message - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Message.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

const QString Message::LDP_PROTOCOL_NAME("OCTLDP");
const int Message::LDP_PROTOCOL_VERSION_MAJOR = 1;
const int Message::LDP_PROTOCOL_VERSION_MINOR = 1;

const QString Message::MESSAGE_TYPE_READY("READY");
const QString Message::MESSAGE_TYPE_BYE("BYE");
const QString Message::MESSAGE_TYPE_SEARCH("SEARCH");
const QString Message::MESSAGE_TYPE_FOUND("FOUND");

const QString Message::DATA_KEY_URI("URI");
const QString Message::DATA_KEY_VTIME("VTIME");
const QString Message::DATA_KEY_UUID("UUID");
const QString Message::DATA_KEY_CLIENT_UUID("CLIENT-UUID");

Message::Message() :
                m_data(new MessageData())
{
    // nothing to do
}

Message::Message(
        const Message& other) :
                m_data(other.m_data)
{
    // nothing to do
}

Message::~Message()
{
    // nothing to do
}

bool Message::isValid() const
{
    return ((m_data->m_protocolName == LDP_PROTOCOL_NAME)
            && (m_data->m_protocolVersionMajor == LDP_PROTOCOL_VERSION_MAJOR)
            && (m_data->m_protocolVersionMinor == LDP_PROTOCOL_VERSION_MINOR)
            && (!m_data->m_messageType.isEmpty()));
}

QString Message::getProtocolName() const
{
    return m_data->m_protocolName;
}

int Message::getProtocolVersionMajor() const
{
    return m_data->m_protocolVersionMajor;
}

int Message::getProtocolVersionMinor() const
{
    return m_data->m_protocolVersionMinor;
}

QString Message::getMessageType() const
{
    return m_data->m_messageType;
}

QList<QString> Message::getDataKeys() const
{
    return m_data->m_data.keys();
}

bool Message::hasData(
        const QString& key) const
{
    return m_data->m_data.contains(key);
}

QString Message::getData(
        const QString& key,
        const QString& defaultValue) const
{
    return m_data->m_data.value(key, defaultValue);
}

void Message::clear()
{
    m_data->m_protocolName.clear();
    m_data->m_protocolVersionMajor = 0;
    m_data->m_protocolVersionMinor = 0;

    m_data->m_messageType.clear();

    m_data->m_data.clear();
}

void Message::setDefaultProtocol()
{
    setProtocolName(LDP_PROTOCOL_NAME);
    setProtocolVersion(LDP_PROTOCOL_VERSION_MAJOR, LDP_PROTOCOL_VERSION_MINOR);
}

void Message::setProtocolName(
        const QString& name)
{
    m_data->m_protocolName = name;
}

void Message::setProtocolVersion(
        const int major,
        const int minor)
{
    m_data->m_protocolVersionMajor = major;
    m_data->m_protocolVersionMinor = minor;
}

void Message::setMessageType(
        const QString& type)
{
    m_data->m_messageType = type;
}

void Message::addData(
        const QString& key,
        const QString& value)
{
    m_data->m_data.insert(key, value);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
