//-----------------------------------------------------------------------------
/// \file
/// MulticastInterface - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "MulticastInterface.hpp"

// library includes
#include <QtCore/QtGlobal>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

QList<MulticastInterface> MulticastInterface::getAllInterfaces()
{
    QList<MulticastInterface> resultList;

    for (const QNetworkInterface& interface : QNetworkInterface::allInterfaces())
    {
        qDebug() << "Checking interface: " << interface.humanReadableName();

        const QNetworkInterface::InterfaceFlags ifaceFlags = interface.flags();

        const bool ifaceValid = interface.isValid();
        const bool ifaceRunning = ifaceFlags & QNetworkInterface::IsRunning;
        // const bool ifaceLoopBack = ifaceFlags & QNetworkInterface::IsLoopBack;

        // Check status of the interface
        if (!ifaceValid || !ifaceRunning /*|| ifaceLoopBack*/)
        {
            // We don't want to use that interface
            continue;
        }

        // Get interface IP address
        for (const QNetworkAddressEntry& addressEntry : interface.addressEntries())
        {
            const QHostAddress ipAddress = addressEntry.ip();

            if (ipAddress.isNull())
            {
                continue;
            }

            // Don't use local host address and protocol different than IPv4.
            // TODO: To be checked if only IPv4 is supported.
            if ((ipAddress.protocol() != QAbstractSocket::IPv4Protocol))
            {
                continue;
            }

            resultList.append(MulticastInterface(interface, ipAddress));
        }
    }

    return resultList;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
