//-----------------------------------------------------------------------------
/// \file
/// MessageParser - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_MessageParser
#define INCLUDED_OCTsync_MessageParser

//-----------------------------------------------------------------------------

// local includes
#include "Message.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class MessageParser
{
// Static constants (public, protected, private)
private:
    static const char CHAR_CR = '\r';
    static const char CHAR_LF = '\n';
    static const char CHAR_SP = ' ';
    static const char CHAR_EQ = '=';
    static const char CHAR_SLASH = '/';
    static const char CHAR_DOT = '.';

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    bool parse(
            Message& message,
            const QHostAddress& sender,
            const quint16 senderPort,
            const char* const data,
            const int size);

private:
    int findLineEnd(
            const char* const data,
            const int startPos,
            const int endPos);

    int findSeparator(
            const char* const data,
            const int startPos,
            const int endPos,
            const char separator);

    int parseDigit(
            const char c);

    bool parseMessageLine(
            Message& message,
            const char* const data,
            const int startPos,
            const int endPos);

    bool parseDataLine(
            Message& message,
            const char* const data,
            const int startPos,
            const int endPos);

    bool parseProtocolInfo(
            Message& message,
            const char* const data,
            const int startPos,
            const int endPos);

    bool parseProtocolName(
            Message& message,
            const char* const data,
            const int startPos,
            const int endPos);

    bool parseProtocolVersion(
            Message& message,
            const char* const data,
            const int startPos,
            const int endPos);

    bool parseMessageType(
            Message& message,
            const char* const data,
            const int startPos,
            const int endPos);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_MessageParser*/
