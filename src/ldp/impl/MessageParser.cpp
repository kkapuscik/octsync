//-----------------------------------------------------------------------------
/// \file
/// MessageParser - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "MessageParser.hpp"

// library includes
// (none)

// system includes
#include <cassert>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

bool MessageParser::parse(
        Message& message,
        const QHostAddress& /*sender*/,
        const quint16 /*senderPort*/,
        const char* const data,
        const int size)
{
    int currentPos = 0;
    bool messageLineParsed = false;

    message.clear();

    while (currentPos < size)
    {
        // find end of line
        const int lineEndPos = findLineEnd(data, currentPos, size);
        if (lineEndPos < 0)
        {
            return false;
        }
        assert(lineEndPos <= size);

        // calculate length without CR+LF
        const int lineLen = (lineEndPos - currentPos) - 2;
        if (lineLen == 0) // empty line
        {
            if (lineEndPos != size)
            {
                return false;
            }
            break;
        }

        // now we have the line
        if (!messageLineParsed)
        {
            if (!parseMessageLine(message, data, currentPos, currentPos + lineLen))
            {
                return false;
            }

            messageLineParsed = true;
        }
        else
        {
            if (!parseDataLine(message, data, currentPos, currentPos + lineLen))
            {
                return false;
            }
        }

        currentPos = lineEndPos;
    }

    return message.isValid();
}

bool MessageParser::parseMessageLine(
        Message& message,
        const char* const data,
        const int startPos,
        const int endPos)
{
    const int separatorPos = findSeparator(data, startPos, endPos, CHAR_SP);
    if (separatorPos < 0)
    {
        return false;
    }

    if (!parseProtocolInfo(message, data, startPos, separatorPos))
    {
        return false;
    }

    if (!parseMessageType(message, data, separatorPos + 1, endPos))
    {
        return false;
    }

    return true;
}

bool MessageParser::parseMessageType(
        Message& message,
        const char* const data,
        const int startPos,
        const int endPos)
{
    const int length = endPos - startPos;
    if (length <= 0)
    {
        return false;
    }

    message.setMessageType(QString::fromUtf8(&data[startPos], length));
    return true;
}

bool MessageParser::parseProtocolInfo(
        Message& message,
        const char* const data,
        const int startPos,
        const int endPos)
{
    const int separatorPos = findSeparator(data, startPos, endPos, CHAR_SLASH);
    if (separatorPos < 0)
    {
        return false;
    }

    if (!parseProtocolName(message, data, startPos, separatorPos))
    {
        return false;
    }

    if (!parseProtocolVersion(message, data, separatorPos + 1, endPos))
    {
        return false;
    }

    return true;
}

bool MessageParser::parseProtocolName(
        Message& message,
        const char* const data,
        const int startPos,
        const int endPos)
{
    const int length = endPos - startPos;
    if (length <= 0)
    {
        return false;
    }

    message.setProtocolName(QString::fromUtf8(&data[startPos], length));
    return true;
}

bool MessageParser::parseProtocolVersion(
        Message& message,
        const char* const data,
        const int startPos,
        const int endPos)
{
    const int length = endPos - startPos;
    if (length != 3)
    {
        return false;
    }

    // check version separator
    if (data[startPos + 1] != CHAR_DOT)
    {
        return false;
    }

    // parse major & minor
    const int major = parseDigit(data[startPos + 0]);
    const int minor = parseDigit(data[startPos + 2]);
    if ((major < 0) || (minor < 0))
    {
        return false;
    }

    message.setProtocolVersion(major, minor);

    return true;
}

bool MessageParser::parseDataLine(
        Message& message,
        const char* const data,
        const int startPos,
        const int endPos)
{
    const int separatorPos = findSeparator(data, startPos, endPos, CHAR_EQ);
    if (separatorPos < 0)
    {
        return false;
    }

    const int keyLength = separatorPos - startPos;
    const int valueLength = endPos - (separatorPos + 1);

    if ((keyLength <= 0) || (valueLength < 0))
    {
        return false;
    }

    QString key = QString::fromUtf8(&data[startPos], keyLength);
    QString value = QString::fromUtf8(&data[separatorPos + 1], valueLength);

    message.addData(key, value);
    return true;
}

int MessageParser::parseDigit(
        const char c)
{
    if (c >= '0' && c <= '9')
    {
        return c - '0';
    }
    else
    {
        return -1;
    }
}

int MessageParser::findSeparator(
        const char* const data,
        const int startPos,
        const int endPos,
        const char separator)
{
    for (int i = startPos; i < endPos; ++i)
    {
        if (data[i] == separator)
        {
            return i;
        }
    }

    return -1;
}

int MessageParser::findLineEnd(
        const char* const data,
        const int startPos,
        const int endPos)
{
    for (int i = startPos + 1; i < endPos; ++i)
    {
        if (data[i] == CHAR_LF)
        {
            if (data[i - 1] == CHAR_CR)
            {
                return i + 1;
            }
        }
    }

    return -1;
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
