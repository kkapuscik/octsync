//-----------------------------------------------------------------------------
/// \file
/// Message - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Message
#define INCLUDED_OCTsync_Message

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QSharedData>
#include <QtCore/QSharedDataPointer>
#include <QtCore/QString>
#include <QtNetwork/QHostAddress>
#include <QtCore/QHash>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Message
{
// Static constants (public, protected, private)
public:
    static const QString LDP_PROTOCOL_NAME;
    static const int LDP_PROTOCOL_VERSION_MAJOR;
    static const int LDP_PROTOCOL_VERSION_MINOR;

    static const QString MESSAGE_TYPE_READY;
    static const QString MESSAGE_TYPE_BYE;
    static const QString MESSAGE_TYPE_SEARCH;
    static const QString MESSAGE_TYPE_FOUND;

    static const QString DATA_KEY_URI;
    static const QString DATA_KEY_VTIME;
    static const QString DATA_KEY_UUID;
    static const QString DATA_KEY_CLIENT_UUID;

// Types (public, protected, private)
private:
    class MessageData : public QSharedData
    {
    public:
        QString m_protocolName;

        int m_protocolVersionMajor;

        int m_protocolVersionMinor;

        QString m_messageType;

        QHash<QString, QString> m_data;
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QSharedDataPointer<MessageData> m_data;

// Methods (public, protected, private)
public:
    Message();

    Message(const Message& other);

    ~Message();

    bool isValid() const;

    void clear();

    void setDefaultProtocol();

    QString getProtocolName() const;

    int getProtocolVersionMajor() const;

    int getProtocolVersionMinor() const;

    QString getMessageType() const;

    void setProtocolName(
            const QString& name);

    void setProtocolVersion(
            const int major,
            const int minor);

    void setMessageType(
            const QString& type);

    QList<QString> getDataKeys() const;

    bool hasData(
            const QString& key) const;

    QString getData(
            const QString& key,
            const QString& defaultValue = QString::null) const;

    void addData(
            const QString& key,
            const QString& value);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Message*/
