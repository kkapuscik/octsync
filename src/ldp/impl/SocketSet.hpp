//-----------------------------------------------------------------------------
/// \file
/// SocketSet - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SocketSet
#define INCLUDED_OCTsync_SocketSet

//-----------------------------------------------------------------------------

// local includes
#include "Socket.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QVector>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SocketSet : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QVector<Socket*> SocketArray;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SocketArray m_sockets;
    bool m_enabled;

// Methods (public, protected, private)
public:
    SocketSet(
            QObject* parent = nullptr);

    bool isEnabled() const;

    void enable();

    void disable();

    int getSocketCount() const;

    Socket& getSocket(int index) const;

signals:
    void messageReceived(
            Message message);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SocketSet*/
