//-----------------------------------------------------------------------------
/// \file
/// Device - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Device.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

Device::Device()
{
    // nothing to d
}

Device::Device(
        const Commons::Uuid& uuid,
        const QUrl url) :
                m_data(new DeviceData(uuid, url))
{
    // nothing to do
}

Device::Device(
        const Device& other) :
                m_data(other.m_data)
{
    // nothing to do
}

Device::~Device()
{
    // nothing to do
}

bool Device::isValid() const
{
    return m_data->m_uuid.isValid() && m_data->m_url.isValid();
}

Commons::Uuid Device::getUuid() const
{
    return m_data->m_uuid;
}

QUrl Device::getUrl() const
{
    return m_data->m_url;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
