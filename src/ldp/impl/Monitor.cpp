//-----------------------------------------------------------------------------
/// \file
/// Monitor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../Monitor.hpp"
#include "SocketSet.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

Monitor::Monitor(
        const Client& client,
        QObject* parent) :
                QObject(parent),
                m_client(client)
{
    Q_ASSERT(client.isValid());

    m_socketSet = new SocketSet(this);

    m_timer = new QTimer(this);
    m_timer->setInterval(DEVICE_TIMEOUT_CHECK_DELAY_SEC * 1000);
    m_timer->setSingleShot(false);
    m_timer->setTimerType(Qt::VeryCoarseTimer);

    connect(m_socketSet, &SocketSet::messageReceived, this, &Monitor::processMessage);
    connect(m_timer, &QTimer::timeout, this, &Monitor::checkDevicesValidity);
}

void Monitor::enable()
{
    if (!m_socketSet->isEnabled())
    {
        m_socketSet->enable();
        m_timer->start();

        sendSearch();
    }
}

void Monitor::disable()
{
    if (m_socketSet->isEnabled())
    {
        m_socketSet->disable();
        m_timer->stop();

        // all devices must be removed
        DeviceMapConstIterator curIter = m_devices.begin();
        DeviceMapConstIterator endIter = m_devices.end();
        for (; curIter != endIter; ++curIter)
        {
            emit deviceRemoved(curIter.value().m_device);
        }
    }
}

void Monitor::sendSearch()
{
    if (m_socketSet->isEnabled())
    {
        Message message;

        message.setDefaultProtocol();
        message.setMessageType(Message::MESSAGE_TYPE_SEARCH);
        message.addData(Message::DATA_KEY_CLIENT_UUID, m_client.getUuid().toString());

        for (int i = 0; i < m_socketSet->getSocketCount(); ++i)
        {
            m_socketSet->getSocket(i).sendMessage(message);
        }
    }
}

QVector<Device> Monitor::getDevices() const
{
    DeviceMapConstIterator curIter = m_devices.begin();
    DeviceMapConstIterator endIter = m_devices.end();

    QVector<Device> devices;
    for (; curIter != endIter; ++curIter)
    {
        devices.append(curIter.value().m_device);
    }

    return devices;
}

void Monitor::processMessage(
        Message message)
{
    qDebug() << "Monitor::processMessage: " << message.getMessageType();

    if ((message.getMessageType() == Message::MESSAGE_TYPE_READY) ||
            (message.getMessageType() == Message::MESSAGE_TYPE_FOUND))
    {
        bool vtimeOk;

        const QUrl url = QUrl(message.getData(Message::DATA_KEY_URI));
        const Commons::Uuid uuid = Commons::Uuid::parse(message.getData(Message::DATA_KEY_UUID));
        const int vTime = message.getData(Message::DATA_KEY_VTIME).toInt(&vtimeOk, 10);

        const Device newDevice(uuid, url);

        if (newDevice.isValid() && vtimeOk && (vTime > 0))
        {
            DeviceMapIterator devIter = m_devices.find(uuid);
            if (devIter == m_devices.end())
            {
                const QDateTime validUntil = QDateTime::currentDateTimeUtc().addSecs(vTime);

                m_devices.insert(uuid, DeviceEntry(newDevice, validUntil));

                emit deviceAdded(newDevice);
            }
            else
            {
                const QDateTime validUntil = QDateTime::currentDateTimeUtc().addSecs(vTime);
                devIter.value().m_validUntil = validUntil;
            }
        }
    }
    else if (message.getMessageType() == Message::MESSAGE_TYPE_BYE)
    {
        const Commons::Uuid uuid = Commons::Uuid::parse(message.getData(Message::DATA_KEY_UUID));

        if (uuid.isValid())
        {
            DeviceMapIterator devIter = m_devices.find(uuid);
            if (devIter != m_devices.end())
            {
                emit deviceRemoved(devIter.value().m_device);

                m_devices.erase(devIter);
            }
        }
    }
}

void Monitor::checkDevicesValidity()
{
    DeviceMapIterator curIter = m_devices.begin();
    DeviceMapIterator endIter = m_devices.end();
    QDateTime currentTime = QDateTime::currentDateTimeUtc();

    QVector<Device> timeoutedDevices;
    for (; curIter != endIter; ++curIter)
    {
        if (curIter.value().m_validUntil < currentTime)
        {
            timeoutedDevices.append(curIter.value().m_device);
        }
    }

    for (int i = 0; i < timeoutedDevices.size(); ++i)
    {
        emit deviceRemoved(timeoutedDevices[i]);

        m_devices.remove(timeoutedDevices[i].getUuid());
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
