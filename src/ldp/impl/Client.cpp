//-----------------------------------------------------------------------------
/// \file
/// Client - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Client.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

Client::Client()
{
    // nothing to do
}

Client::Client(
        const Commons::Uuid& uuid) :
                m_uuid(uuid)
{
    // nothing to do
}

bool Client::isValid() const
{
    return m_uuid.isValid();
}

const Commons::Uuid& Client::getUuid() const
{
    return m_uuid;
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
