//-----------------------------------------------------------------------------
/// \file
/// Socket - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Socket
#define INCLUDED_OCTsync_Socket

//-----------------------------------------------------------------------------

// local includes
#include "Message.hpp"
#include "MulticastInterface.hpp"

// library includes
#include <QtCore/QObject>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QNetworkInterface>

// system includes
// (none)

// using clauses
// (none)

// forward references
class QUdpSocket;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Socket : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const QHostAddress LDP_GROUP_ADDRESS;
    static const uint16_t LDP_PORT;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    MulticastInterface m_interface;
    bool m_isEnabled;
    QUdpSocket* m_socket;

// Methods (public, protected, private)
public:
    Socket(
            const MulticastInterface& iface,
            QObject* parent = nullptr);

    bool init();

    bool isEnabled() const;

    void enable();

    void disable();

    bool sendMessage(
            Message message);

    const QHostAddress& getAddress() const
    {
        return m_interface.getAddress();
    }

signals:
    void messageReceived(
            Message message);

private:
    void readPendingDatagrams();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Socket*/
