//-----------------------------------------------------------------------------
/// \file
/// MulticastInterface - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_MulticastInterface
#define INCLUDED_OCTsync_MulticastInterface

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QHostAddress>
#include <QtCore/QList>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class MulticastInterface
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
public:
    static QList<MulticastInterface> getAllInterfaces();

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QNetworkInterface m_interface;
    QHostAddress m_address;

// Methods (public, protected, private)
public:
    const QNetworkInterface& getInterface() const
    {
        return m_interface;
    }

    const QHostAddress& getAddress() const
    {
        return m_address;
    }

private:
    MulticastInterface(
            const QNetworkInterface interface,
            QHostAddress address) :
                    m_interface(interface),
                    m_address(address)
    {
        // nothing to do
    }

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_MulticastInterface*/
