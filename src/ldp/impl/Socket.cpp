//-----------------------------------------------------------------------------
/// \file
/// Socket - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Socket.hpp"
#include "MessageParser.hpp"
#include "MessageBuilder.hpp"

// library includes
#include <QtNetwork/QUdpSocket>
#include <QtNetwork/QNetworkInterface>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

const QHostAddress Socket::LDP_GROUP_ADDRESS("239.255.255.251");
const uint16_t Socket::LDP_PORT = 1901;

Socket::Socket(
        const MulticastInterface& iface,
        QObject* parent) :
                QObject(parent),
                m_interface(iface),
                m_isEnabled(false)
{
    m_socket = new QUdpSocket(this);

    connect(m_socket, &QUdpSocket::readyRead, this, &Socket::readPendingDatagrams);
}

bool Socket::isEnabled() const
{
    return m_isEnabled;
}

bool Socket::init()
{
    bool result = false;

    qDebug() << "Trying to bind to interface = " << m_interface.getInterface().humanReadableName()
            << " -> " << m_interface.getAddress().toString();

    if (m_socket->bind(QHostAddress::AnyIPv4, LDP_PORT,
            QAbstractSocket::ShareAddress | QAbstractSocket::ReuseAddressHint))
    {
        qDebug() << "Bind successful";

        m_socket->setMulticastInterface(m_interface.getInterface());
        m_socket->setSocketOption(QAbstractSocket::MulticastLoopbackOption, 1);
        m_socket->setSocketOption(QAbstractSocket::MulticastTtlOption, 4);

        result = true;
    }
    else
    {
        qDebug() << "Cannot bind";
    }

    return result;
}

void Socket::enable()
{
    if (!m_isEnabled)
    {
        // TODO: Proper error handling.
        if (m_socket->joinMulticastGroup(LDP_GROUP_ADDRESS, m_interface.getInterface()))
        {
            m_isEnabled = true;
        }
        else
        {
            qDebug() << "Cannot join multicast group";
        }
    }

    qDebug() << "socket " << this << " enabled";
}

void Socket::disable()
{
    if (m_isEnabled)
    {
        if (!m_socket->leaveMulticastGroup(LDP_GROUP_ADDRESS, m_interface.getInterface()))
        {
            qDebug() << "Cannot leave multicast group";
        }

        m_isEnabled = false;
    }

    qDebug() << "socket " << this << " disabled";
}

bool Socket::sendMessage(
        Message message)
{
    bool result = false;

    if (m_isEnabled)
    {
        QByteArray datagram;

        if (MessageBuilder().build(message, datagram))
        {
            if (m_socket->writeDatagram(datagram.constData(), datagram.size(), LDP_GROUP_ADDRESS,
                    LDP_PORT) == datagram.size())
            {
                qDebug() << "Message sent: " << this << " " << message.getMessageType();
                result = true;
            }
            else
            {
                qDebug() << "Cannot send datagram";
            }
        }
        else
        {
            qDebug() << "Cannot build message";
        }
    }

    return result;
}

void Socket::readPendingDatagrams()
{
    while (m_socket->hasPendingDatagrams())
    {
        QByteArray datagram;

        qint64 datagramSize = m_socket->pendingDatagramSize();

        // TODO: limit size
        datagram.resize(datagramSize);

        QHostAddress sender;
        quint16 senderPort;

        // TODO: proper error handling
        quint64 bytesRead = m_socket->readDatagram(datagram.data(), datagram.size(), &sender,
                &senderPort);
        if (m_isEnabled)
        {
            if (bytesRead > 0)
            {
                Message parsedMessage;
                if (MessageParser().parse(parsedMessage, sender, senderPort, datagram.constData(),
                        datagram.size()))
                {
                    qDebug() << "Received: " << this << " " << parsedMessage.getMessageType();

                    emit messageReceived(parsedMessage);
                }
                else
                {
                    qDebug() << "Cannot parse datagram";
                }
            }
            else
            {
                qDebug() << "Cannot read datagram";
            }
        }
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
