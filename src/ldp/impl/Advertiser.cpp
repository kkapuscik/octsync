//-----------------------------------------------------------------------------
/// \file
/// Advertiser - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Advertiser.hpp"
#include "SocketSet.hpp"
#include "Message.hpp"

// library includes
// (none)

// system includes
#include <cassert>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

Advertiser::Advertiser(
        QObject* parent) :
                QObject(parent),
                m_validityTime(DEFAULT_VALIDITY_TIME)
{
    m_socketSet = new SocketSet(this);

    m_timer = new QTimer(this);
    m_timer->setInterval((m_validityTime / 2) * 1000);
    m_timer->setSingleShot(false);
    m_timer->setTimerType(Qt::VeryCoarseTimer);

    connect(m_socketSet, &SocketSet::messageReceived, this, &Advertiser::processMessage);
    connect(m_timer, &QTimer::timeout, this, &Advertiser::sendAdvertisements);
}

Advertiser::~Advertiser()
{
    disable();
}

void Advertiser::enable()
{
    if (!m_socketSet->isEnabled())
    {
        m_socketSet->enable();
        m_timer->start();

        sendMessages(true);
    }
}

void Advertiser::disable()
{
    if (m_socketSet->isEnabled())
    {
        sendMessages(false);

        m_timer->stop();
        m_socketSet->disable();
    }
}

int Advertiser::getValidityTime() const
{
    return m_validityTime;
}

void Advertiser::setValidityTime(
        int seconds)
{
    if ((seconds >= 60) && (seconds <= 60 * 60))
    {
        m_validityTime = seconds;

        sendMessages(true);

        m_timer->setInterval(m_validityTime / 2);
        m_timer->start(); // with auto-restart
    }
    else
    {
        assert(0);
    }
}

void Advertiser::addDevice(
        const Device& device)
{
    Q_ASSERT(device.isValid());

    DeviceIterator iter = m_devices.find(device.getUuid());
    if (iter == m_devices.end())
    {
        // store the device to be advertised
        m_devices.insert(device.getUuid(), device);

        // send 'added' notification immediately (if enabled)
        if (m_socketSet->isEnabled())
        {
            sendMessage(device, true);
        }
    }
}

void Advertiser::removeDevice(
        const Device& device)
{
    Q_ASSERT(device.isValid());

    DeviceIterator iter = m_devices.find(device.getUuid());
    if (iter != m_devices.end())
    {
        // remove from collection of advertised devices
        m_devices.erase(iter);

        // send 'removed' notification immediately (if enabled)
        if (m_socketSet->isEnabled())
        {
            sendMessage(device, false);
        }
    }
}

void Advertiser::processMessage(
        Message message)
{
    if (message.getMessageType() == Message::MESSAGE_TYPE_SEARCH)
    {
        QString clientUuid = message.getData(Message::DATA_KEY_CLIENT_UUID);
        if (!clientUuid.isNull())
        {
            sendMessages(true, clientUuid);
        }
    }
}

void Advertiser::sendAdvertisements()
{
    sendMessages(true);
}

void Advertiser::sendMessages(
        const bool ready,
        const QString& clientUuid)
{
    DeviceConstIterator curIter = m_devices.begin();
    DeviceConstIterator endIter = m_devices.end();
    for (; curIter != endIter; ++curIter)
    {
        sendMessage(curIter.value(), ready, clientUuid);
    }
}

void Advertiser::sendMessage(
        const Device& device,
        const bool ready,
        const QString& clientUuid)
{
    if (ready)
    {
        if (clientUuid.isNull())
        {
            sendReadyMessage(device);
        }
        else
        {
            sendFoundMessage(device, clientUuid);
        }
    }
    else
    {
        sendByeMessage(device);
    }
}

void Advertiser::sendReadyMessage(
        const Device& device)
{
    for (int i = 0; i < m_socketSet->getSocketCount(); ++i)
    {
        Socket& socket = m_socketSet->getSocket(i);

        Message message;

        message.setDefaultProtocol();
        message.setMessageType(Message::MESSAGE_TYPE_READY);
        message.addData(Message::DATA_KEY_URI, getDeviceUrl(device, socket));
        message.addData(Message::DATA_KEY_VTIME, QString("%1").arg(m_validityTime));
        message.addData(Message::DATA_KEY_UUID, device.getUuid().toString());

        socket.sendMessage(message);
    }
}

void Advertiser::sendByeMessage(
        const Device& device)
{
    Message message;

    message.setDefaultProtocol();
    message.setMessageType(Message::MESSAGE_TYPE_BYE);
    message.addData(Message::DATA_KEY_UUID, device.getUuid().toString());

    for (int i = 0; i < m_socketSet->getSocketCount(); ++i)
    {
        m_socketSet->getSocket(i).sendMessage(message);
    }
}

void Advertiser::sendFoundMessage(
        const Device& device,
        const QString& clientUuid)
{
    for (int i = 0; i < m_socketSet->getSocketCount(); ++i)
    {
        Socket& socket = m_socketSet->getSocket(i);

        Message message;

        message.setDefaultProtocol();
        message.setMessageType(Message::MESSAGE_TYPE_FOUND);
        message.addData(Message::DATA_KEY_CLIENT_UUID, clientUuid);
        message.addData(Message::DATA_KEY_URI, getDeviceUrl(device, socket));
        message.addData(Message::DATA_KEY_VTIME, QString("%1").arg(m_validityTime));

        message.addData(Message::DATA_KEY_UUID, device.getUuid().toString());

        socket.sendMessage(message);
    }
}

QString Advertiser::getDeviceUrl(
        const Device& device,
        const Socket& socket)
{
    QUrl url = device.getUrl();
    url.setHost(socket.getAddress().toString());
    return url.toString();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
