//-----------------------------------------------------------------------------
/// \file
/// Namespace definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

namespace OCTsync
{

/// OCT LDP protocol implementation.
namespace Ldp
{

}

}
