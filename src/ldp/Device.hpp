//-----------------------------------------------------------------------------
/// \file
/// Device - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_BopRpc_Device
#define INCLUDED_OCTsync_BopRpc_Device

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QSharedDataPointer>
#include <QtCore/QUuid>
#include <QtCore/QUrl>
#include <QtCore/QDateTime>
#include <QtNetwork/QHostAddress>
#include <commons/Uuid.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Device
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    class DeviceData : public QSharedData
    {
    public:
        Commons::Uuid m_uuid;

        QUrl m_url;

        DeviceData(
                const Commons::Uuid uuid,
                const QUrl& url) :
                        m_uuid(uuid),
                        m_url(url)
        {
            // nothing to do
        }
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QSharedDataPointer<DeviceData> m_data;

// Methods (public, protected, private)
public:
    Device();

    Device(
            const Commons::Uuid& uuid,
            const QUrl url);

    Device(
            const Device& other);

    ~Device();

    bool isValid() const;

    Commons::Uuid getUuid() const;

    QUrl getUrl() const;
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_BopRpc_Device*/
