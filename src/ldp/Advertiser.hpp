//-----------------------------------------------------------------------------
/// \file
/// Advertiser - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Advertiser
#define INCLUDED_OCTsync_Advertiser

//-----------------------------------------------------------------------------

// local includes
#include "Device.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QMap>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
class SocketSet;
class Socket;
class Message;

/// TODO: Class documentation
///
class Advertiser : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const int DEFAULT_VALIDITY_TIME = 60; // 60 seconds

// Types (public, protected, private)
private:
    typedef QMap<Commons::Uuid, Device> DeviceMap;
    typedef DeviceMap::Iterator DeviceIterator;
    typedef DeviceMap::ConstIterator DeviceConstIterator;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    int m_validityTime;

    SocketSet* m_socketSet;
    QTimer* m_timer;

    DeviceMap m_devices;

// Methods (public, protected, private)
public:
    Advertiser(
            QObject* parent = nullptr);

    virtual ~Advertiser();

    void enable();

    void disable();

    int getValidityTime() const;

    void setValidityTime(
            int seconds);

    void addDevice(
            const Device& device);

    void removeDevice(
            const Device& device);

private:
    void processMessage(
            Message message);

    void sendAdvertisements();

    void sendMessages(
            const bool ready,
            const QString& clientUuid = QString::null);

    void sendMessage(
            const Device& device,
            const bool ready,
            const QString& clientUuid = QString::null);

    void sendReadyMessage(
            const Device& device);

    void sendByeMessage(
            const Device& device);

    void sendFoundMessage(
            const Device& device,
            const QString& clientUuid = QString::null);

    QString getDeviceUrl(
            const Device& device,
            const Socket& socket);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Advertiser*/
