//-----------------------------------------------------------------------------
/// \file
/// Monitor - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Monitor
#define INCLUDED_OCTsync_Monitor

//-----------------------------------------------------------------------------

// local includes
#include "Device.hpp"
#include "Client.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QVector>
#include <QtCore/QTimer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
class SocketSet;
class Message;

/// TODO: Class documentation
///
class Monitor : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const int DEVICE_TIMEOUT_CHECK_DELAY_SEC = 30;

// Types (public, protected, private)
private:
    struct DeviceEntry
    {
        Device m_device;
        QDateTime m_validUntil;

        DeviceEntry(
                const Device& device,
                const QDateTime& validUntil) :
                        m_device(device),
                        m_validUntil(validUntil)
        {
            // nothing to do
        }
    };

    typedef QMap<Commons::Uuid, DeviceEntry> DeviceMap;
    typedef DeviceMap::Iterator DeviceMapIterator;
    typedef DeviceMap::ConstIterator DeviceMapConstIterator;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Client m_client;

    SocketSet* m_socketSet;

    DeviceMap m_devices;

    QTimer* m_timer;

// Methods (public, protected, private)
public:
    Monitor(
            const Client& client,
            QObject* parent = nullptr);

    void enable();

    void disable();

    void sendSearch();

    QVector<Device> getDevices() const;

private:
    void processMessage(
            Message message);

    void checkDevicesValidity();

signals:
    void deviceAdded(
            Device device);

    void deviceRemoved(
            Device device);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Monitor*/
