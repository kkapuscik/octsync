//-----------------------------------------------------------------------------
/// \file
/// Client - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Client
#define INCLUDED_OCTsync_Client

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <commons/Uuid.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ldp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Client
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_uuid;

// Methods (public, protected, private)
public:
    Client();

    Client(
            const Commons::Uuid& uuid);

    bool isValid() const;

    const Commons::Uuid& getUuid() const;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Client*/
