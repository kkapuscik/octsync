//-----------------------------------------------------------------------------
/// \file
/// LogManager - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2014 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_LogManager
#define INCLUDED_OCTsync_LogManager

//-----------------------------------------------------------------------------

// local includes
#include "LogMessage.hpp"

// library includes
#include <QtCore/QtGlobal>
#include <QtCore/QObject>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ui
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class LogManager : public QObject
{
    Q_OBJECT

// Static constants (public, protected, private)
public:
    static LogManager* getInstance();

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    void initialize();

    void handleMessage(QtMsgType type,
                       const QMessageLogContext& context,
                       const QString& msg);

private:
    LogManager();

signals:
    void messageLogged(const LogMessage& message);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_LogManager*/
