//-----------------------------------------------------------------------------
/// \file
/// LogMessage - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2014 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_LogMessage
#define INCLUDED_OCTsync_LogMessage

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QtGlobal>
#include <QtCore/QString>
#include <QtCore/QDebug>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ui
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class LogMessage
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    const QtMsgType m_type;
    const QMessageLogContext& m_context;
    const QString& m_text;


// Methods (public, protected, private)
public:
    LogMessage(
            const QtMsgType type,
            const QMessageLogContext& context,
            const QString& text);

    QtMsgType getType() const
    {
        return m_type;
    }

    const QMessageLogContext& getContext() const
    {
        return m_context;
    }

    const QString& getText() const
    {
        return m_text;
    }

    QString getTypeString() const;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_LogMessage*/
