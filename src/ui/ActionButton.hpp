//-----------------------------------------------------------------------------
/// \file
/// ActionButton - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2014 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ActionButton
#define INCLUDED_OCTsync_ActionButton

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QPushButton>
#include <QtWidgets/QAction>
#include <QtCore/QPointer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ui
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ActionButton : public QPushButton
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QPointer<QAction> m_action;

// Methods (public, protected, private)
public:
    explicit ActionButton(
            QAction* action,
            QWidget* parent = nullptr);

private:
    void onActionChanged();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ActionButton*/
