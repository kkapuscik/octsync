#-------------------------------------------------
#
# Project created by QtCreator 2014-10-02T19:03:54
#
#-------------------------------------------------

QT       += widgets

TARGET = ui
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    impl/ActionButton.cpp \
    impl/LogManager.cpp \
    impl/LogMessage.cpp

HEADERS += \
    ActionButton.hpp \
    LogManager.hpp \
    LogMessage.hpp

unix {
    target.path = /usr/lib
    INSTALLS += target
}

INCLUDEPATH += $$PWD/..
