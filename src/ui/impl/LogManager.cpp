//-----------------------------------------------------------------------------
/// \file
/// LogManager - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2014 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "LogManager.hpp"
#include "LogMessage.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ui
{

static void messageHandler(QtMsgType type,
                           const QMessageLogContext& context,
                           const QString& msg)
{
    LogManager::getInstance()->handleMessage(type, context, msg);
}

LogManager* LogManager::getInstance()
{
    static LogManager theInstance;

    return &theInstance;
}

void LogManager::initialize()
{
    qInstallMessageHandler(messageHandler);
}

void LogManager::handleMessage(QtMsgType type,
                               const QMessageLogContext& context,
                               const QString& msg)
{
    LogMessage logMessage(type, context, msg);

    bool terminate = false;

    switch (type) {
        case QtDebugMsg:
        case QtWarningMsg:
        case QtCriticalMsg:
            break;
        case QtFatalMsg:
        default:
            terminate = true;
            break;
    }

    emit messageLogged(logMessage);

    if (terminate) {
        abort();
    }
}

LogManager::LogManager()
{
    // nothing to do
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
