//-----------------------------------------------------------------------------
/// \file
/// LogMessage - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2014 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "LogMessage.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ui
{

LogMessage::LogMessage(
        const QtMsgType type,
        const QMessageLogContext& context,
        const QString& text) :
    m_type(type),
    m_context(context),
    m_text(text)
{
    // nothing to do
}

QString LogMessage::getTypeString() const
{
    QString typeStr;

    switch (m_type) {
        case QtDebugMsg:
            typeStr = QStringLiteral("Debug");
            break;
        case QtWarningMsg:
            typeStr = QStringLiteral("Warng");
            break;
        case QtCriticalMsg:
            typeStr = QStringLiteral("Critc");
            break;
        case QtFatalMsg:
            typeStr = QStringLiteral("Fatal");
            break;
        default:
            typeStr = QStringLiteral("?????");
            break;
    }

    return typeStr;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
