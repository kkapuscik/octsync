//-----------------------------------------------------------------------------
/// \file
/// ActionButton - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2014 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ActionButton.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Ui
{

ActionButton::ActionButton(
        QAction* action,
        QWidget* parent) :
                QPushButton(parent),
                m_action(action)
{
    Q_ASSERT(action);

    connect(action, &QAction::changed, this, &ActionButton::onActionChanged);
    connect(this, &QAbstractButton::clicked, action, &QAction::trigger);

    onActionChanged();
}

void ActionButton::onActionChanged()
{
    if (m_action)
    {
        Q_ASSERT(!m_action->isCheckable());

        setEnabled(m_action->isEnabled());
        setText(m_action->text());
        setIcon(m_action->icon());
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
