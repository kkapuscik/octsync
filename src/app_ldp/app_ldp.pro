#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T20:44:58
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = app_ldp
TEMPLATE = app
CONFIG += c++11

SOURCES += \
    impl/TestServer.cpp \
    impl/app_ldp_main.cpp \
    impl/LdpMainWindow.cpp \
    impl/LdpEngine.cpp

HEADERS  += \
    namespace.hpp \
    impl/TestServer.hpp \
    impl/LdpMainWindow.hpp \
    impl/LdpEngine.hpp

OTHER_FILES += \
    CMakeLists.txt

RESOURCES += \
    ../../data/octsync.qrc

CONFIG += mobility
MOBILITY =

INCLUDEPATH += $$PWD/..

win32-g++: {
    message("Building for WIN G++")
} else:unix: {
    message("Building for UNIX")
} else {
    error("Unsupported platform configuration")
}

# add library COMMONS
DEPENDPATH += $$PWD/../commons
LIBS += -L$$OUT_PWD/../commons/ -lcommons
PRE_TARGETDEPS += $$OUT_PWD/../commons/libcommons.a

# add library LDP
DEPENDPATH += $$PWD/../ldp
LIBS += -L$$OUT_PWD/../ldp/ -lldp
PRE_TARGETDEPS += $$OUT_PWD/../ldp/libldp.a

# add library BOPRPC
DEPENDPATH += $$PWD/../boprpc
LIBS += -L$$OUT_PWD/../boprpc/ -lboprpc
PRE_TARGETDEPS += $$OUT_PWD/../boprpc/libboprpc.a

# add library BOP
DEPENDPATH += $$PWD/../bop
LIBS += -L$$OUT_PWD/../bop/ -lbop
PRE_TARGETDEPS += $$OUT_PWD/../bop/libbop.a

# add library UI
DEPENDPATH += $$PWD/../ui
LIBS += -L$$OUT_PWD/../ui/ -lui
PRE_TARGETDEPS += $$OUT_PWD/../ui/libui.a
