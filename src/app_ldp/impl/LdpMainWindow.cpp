//-----------------------------------------------------------------------------
/// \file
/// LdpMainWindow - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "LdpMainWindow.hpp"

// library includes
#include <QtCore/QtGlobal>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMessageBox>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppLdp
{

LdpMainWindow::LdpMainWindow(
        QWidget* parent) :
                QMainWindow(parent)
{
    m_engine = new LdpEngine(this);

    createActions();
    createWidgets();

    resize(800, 600);
    move(QApplication::desktop()->screen()->rect().center() - rect().center());

    connectActions();

    m_monitorEnabledAction->activate(QAction::Trigger);
    m_advertiserEnabledAction->activate(QAction::Trigger);
    m_deviceEnabledAction->activate(QAction::Trigger);
    m_serverEnabledAction->activate(QAction::Trigger);
}

void LdpMainWindow::addLogMessage(const LogMessage& message)
{
    QString messageStr("%1: %2");

    messageStr = messageStr.arg(message.getTypeString()).arg(message.getText());

    m_logTextEdit->append(messageStr);
}

void LdpMainWindow::createActions()
{
    m_appQuit = new QAction(tr("Quit"), this);

    m_monitorEnabledAction = new QAction(tr("Monitor Enabled"), this);
    m_monitorEnabledAction->setCheckable(true);

    m_advertiserEnabledAction = new QAction(tr("Advertiser Enabled"), this);
    m_advertiserEnabledAction->setCheckable(true);

    m_deviceEnabledAction = new QAction(tr("Device Enabled"), this);
    m_deviceEnabledAction->setCheckable(true);

    m_serverEnabledAction = new QAction(tr("Server Enabled"), this);
    m_serverEnabledAction->setCheckable(true);

    m_appAbout = new QAction(tr("About..."), this);
    m_appAboutQt = new QAction(tr("About Qt..."), this);
}

void LdpMainWindow::createWidgets()
{
    m_logTextEdit = new QTextEdit(this);
    m_logTextEdit->setReadOnly(true);

    m_menuFile = new QMenu(this);
    m_menuFile->setTitle(tr("&File"));
    m_menuFile->addAction(m_appQuit);

    m_menuApplication = new QMenu(this);
    m_menuApplication->setTitle(tr("&Application"));
    m_menuApplication->addAction(m_monitorEnabledAction);
    m_menuApplication->addSeparator();
    m_menuApplication->addAction(m_advertiserEnabledAction);
    m_menuApplication->addAction(m_deviceEnabledAction);
    m_menuApplication->addSeparator();
    m_menuApplication->addAction(m_serverEnabledAction);

    m_menuHelp = new QMenu(this);
    m_menuHelp->setTitle(tr("&Help"));
    m_menuHelp->addAction(m_appAbout);
    m_menuHelp->addAction(m_appAboutQt);

    menuBar()->addMenu(m_menuFile);
    menuBar()->addMenu(m_menuApplication);
    menuBar()->addMenu(m_menuHelp);

    setCentralWidget(m_logTextEdit);
}

void LdpMainWindow::connectActions()
{
    connect(m_appQuit, &QAction::triggered, this, &LdpMainWindow::close);
    connect(m_monitorEnabledAction, &QAction::triggered, this, &LdpMainWindow::setMonitorStatus);
    connect(m_advertiserEnabledAction, &QAction::triggered, this, &LdpMainWindow::setAdvertiserStatus);
    connect(m_deviceEnabledAction, &QAction::triggered, this, &LdpMainWindow::setDeviceStatus);
    connect(m_serverEnabledAction, &QAction::triggered, this, &LdpMainWindow::setServerStatus);
    connect(m_appAbout, &QAction::triggered, this, &LdpMainWindow::about);
    connect(m_appAboutQt, &QAction::triggered, this, &LdpMainWindow::aboutQt);
}

void LdpMainWindow::about()
{
    QMessageBox::about(this, tr("About"),
            tr("LDP Test App\nCopyright (C) 2014 Krzysztof Kapuscik"));
}

void LdpMainWindow::aboutQt()
{
    QMessageBox::aboutQt(this, tr("About Qt"));
}

void LdpMainWindow::setMonitorStatus(const bool enable)
{
    m_logTextEdit->append(enable ? "Enable monitor" : "Disable monitor");
    m_logTextEdit->moveCursor(QTextCursor::End);
    m_logTextEdit->ensureCursorVisible();

    m_engine->setMonitorStatus(enable);
}

void LdpMainWindow::setAdvertiserStatus(const bool enable)
{
    m_logTextEdit->append(enable ? "Enable advertiser" : "Disable advertiser");
    m_logTextEdit->moveCursor(QTextCursor::End);
    m_logTextEdit->ensureCursorVisible();

    m_engine->setAdvertiserStatus(enable);
}

void LdpMainWindow::setDeviceStatus(const bool enable)
{
    m_logTextEdit->append(enable ? "Enable device" : "Disable device");
    m_logTextEdit->moveCursor(QTextCursor::End);
    m_logTextEdit->ensureCursorVisible();

    m_engine->setDeviceStatus(enable);
}

void LdpMainWindow::setServerStatus(const bool enable)
{
    m_logTextEdit->append(enable ? "Enable server" : "Disable server");
    m_logTextEdit->moveCursor(QTextCursor::End);
    m_logTextEdit->ensureCursorVisible();

    m_engine->setServerStatus(enable);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
