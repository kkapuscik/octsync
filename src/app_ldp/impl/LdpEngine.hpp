//-----------------------------------------------------------------------------
/// \file
/// LdpEngine - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_LdpEngine
#define INCLUDED_OCTsync_LdpEngine

//-----------------------------------------------------------------------------

// local includes
#include "TestServer.hpp"

// library includes
#include <QtCore/QObject>
#include <ldp/Device.hpp>
#include <ldp/Client.hpp>
#include <ldp/Advertiser.hpp>
#include <ldp/Monitor.hpp>
#include <boprpc/Client.hpp>
#include <bop/ObjectBase.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppLdp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class LdpEngine : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)
private:
    Ldp::Device m_testDevice;

    Ldp::Client m_testClient;

    Ldp::Advertiser* m_advertiser;

    Ldp::Monitor* m_monitor;

    Bop::ObjectFactoryManager* m_factoryManager;

    TestServer* m_bopServer;

    Bop::Client* m_bopClient;

// Methods (public, protected, private)
public:
    LdpEngine(
            QObject* parent = nullptr);

    ~LdpEngine();

    void setMonitorStatus(
            const bool enable);

    void setAdvertiserStatus(
            const bool enable);

    void setDeviceStatus(
            const bool enable);

    void setServerStatus(
            const bool enable);

private:
    void onDeviceAdded(
            Ldp::Device device);

    void onDeviceRemoved(
            Ldp::Device device);

    void onObjectReceived(
            QUrl url,
            Bop::ConstObjectSharedPtr object);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_LdpEngine*/
