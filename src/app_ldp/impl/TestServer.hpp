//-----------------------------------------------------------------------------
/// \file
/// TestServer - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_TestServer
#define INCLUDED_OCTsync_TestServer

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <bop/Server.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppLdp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class TestServer : public Bop::Server
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    TestServer(
            QObject* parent = nullptr);

protected:
    virtual std::unique_ptr<Bop::ObjectBase> processRequest(
            const Bop::ObjectBase& requestMessage);


};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_TestServer*/
