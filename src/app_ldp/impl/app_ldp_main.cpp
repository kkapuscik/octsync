#include <ui/LogManager.hpp>
#include <QtWidgets/QApplication>

#include "LdpMainWindow.hpp"

using OCTsync::AppLdp::LdpMainWindow;
using OCTsync::Ui::LogManager;

int main(
        int argc,
        char** argv)
{
    QApplication application(argc, argv);
    LdpMainWindow mainWindow;

    LogManager* const logManager = LogManager::getInstance();

    logManager->initialize();

    QApplication::connect(logManager, &LogManager::messageLogged,
                          &mainWindow, &LdpMainWindow::addLogMessage);

    mainWindow.show();
    return application.exec();
}
