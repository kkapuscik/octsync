//-----------------------------------------------------------------------------
/// \file
/// LdpMainWindow - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_LdpMainWindow
#define INCLUDED_OCTsync_LdpMainWindow

//-----------------------------------------------------------------------------

// local includes
#include "LdpEngine.hpp"

// library includes
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QAction>
#include <QtWidgets/QMenu>
#include <ui/LogMessage.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::LogMessage;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppLdp
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class LdpMainWindow : public QMainWindow
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    LdpEngine* m_engine;

    QTextEdit* m_logTextEdit;

    QMenu* m_menuFile;
    QMenu* m_menuApplication;
    QMenu* m_menuHelp;

    QAction* m_appQuit;
    QAction* m_appAbout;
    QAction* m_appAboutQt;
    QAction* m_monitorEnabledAction;
    QAction* m_advertiserEnabledAction;
    QAction* m_deviceEnabledAction;
    QAction* m_serverEnabledAction;

// Methods (public, protected, private)
public:
    LdpMainWindow(
            QWidget* parent = nullptr);

    void addLogMessage(
            const LogMessage& message);

private:
    void createActions();
    void createWidgets();
    void connectActions();

    void about();
    void aboutQt();

    void setMonitorStatus(
            const bool enable);
    void setAdvertiserStatus(
            const bool enable);
    void setDeviceStatus(
            const bool enable);
    void setServerStatus(
            const bool enable);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_LdpMainWindow*/
