//-----------------------------------------------------------------------------
/// \file
/// TestServer - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "TestServer.hpp"

// library includes
#include <boprpc/Protocol.hpp>
#include <boprpc/Request.hpp>
#include <boprpc/Command.hpp>
#include <boprpc/Response.hpp>
#include <boprpc/Result.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::Protocol;
using OCTsync::BopRpc::Request;
using OCTsync::BopRpc::Command;
using OCTsync::BopRpc::Response;
using OCTsync::BopRpc::Result;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppLdp
{

TestServer::TestServer(
        QObject* parent) :
                Bop::Server(parent)
{
    // nothing to do
}

std::unique_ptr<Bop::ObjectBase> TestServer::processRequest(
        const Bop::ObjectBase& requestMessage)
{
    qDebug() << "TestServer::processRequest()";

    if (requestMessage.getId() == Protocol::BOX_ID)
    {
        const Protocol& protocol = static_cast<const Protocol&>(requestMessage);

        if (protocol.isRequest())
        {
            const Request* request = protocol.getRequest();
            const Command* command = protocol.getCommand();

            std::unique_ptr<Response> response(new Response());
            response->setContext(request->getContext());
            response->setErrorCode(0);

            std::unique_ptr<Result> result(new Result());

            std::unique_ptr<Protocol> responseMessage(new Protocol());
            responseMessage->setResponse(std::move(response), std::move(result));

            return std::move(responseMessage);
        }
    }

    return nullptr;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
