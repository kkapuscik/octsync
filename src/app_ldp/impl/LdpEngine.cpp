//-----------------------------------------------------------------------------
/// \file
/// LdpEngine - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "LdpEngine.hpp"

// library includes
#include <QtCore/QDebug>
#include <boprpc/Protocol.hpp>
#include <boprpc/Request.hpp>
#include <boprpc/Command.hpp>
#include <boprpc/RpcObjectFactory.hpp>

// system includes
#include <memory>

// using clauses
using std::unique_ptr;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppLdp
{

LdpEngine::LdpEngine(
        QObject* parent) :
                QObject(parent)
{
    m_testClient = Ldp::Client(Commons::Uuid::generate());

    m_advertiser = new Ldp::Advertiser(this);

    m_monitor = new Ldp::Monitor(m_testClient, this);

    m_factoryManager = new Bop::ObjectFactoryManager(this);
    m_factoryManager->addObjectFactory(new BopRpc::RpcObjectFactory());

    m_bopServer = new TestServer(this);
    m_bopServer->setObjectFactoryManager(m_factoryManager);

    m_bopClient = new Bop::Client(m_factoryManager, this);

    connect(m_bopClient, &Bop::Client::objectReceived, this, &LdpEngine::onObjectReceived);

    connect(m_monitor, &Ldp::Monitor::deviceAdded, this, &LdpEngine::onDeviceAdded);

    connect(m_monitor, &Ldp::Monitor::deviceRemoved, this, &LdpEngine::onDeviceRemoved);
    // TODO
}

LdpEngine::~LdpEngine()
{
    // nothing to do
}

void LdpEngine::setMonitorStatus(
        const bool enable)
{
    if (enable)
    {
        m_monitor->enable();
    }
    else
    {
        m_monitor->disable();
    }
    // TODO
}

void LdpEngine::setAdvertiserStatus(
        const bool enable)
{
    if (enable)
    {
        m_advertiser->enable();
    }
    else
    {
        m_advertiser->disable();
    }
    // TODO
}

void LdpEngine::setDeviceStatus(
        const bool enable)
{
    // TODO
}

void LdpEngine::onDeviceAdded(
        Ldp::Device device)
{
    qDebug() << "Device added: " << device.getUuid().toString();

    static quint32 nextContext = 1;
    quint32 context = nextContext++;

    std::unique_ptr<BopRpc::Request> requestBox(new BopRpc::Request());
    std::unique_ptr<BopRpc::Command> commandBox(new BopRpc::Command());

    commandBox->setName("GetDeviceInfo");

    unique_ptr<BopRpc::Protocol> protocolBox(new BopRpc::Protocol());
    protocolBox->setRequest(std::move(requestBox), std::move(commandBox));

    m_bopClient->sendObject(device.getUrl(), true,
            Bop::ConstObjectSharedPtr(std::move(protocolBox)));
}

void LdpEngine::onDeviceRemoved(
        Ldp::Device device)
{
    qDebug() << "Device removed: " << device.getUuid().toString();
}

void LdpEngine::onObjectReceived(
        QUrl url,
        Bop::ConstObjectSharedPtr object)
{
    qDebug() << "LdpEngine::onObjectReceived() " << url << ' ' << object.get();
}

void LdpEngine::setServerStatus(
        const bool enable)
{
    if (enable)
    {
        m_bopServer->enable();

        QUrl bopServerUrl = m_bopServer->getUrl();
        if (bopServerUrl.isValid())
        {
            m_testDevice = Ldp::Device(Commons::Uuid::generate(), bopServerUrl);

            m_advertiser->addDevice(m_testDevice);
        }
    }
    else
    {
        m_advertiser->removeDevice(m_testDevice);
        m_bopServer->disable();
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
