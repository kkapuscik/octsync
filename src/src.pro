TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    commons \
    ldp \
    bop \
    boprpc \
    syncserver \
    app_ldp \
    ui \
    app_syncclient \
    app_syncserver \
    app_synctest
