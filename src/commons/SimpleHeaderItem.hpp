//-----------------------------------------------------------------------------
/// \file
/// SimpleHeaderItem - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SimpleHeaderItem
#define INCLUDED_OCTsync_SimpleHeaderItem

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QMap>
#include <QtCore/QSettings>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SimpleHeaderItem
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QMap<int, QVariant> DataMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    DataMap m_data;

// Methods (public, protected, private)
public:
    SimpleHeaderItem();

    explicit SimpleHeaderItem(
            const QString& title);

    void setData(
            const QVariant& value,
            const int role = Qt::DisplayRole);

    QVariant data(
            const int role = Qt::DisplayRole) const;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SimpleHeaderItem*/
