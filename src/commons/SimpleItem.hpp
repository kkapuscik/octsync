//-----------------------------------------------------------------------------
/// \file
/// SimpleItem - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SimpleItem
#define INCLUDED_OCTsync_SimpleItem

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QMap>
#include <QtCore/QSettings>
#include <QtCore/QVector>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SimpleItem
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QVector<QVariant> DisplayDataArray;
    typedef QMap<int, QVariant> CustomDataMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    DisplayDataArray m_displayData;
    CustomDataMap m_customData;

// Methods (public, protected, private)
public:
    SimpleItem();

    QVariant data(
            const int column,
            const int role = Qt::DisplayRole) const;

    QVariant getDisplayData(
            const int column) const;

    void setDisplayData(
            const int column,
            const QVariant& value);

    QVariant getData(
            const int role) const;

    void setData(
            const QVariant& value,
            const int role);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SimpleItem*/
