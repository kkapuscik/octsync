//-----------------------------------------------------------------------------
/// \file
/// Class utility macros.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_COMMONS_CLASS_HPP
#define OCTSYNC_COMMONS_CLASS_HPP

//-----------------------------------------------------------------------------

/// Class copying is forbidden.
#if 0
#define OCT_CLASS_NO_COPY(CLASSNAME) \
    CLASSNAME(const CLASSNAME&) = delete; \
    CLASSNAME& operator=(const CLASSNAME&) = delete;
#else
#define OCT_CLASS_NO_COPY(CLASSNAME) \
    private: \
        CLASSNAME(const CLASSNAME&); \
        CLASSNAME& operator=(const CLASSNAME&);
#endif

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_COMMONS_CLASS_HPP*/
