//-----------------------------------------------------------------------------
/// \file
/// OctException class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_COMMONS_OCT_EXCEPTION_HPP
#define OCTSYNC_COMMONS_OCT_EXCEPTION_HPP

//-----------------------------------------------------------------------------

#include <QtCore/QString>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

/// Base class for all OCTaedr modules exceptions.
class OctException
{
private:
    /// Exception message.
    const QString m_message;

public:
    /// Destroys the exception.
    virtual ~OctException()
    {
        // nothing to do
    }

    /// Returns exception message.
    ///
    /// \return
    /// Message given to the exception constructor.
    const QString& getMessage() const
    {
        return m_message;
    }

protected:
    /// Constructs the exception
    ///
    /// \param message
    ///     Exception message.
    OctException(
            QString message) :
                    m_message(message)
    {
        // nothing to do
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_COMMONS_OCT_EXCEPTION_HPP*/
