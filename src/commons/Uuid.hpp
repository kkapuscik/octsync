//-----------------------------------------------------------------------------
/// \file
/// Uuid - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Uuid
#define INCLUDED_OCTsync_Uuid

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QUuid>
#include <QtCore/QDebug>

// system includes
#include <cstdint>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Uuid
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
public:
    static Uuid parse(
            const QString& canonical);

    static Uuid generate();

private:
    static uint8_t parseDigit(
            char c);

    static char toDigit(
            uint8_t value);

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QUuid m_uuid;

// Methods (public, protected, private)
public:
    Uuid();

    Uuid(
            const QUuid& uuid);

    bool isValid() const;

    QString toString() const;

    QUuid toQUuid() const
    {
        return m_uuid;
    }

private:
    friend bool operator <(
            const Uuid& uuid1,
            const Uuid& uuid2);

    friend bool operator ==(
            const Uuid& uuid1,
            const Uuid& uuid2);

    friend bool operator !=(
            const Uuid& uuid1,
            const Uuid& uuid2);

    friend uint qHash(
            const Uuid& uuid,
            uint seed = 0)
    {
        return qHash(uuid.m_uuid, seed);
    }

};

QDebug& operator <<(
        QDebug& dbgstr,
        const Uuid& uuid);

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Uuid*/
