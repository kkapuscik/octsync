//-----------------------------------------------------------------------------
/// \file
/// LogEntry class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_COMMONS_LOG_HPP
#define OCTSYNC_COMMONS_LOG_HPP

//-----------------------------------------------------------------------------

#include "LogEntry.hpp"

#include <QtCore/QMutex>
#include <QtCore/QList>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

class LogListener
{
public:
    virtual ~LogListener()
    {
        // nothing to do
    }

    virtual void entryAdded(
            LogEntry entry) = 0;

protected:
    LogListener()
    {
        // nothing to do
    }
};

class Log
{
private:
    QMutex m_mutex;
    QList<LogListener*> m_listeners;

public:
    static Log& getInstance()
    {
        static Log theInstance;

        return theInstance;
    }

    static void error(
            QString component,
            QString message)
    {
        getInstance().addMessage(LogEntry(LogLevel::ERROR, component, message));
    }

    static void warning(
            QString component,
            QString message)
    {
        getInstance().addMessage(LogEntry(LogLevel::WARNING, component, message));
    }

    static void info(
            QString component,
            QString message)
    {
        getInstance().addMessage(LogEntry(LogLevel::INFO, component, message));
    }

    static void debug(
            QString component,
            QString message)
    {
        getInstance().addMessage(LogEntry(LogLevel::DEBUG, component, message));
    }

    void addMessage(
            const LogEntry& entry);

    void addListener(
            LogListener* const listener);

private:
    Log();

};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_COMMONS_LOG_HPP*/
