//-----------------------------------------------------------------------------
/// \file
/// OutOfBoundsException - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_OutOfBoundsException
#define INCLUDED_OCTsync_OutOfBoundsException

//-----------------------------------------------------------------------------

// local includes
#include "OctException.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class OutOfBoundsException : public OctException
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    OutOfBoundsException(
            const QString& message,
            const int index) :
                    Commons::OctException(createMessage(message, index))
    {
        // nothing to do
    }

    OutOfBoundsException(
            const QString& message,
            const int index,
            const int size) :
                    Commons::OctException(createMessage(message, index, size))
    {
        // nothing to do
    }

private:
    static QString createMessage(
            const QString& message,
            const int index)
    {
        return QString("%1. Index: %2").arg(message).arg(index);
    }

    static QString createMessage(
            const QString& message,
            const int index,
            const int size)
    {
        return QString("%1. Index: %2, size: %3").arg(message).arg(index).arg(size);
    }
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_OutOfBoundsException*/
