//-----------------------------------------------------------------------------
/// \file
/// Uuid - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../Uuid.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

Uuid Uuid::parse(
        const QString& canonical)
{
    // add braces
    const QString stdForm = QString("{%1}").arg(canonical);
    // parse
    return Uuid(QUuid(stdForm.toUtf8()));
}

Uuid Uuid::generate()
{
    return Uuid(QUuid::createUuid());
}

uint8_t Uuid::parseDigit(
        char c)
{
    if (c >= '0' && c <= '9')
    {
        return c - '0';
    }
    else if (c >= 'a' && c <= 'f')
    {
        return c - 'a' + 10;
    }
    else if (c >= 'A' && c <= 'F')
    {
        return c - 'A' + 10;
    }
    else
    {
        return 0xFF;
    }
}

char Uuid::toDigit(
        uint8_t value)
{
    if (/*value >= 0 &&*/ value <= 9)
    {
        return '0' + value;
    }
    else if (value >= 10 && value < 16)
    {
        return 'a' + (value - 10);
    }
    return '?';
}


Uuid::Uuid()
{
    // nothing to do
}

Uuid::Uuid(
        const QUuid& uuid) :
                m_uuid(uuid)
{
    // nothing to do
}

bool Uuid::isValid() const
{
    return !m_uuid.isNull();
}

QString Uuid::toString() const
{
    const QString uuidStr = m_uuid.toString();

    // remove the braces
    return uuidStr.mid(1, uuidStr.length() - 2);
}

bool operator <(
        const Uuid& uuid1,
        const Uuid& uuid2)
{
    return uuid1.m_uuid < uuid2.m_uuid;
}

bool operator ==(
        const Uuid& uuid1,
        const Uuid& uuid2)
{
    return uuid1.m_uuid == uuid2.m_uuid;
}

bool operator !=(
        const Uuid& uuid1,
        const Uuid& uuid2)
{
    return uuid1.m_uuid != uuid2.m_uuid;
}

QDebug& operator <<(
        QDebug& dbgstr,
        const Uuid& uuid)
{
    dbgstr << uuid.toQUuid();
    return dbgstr;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
