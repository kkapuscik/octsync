//-----------------------------------------------------------------------------
/// \file
/// SimpleItem - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SimpleItem.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

SimpleItem::SimpleItem()
{
    // nothing to do
}

QVariant SimpleItem::data(
        const int column,
        const int role) const
{
    QVariant value;

    if ((role == Qt::DisplayRole) || (role == Qt::ToolTipRole))
    {
        if ((column >= 0) && (column < m_displayData.size()))
        {
            value = m_displayData[column];
        }
    }
    else
    {
        value = m_customData.value(role);
    }

    return value;
}

QVariant SimpleItem::getDisplayData(
        const int column) const
{
    return m_displayData.value(column);
}

void SimpleItem::setDisplayData(
        const int column,
        const QVariant& value)
{
    if (column >= m_displayData.size())
    {
        m_displayData.resize(column + 1);
        Q_ASSERT(column < m_displayData.size());
    }

    m_displayData[column] = value;
}

QVariant SimpleItem::getData(
        const int role) const
{
    return m_customData.value(role);
}

void SimpleItem::setData(
        const QVariant& value,
        const int role)
{
    m_customData.insert(role, value);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
