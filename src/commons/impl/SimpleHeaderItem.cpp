//-----------------------------------------------------------------------------
/// \file
/// SimpleHeaderItem - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SimpleHeaderItem.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

SimpleHeaderItem::SimpleHeaderItem()
{
    // nothing to do
}

SimpleHeaderItem::SimpleHeaderItem(
        const QString& title)
{
    setData(title, Qt::DisplayRole);
}

void SimpleHeaderItem::setData(
        const QVariant& value,
        const int role)
{
    m_data.insert(role, value);
}

QVariant SimpleHeaderItem::data(
        const int role) const
{
    return m_data.value(role);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
