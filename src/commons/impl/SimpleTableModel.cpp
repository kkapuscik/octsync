//-----------------------------------------------------------------------------
/// \file
/// SimpleTableModel - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SimpleTableModel.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

SimpleTableModel::SimpleTableModel(
        QObject* parent) :
                QAbstractTableModel(parent)
{
    // nothing to do
}

void SimpleTableModel::appendHeader(
        const QString& title)
{

    appendHeader(HeaderItemPtr(new SimpleHeaderItem(title)));
}

void SimpleTableModel::appendHeader(
        HeaderItemPtr headerItem)
{
    insertHeader(columnCount(), headerItem);
}

void SimpleTableModel::insertHeader(
        const int column,
        HeaderItemPtr headerItem)
{
    Q_ASSERT(headerItem);

    int fixedColumn = column;

    if (column < 0)
    {
        fixedColumn = 0;
    }
    else if (column > columnCount())
    {
        fixedColumn = columnCount();
    }

    m_headerItems.insert(m_headerItems.begin() + fixedColumn, headerItem);

    emit headerDataChanged(Qt::Horizontal, fixedColumn, fixedColumn);
}

void SimpleTableModel::appendItem(
        DataItemPtr item)
{
    insertItem(rowCount(), item);
}

void SimpleTableModel::insertItem(
        const int row,
        DataItemPtr item)
{
    Q_ASSERT(item);

    int fixedRow = row;

    if (row < 0)
    {
        fixedRow = 0;
    }
    else if (row > rowCount())
    {
        fixedRow = rowCount();
    }

    beginInsertRows(QModelIndex(), fixedRow, fixedRow);
    m_dataItems.insert(m_dataItems.begin() + fixedRow, item);
    endInsertRows();
}

SimpleTableModel::DataItemPtr SimpleTableModel::removeItem(
        const int row)
{
    DataItemPtr item;

    if ((row >= 0) && (row < rowCount()))
    {
        auto itemIterator = m_dataItems.begin() + row;
        item = *itemIterator;

        beginRemoveRows(QModelIndex(), row, row);
        m_dataItems.erase(itemIterator);
        endRemoveRows();
    }

    return item;
}

SimpleTableModel::DataItemPtr SimpleTableModel::getItem(
        const int row) const
{
    DataItemPtr item;

    if ((row >= 0) && (row < rowCount()))
    {
        auto itemIterator = m_dataItems.begin() + row;
        item = *itemIterator;
    }

    return item;
}

QVariant SimpleTableModel::data(
        const QModelIndex& index,
        int role) const
{
    QVariant value;

    if (index.isValid())
    {
        // extract row & column
        const int row = index.row();

        // check if we have the item
        if (row < rowCount())
        {
            const SimpleItem* item = m_dataItems.at(row).get();

            value = item->data(index.column(), role);
        }
    }

    return value;
}

QVariant SimpleTableModel::headerData(
        int section,
        Qt::Orientation orientation,
        int role) const
{
    QVariant value;

    if (orientation == Qt::Horizontal)
    {
        if ((section >= 0) && (section < columnCount()))
        {
            const SimpleHeaderItem* headerItem = m_headerItems.at(section).get();

            value = headerItem->data(role);
        }
    }

    return value;
}

int SimpleTableModel::rowCount(
        const QModelIndex& /*parent*/) const
{
    return (int) m_dataItems.size();
}

int SimpleTableModel::columnCount(
        const QModelIndex& /*parent*/) const
{
    return (int) m_headerItems.size();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
