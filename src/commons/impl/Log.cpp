//-----------------------------------------------------------------------------
/// \file
/// Log - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2014 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Log.hpp"

// library includes
// (none)

// system includes
#include <QMutexLocker>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

Log::Log()
{
    // nothing to do
}

void Log::addMessage(
        const LogEntry& entry)
{
    QMutexLocker locker(&m_mutex);

    for (auto listenerIter = m_listeners.begin(), endIter = m_listeners.end();
            listenerIter != endIter; ++listenerIter)
    {
        (*listenerIter)->entryAdded(entry);
    }
}

void Log::addListener(
        LogListener* const listener)
{
    Q_ASSERT(listener != nullptr);

    QMutexLocker locker(&m_mutex);

    m_listeners.append(listener);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
