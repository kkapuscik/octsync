//-----------------------------------------------------------------------------
/// \file
/// SimpleTableModel - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SimpleTableModel
#define INCLUDED_OCTsync_SimpleTableModel

//-----------------------------------------------------------------------------

// local includes
#include "SimpleItem.hpp"
#include "SimpleHeaderItem.hpp"

// library includes
#include <QtCore/QAbstractItemModel>

// system includes
#include <memory>
#include <deque>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SimpleTableModel : public QAbstractTableModel
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    typedef std::shared_ptr<SimpleItem> DataItemPtr;
    typedef std::shared_ptr<SimpleHeaderItem> HeaderItemPtr;

private:
    typedef std::deque<DataItemPtr> DataItemArray;
    typedef std::deque<HeaderItemPtr> HeaderItemArray;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    HeaderItemArray m_headerItems;
    DataItemArray m_dataItems;

// Methods (public, protected, private)
public:
    explicit SimpleTableModel(
            QObject* parent = nullptr);

    void appendHeader(
            const QString& title);

    void appendHeader(
            HeaderItemPtr headerItem);

    void appendItem(
            DataItemPtr item);

    void insertItem(
            const int row,
            DataItemPtr item);

    DataItemPtr removeItem(
            const int row);

    DataItemPtr getItem(
            const int row) const;

    virtual QVariant data(
            const QModelIndex& index,
            int role = Qt::DisplayRole) const;

    virtual QVariant headerData(
            int section,
            Qt::Orientation orientation,
            int role = Qt::DisplayRole) const;

    virtual int rowCount(
            const QModelIndex& parent = QModelIndex()) const;

    virtual int columnCount(
            const QModelIndex& parent = QModelIndex()) const;

private:
    void insertHeader(
            const int column,
            HeaderItemPtr headerItem);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SimpleTableModel*/
