//-----------------------------------------------------------------------------
/// \file
/// FatalException class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_COMMONS_FATAL_EXCEPTION_HPP
#define OCTSYNC_COMMONS_FATAL_EXCEPTION_HPP

//-----------------------------------------------------------------------------

#include "OctException.hpp"

#include <string>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

/// Exception thrown in case of fatal (unrecoverable) errors.
class FatalException : public OctException
{
public:
    /// Constructs the exception
    ///
    /// \param message
    ///     Exception message.
    FatalException(
            QString message) :
                    OctException(message)
    {
        // nothing to do
    }

    /// Destroys the exception.
    virtual ~FatalException()
    {
        // nothing to do
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_COMMONS_FATAL_EXCEPTION_HPP*/
