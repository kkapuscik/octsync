//-----------------------------------------------------------------------------
/// \file
/// LogEntry class declaration.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2012 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef OCTSYNC_COMMONS_LOG_ENTRY_HPP
#define OCTSYNC_COMMONS_LOG_ENTRY_HPP

//-----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QtCore/QDateTime>

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

enum class LogLevel
{
    ERROR,
    WARNING,
    INFO,
    DEBUG
};

class LogEntry
{
private:
    const LogLevel m_level;
    const QString m_component;
    const QString m_message;
    const QDateTime m_timestamp;

public:
    LogEntry(
            LogLevel level,
            QString component,
            QString message) :
                    m_level(level),
                    m_component(component),
                    m_message(message),
                    m_timestamp(QDateTime::currentDateTime())
    {
        // nothing to do
    }

    LogLevel getLevel() const
    {
        return m_level;
    }

    const QString& getComponent() const
    {
        return m_component;
    }

    const QString& getMessage() const
    {
        return m_message;
    }

    const QDateTime& getTimestamp() const
    {
        return m_timestamp;
    }
};

}
}

//-----------------------------------------------------------------------------

#endif /*OCTSYNC_COMMONS_LOG_ENTRY_HPP*/
