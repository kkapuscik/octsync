//-----------------------------------------------------------------------------
/// \file
/// SimpleTreeModel - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SimpleTreeModel
#define INCLUDED_OCTsync_SimpleTreeModel

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SimpleTreeModel
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
// (none)

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SimpleTreeModel*/
