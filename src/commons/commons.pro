#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T18:45:14
#
#-------------------------------------------------

QT       -= gui

TARGET = commons
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    impl/Log.cpp \
    impl/SimpleHeaderItem.cpp \
    impl/SimpleItem.cpp \
    impl/SimpleTableModel.cpp \
    impl/SimpleTreeModel.cpp \
    impl/Uuid.cpp

HEADERS += \
    class.hpp \
    FatalException.hpp \
    Log.hpp \
    LogEntry.hpp \
    Map.hpp \
    namespace.hpp \
    OctException.hpp \
    OutOfBoundsException.hpp \
    SimpleHeaderItem.hpp \
    SimpleItem.hpp \
    SimpleTableModel.hpp \
    SimpleTreeModel.hpp \
    Uuid.hpp
unix {
    target.path = /usr/lib
    INSTALLS += target
}

OTHER_FILES += \
    CMakeLists.txt

INCLUDEPATH += $$PWD/..
