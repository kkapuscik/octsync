//-----------------------------------------------------------------------------
/// \file
/// Map - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Map
#define INCLUDED_OCTsync_Map

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
// (none)

// system includes
// (none)

// using clauses
#include <map>

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Commons
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
template<typename KeyType, typename ValueType, typename CompareFn = std::less<KeyType> >
class Map : public std::map<KeyType, ValueType, CompareFn>
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef std::map<KeyType, ValueType, CompareFn> BaseType;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    Map()
    {
        // nothing to do
    }

    typename BaseType::const_iterator cfind(
            const KeyType& key) const
    {
        return BaseType::find(key);
    }

};

}
// end of namespace
}// end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Map*/
