#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T21:57:44
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# ANDROID_PACKAGE_SOURCE_DIR = $$PWD/src/app_syncserver/android

TARGET = app_syncserver
TEMPLATE = app
CONFIG += mobility c++11

SOURCES += \
    impl/LogView.cpp \
    impl/ShareEditDialog.cpp \
    impl/SharesTable.cpp \
    impl/SharesTableModel.cpp \
    impl/SharesView.cpp \
    impl/SyncServerApplication.cpp \
    impl/app_syncserver_main.cpp \
    impl/SyncServerEngine.cpp \
    impl/SyncServerWindow.cpp \
    impl/SecuritySettingsDialog.cpp

HEADERS  += \
    namespace.hpp \
    impl/SyncServerEngine.hpp \
    impl/LogView.hpp \
    impl/ShareEditDialog.hpp \
    impl/SharesTable.hpp \
    impl/SharesTableModel.hpp \
    impl/SharesView.hpp \
    impl/SyncServerApplication.hpp \
    impl/SyncServerWindow.hpp \
    impl/SecuritySettingsDialog.hpp

RESOURCES += \
    ../../data/octsync.qrc

MOBILITY =

OTHER_FILES += \
    CMakeLists.txt \
    android/AndroidManifest.xml

INCLUDEPATH += $$PWD/../

win32-g++: {
    message("Building for WIN G++")
} else:unix: {
    message("Building for UNIX")
} else {
    error("Unsupported platform configuration")
}

# add library COMMONS
DEPENDPATH += $$PWD/../commons
LIBS += -L$$OUT_PWD/../commons/ -lcommons
PRE_TARGETDEPS += $$OUT_PWD/../commons/libcommons.a

# add library LDP
DEPENDPATH += $$PWD/../ldp
LIBS += -L$$OUT_PWD/../ldp/ -lldp
PRE_TARGETDEPS += $$OUT_PWD/../ldp/libldp.a

# add library BOPRPC
DEPENDPATH += $$PWD/../boprpc
LIBS += -L$$OUT_PWD/../boprpc/ -lboprpc
PRE_TARGETDEPS += $$OUT_PWD/../boprpc/libboprpc.a

# add library BOP
DEPENDPATH += $$PWD/../bop
LIBS += -L$$OUT_PWD/../bop/ -lbop
PRE_TARGETDEPS += $$OUT_PWD/../bop/libbop.a

# Add library syncserver
DEPENDPATH += $$PWD/../syncserver
LIBS += -L$$OUT_PWD/../syncserver/ -lsyncserver
PRE_TARGETDEPS += $$OUT_PWD/../syncserver/libsyncserver.a

# add library UI
DEPENDPATH += $$PWD/../ui
LIBS += -L$$OUT_PWD/../ui/ -lui
PRE_TARGETDEPS += $$OUT_PWD/../ui/libui.a
