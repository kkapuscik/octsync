//-----------------------------------------------------------------------------
/// \file
/// SyncServerWindow - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncServerWindow
#define INCLUDED_OCTsync_SyncServerWindow

//-----------------------------------------------------------------------------

// local includes
#include "SyncServerEngine.hpp"
#include "LogView.hpp"
#include "SharesView.hpp"

// library includes
#include <QtWidgets/QMainWindow>
#include <ui/LogMessage.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::LogMessage;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncServerWindow : public QMainWindow
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncServerEngine* m_engine;

    QAction* m_actionAppExit;
    QAction* m_actionAppAbout;
    QAction* m_actionAppAboutQt;
    QAction* m_actionSecuritySettings;
    QAction* m_actionServerEnabled;
    QAction* m_actionSecureModeEnabled;

    SharesView* m_sharesView;
    LogView* m_logView;

// Methods (public, protected, private)
public:
    SyncServerWindow(
            SyncServerEngine* engine,
            QWidget* parent = nullptr);

    void addLogMessage(
            const LogMessage& message);

private:
    void createActions();

    void createMainMenu();

    void createWidgets();

    void connectActions();

    void onSecuritySettings();

    void onAbout();

    void onAboutQt();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncServerWindow*/
