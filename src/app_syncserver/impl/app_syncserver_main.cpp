#include "SyncServerApplication.hpp"

using OCTsync::AppSyncServer::SyncServerApplication;

int main(
        int argc,
        char** argv)
{
    return SyncServerApplication(argc, argv).exec();
}
