//-----------------------------------------------------------------------------
/// \file
/// SyncServerApplication - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncServerApplication
#define INCLUDED_OCTsync_SyncServerApplication

//-----------------------------------------------------------------------------

// local includes
#include "SyncServerWindow.hpp"
#include "SyncServerEngine.hpp"

// library includes
#include <QtWidgets/QApplication>
#include <QtWidgets/QSystemTrayIcon>
#include <QtWidgets/QMenu>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncServerApplication : public QApplication
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    std::unique_ptr<SyncServerWindow> m_mainWindow;
    QSystemTrayIcon* m_trayIcon;
    SyncServerEngine* m_engine;
    std::unique_ptr<QMenu> m_trayContextMenu;

// Methods (public, protected, private)
public:
    SyncServerApplication(
            int& argc,
            char** argv);

    int exec();

private:
    void onTrayIconActivated(
            QSystemTrayIcon::ActivationReason reason);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncServerApplication*/
