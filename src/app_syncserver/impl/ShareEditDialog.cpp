//-----------------------------------------------------------------------------
/// \file
/// ShareEditDialog - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ShareEditDialog.hpp"

// library includes
#include <QtWidgets/QLayout>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>

// system includes
// (none)

// using clauses
using OCTsync::Ui::ActionButton;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

ShareEditDialog::ShareEditDialog(
        QWidget* parent,
        Qt::WindowFlags flags) :
                QDialog(parent, flags)
{
    setSizeGripEnabled(true);
    resize(600, 300);

    createActions();
    createWidgets();
    connectActions();
}

void ShareEditDialog::setData(
        const SyncServer::Share& share)
{
    m_uuidEdit->setText(share.getUuid().toString());
    m_nameEdit->setText(share.getName());
    m_descriptionEdit->setText(share.getDescription());
    m_rootDirEdit->setText(share.getRootDir().path());
    m_readOnlyCheckbox->setCheckState(share.isReadOnly() ? Qt::Checked : Qt::Unchecked);
}

SyncServer::Share ShareEditDialog::getData() const
{
    SyncServer::Share share;

    share.setUuid(Commons::Uuid::parse(m_uuidEdit->text()));
    share.setName(m_nameEdit->text());
    share.setDescription(m_descriptionEdit->text());
    share.setRootDir(QDir(m_rootDirEdit->text()));
    share.setReadOnly(m_readOnlyCheckbox->checkState() == Qt::Checked);

    return share;
}

void ShareEditDialog::createActions()
{
    m_actionDialogOk = new QAction(this);
    m_actionDialogOk->setText(tr("OK"));

    m_actionDialogCancel = new QAction(this);
    m_actionDialogCancel->setText(tr("Cancel"));

    m_actionRootDirBrowse = new QAction(this);
    m_actionRootDirBrowse->setText("Browse...");
}

void ShareEditDialog::createWidgets()
{
    m_uuidEdit = new QLineEdit(this);
    m_nameEdit = new QLineEdit(this);
    m_descriptionEdit = new QLineEdit(this);
    m_rootDirEdit = new QLineEdit(this);
    m_readOnlyCheckbox = new QCheckBox(this);
    m_readOnlyCheckbox->setTristate(false);

    m_uuidEdit->setReadOnly(true);

    QHBoxLayout* const rootDirLayout = new QHBoxLayout();
    rootDirLayout->addWidget(m_rootDirEdit, 1);
    rootDirLayout->addWidget(new ActionButton(m_actionRootDirBrowse, this), 0);

    QFormLayout* const formLayout = new QFormLayout();
    formLayout->addRow(tr("Id."), m_uuidEdit);
    formLayout->addRow(tr("Name"), m_nameEdit);
    formLayout->addRow(tr("Description"), m_descriptionEdit);
    formLayout->addRow(tr("Directory"), rootDirLayout);
    formLayout->addRow(tr("ReadOnly"), m_readOnlyCheckbox);

    ActionButton* const dialogOkButton = new ActionButton(m_actionDialogOk, this);
    ActionButton* const dialogCancelButton = new ActionButton(m_actionDialogCancel, this);

    QHBoxLayout* const dialogButtonsLayout = new QHBoxLayout();
    dialogButtonsLayout->addSpacing(10);
    dialogButtonsLayout->addWidget(dialogOkButton);
    dialogButtonsLayout->addStretch(1);
    dialogButtonsLayout->addWidget(dialogCancelButton);
    dialogButtonsLayout->addSpacing(10);

    QVBoxLayout* const mainLayout = new QVBoxLayout(this);
    mainLayout->setSpacing(15);
    mainLayout->addLayout(formLayout);
    mainLayout->addLayout(dialogButtonsLayout);

    dialogOkButton->setDefault(true);
}

void ShareEditDialog::connectActions()
{
    connect(m_actionRootDirBrowse, &QAction::triggered, this, &ShareEditDialog::onRootDirBrowse);
    connect(m_actionDialogOk, &QAction::triggered, this, &ShareEditDialog::onDialogOk);
    connect(m_actionDialogCancel, &QAction::triggered, this, &QDialog::reject);
}

void ShareEditDialog::onRootDirBrowse()
{
    QFileDialog dialog;

    const QString newPath = QFileDialog::getExistingDirectory(this, tr("Select directory"),
            m_rootDirEdit->text(), QFileDialog::ShowDirsOnly);
    if (!newPath.isEmpty())
    {
        m_rootDirEdit->setText(newPath);
    }
}

void ShareEditDialog::onDialogOk()
{
    const SyncServer::Share share = getData();
    if (share.isValid())
    {
        accept();
    }
    else
    {
        QMessageBox::information(this, tr("Invalid data"), tr("Please enter valid share data."));
    }
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
