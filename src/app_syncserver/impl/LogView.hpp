//-----------------------------------------------------------------------------
/// \file
/// LogView - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_LogView
#define INCLUDED_OCTsync_LogView

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QFrame>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QAction>
#include <ui/LogMessage.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::LogMessage;

// forward references
class QCheckBox;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class LogView : public QFrame
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    bool m_debugEnabled;

    QTextEdit* m_logTextEdit;
    QAction* m_actionLogClear;
    QAction* m_actionLogSaveAs;
    QCheckBox* m_checkboxEnableDebug; // TODO: create ActionCheckbox

// Methods (public, protected, private)
public:
    explicit LogView(
            QWidget* parent = nullptr);

    void addMessage(
            const LogMessage& message);

private:
    void createActions();
    void createWidgets();
    void connectActions();

    void onLogEnableDebug(
            int state);
    void onLogClear();
    void onLogSaveAs();
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_LogView*/
