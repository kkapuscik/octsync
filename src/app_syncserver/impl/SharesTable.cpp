//-----------------------------------------------------------------------------
/// \file
/// SharesTable - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SharesTable.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

SharesTable::SharesTable(
        SyncServerEngine* engine,
        QWidget* parent) :
                QTableView(parent)
{
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);

    setModel(engine->getSharesTableModel());
}

Commons::Uuid SharesTable::getShareUuid(
            const QModelIndex& index) const
{
    Commons::Uuid shareUuid;

    Q_ASSERT(model());

    QVariant shareId = model()->data(model()->index(index.row(), 0), SyncServerEngine::ROLE_SHARE_UUID);
    if (shareId.type() == QVariant::Uuid)
    {
        shareUuid = shareId.toUuid();
    }

    return shareUuid;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
