//-----------------------------------------------------------------------------
/// \file
/// SecuritySettingsDialog - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SecuritySettingsDialog
#define INCLUDED_OCTsync_SecuritySettingsDialog

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QDialog>

// system includes
// (none)

// using clauses
// (none)

// forward references
class QLineEdit;
class QAction;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SecuritySettingsDialog : public QDialog
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QLineEdit* m_keyFileEdit;
    QLineEdit* m_certFileEdit;
    QLineEdit* m_password1Edit;
    QLineEdit* m_password2Edit;

    QAction* m_actionDialogOk;
    QAction* m_actionDialogCancel;
    QAction* m_actionKeyBrowse;
    QAction* m_actionCertBrowse;

// Methods (public, protected, private)
public:
    explicit SecuritySettingsDialog(
            QWidget* parent = nullptr,
            Qt::WindowFlags f = 0);

    QString getKeyFilePath() const;

    void setKeyFilePath(
            const QString& path);

    QString getCertFilePath() const;

    void setCertFilePath(
            const QString& path);

    QString getPassword() const;

    void setPassword(
            const QString& password);

private:
    void createActions();
    void createWidgets();
    void connectActions();

    void onKeyBrowse();
    void onCertBrowse();
    void onDialogOk();
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SecuritySettingsDialog*/
