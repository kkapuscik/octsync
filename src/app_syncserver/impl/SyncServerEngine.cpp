//-----------------------------------------------------------------------------
/// \file
/// SyncServerEngine - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncServerEngine.hpp"
#include "SharesTableModel.hpp"

// library includes
#include <QtWidgets/QMessageBox>
#include <syncserver/ServerData.hpp>
#include <syncserver/SyncService.hpp>

// system includes
// (none)

// using clauses
using OCTsync::SyncServer::ServerData;
using OCTsync::SyncServer::SyncService;
using OCTsync::BopRpc::Service;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

const QString SyncServerEngine::OPTION_NAME_SERVER_CFGFILE("-servercfg=");
const QString SyncServerEngine::ORGANIZATION_NAME("OCTaedr");
const QString SyncServerEngine::APPLICATION_NAME("SyncServer");

SyncServerEngine::SyncServerEngine(
        QObject* parent) :
                QObject(parent),
                m_serverEnabled(false),
                m_serverSecure(false),
                m_syncService(nullptr),
                m_rpcServer(nullptr)
{
    m_ldpAdvertiser = new Ldp::Advertiser(this);

    m_serverData = new ServerData(this);
    connect(m_serverData, &ServerData::changed, this, &SyncServerEngine::onServerDataChanged);

    m_sharesTableModel = new SharesTableModel(this);
    m_sharesTableModel->setServerData(QPointer<ServerData>(m_serverData));

    m_syncService = new SyncService(this);
    m_syncService->setServerData(QPointer<ServerData>(m_serverData));

    m_rpcServer = new BopRpc::Device(this);
    m_rpcServer->addService(1, QPointer<Service>(m_syncService));

    loadSettings();
}

SyncServerEngine::~SyncServerEngine()
{
    // nothing to do
}

void SyncServerEngine::loadSettings()
{
    std::unique_ptr<QSettings> settings = createSettingsObject();

    if (settings)
    {
        if (!m_serverData->load(*settings))
        {
            QMessageBox::warning(nullptr, tr("Load Settings"), tr("Cannot load application settings."));
        }
    }
    else
    {
        QMessageBox::warning(nullptr, tr("Load Settings"), tr("Cannot create settings object."));
    }
}

void SyncServerEngine::processArguments(
        QStringList arguments)
{
    QString configFileName;

    for (QString arg : arguments)
    {
        if (arg.startsWith(OPTION_NAME_SERVER_CFGFILE))
        {
            configFileName = arg.mid(OPTION_NAME_SERVER_CFGFILE.length());
        }
    }

    if (!configFileName.isEmpty())
    {
        m_configFileName = configFileName;
    }
}

void SyncServerEngine::start()
{
    setServerSecure(m_serverData->isServerSecure());
    setServerEnabled(m_serverData->isServerEnabled());
}

std::unique_ptr<QSettings> SyncServerEngine::createSettingsObject()
{
    if (m_configFileName.isEmpty())
    {
        return std::unique_ptr<QSettings>(
                new QSettings(QSettings::IniFormat, QSettings::UserScope, ORGANIZATION_NAME,
                        APPLICATION_NAME, this));
    }
    else
    {
        return std::unique_ptr<QSettings>(
                new QSettings(m_configFileName, QSettings::IniFormat, this));
    }
}

void SyncServerEngine::onServerDataChanged()
{
    std::unique_ptr<QSettings> settings = createSettingsObject();

    if (settings)
    {
        if (!m_serverData->save(*settings))
        {
            QMessageBox::warning(nullptr, tr("Save Settings"), tr("Cannot save application settings."));
        }
    }
    else
    {
        QMessageBox::warning(nullptr, tr("Save Settings"), tr("Cannot create settings object."));
    }
}

void SyncServerEngine::setServerEnabled(
        const bool enable)
{
    if (enable == m_serverEnabled)
    {
        return;
    }

    if (enable)
    {
        // enable RPC server
        m_rpcServer->enable();

        // update LDP device data
        m_ldpDevice = Ldp::Device(m_serverData->getServerUuid(), m_rpcServer->getUrl());

        // advertise device in LDP network
        m_ldpAdvertiser->addDevice(m_ldpDevice);
        m_ldpAdvertiser->enable();
    }
    else
    {
        m_ldpAdvertiser->removeDevice(m_ldpDevice);
        m_ldpAdvertiser->disable();

        m_rpcServer->disable();
    }

    m_serverData->setServerEnabled(enable);

    m_serverEnabled = enable;
    emit serverStateChanged(m_serverEnabled);
}

QSslKey SyncServerEngine::loadSslKey(
        const QString& fileName)
{
    QSslKey sslKey;

    if (!fileName.isEmpty())
    {
        QFile keyFile(fileName);
        if (keyFile.open(QIODevice::ReadOnly))
        {
            auto keyData = keyFile.readAll();

            sslKey = QSslKey(keyData, QSsl::Rsa, QSsl::Pem);

            keyFile.close();
        }
    }

    return sslKey;
}

QSslCertificate SyncServerEngine::loadSslCertificate(
        const QString& fileName)
{
    QSslCertificate sslCertificate;

    if (!fileName.isEmpty())
    {
        QFile certificateFile(fileName);
        if (certificateFile.open(QIODevice::ReadOnly))
        {
            auto certData = certificateFile.readAll();

            sslCertificate = QSslCertificate(certData, QSsl::Pem);

            certificateFile.close();
        }
    }

    return sslCertificate;
}

void SyncServerEngine::setServerSecure(
        const bool secure)
{
    if (secure == m_serverSecure)
    {
        return;
    }

    const bool serverEnabled = m_serverEnabled;

    if (serverEnabled)
    {
        setServerEnabled(false);
    }

    bool newSecure = secure;
    if (newSecure)
    {
        QSslCertificate sslCertificate = loadSslCertificate(m_serverData->getSllCertificatePath());
        QSslKey sslKey = loadSslKey(m_serverData->getSslKeyPath());

        // if anything is wrong turn on secure mode
        if (sslCertificate.isNull() || sslKey.isNull())
        {
            newSecure = false;

            QMessageBox::information(nullptr, tr("Set Server Secure"), tr("Invalid SSL settings."));
        }

        m_rpcServer->setMode(newSecure, sslCertificate, sslKey);
    }

    if (serverEnabled)
    {
        setServerEnabled(true);
    }

    m_serverData->setServerSecure(newSecure);

    m_serverSecure = newSecure;
    emit serverModeChanged(m_serverSecure);
}

QAbstractItemModel* SyncServerEngine::getSharesTableModel()
{
    return m_sharesTableModel;
}

SyncServer::Share SyncServerEngine::getShare(
        const Commons::Uuid& uuid) const
{
    return m_serverData->getShare(uuid);
}

void SyncServerEngine::addShare(
        const SyncServer::Share& share)
{
    m_serverData->addShare(share);
}

void SyncServerEngine::modifyShare(
        const SyncServer::Share& share)
{
    m_serverData->modifyShare(share);
}

void SyncServerEngine::removeShare(
        const Commons::Uuid& uuid)
{
    m_serverData->removeShare(uuid);
}

void SyncServerEngine::removeAllShares()
{
    m_serverData->removeAllShares();
}

QString SyncServerEngine::getSslCertificatePath() const
{
    return m_serverData->getSllCertificatePath();
}

void SyncServerEngine::setSslCertificatePath(
        const QString& certificatePath)
{
    m_serverData->setSslCertificatePath(certificatePath);
}

QString SyncServerEngine::getSslKeyPath() const
{
    return m_serverData->getSslKeyPath();
}

void SyncServerEngine::setSslKeyPath(
        const QString& keyPath)
{
    m_serverData->setSslKeyPath(keyPath);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
