//-----------------------------------------------------------------------------
/// \file
/// ShareEditDialog - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ShareEditDialog
#define INCLUDED_OCTsync_ShareEditDialog

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QDialog>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QCheckBox>
#include <syncserver/Share.hpp>
#include <ui/ActionButton.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ShareEditDialog : public QDialog
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QAction* m_actionDialogOk;
    QAction* m_actionDialogCancel;
    QAction* m_actionRootDirBrowse;

    QLineEdit* m_uuidEdit;
    QLineEdit* m_nameEdit;
    QLineEdit* m_descriptionEdit;
    QLineEdit* m_rootDirEdit;
    QCheckBox* m_readOnlyCheckbox;

// Methods (public, protected, private)
public:
    explicit ShareEditDialog(
            QWidget* parent = nullptr,
            Qt::WindowFlags f = 0);

    void setData(
            const SyncServer::Share& share);

    SyncServer::Share getData() const;

private:
    void createActions();
    void createWidgets();
    void connectActions();

    void onRootDirBrowse();

    void onDialogOk();
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ShareEditDialog*/
