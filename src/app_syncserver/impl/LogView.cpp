//-----------------------------------------------------------------------------
/// \file
/// LogView - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "LogView.hpp"

// library includes
#include <QtCore/QFile>
#include <QtWidgets/QLayout>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QCheckBox>
#include <ui/ActionButton.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::ActionButton;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

LogView::LogView(
        QWidget* parent) :
                QFrame(parent),
                m_debugEnabled(false)
{
    setFrameStyle(QFrame::StyledPanel | QFrame::Plain);
    setLineWidth(0);
    setMidLineWidth(0);

    createActions();
    createWidgets();
    connectActions();
}

void LogView::addMessage(
        const LogMessage& message)
{
    if (!m_debugEnabled)
    {
        if (message.getType() == QtDebugMsg)
        {
            return;
        }
    }

    QString messageStr("%1: %2");

    messageStr = messageStr.arg(message.getTypeString()).arg(message.getText());

    m_logTextEdit->append(messageStr);

    auto const sb = m_logTextEdit->verticalScrollBar();
    sb->setValue(sb->maximum());

    m_logTextEdit->ensureCursorVisible();
}

void LogView::createActions()
{
    m_actionLogClear = new QAction(this);
    m_actionLogClear->setText("Clear");

    m_actionLogSaveAs = new QAction(this);
    m_actionLogSaveAs->setText("Save As...");
}

void LogView::createWidgets()
{
    m_logTextEdit = new QTextEdit(this);
    m_logTextEdit->setReadOnly(true);
    m_logTextEdit->setAcceptRichText(false);

    QWidget* buttonsWidget = new QWidget(this);
    QVBoxLayout* buttonsLayout = new QVBoxLayout(buttonsWidget);

    m_checkboxEnableDebug = new QCheckBox(buttonsWidget);
    m_checkboxEnableDebug->setText("Debug Messages");
    m_checkboxEnableDebug->setChecked(m_debugEnabled);

    buttonsLayout->setSpacing(10);
    buttonsLayout->addWidget(new ActionButton(m_actionLogClear, buttonsWidget), 0);
    buttonsLayout->addWidget(new ActionButton(m_actionLogSaveAs, buttonsWidget), 0);
    buttonsLayout->addWidget(m_checkboxEnableDebug, 0);
    buttonsLayout->addStretch(1);

    QHBoxLayout* mainLayout = new QHBoxLayout(this);
    mainLayout->setSpacing(10);
    mainLayout->addWidget(m_logTextEdit, 1);
    mainLayout->addWidget(buttonsWidget);
}

void LogView::connectActions()
{
    connect(m_actionLogClear, &QAction::triggered, this, &LogView::onLogClear);
    connect(m_actionLogSaveAs, &QAction::triggered, this, &LogView::onLogSaveAs);
    connect(m_checkboxEnableDebug, &QCheckBox::stateChanged, this, &LogView::onLogEnableDebug);
}

void LogView::onLogEnableDebug(
        int state)
{
    m_debugEnabled = (state != Qt::Unchecked);
}

void LogView::onLogClear()
{
    m_logTextEdit->setText("");
}

void LogView::onLogSaveAs()
{
    const QString logFileName = QFileDialog::getSaveFileName(this, tr("Save Log"), QString(),
                                 tr("Text Files (*.txt)"));
    if (!logFileName.isEmpty())
    {
        bool success = false;

        QFile logFile(logFileName);
        if (logFile.open(QFile::WriteOnly | QFile::Truncate | QFile::Text))
        {
            const auto text = m_logTextEdit->toPlainText().toUtf8();

            if (logFile.write(text) == text.size())
            {
                success = true;
            }

            logFile.close();
        }

        if (!success)
        {
            QMessageBox::warning(this, tr("Save Log"), tr("Cannot save log file."));
        }
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
