//-----------------------------------------------------------------------------
/// \file
/// SyncServerEngine - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncServerEngine
#define INCLUDED_OCTsync_SyncServerEngine

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QObject>
#include <QtCore/QAbstractItemModel>
#include <QtCore/QSettings>
#include <boprpc/Device.hpp>
#include <ldp/Device.hpp>
#include <ldp/Advertiser.hpp>
#include <syncserver/SyncService.hpp>
#include <syncserver/ServerData.hpp>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
class SharesTableModel;

/// TODO: Class documentation
///
class SyncServerEngine : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const QString ORGANIZATION_NAME;
    static const QString APPLICATION_NAME;
    static const QString OPTION_NAME_SERVER_CFGFILE;


// Types (public, protected, private)
public:
    enum Role
    {
        ROLE_VALIDITY       =   Qt::UserRole,
        ROLE_SHARE_UUID,
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
public:
    static QSslKey loadSslKey(
            const QString& fileName);

    static QSslCertificate loadSslCertificate(
            const QString& fileName);

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QString m_configFileName;

    bool m_serverEnabled;
    bool m_serverSecure;

    Ldp::Device m_ldpDevice;
    Ldp::Advertiser* m_ldpAdvertiser;

    SyncServer::ServerData* m_serverData;
    SyncServer::SyncService* m_syncService;
    BopRpc::Device* m_rpcServer;

    SharesTableModel* m_sharesTableModel;

// Methods (public, protected, private)
public:
    SyncServerEngine(
            QObject* parent = nullptr);

    virtual ~SyncServerEngine();

    void processArguments(
            QStringList arguments);

    void start();

    void setServerEnabled(
            const bool enable);

    void setServerSecure(
            const bool secure);

    QAbstractItemModel* getSharesTableModel();

    SyncServer::Share getShare(
            const Commons::Uuid& uuid) const;

    void addShare(
            const SyncServer::Share& share);

    void modifyShare(
            const SyncServer::Share& share);

    void removeShare(
            const Commons::Uuid& uuid);

    void removeAllShares();

    QString getSslCertificatePath() const;

    void setSslCertificatePath(
            const QString& certificatePath);

    QString getSslKeyPath() const;

    void setSslKeyPath(
            const QString& keyPath);

private:
    void loadSettings();

    void onServerDataChanged();

    std::unique_ptr<QSettings> createSettingsObject();

signals:
    void serverStateChanged(bool enabled);
    void serverModeChanged(bool secure);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncServerEngine*/
