//-----------------------------------------------------------------------------
/// \file
/// SharesTable - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SharesTable
#define INCLUDED_OCTsync_SharesTable

//-----------------------------------------------------------------------------

// local includes
#include "SyncServerEngine.hpp"

// library includes
#include <QtWidgets/QTableView>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SharesTable : public QTableView
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    explicit SharesTable(
            SyncServerEngine* engine,
            QWidget* parent = nullptr);

    Commons::Uuid getShareUuid(
                const QModelIndex& index) const;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SharesTable*/
