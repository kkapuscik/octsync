//-----------------------------------------------------------------------------
/// \file
/// SharesTableModel - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SharesTableModel.hpp"
#include "SyncServerEngine.hpp"

// library includes
#include <syncserver/ShareRepository.hpp>
#include <syncserver/Share.hpp>
#include <commons/Uuid.hpp>
#include <commons/SimpleTableModel.hpp>
#include <commons/SimpleHeaderItem.hpp>
#include <commons/SimpleItem.hpp>

// system includes
// (none)

// using clauses
using OCTsync::SyncServer::ServerData;
using OCTsync::SyncServer::ShareRepository;
using OCTsync::SyncServer::Share;
using OCTsync::Commons::Uuid;
using OCTsync::Commons::SimpleTableModel;
using OCTsync::Commons::SimpleHeaderItem;
using OCTsync::Commons::SimpleItem;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

class ShareItem : public SimpleItem
{
private:
    bool m_valid;

public:
    ShareItem(
            const Share& share) :
                    m_valid(false)
    {
        setData(QVariant(share.getUuid().toQUuid()), SyncServerEngine::ROLE_SHARE_UUID);
        update(share);
    }

    void update(
            const Share& share)
    {
        QString flagsStr = QString("%1").arg(share.isReadOnly() ? "Read/Only" : "Read/Write");

        setDisplayData(0, share.getUuid().toString());
        setDisplayData(1, share.getName());
        setDisplayData(2, share.getDescription());
        setDisplayData(3, share.getRootDir().path());
        setDisplayData(4, flagsStr);
    }

    void invalidate()
    {
        m_valid = false;
    }

    void markValid()
    {
        m_valid = true;
    }

    bool isValid() const
    {
        return m_valid;
    }
};

SharesTableModel::SharesTableModel(
        QObject* parent) :
                SimpleTableModel(parent)
{
    appendHeader(tr("Id."));
    appendHeader(tr("Name"));
    appendHeader(tr("Description"));
    appendHeader(tr("Path"));
    appendHeader(tr("Flags"));
}

void SharesTableModel::setServerData(
        QPointer<SyncServer::ServerData> data)
{
    if (m_data)
    {
        disconnect(m_data.data(), &ServerData::changed, this, &SharesTableModel::updateModel);
    }

    m_data = data;

    if (m_data)
    {
        connect(m_data.data(), &ServerData::changed, this, &SharesTableModel::updateModel);
    }

    updateModel();
}

void SharesTableModel::updateModel()
{
    QMap<Uuid, SimpleItem> currentItems;

    // TODO: optimize

    // mark all as invalid
    for (int row = rowCount() - 1; row >= 0; --row)
    {
        getItem(row)->setData(false, SyncServerEngine::ROLE_VALIDITY);
    }

    if (m_data)
    {
        // update existing, add missing
        const ShareRepository& shareRepo = m_data->getShares();
        for (const Uuid& shareId : shareRepo.getShareKeys())
        {
            const Share& share = shareRepo.getShare(shareId);

            // check if exists
            DataItemPtr shareItem = m_shareItems.value(shareId);
            if (!shareItem)
            {
                // do not exist - create
                shareItem = createItem(share);

                // add to model
                appendItem(shareItem);

                // and store in lookup collection
                m_shareItems.insert(share.getUuid(), shareItem);
            }
            else
            {
                // exist - update
                updateItem(shareItem, share);
            }

            // set as valid
            shareItem->setData(true, SyncServerEngine::ROLE_VALIDITY);
        }
    }

    // remove items no longer valid
    // (removed from last to make the iteration safe)
    for (int row = rowCount() - 1; row >= 0; --row)
    {
        DataItemPtr item = getItem(row);

        if (!item->data(0, SyncServerEngine::ROLE_VALIDITY).toBool())
        {
            // remove from model
            removeItem(row);

            // remove from items mapping
            m_shareItems.remove(item->data(SyncServerEngine::ROLE_SHARE_UUID).toUuid());
        }
    }
}

SharesTableModel::DataItemPtr SharesTableModel::createItem(
        const SyncServer::Share& share)
{
    DataItemPtr item(new SimpleItem());

    item->setData(QVariant(share.getUuid().toQUuid()), SyncServerEngine::ROLE_SHARE_UUID);
    updateItem(item, share);

    return item;
}

void SharesTableModel::updateItem(
        DataItemPtr item,
        const SyncServer::Share& share)
{
    QString flagsStr = QString("%1").arg(share.isReadOnly() ? "Read/Only" : "Read/Write");

    item->setDisplayData(0, share.getUuid().toString());
    item->setDisplayData(1, share.getName());
    item->setDisplayData(2, share.getDescription());
    item->setDisplayData(3, share.getRootDir().path());
    item->setDisplayData(4, flagsStr);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
