//-----------------------------------------------------------------------------
/// \file
/// SharesView - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SharesView.hpp"
#include "ShareEditDialog.hpp"

// library includes
#include <QtWidgets/QLayout>
#include <QtWidgets/QMessageBox>
#include <ui/ActionButton.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::ActionButton;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

SharesView::SharesView(
        SyncServerEngine* engine,
        QWidget* parent) :
                QFrame(parent),
                m_engine(engine)
{
    Q_ASSERT(engine);

    setFrameStyle(QFrame::StyledPanel | QFrame::Plain);
    setLineWidth(0);
    setMidLineWidth(0);

    createActions();
    createWidgets();
    connectActions();
}

void SharesView::createActions()
{
    m_actionAddShare = new QAction(this);
    m_actionAddShare->setText("Add Share...");

    m_actionEditShare = new QAction(this);
    m_actionEditShare->setText("Edit Share...");

    m_actionRemoveShare = new QAction(this);
    m_actionRemoveShare->setText("Remove Share");

    m_actionRemoveAllShares = new QAction(this);
    m_actionRemoveAllShares->setText("Remove All Shares");
}

void SharesView::createWidgets()
{
    m_sharesTable = new SharesTable(m_engine, this);

    QWidget* buttonsWidget = new QWidget(this);
    QHBoxLayout* buttonsLayout = new QHBoxLayout(buttonsWidget);
    buttonsLayout->setSpacing(5);
    buttonsLayout->addWidget(new ActionButton(m_actionAddShare, buttonsWidget));
    buttonsLayout->addWidget(new ActionButton(m_actionEditShare, buttonsWidget));
    buttonsLayout->addWidget(new ActionButton(m_actionRemoveShare, buttonsWidget));
    buttonsLayout->addWidget(new ActionButton(m_actionRemoveAllShares, buttonsWidget));
    buttonsLayout->addStretch(1);

    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->setSpacing(5);
    mainLayout->addWidget(m_sharesTable);
    mainLayout->addWidget(buttonsWidget);
}

void SharesView::connectActions()
{
    connect(m_actionAddShare, &QAction::triggered, this, &SharesView::onAddShareRequested);
    connect(m_actionEditShare, &QAction::triggered, this, &SharesView::onEditShareRequested);
    connect(m_actionRemoveShare, &QAction::triggered, this, &SharesView::onRemoveShareRequested);
    connect(m_actionRemoveAllShares, &QAction::triggered, this,
            &SharesView::onRemoveAllSharesRequested);

    connect(m_sharesTable->selectionModel(), &QItemSelectionModel::currentChanged, this,
            &SharesView::onCurrentChanged);
}

void SharesView::onCurrentChanged(
        const QModelIndex& current,
        const QModelIndex& /*previous*/)
{
    m_actionEditShare->setEnabled(current.isValid());
    m_actionRemoveShare->setEnabled(current.isValid());
}

void SharesView::onAddShareRequested()
{
    SyncServer::Share share;

    share.setUuid(Commons::Uuid::generate());

    ShareEditDialog dialog;
    dialog.setData(share);

    if (dialog.exec() == QDialog::Accepted)
    {
        share = dialog.getData();

        m_engine->addShare(share);
    }
}

void SharesView::onEditShareRequested()
{
    const QModelIndex current = m_sharesTable->currentIndex();
    if (current.isValid())
    {
        const Commons::Uuid shareId = m_sharesTable->getShareUuid(current);

        SyncServer::Share share = m_engine->getShare(shareId);
        if (share.isValid())
        {
            ShareEditDialog dialog;
            dialog.setData(share);

            if (dialog.exec() == QDialog::Accepted)
            {
                share = dialog.getData();

                m_engine->modifyShare(share);
            }
        }
    }
}

void SharesView::onRemoveShareRequested()
{
    const QModelIndex current = m_sharesTable->currentIndex();
    if (current.isValid())
    {
        const Commons::Uuid shareId = m_sharesTable->getShareUuid(current);

        SyncServer::Share share = m_engine->getShare(shareId);
        if (share.isValid())
        {
            if (QMessageBox::question(this, tr("Remove Share?"),
                    tr("Are you sure you want to remove selected share?")) == QMessageBox::Yes)
            {
                m_engine->removeShare(shareId);
            }
        }
    }
}

void SharesView::onRemoveAllSharesRequested()
{
    if (QMessageBox::question(this, tr("Remove All Shares?"),
            tr("Are you sure you want to remove all shares?")) == QMessageBox::Yes)
    {
        m_engine->removeAllShares();
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
