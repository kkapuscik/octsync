//-----------------------------------------------------------------------------
/// \file
/// SyncServerWindow - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncServerWindow.hpp"
#include "SecuritySettingsDialog.hpp"

// library includes
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QAction>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QSplitter>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

SyncServerWindow::SyncServerWindow(
        SyncServerEngine* engine,
        QWidget* parent) :
                QMainWindow(parent),
                m_engine(engine)
{
    Q_ASSERT(engine);

    setWindowTitle("SyncServer");
    setWindowIcon(QIcon(":/icons/octsyncserver"));

    createActions();
    createWidgets();
    createMainMenu();

    resize(800, 600);
    move(QApplication::desktop()->screen()->rect().center() - rect().center());

    connectActions();
}

void SyncServerWindow::addLogMessage(
        const LogMessage& message)
{
    m_logView->addMessage(message);
}

void SyncServerWindow::createActions()
{
    m_actionAppExit = new QAction(this);
    m_actionAppExit->setText(tr("E&xit"));

    m_actionAppAbout = new QAction(this);
    m_actionAppAbout->setText(tr("&About"));

    m_actionAppAboutQt = new QAction(this);
    m_actionAppAboutQt->setText(tr("About &Qt"));

    m_actionSecuritySettings = new QAction(this);
    m_actionSecuritySettings->setText(tr("Security Settings..."));

    m_actionServerEnabled = new QAction(this);
    m_actionServerEnabled->setText(tr("&Server Enabled"));
    m_actionServerEnabled->setCheckable(true);

    m_actionSecureModeEnabled = new QAction(this);
    m_actionSecureModeEnabled->setText(tr("Secure &Mode"));
    m_actionSecureModeEnabled->setCheckable(true);
}

void SyncServerWindow::createMainMenu()
{
    QMenu* menuFile;
    QMenu* menuServer;
    QMenu* menuHelp;

    menuFile = new QMenu(this);
    menuFile->setTitle(tr("&File"));
    menuFile->addAction(m_actionAppExit);

    menuServer = new QMenu(this);
    menuServer->setTitle(tr("&Server"));
    menuServer->addAction(m_actionSecuritySettings);
    menuServer->addSeparator();
    menuServer->addAction(m_actionServerEnabled);
    menuServer->addAction(m_actionSecureModeEnabled);

    menuHelp = new QMenu(this);
    menuHelp->setTitle(tr("&Help"));
    menuHelp->addAction(m_actionAppAbout);
    menuHelp->addSeparator();
    menuHelp->addAction(m_actionAppAboutQt);

    menuBar()->addMenu(menuFile);
    menuBar()->addMenu(menuServer);
    menuBar()->addMenu(menuHelp);
}

void SyncServerWindow::createWidgets()
{
    QSplitter* mainSplitter = new QSplitter(Qt::Vertical, this);

    m_sharesView = new SharesView(m_engine, mainSplitter);
    m_logView = new LogView(mainSplitter);

    mainSplitter->addWidget(m_sharesView);
    mainSplitter->addWidget(m_logView);

    mainSplitter->setStretchFactor(0, 2);
    mainSplitter->setStretchFactor(1, 1);

    setCentralWidget(mainSplitter);
}

void SyncServerWindow::connectActions()
{
    connect(m_actionAppExit, &QAction::triggered, this, &SyncServerWindow::close);
    connect(m_actionAppAbout, &QAction::triggered, this, &SyncServerWindow::onAbout);
    connect(m_actionAppAboutQt, &QAction::triggered, this, &SyncServerWindow::onAboutQt);
    connect(m_actionSecuritySettings, &QAction::triggered, this, &SyncServerWindow::onSecuritySettings);
    connect(m_actionServerEnabled, &QAction::triggered, m_engine, &SyncServerEngine::setServerEnabled);
    connect(m_actionSecureModeEnabled, &QAction::triggered, m_engine, &SyncServerEngine::setServerSecure);

    connect(m_engine, &SyncServerEngine::serverStateChanged, m_actionServerEnabled, &QAction::setChecked);
    connect(m_engine, &SyncServerEngine::serverModeChanged, m_actionSecureModeEnabled, &QAction::setChecked);
}

void SyncServerWindow::onSecuritySettings()
{
    SecuritySettingsDialog settingsDialog;

    settingsDialog.setCertFilePath(m_engine->getSslCertificatePath());
    settingsDialog.setKeyFilePath(m_engine->getSslKeyPath());
    // TODO
    // settingsDialog.setPassword(m_engine->getPassword());

    if (settingsDialog.exec() == QDialog::Accepted)
    {
        m_engine->setSslCertificatePath(settingsDialog.getCertFilePath());
        m_engine->setSslKeyPath(settingsDialog.getKeyFilePath());
        // TODO
        // m_engine->setPassword(settingsDialog.getPassword());
    }
}

void SyncServerWindow::onAbout()
{
    QMessageBox::about(this, tr("Sync Server - About"),
            tr("Sync Server v0.2\n\n"
               "Copyright (C) 2014 OCTaedr Software\n"
               "- Krzysztof Kapuscik\n"
               "- Michal Kowal\n\n"
               "All Rights Reserved"));
}

void SyncServerWindow::onAboutQt()
{
    QMessageBox::aboutQt(this, tr("SyncServer - About Qt"));
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
