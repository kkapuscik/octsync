//-----------------------------------------------------------------------------
/// \file
/// SecuritySettingsDialog - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SecuritySettingsDialog.hpp"
#include "SyncServerEngine.hpp"

// library includes
#include <QtWidgets/QLayout>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QLineEdit>
#include <ui/ActionButton.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::ActionButton;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

SecuritySettingsDialog::SecuritySettingsDialog(
        QWidget* parent,
        Qt::WindowFlags flags) :
                QDialog(parent, flags)
{
    setSizeGripEnabled(true);
    resize(600, 300);

    createActions();
    createWidgets();
    connectActions();
}

QString SecuritySettingsDialog::getKeyFilePath() const
{
    return m_keyFileEdit->text();
}

void SecuritySettingsDialog::setKeyFilePath(
        const QString& path)
{
    m_keyFileEdit->setText(path);
}

QString SecuritySettingsDialog::getCertFilePath() const
{
    return m_certFileEdit->text();
}

void SecuritySettingsDialog::setCertFilePath(
        const QString& path)
{
    m_certFileEdit->setText(path);
}

QString SecuritySettingsDialog::getPassword() const
{
    return m_password1Edit->text();
}

void SecuritySettingsDialog::setPassword(
        const QString& password)
{
    m_password1Edit->setText(password);
    m_password2Edit->setText(password);
}

void SecuritySettingsDialog::createActions()
{
    m_actionDialogOk = new QAction(this);
    m_actionDialogOk->setText(tr("OK"));

    m_actionDialogCancel = new QAction(this);
    m_actionDialogCancel->setText(tr("Cancel"));

    m_actionKeyBrowse = new QAction(this);
    m_actionKeyBrowse->setText("Browse...");

    m_actionCertBrowse = new QAction(this);
    m_actionCertBrowse->setText("Browse...");
}

void SecuritySettingsDialog::createWidgets()
{
    m_keyFileEdit = new QLineEdit(this);
    m_certFileEdit = new QLineEdit(this);
    m_password1Edit = new QLineEdit(this);
    m_password2Edit = new QLineEdit(this);

    m_password1Edit->setEchoMode(QLineEdit::Password);
    m_password1Edit->setInputMethodHints(Qt::ImhNone);
    m_password2Edit->setEchoMode(QLineEdit::Password);
    m_password2Edit->setInputMethodHints(Qt::ImhNone);

    // TODO
    m_password1Edit->setEnabled(false);
    m_password2Edit->setEnabled(false);

    auto const keyBrowseButton = new ActionButton(m_actionKeyBrowse);
    auto const certBrowseButton = new ActionButton(m_actionCertBrowse);

    auto const keyEditLayout = new QHBoxLayout();
    keyEditLayout->addWidget(m_keyFileEdit, 1);
    keyEditLayout->addWidget(keyBrowseButton, 0);

    auto const certEditLayout = new QHBoxLayout();
    certEditLayout->addWidget(m_certFileEdit, 1);
    certEditLayout->addWidget(certBrowseButton, 0);

    auto const formLayout = new QFormLayout();
    formLayout->addRow("Key File", keyEditLayout);
    formLayout->addRow("Certificate File", certEditLayout);
    formLayout->addRow("Password", m_password1Edit);
    formLayout->addRow("Password (repeat)", m_password2Edit);

    auto const dialogOkButton = new ActionButton(m_actionDialogOk, this);
    auto const dialogCancelButton = new ActionButton(m_actionDialogCancel, this);

    QHBoxLayout* const dialogButtonsLayout = new QHBoxLayout();
    dialogButtonsLayout->addSpacing(10);
    dialogButtonsLayout->addWidget(dialogOkButton);
    dialogButtonsLayout->addStretch(1);
    dialogButtonsLayout->addWidget(dialogCancelButton);
    dialogButtonsLayout->addSpacing(10);

    QVBoxLayout* const mainLayout = new QVBoxLayout(this);
    mainLayout->setSpacing(15);
    mainLayout->addLayout(formLayout);
    mainLayout->addLayout(dialogButtonsLayout);

    dialogOkButton->setDefault(true);
}

void SecuritySettingsDialog::connectActions()
{
    connect(m_actionKeyBrowse, &QAction::triggered, this, &SecuritySettingsDialog::onKeyBrowse);
    connect(m_actionCertBrowse, &QAction::triggered, this, &SecuritySettingsDialog::onCertBrowse);
    connect(m_actionDialogOk, &QAction::triggered, this, &SecuritySettingsDialog::onDialogOk);
    connect(m_actionDialogCancel, &QAction::triggered, this, &QDialog::reject);
}

void SecuritySettingsDialog::onKeyBrowse()
{
    const QString newPath = QFileDialog::getOpenFileName(this, tr("Select Key File"),
            m_keyFileEdit->text());
    if (!newPath.isEmpty())
    {
        m_keyFileEdit->setText(newPath);
    }
}

void SecuritySettingsDialog::onCertBrowse()
{
    QFileDialog d;

    const QString newPath = QFileDialog::getOpenFileName(this, tr("Select Certificate File"),
            m_certFileEdit->text());
    if (!newPath.isEmpty())
    {
        m_certFileEdit->setText(newPath);
    }
}

void SecuritySettingsDialog::onDialogOk()
{
#if 0
    if (m_password1Edit->text().length() == 0)
    {
        QMessageBox::information(this, tr("Invalid data"), tr("Password cannot be empty."));
        return;
    }
    if (m_password1Edit->text() != m_password2Edit->text())
    {
        QMessageBox::information(this, tr("Invalid data"), tr("Passwords do not match."));
        return;
    }
#endif

    if (m_keyFileEdit->text().length() > 0)
    {
        const auto sslKey = SyncServerEngine::loadSslKey(m_keyFileEdit->text());
        if (sslKey.isNull())
        {
            QMessageBox::information(this, tr("Invalid data"), tr("SSL key is not valid."));
            return;
        }
    }

    if (m_certFileEdit->text().length() > 0)
    {
        const auto sslCert = SyncServerEngine::loadSslCertificate(m_certFileEdit->text());
        if (sslCert.isNull())
        {
            QMessageBox::information(this, tr("Invalid data"), tr("SSL certificate is not valid."));
            return;
        }
    }

    accept();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
