//-----------------------------------------------------------------------------
/// \file
/// SharesView - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SharesView
#define INCLUDED_OCTsync_SharesView

//-----------------------------------------------------------------------------

// local includes
#include "SharesTable.hpp"
#include "SyncServerEngine.hpp"

// library includes
#include <QtWidgets/QFrame>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SharesView : public QFrame
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncServerEngine* m_engine;
    SharesTable* m_sharesTable;

    QAction* m_actionAddShare;
    QAction* m_actionEditShare;
    QAction* m_actionRemoveShare;
    QAction* m_actionRemoveAllShares;

// Methods (public, protected, private)
public:
    explicit SharesView(
            SyncServerEngine* engine,
            QWidget* parent = nullptr);

private:
    void createActions();
    void createWidgets();
    void connectActions();

    void onCurrentChanged(
            const QModelIndex& current,
            const QModelIndex& previous);

    void onAddShareRequested();
    void onEditShareRequested();
    void onRemoveShareRequested();
    void onRemoveAllSharesRequested();
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SharesView*/
