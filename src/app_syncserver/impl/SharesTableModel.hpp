//-----------------------------------------------------------------------------
/// \file
/// SharesTableModel - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SharesTableModel
#define INCLUDED_OCTsync_SharesTableModel

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtGui/QStandardItemModel>
#include <QtCore/QAbstractTableModel>
#include <QtCore/QPointer>
#include <QtCore/QMap>
#include <syncserver/ServerData.hpp>
#include <commons/SimpleTableModel.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SharesTableModel : public Commons::SimpleTableModel
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QMap<Commons::Uuid, DataItemPtr> ShareItemsMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QPointer<SyncServer::ServerData> m_data;
    ShareItemsMap m_shareItems;

// Methods (public, protected, private)
public:
    explicit SharesTableModel(
            QObject* parent = nullptr);

    void setServerData(
            QPointer<SyncServer::ServerData> data);

private:
    void updateModel();

    DataItemPtr createItem(
            const SyncServer::Share& share);

    void updateItem(
            DataItemPtr item,
            const SyncServer::Share& share);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SharesTableModel*/
