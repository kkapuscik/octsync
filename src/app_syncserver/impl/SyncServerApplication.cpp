//-----------------------------------------------------------------------------
/// \file
/// SyncServerApplication - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncServerApplication.hpp"

// library includes
#include <QtWidgets/QMenu>
#include <QtWidgets/QAction>
#include <QtCore/QtGlobal>
#include <ui/LogManager.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::LogManager;

// forward references
// (none)

//-----------------------------------------------------------------------------

// #define USE_TRAY_ICON

//-----------------------------------------------------------------------------

static void initResources()
{
    Q_INIT_RESOURCE(octsync);
}

namespace OCTsync
{
namespace AppSyncServer
{

SyncServerApplication::SyncServerApplication(
        int& argc,
        char** argv) :
                QApplication(argc, argv)
{
    m_engine = new SyncServerEngine(this);
    m_engine->processArguments(arguments());

    m_mainWindow.reset(new SyncServerWindow(m_engine));

    LogManager* const logManager = LogManager::getInstance();

    logManager->initialize();

    QApplication::connect(logManager, &LogManager::messageLogged,
                          m_mainWindow.get(), &SyncServerWindow::addLogMessage);

#ifdef USE_TRAY_ICON
    m_trayContextMenu.reset(new QMenu());
    QAction* trayShowWindowAction = m_trayContextMenu->addAction(tr("&Show"));
    connect(trayShowWindowAction, &QAction::triggered, m_mainWindow.get(), &QMainWindow::show);
    m_trayContextMenu->addSeparator();
    QAction* trayQuitAppAction = m_trayContextMenu->addAction(tr("&Quit"));
    connect(trayQuitAppAction, &QAction::triggered, &QApplication::quit);

    m_trayIcon = new QSystemTrayIcon(this);
    connect(m_trayIcon, &QSystemTrayIcon::activated, this,
            &SyncServerApplication::onTrayIconActivated);
    connect(m_trayIcon, &QSystemTrayIcon::messageClicked, m_mainWindow.get(), &QMainWindow::show);
#else
    m_trayIcon = nullptr;
#endif
}

int SyncServerApplication::exec()
{
    initResources();

    m_engine->start();

    if (m_trayIcon)
    {
        m_trayIcon->show();
        m_trayIcon->setContextMenu(m_trayContextMenu.get());
    }

    m_mainWindow->show();

    return QApplication::exec();
}

void SyncServerApplication::onTrayIconActivated(
        QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
        case QSystemTrayIcon::Trigger:
            m_mainWindow->show();
            break;
        default:
            break;
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
