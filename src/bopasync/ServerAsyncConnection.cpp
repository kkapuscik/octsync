//-----------------------------------------------------------------------------
/// \file
/// ServerAsyncConnection - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ServerAsyncConnection.hpp"

// library includes
#include <QtCore/QThread>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ServerAsyncConnection::ServerAsyncConnection(
        QThread* const connectionThread,
        QTcpSocket* const clientSocket) :
                m_thread(connectionThread),
                m_socket(clientSocket)
{
    // move connection to execution thread
    moveToThread(m_thread);

    // unset socket parent to allow move to other thread
    m_socket->setParent(nullptr);

    // move the socket to the execution thread
    m_socket->moveToThread(m_thread);

    // and set connection as parent of the socket
    m_socket->setParent(this);

    // connect thread signals
    connect(m_thread, &QThread::started, this, &ServerAsyncConnection::onThreadStart);
    connect(m_thread, &QThread::finished, this, &ServerAsyncConnection::deleteLater);

    // connect socket signals
    void (QTcpSocket::*errorPtr)(
            QAbstractSocket::SocketError) = &QTcpSocket::error;

    connect(m_socket, &QTcpSocket::disconnected, this, &ServerAsyncConnection::onSocketDisconnect);
    connect(m_socket, errorPtr, this, &ServerAsyncConnection::onSocketError);
    connect(m_socket, &QTcpSocket::stateChanged, this, &ServerAsyncConnection::onSocketStateChange);
    connect(m_socket, &QTcpSocket::bytesWritten, this, &ServerAsyncConnection::onSocketBytesWritten);
    connect(m_socket, &QTcpSocket::readChannelFinished, this,
            &ServerAsyncConnection::onSocketReadChannelFinished);
    connect(m_socket, &QTcpSocket::readyRead, this, &ServerAsyncConnection::onSocketReadyRead);
}

void ServerAsyncConnection::onThreadStart()
{
    qDebug() << "onThreadStart()";

    // TODO

}

void ServerAsyncConnection::onSocketDisconnect()
{
    qDebug() << "onSocketDisconnect()";
}

void ServerAsyncConnection::onSocketError(
        QAbstractSocket::SocketError socketError)
{
    qDebug() << "onSocketError()" << socketError;
}

void ServerAsyncConnection::onSocketStateChange(
        QAbstractSocket::SocketState socketState)
{
    qDebug() << "onSocketStateChange() " << socketState;
}

void ServerAsyncConnection::onSocketBytesWritten(
        qint64 bytes)
{
    qDebug() << "onSocketBytesWritten() " << bytes;
}

void ServerAsyncConnection::onSocketReadChannelFinished()
{
    qDebug() << "onSocketReadChannelFinished()";
}

void ServerAsyncConnection::onSocketReadyRead()
{
    qDebug() << "ServerAsyncConnection::onSocketReadyRead()";

    while (m_socket->bytesAvailable() > 0)
    {
        char bytes[4];

        int read = m_socket->read(bytes, sizeof(bytes));
        for (int i = 0; i < read; ++i)
        {
            qDebug() << "- byte read: " << (quint32)bytes[i];
        }
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
