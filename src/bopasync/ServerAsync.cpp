//-----------------------------------------------------------------------------
/// \file
/// ServerAsync - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../ServerAsync.hpp"
#include "ServerAsyncConnection.hpp"

// library includes
#include <QtCore/QThread>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QNetworkInterface>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ServerAsync::ServerAsync(
        QObject* parent) :
                QObject(parent)
{
    m_serverSocket = new QTcpServerAsync(this);

    connect(m_serverSocket, &QTcpServerAsync::newConnection, this, &ServerAsync::acceptNewConnection);
    connect(m_serverSocket, &QTcpServerAsync::acceptError, this, &ServerAsync::processAcceptError);
}

ServerAsync::~ServerAsync()
{
    // 'free' connection threads
    // TODO

}

void ServerAsync::enable()
{
    if (m_serverSocket->listen())
    {
        qDebug() << "ServerAsync running on port: " << m_serverSocket->serverPort();
    }
}

void ServerAsync::disable()
{
    m_serverSocket->close();
}

QUrl ServerAsync::getUrl() const
{
    QUrl url;

    url.setScheme("boprpc");
    // url.setHost(m_serverSocket->serverAddress().toString());

    QList<QHostAddress> addrList = QNetworkInterface::allAddresses();
    for (int i = 0; i < addrList.size(); ++i)
    {
        QHostAddress address = addrList.at(i);

        if (address.isLoopback())
        {
            continue;
        }

        url.setHost(address.toString());
        break;
    }

    url.setPort(m_serverSocket->serverPort());

    return url;
}

void ServerAsync::processAcceptError(
        QAbstractSocket::SocketError socketError)
{
    // TODO
}

void ServerAsync::acceptNewConnection()
{
    QTcpSocket* clientSocket = m_serverSocket->nextPendingConnection();
    if (clientSocket)
    {
        // create thread for handling the connection
        QThread* connectionThread = new QThread();

        // make sure the thread will be destroyed when finished
        connect(connectionThread, &QThread::finished, connectionThread, &QThread::deleteLater);

        // create connection object
        ServerAsyncConnection* connection = new ServerAsyncConnection(connectionThread, clientSocket);

        // start thread & event loop
        connectionThread->start();

        // TODO: store connection/thread to stop them on disable()/destructor() call

        // TODO limit number of connections using:
        // m_serverSocket->resumeAccepting();
        // m_serverSocket->pauseAccepting();
    }
    else
    {
        qDebug() << "ServerAsync failed to accept connection";
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
