//-----------------------------------------------------------------------------
/// \file
/// ServerAsyncConnection - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ServerAsyncConnection
#define INCLUDED_OCTsync_ServerAsyncConnection

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QObject>
#include <QtNetwork/QTcpSocket>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ServerAsyncConnection : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QThread* const m_thread;
    QTcpSocket* const m_socket;

// Methods (public, protected, private)
public:
    ServerAsyncConnection(
            QThread* const connectionThread,
            QTcpSocket* const clientSocket);

#if 0

    Socket:
    void connected()
    void disconnected()
    void error(QAbstractSocket::SocketError socketError)
    void hostFound()
    void proxyAuthenticationRequired(const QNetworkProxy & proxy, QAuthenticator * authenticator)
    void stateChanged(QAbstractSocket::SocketState socketState)

    void aboutToClose()
    void bytesWritten(qint64 bytes)
    void readChannelFinished()
    void readyRead()

    void destroyed(QObject * obj = 0)
    void objectNameChanged(const QString & objectName)

    Thread:
    void finished()
    void started()

#endif
private:
    void onThreadStart();

    void onSocketDisconnect();
    void onSocketError(QAbstractSocket::SocketError socketError);
    void onSocketStateChange(QAbstractSocket::SocketState socketState);

    void onSocketBytesWritten(qint64 bytes);
    void onSocketReadChannelFinished();
    void onSocketReadyRead();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ServerAsyncConnection*/
