//-----------------------------------------------------------------------------
/// \file
/// ServerAsync - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ServerAsync
#define INCLUDED_OCTsync_ServerAsync

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QObject>
#include <QtNetwork/QTcpServer>
#include <QtCore/QUrl>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ServerAsync : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QTcpServer* m_serverSocket;

// Methods (public, protected, private)
public:
    ServerAsync(
            QObject* parent = nullptr);

    virtual ~ServerAsync();

    void enable();

    void disable();

    QUrl getUrl() const;

private:
    void processAcceptError(
            QAbstractSocket::SocketError socketError);

    void acceptNewConnection();
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ServerAsync*/
