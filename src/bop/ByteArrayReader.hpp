//-----------------------------------------------------------------------------
/// \file
/// ByteArrayReader - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ByteArrayReader
#define INCLUDED_OCTsync_ByteArrayReader

//-----------------------------------------------------------------------------

// local includes
#include "ByteArray.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ByteArrayReader
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    const ByteArray& m_byteArray;
    int m_position;
    int m_rangeOffset;
    int m_rangeLength;

// Methods (public, protected, private)
public:
    explicit ByteArrayReader(
            const ByteArray& byteArray);

    explicit ByteArrayReader(
            const ByteArray& byteArray,
            const int offset);

    explicit ByteArrayReader(
            const ByteArray& byteArray,
            const int offset,
            const int length);

    explicit ByteArrayReader(
            const ByteArrayReader& reader);

    explicit ByteArrayReader(
            const ByteArrayReader& reader,
            const int offset);

    explicit ByteArrayReader(
            const ByteArrayReader& reader,
            const int offset,
            const int length);

    void Init(
            const int relativeRangeOffset,
            const int relativeRangeLength,
            const int parentRangeOffset,
            const int parentRangeLength);

    int getSize() const
    {
        return m_rangeLength;
    }

    bool hasMoreData() const;

    void setPosition(
            const int position);

    void skip(
            const int bytes);

    BoxId decodeBoxId();

    BoxType decodeBoxType();

    quint8 decodeUint8();

    quint16 decodeUint16();

    quint32 decodeUint32();

    quint64 decodeUint64();

    qint8 decodeInt8();

    qint16 decodeInt16();

    qint32 decodeInt32();

    qint64 decodeInt64();

    QByteArray decodeByteArray(
            const int size);

private:
    quint8 getNextByte();
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ByteArrayReader*/
