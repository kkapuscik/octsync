//-----------------------------------------------------------------------------
/// \file
/// Error - enum definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Error
#define INCLUDED_OCTsync_Error

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Enum documentation
///
enum class Error
{
    NONE = 0,

    TIMEOUT,
    WRITE,
    READ,

    UNKNOWN = -1
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Error*/
