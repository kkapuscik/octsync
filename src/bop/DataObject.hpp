//-----------------------------------------------------------------------------
/// \file
/// DataObject - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_DataObject
#define INCLUDED_OCTsync_DataObject

//-----------------------------------------------------------------------------

// local includes
#include "ObjectBase.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class DataObject : public ObjectBase
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    DataObject(
            const BoxId& id);

protected:
    virtual DecodeResult decodePayload(
            ObjectFactoryManager& /*manager*/,
            ByteArrayReader& reader)
    {
        return decodePayload(reader);
    }

    virtual DecodeResult decodePayload(
            ByteArrayReader& reader) = 0;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_DataObject*/
