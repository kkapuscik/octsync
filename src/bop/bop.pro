#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T18:59:10
#
#-------------------------------------------------

QT       -= gui

TARGET = bop
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    impl/BoxId.cpp \
    impl/ByteArrayReader.cpp \
    impl/ByteArrayWriter.cpp \
    impl/Client.cpp \
    impl/ClientConnection.cpp \
    impl/ClientConnectionPrivate.cpp \
    impl/ContainerObject.cpp \
    impl/DataObject.cpp \
    impl/ObjectBase.cpp \
    impl/ObjectFactoryManager.cpp \
    impl/ObjectReader.cpp \
    impl/ObjectWriter.cpp \
    impl/Server.cpp \
    impl/ServerConnection.cpp \
    impl/SslServerSocket.cpp

HEADERS += \
    BoxId.hpp \
    BoxType.hpp \
    ByteArray.hpp \
    ByteArrayReader.hpp \
    ByteArrayWriter.hpp \
    Client.hpp \
    ClientConnection.hpp \
    ContainerObject.hpp \
    DataObject.hpp \
    Error.hpp \
    namespace.hpp \
    ObjectBase.hpp \
    ObjectFactory.hpp \
    ObjectFactoryManager.hpp \
    Server.hpp \
    impl/ClientConnectionPrivate.hpp \
    impl/ObjectReader.hpp \
    impl/ObjectWriter.hpp \
    impl/ServerConnection.hpp \
    impl/SslServerSocket.hpp
unix {
    target.path = /usr/lib
    INSTALLS += target
}

OTHER_FILES += \
    Commands.txt \
    CMakeLists.txt

INCLUDEPATH += $$PWD/..
