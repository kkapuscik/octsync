//-----------------------------------------------------------------------------
/// \file
/// BoxId - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_BoxId
#define INCLUDED_OCTsync_BoxId

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QtGlobal>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class BoxId
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    quint32 m_id;

// Methods (public, protected, private)
public:
    BoxId();

    BoxId(
            const char c1,
            const char c2,
            const char c3,
            const char c4);

    void toCharArray(char* const buffer) const;

    bool isValid() const;

private:
    bool isValid(const char c) const;

    friend bool operator != (const BoxId& box1, const BoxId& box2);
    friend bool operator == (const BoxId& box1, const BoxId& box2);
    friend bool operator < (const BoxId& box1, const BoxId& box2);
    friend bool operator > (const BoxId& box1, const BoxId& box2);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_BoxId*/
