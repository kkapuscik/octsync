//-----------------------------------------------------------------------------
/// \file
/// ObjectBase - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ObjectBase
#define INCLUDED_OCTsync_ObjectBase

//-----------------------------------------------------------------------------

// local includes
#include "BoxType.hpp"
#include "BoxId.hpp"
#include "ByteArray.hpp"
#include "ByteArrayReader.hpp"
#include "ByteArrayWriter.hpp"

// library includes
#include <QtCore/QtGlobal>
#include <QtCore/QDebug>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
class ObjectFactoryManager;

/// TODO: Class documentation
///
class ObjectBase
{
    // To allow call to protected decode()
    // TODO: can this be fixed?
    friend class ContainerObject;

// Static constants (public, protected, private)
private:
    /// Size of the BOP object header.
    static const int HEADER_SIZE = 4 + 1 + 3 + 4;

    /// Maximum size of the object.
    ///
    /// \todo Change to an option.
    static const int MAX_BOP_SIZE = 64 * 1024;

// Types (public, protected, private)
public:
    enum class DecodeResult
    {
        /// Decoding finished with success.
        SUCCESS,
        /// There is not enough data to decode the object.
        NOT_ENOUGH_DATA,
        /// The given object size is too big and cannot be decoded.
        SIZE_TOO_BIG,
        /// Given box data is invalid.
        INVALID_DATA
    };

    enum class EncodeResult
    {
        SUCCESS,
        INVALID_DATA
    };

protected:
    struct BoxHeader
    {
        BoxId m_id;
        BoxType m_type;
        quint32 m_size;
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    BoxId m_id;
    BoxType m_type;

// Methods (public, protected, private)
public:
    ObjectBase(
            const BoxId& id,
            const BoxType& type);

    virtual ~ObjectBase();

    BoxId getId() const
    {
        return m_id;
    }

    BoxType getType() const
    {
        return m_type;
    }

    DecodeResult decode(
            ObjectFactoryManager& manager,
            const ByteArray& bytes,
            int& bytesNeeded);

    EncodeResult encode(
            ByteArray& bytes) const;

    virtual bool isValid() const = 0;

protected:
    DecodeResult decode(
            ObjectFactoryManager& manager,
            ByteArrayReader& reader);

    EncodeResult encodeInternal(
            ByteArrayWriter& writer) const;

    DecodeResult decodeSize(
            const ByteArray& bytes,
            int& bytesNeeded);

    DecodeResult decodeHeader(
            ByteArrayReader& reader,
            BoxHeader& header);

    virtual DecodeResult decodePayload(
            ObjectFactoryManager& manager,
            ByteArrayReader& payloadReader) = 0;

    virtual EncodeResult encodePayload(
            ByteArrayWriter& bytes) const = 0;
};

QDebug operator<<(
        QDebug dbg,
        const ObjectBase::EncodeResult& result);

QDebug operator<<(
        QDebug dbg,
        const ObjectBase::DecodeResult& result);

// TODO Documentation
typedef std::shared_ptr<ObjectBase> ObjectSharedPtr;
// TODO Documentation
typedef std::shared_ptr<const ObjectBase> ConstObjectSharedPtr;

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ObjectBase*/
