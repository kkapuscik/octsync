//-----------------------------------------------------------------------------
/// \file
/// BoxType - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_BoxType
#define INCLUDED_OCTsync_BoxType

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QtGlobal>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// Box types definitions.
///
class BoxType
{
// Static constants (public, protected, private)
public:
    /// Data box.
    static const quint8 DATA = 0x00;

    /// Container box.
    static const quint8 CONTAINER = 0x01;

private:
    /// Invalid box type.
    static const quint8 INVALID = 0xFF;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    quint8 m_type;

// Methods (public, protected, private)
public:
    BoxType()
    {
        setType(INVALID);
    }

    BoxType(
            quint8 type)
    {
        setType(type);
    }

    quint8 getType() const
    {
        return m_type;
    }

    void setType(
            quint8 type)
    {
        m_type = type;
    }

    bool isData() const
    {
        return m_type == DATA;
    }

    bool isContainer() const
    {
        return m_type == CONTAINER;
    }

    bool isValid() const
    {
        return isData() || isContainer();
    }

    friend bool operator !=(
            const BoxType& box1,
            const BoxType& box2)
    {
        return box1.m_type != box2.m_type;
    }

    friend bool operator ==(
            const BoxType& box1,
            const BoxType& box2)
    {
        return box1.m_type == box2.m_type;
    }
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_BoxType*/
