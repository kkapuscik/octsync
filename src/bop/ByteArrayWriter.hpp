//-----------------------------------------------------------------------------
/// \file
/// ByteArrayWriter - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ByteArrayWriter
#define INCLUDED_OCTsync_ByteArrayWriter

//-----------------------------------------------------------------------------

// local includes
#include "ByteArray.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ByteArrayWriter
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    ByteArray& m_byteArray;

// Methods (public, protected, private)
public:
    ByteArrayWriter(
            ByteArray& byteArray) :
                    m_byteArray(byteArray)
    {
        // nothing to do
    }

    int getSize() const
    {
        return m_byteArray.getSize();
    }

    void appendBoxId(
            const BoxId& id);

    void appendBoxType(
            const BoxType& type);

    void appendInt8(
            const qint8 value);

    void appendInt16(
            const qint16 value);

    void appendInt32(
            const qint32 value);

    void appendInt64(
            const qint64 value);

    void appendUint8(
            const quint8 value);

    void appendUint16(
            const quint16 value);

    void appendUint32(
            const quint32 value);

    void appendUint64(
            const quint64 value);

    void append(
            const QByteArray& array);

    void overwriteUint32(
            const int offset,
            const quint32 value);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ByteArrayWriter*/
