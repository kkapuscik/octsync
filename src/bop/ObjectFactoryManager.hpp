//-----------------------------------------------------------------------------
/// \file
/// ObjectFactoryManager - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ObjectFactoryManager
#define INCLUDED_OCTsync_ObjectFactoryManager

//-----------------------------------------------------------------------------

// local includes
#include "ObjectFactory.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QScopedPointer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ObjectFactoryManager : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QSet<ObjectFactory*> ObjectFactorySet;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    ObjectFactorySet m_factories;

// Methods (public, protected, private)
public:
    ObjectFactoryManager(
            QObject* parent = nullptr);

    void addObjectFactory(
            ObjectFactory* const factory);

    std::unique_ptr<ObjectBase> createObject(
            const BoxId& id) const;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ObjectFactoryManager*/
