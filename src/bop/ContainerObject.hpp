//-----------------------------------------------------------------------------
/// \file
/// ContainerObject - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ContainerObject
#define INCLUDED_OCTsync_ContainerObject

//-----------------------------------------------------------------------------

// local includes
#include "ObjectBase.hpp"

// library includes
#include <vector>
#include <memory>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

// TODO: introduce unique_ptr

/// TODO: Class documentation
///
class ContainerObject : public ObjectBase
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    typedef std::unique_ptr<ObjectBase> ObjectPtr;
    typedef std::vector<ObjectPtr> ObjectVector;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    ObjectVector m_children;

// Methods (public, protected, private)
public:
    ContainerObject(
            const BoxId& id);

    virtual ~ContainerObject();

    void appendChild(
            ObjectPtr child);

    void insertChild(
            const int index,
            ObjectPtr child);

    void deleteAllChildren();

    ObjectPtr removeChild(
            const int index);

    void deleteChild(
            const int index);

    int getChildCount() const;

    ObjectBase* getChild(
            const BoxId id) const;

    ObjectBase* getChild(
            const int index) const;

protected:
    virtual bool isValid() const;

    virtual DecodeResult decodePayload(
            ObjectFactoryManager& manager,
            ByteArrayReader& reader);

    virtual EncodeResult encodePayload(
            ByteArrayWriter& writer) const;

    virtual bool validateChildren() const = 0;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ContainerObject*/
