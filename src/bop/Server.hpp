//-----------------------------------------------------------------------------
/// \file
/// Server - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Server
#define INCLUDED_OCTsync_Server

//-----------------------------------------------------------------------------

// local includes
#include "ObjectBase.hpp"
#include "ObjectFactoryManager.hpp"
#include "impl/SslServerSocket.hpp"

// library includes
#include <QtCore/QObject>
#include <QtNetwork/QTcpServer>
#include <QtCore/QUrl>
#include <QtCore/QSet>
#include <QtCore/QPointer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
class ServerConnection;

/// TODO: Class documentation
///
class Server : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    typedef QSet<ServerConnection*> ServerConnectionSet;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SslServerSocket* m_serverSocket;
    QPointer<ObjectFactoryManager> m_factoryManager;
    ServerConnectionSet m_connections;

// Methods (public, protected, private)
public:
    Server(
            QObject* parent);

    void setObjectFactoryManager(
            QPointer<ObjectFactoryManager> factoryManager);

    virtual ~Server();

    void setMode(
            const bool secure,
            QSslCertificate sslCertificate,
            QSslKey sslKey);

    void enable();

    void disable();

    QUrl getUrl() const;

protected:
    virtual std::unique_ptr<ObjectBase> processRequest(
            const ObjectBase& request) = 0;

private:
    void processAcceptError(
            QAbstractSocket::SocketError socketError);

    void acceptNewConnection();

    void connectionFinished(
            ServerConnection* connection);

// TODO: clean this up
    friend class ServerConnection;
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Server*/
