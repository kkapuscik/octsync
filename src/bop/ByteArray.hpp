//-----------------------------------------------------------------------------
/// \file
/// ByteArray - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ByteArray
#define INCLUDED_OCTsync_ByteArray

//-----------------------------------------------------------------------------

// local includes
#include "BoxId.hpp"
#include "BoxType.hpp"

// library includes
#include <commons/OutOfBoundsException.hpp>
#include <QtCore/QByteArray>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ByteArray
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QByteArray m_array;

// Methods (public, protected, private)
public:
    ByteArray()
    {
        // nothing to do
    }

    ByteArray(
            const ByteArray& other) :
                    m_array(other.m_array)
    {
        // nothing to do
    }

    void clear()
    {
        m_array.clear();
    }

    char* getData()
    {
        return m_array.data();
    }

    const char* getConstData() const
    {
        return m_array.constData();
    }

    int getSize() const
    {
        return m_array.size();
    }

    quint8 get(
            const int index) const
    {
        if ((index < 0) || (index >= getSize()))
        {
            throw Commons::OutOfBoundsException("Index outside array", index, getSize());
        }

        return m_array[index];
    }

    void set(
            const int index,
            const quint8 value)
    {
        if ((index < 0) || (index >= getSize()))
        {
            throw Commons::OutOfBoundsException("Index outside array", index, getSize());
        }

        m_array[index] = value;
    }

    void append(
            const quint8 value)
    {
        m_array.append(value);
    }

    void append(
            const QByteArray& array)
    {
        m_array.append(array);
    }

    void append(
            const char* const data,
            const int size)
    {
        Q_ASSERT(data);
        Q_ASSERT(size >= 0);

        m_array.append(data, size);
    }

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ByteArray*/
