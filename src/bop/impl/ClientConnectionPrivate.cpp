//-----------------------------------------------------------------------------
/// \file
/// ClientConnectionPrivate - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ClientConnectionPrivate.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// TODO: there is something broken with the timer
//       it does not seem to react to timeout() signal
ClientConnectionPrivate::ClientConnectionPrivate(
        const QUrl& url,
        QObject* parent) :
                QObject(parent),
                m_url(url),
                m_connected(false)
{
    m_socket = new QSslSocket(this);

    m_connectRetryTimer = new QTimer(this);
    m_connectRetryTimer->setSingleShot(true);

    // connect socket signals
    connect(m_socket, &QTcpSocket::connected, this, &ClientConnectionPrivate::onSocketConnected);
    connect(m_socket, &QTcpSocket::disconnected, this,
            &ClientConnectionPrivate::onSocketDisconnected);
    connect(m_socket, &QTcpSocket::readyRead, this, &ClientConnectionPrivate::onSocketReadyRead);
    connect(m_socket, &QTcpSocket::bytesWritten, this,
            &ClientConnectionPrivate::onSocketBytesWritten);

    void (QTcpSocket::*errorMethod)(
            QAbstractSocket::SocketError) = &QTcpSocket::error;
    connect(m_socket, errorMethod, this, &ClientConnectionPrivate::onSocketError);

    void (QSslSocket::*sslErrorMethod)(
            const QList<QSslError>&) = &QSslSocket::sslErrors;
    connect(m_socket, sslErrorMethod, this, &ClientConnectionPrivate::onSslErrors);
}

void ClientConnectionPrivate::setObjectFactoryManager(
        QPointer<ObjectFactoryManager> manager)
{
    m_objectFactoryManager = manager;
}

void ClientConnectionPrivate::connectToServer()
{
    if (!m_connected)
    {
        m_connected = true;

        // reset the timer interval
        m_connectRetryTimer->setInterval(DEFAULT_RETRY_INTERVAL_MS);

        // connect
        bool urlValid = false;
        const QString scheme = m_url.scheme();
        const QString host = m_url.host();
        const int port = m_url.port();
        if (!host.isEmpty() && port != 0)
        {
            if (scheme == "bop")
            {
                urlValid = true;
                m_socket->connectToHost(host, port);
            }
            else if (scheme == "sbop")
            {
                urlValid = true;

                m_socket->connectToHostEncrypted(host, port);
            }
        }

        if (!urlValid)
        {
            qDebug() << "Cannot connect to host - invalid URL: " << m_url;
        }
    }
}

void ClientConnectionPrivate::disconnectFromServer()
{
    if (m_connected)
    {
        m_connected = false;

        // abort all socket actions
        m_socket->abort();

        // if there is connect retry timer enabled, abort it too
        m_connectRetryTimer->stop();
    }
}

bool ClientConnectionPrivate::isSendingObject()
{
    return (bool) m_objectToSend;
}

void ClientConnectionPrivate::setObjectToSend(
        ConstObjectSharedPtr object)
{
    Q_ASSERT(!isSendingObject());

    m_objectToSend = object;
    m_writer.setObject(object);

    // try to write something
    onSocketBytesWritten(0);
}

void ClientConnectionPrivate::onSocketConnected()
{
    if (m_connected)
    {
        emit connected();

        // try to write something
        onSocketBytesWritten(0);
    }
}

void ClientConnectionPrivate::onSocketDisconnected()
{
    if (m_connected)
    {
        emit disconnected();

        // schedule retry timer
        m_connectRetryTimer->setInterval(m_connectRetryTimer->interval() * 2);
        m_connectRetryTimer->start();

        // reset writer
        m_writer.setObject(m_objectToSend);
        m_reader.reset();
    }
}

void ClientConnectionPrivate::onSocketReadyRead()
{
    while (m_socket->bytesAvailable() > 0)
    {
        if (!m_objectFactoryManager)
        {
            // TODO
            m_socket->abort();
            return;
        }

        switch (m_reader.read(*m_socket, *m_objectFactoryManager))
        {
            case ObjectReader::ReadResult::COMPLETE:
            {
                ConstObjectSharedPtr object = m_reader.takeObject();

                emit objectReceived(object);

                break;
            }

            case ObjectReader::ReadResult::INCOMPLETE:
                // do nothing
                break;

            case ObjectReader::ReadResult::ERROR:
            default:
                // TODO
                m_socket->abort();
                return;
        }

    }
}

void ClientConnectionPrivate::onSocketBytesWritten(
        qint64 bytes)
{
    if (!m_objectToSend)
    {
        return;
    }

    if (m_socket->state() != QAbstractSocket::ConnectedState)
    {
        return;
    }

    // calculate maximum number of bytes to write
    qint64 maxBytes = SOCKET_WRITE_LIMIT - m_socket->bytesToWrite();
    if (maxBytes > 0)
    {
        switch (m_writer.write(*m_socket, maxBytes))
        {
            case ObjectWriter::WriteResult::COMPLETE:
            {
                // take the object
                ConstObjectSharedPtr object = m_objectToSend;
                m_objectToSend.reset();

                // reset the writer
                m_writer.setObject(m_objectToSend);

                // object completely written - report
                emit objectSendFinished(object, Error::NONE);
                break;
            }

            case ObjectWriter::WriteResult::INCOMPLETE:
                // nothing to do
                break;

            case ObjectWriter::WriteResult::ERROR:
            default:
            {
                // take the object
                ConstObjectSharedPtr object = m_objectToSend;
                m_objectToSend.reset();

                // reset the writer
                m_writer.setObject(m_objectToSend);

                // notify about error
                emit objectSendFinished(m_objectToSend, Error::WRITE);

                break;
            }
        }
    }
}

void ClientConnectionPrivate::onSocketError(
        QAbstractSocket::SocketError socketError)
{
    qDebug() << "ClientConnectionPrivate::onSocketError " << socketError << " "
            << m_socket->errorString();
}

void ClientConnectionPrivate::onSslErrors(
        const QList<QSslError>& errors)
{
    bool allIgnored = true;

    for (const auto sslError : errors)
    {
        qDebug() << "SSL error: " << (int) sslError.error() << " " << sslError;

        switch (sslError.error())
        {
            case QSslError::NoError:
            case QSslError::SelfSignedCertificate:
            case QSslError::HostNameMismatch:
                break;
            default:
                allIgnored = false;
                break;
        }
    }

    if (allIgnored)
    {
        m_socket->ignoreSslErrors();
    }

#if 0
    static QList<QSslError> ignoredSslErrors;
    if (ignoredSslErrors.size() == 0)
    {
        ignoredSslErrors.append(QSslError::SelfSignedCertificate);
        ignoredSslErrors.append(QSslError::HostNameMismatch);
    }

    for (const auto sslError : ignoredSslErrors)
    {
        qDebug() << "Ignored error: " << (int) sslError.error() << " " << sslError;
    }

    m_socket->ignoreSslErrors(ignoredSslErrors);
#endif
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
