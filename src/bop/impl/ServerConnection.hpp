//-----------------------------------------------------------------------------
/// \file
/// ServerConnection - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ServerConnection
#define INCLUDED_OCTsync_ServerConnection

//-----------------------------------------------------------------------------

// local includes
#include "../ObjectBase.hpp"

// library includes
#include <QtCore/QObject>
#include <QtNetwork/QTcpSocket>
#include <QtCore/QScopedPointer>
#include <QtCore/QPointer>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
class Server;

/// TODO: Class documentation
///
class ServerConnection : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    enum class ProcessingResult
    {
        CONTINUE,
        TERMINATE,
        ERROR
    };

    enum class State
    {
        READING,
        WRITING,
        TERMINATED
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    State m_state;
    QPointer<Server> m_server;
    QTcpSocket* const m_socket;
    ByteArray m_readData;
    ByteArray m_writeData;
    int m_writeOffset;

    std::unique_ptr<ObjectBase> m_currentRequest;
    std::unique_ptr<ObjectBase> m_currentResponse;

// Methods (public, protected, private)
public:
    ServerConnection(
            QTcpSocket* const clientSocket,
            QPointer<Server> server);

private:
    State getState() const;

    void setState(
            const State newState);

    void onSocketDisconnect();

    void onSocketBytesWritten(
            qint64 bytes);
    void onSocketReadChannelFinished();
    void onSocketReadyRead();
    void onSocketReadyWrite();

    void processAllDataWritten();

    ProcessingResult readObjectId();
    ProcessingResult readObject();
    ProcessingResult readMoreData(
            const int bytes);

    ProcessingResult processObject();

    bool prepareDataToSend();
    bool hasDataToWrite();

    void terminate();

    void onSocketError(
            QAbstractSocket::SocketError socketError);

signals:
    void resumeRead();
    void resumeWrite();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ServerConnection*/
