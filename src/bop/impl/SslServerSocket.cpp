//-----------------------------------------------------------------------------
/// \file
/// SslServerSocket - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SslServerSocket.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

SslServerSocket::SslServerSocket(
        QObject* const parent)
    : QTcpServer(parent),
      m_secure(false)
{
    // nothing
}

SslServerSocket::~SslServerSocket()
{
    // nothing
}

bool SslServerSocket::isSecure() const
{
    return m_secure;
}

void SslServerSocket::setMode(
        const bool secure,
        QSslCertificate sslCertificate,
        QSslKey sslKey)
{
    if (secure)
    {
        Q_ASSERT(!sslCertificate.isNull());
        Q_ASSERT(!sslKey.isNull());
    }

    m_secure = secure;
    m_sslCertificate = sslCertificate;
    m_sslKey = sslKey;
}

QTcpSocket* SslServerSocket::createConnectionSocket(
        const qintptr handle)
{
    if (m_secure)
    {
        return createSecureConnectionSocket(handle);
    }
    else
    {
        return createPlainConnectionSocket(handle);
    }
}

QTcpSocket* SslServerSocket::createSecureConnectionSocket(
        const qintptr handle)
{
    auto const socket = new QSslSocket(this);
    socket->setSocketDescriptor(handle);

    socket->setPrivateKey(m_sslKey);
    socket->setLocalCertificate(m_sslCertificate);
    socket->startServerEncryption();

    return socket;
}

QTcpSocket* SslServerSocket::createPlainConnectionSocket(
        const qintptr handle)
{
    auto const socket = new QTcpSocket(this);
    socket->setSocketDescriptor(handle);
    return socket;
}

void SslServerSocket::incomingConnection(
        const qintptr handle)
{
    auto const socket = createConnectionSocket(handle);
    addPendingConnection(socket);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
