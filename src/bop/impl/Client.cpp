//-----------------------------------------------------------------------------
/// \file
/// Client - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../Client.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

Client::Client(
        QPointer<ObjectFactoryManager> factoryManager,
        QObject* parent) :
                QObject(parent),
                m_factoryManager(factoryManager)
{
    // nothing to do
}

void Client::abortConnection(
        const QUrl& url)
{
    ClientConnection* connection = m_connections.value(url, nullptr);
    if (connection)
    {
        m_connections.remove(url);

        connection->disconnectFromServer();
        // TODO: aborting all communication
        connection->deleteLater();
    }
}

ClientConnection* Client::getConnection(
        const QUrl& url,
        const bool createFlag)
{
    ClientConnection* connection = m_connections.value(url, nullptr);
    if (!connection && createFlag)
    {
        connection = new ClientConnection(m_factoryManager, url, this);
        m_connections.insert(url, connection);

        connect(connection, &QObject::destroyed, this, &Client::onConnectionDestroyed);
        connect(connection, &ClientConnection::stateChanged, this, &Client::connectionStateChanged);
        connect(connection, &ClientConnection::objectReceived, this, &Client::objectReceived);
        connect(connection, &ClientConnection::objectSendFinished, this,
                &Client::objectSendFinished);

        connection->connectToServer();
    }

    return connection;
}

void Client::onConnectionDestroyed(
        QObject* obj)
{
    ClientConnection* const connection = reinterpret_cast<ClientConnection*>(obj);

    m_connections.remove(connection->getUrl());
}

bool Client::sendObject(
        const QUrl& url,
        const bool createFlag,
        ConstObjectSharedPtr object)
{
    ClientConnection* connection = getConnection(url, createFlag);
    if (connection)
    {
        connection->sendObject(object);
        return true;
    }
    else
    {
        return false;
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
