//-----------------------------------------------------------------------------
/// \file
/// ObjectWriter - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ObjectWriter
#define INCLUDED_OCTsync_ObjectWriter

//-----------------------------------------------------------------------------

// local includes
#include "../ObjectBase.hpp"
#include "../ByteArray.hpp"

// library includes
#include <QtNetwork/QTcpSocket>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ObjectWriter
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    /// Result of the write operation.
    enum class WriteResult
    {
        /// All request data was written.
        COMPLETE,
        /// Not all request data was written.
        INCOMPLETE,
        /// Error occurred during write operation.
        ERROR
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    ByteArray m_buffer;
    int m_offset;

// Methods (public, protected, private)
public:
    ObjectWriter();

    bool setObject(
            ConstObjectSharedPtr object);

    WriteResult write(
            QTcpSocket& socket,
            const qint64 maxBytes);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ObjectWriter*/
