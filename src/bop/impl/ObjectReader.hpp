//-----------------------------------------------------------------------------
/// \file
/// ObjectReader - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ObjectReader
#define INCLUDED_OCTsync_ObjectReader

//-----------------------------------------------------------------------------

// local includes
#include "../ByteArray.hpp"
#include "../ObjectFactoryManager.hpp"

// library includes
#include <QtNetwork/QTcpSocket>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ObjectReader
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    /// Result of the read operation.
    enum class ReadResult
    {
        /// All response data was read.
        COMPLETE,
        /// Not all response data was read.
        INCOMPLETE,
        /// Error occurred during read operation.
        ERROR
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    ByteArray m_buffer;
    ObjectSharedPtr m_object;

// Methods (public, protected, private)
public:
    ObjectReader();

    void reset();

    ConstObjectSharedPtr takeObject();

    ReadResult read(
            QTcpSocket& socket,
            ObjectFactoryManager& factoryManager);

private:
    ReadResult readObjectId(
            QTcpSocket& socket,
            ObjectFactoryManager& factoryManager);

    ReadResult readObject(
            QTcpSocket& socket,
            ObjectFactoryManager& factoryManager);

    ReadResult readMoreData(
            QTcpSocket& socket,
            const int byteCount);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ObjectReader*/
