//-----------------------------------------------------------------------------
/// \file
/// ObjectFactoryManager - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ObjectFactoryManager.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ObjectFactoryManager::ObjectFactoryManager(
        QObject* parent) :
                QObject(parent)
{
    // nothing to do
}

void ObjectFactoryManager::addObjectFactory(
        ObjectFactory* const factory)
{
    Q_ASSERT(factory);

    factory->setParent(this); // TODO this shall be removed, user shall decided when it will be deleted

    m_factories.insert(factory);
}

std::unique_ptr<ObjectBase> ObjectFactoryManager::createObject(
        const BoxId& id) const
{
    std::unique_ptr<ObjectBase> result;

    ObjectFactorySet::ConstIterator curIter, endIter;
    for (curIter = m_factories.constBegin(), endIter = m_factories.constEnd(); curIter != endIter;
            ++curIter)
    {
        result = (*curIter)->createObject(id);
        if (result)
        {
            break;
        }
    }

    return result;
}

}
// end of namespace
}// end of namespace

//-----------------------------------------------------------------------------
