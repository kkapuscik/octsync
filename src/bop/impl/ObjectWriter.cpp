//-----------------------------------------------------------------------------
/// \file
/// ObjectWriter - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ObjectWriter.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ObjectWriter::ObjectWriter() : m_offset(0)
{
    // nothing to do
}

bool ObjectWriter::setObject(
        ConstObjectSharedPtr object)
{
    bool result = false;

    m_buffer.clear();
    m_offset = 0;

    if (object)
    {
        ObjectBase::EncodeResult encodeResult = object->encode(m_buffer);
        qDebug() << "- encode result: " << encodeResult;

        if (encodeResult == ObjectBase::EncodeResult::SUCCESS)
        {
            result = true;
            qDebug() << "- encoded message size: " << m_buffer.getSize();
        }
        else
        {
            qDebug() << "- message encoding failed: " << encodeResult;
        }
    }
    else
    {
        result = true;
        qDebug() << "- writer reset";
    }

    return result;
}

ObjectWriter::WriteResult ObjectWriter::write(
        QTcpSocket& socket,
        const qint64 maxBytes)
{
    WriteResult result = WriteResult::ERROR;

    Q_ASSERT(m_offset <= m_buffer.getSize());

    qint64 bytesToWrite = m_buffer.getSize() - m_offset;
    if (bytesToWrite > maxBytes)
    {
        bytesToWrite = maxBytes;
    }

    if (bytesToWrite > 0)
    {
        qint64 bytesWritten = socket.write(m_buffer.getConstData() + m_offset,
                bytesToWrite);
        if (bytesWritten >= 0)
        {
            m_offset += bytesWritten;

            Q_ASSERT(m_offset <= m_buffer.getSize());

            if (m_offset < m_buffer.getSize())
            {
                result = WriteResult::INCOMPLETE;
            }
            else
            {
                result = WriteResult::COMPLETE;
            }
        }
    }
    else
    {
        result = WriteResult::COMPLETE;
    }

    if (result != WriteResult::INCOMPLETE)
    {
        m_buffer.clear();
        m_offset = 0;
    }

    return result;
}


}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
