//-----------------------------------------------------------------------------
/// \file
/// ClientConnectionPrivate - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ClientConnectionPrivate
#define INCLUDED_OCTsync_ClientConnectionPrivate

//-----------------------------------------------------------------------------

// local includes
#include "../ObjectBase.hpp"
#include "../Error.hpp"
#include "ObjectWriter.hpp"
#include "ObjectReader.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QTimer>
#include <QtCore/QPointer>
#include <QtNetwork/QSslSocket>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ClientConnectionPrivate : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const qint64 SOCKET_WRITE_LIMIT = 16 * 1024;

    static const int DEFAULT_RETRY_INTERVAL_MS = 200;
    static const int MAX_RETRY_INTERVAL_MS = 30 * 1000;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QUrl m_url;
    bool m_connected;
    ConstObjectSharedPtr m_objectToSend;
    QSslSocket* m_socket;
    QTimer* m_connectRetryTimer;

    QPointer<ObjectFactoryManager> m_objectFactoryManager;

    ObjectWriter m_writer;
    ObjectReader m_reader;

// Methods (public, protected, private)
public:
    ClientConnectionPrivate(
            const QUrl& url,
            QObject* parent = nullptr);

    const QUrl& getUrl() const
    {
        return m_url;
    }

    void setObjectFactoryManager(
            QPointer<ObjectFactoryManager> manager);

    void connectToServer();

    void disconnectFromServer();

    bool isSendingObject();

    void setObjectToSend(
            ConstObjectSharedPtr object);

private:
    void onSocketConnected();

    void onSocketDisconnected();

    void onSocketReadyRead();

    void onSocketBytesWritten(
            qint64 bytes);

    void onSocketError(
            QAbstractSocket::SocketError socketError);

    void onSslErrors(
            const QList<QSslError> &errors);

signals:
    void objectSendFinished(
            ConstObjectSharedPtr object,
            Error error);

    void objectReceived(
            ConstObjectSharedPtr object);

    void connected();

    void disconnected();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ClientConnectionPrivate*/
