//-----------------------------------------------------------------------------
/// \file
/// ContainerObject - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ContainerObject.hpp"
#include "../ObjectFactoryManager.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ContainerObject::ContainerObject(
        const BoxId& id) :
                ObjectBase(id, BoxType::CONTAINER)
{
    // nothing to do
}

ContainerObject::~ContainerObject()
{
    deleteAllChildren();
}

void ContainerObject::appendChild(
        ObjectPtr child)
{
    Q_ASSERT(child);

    m_children.push_back(std::move(child));
}

void ContainerObject::insertChild(
        const int index,
        ObjectPtr child)
{
    Q_ASSERT(child);
    Q_ASSERT(index >= 0 && index <= getChildCount());

    m_children.insert(m_children.begin() + index, std::move(child));
}

void ContainerObject::deleteAllChildren()
{
    m_children.clear();
}

ContainerObject::ObjectPtr ContainerObject::removeChild(
        const int index)
{
    ObjectPtr object;

    if ((index >= 0) && (index < m_children.size()))
    {
        object = std::move(m_children.at(index));
        m_children.erase(m_children.begin() + index);
    }

    return object;
}

void ContainerObject::deleteChild(
        const int index)
{
    // take the pointer and forget it
    removeChild(index);
}

int ContainerObject::getChildCount() const
{
    return m_children.size();
}

ObjectBase* ContainerObject::getChild(
        const BoxId id) const
{
    ObjectBase* object = nullptr;

    for (int i = 0; i < m_children.size(); ++i)
    {
        if (m_children.at(i)->getId() == id)
        {
            object = m_children.at(i).get();
            break;
        }
    }

    return object;
}

ObjectBase* ContainerObject::getChild(
        const int index) const
{
    ObjectBase* object = nullptr;

    if ((index >= 0) && (index < m_children.size()))
    {
        object = m_children.at(index).get();
    }

    return object;
}

bool ContainerObject::isValid() const
{
    bool result = true;

    for (int i = 0; i < m_children.size(); ++i)
    {
        result &= m_children.at(i)->isValid();
        if (!result)
        {
            break;
        }
    }

    if (result)
    {
        result &= validateChildren();
    }

    return result;
}

ObjectBase::DecodeResult ContainerObject::decodePayload(
        ObjectFactoryManager& manager,
        ByteArrayReader& reader)
{
    for (int offset = 0; offset < reader.getSize();)
    {
        BoxHeader childHeader;

        // try to decode child header
        ByteArrayReader headerReader(reader, offset);
        DecodeResult headerResult = decodeHeader(headerReader, childHeader);
        if (headerResult != DecodeResult::SUCCESS)
        {
            return headerResult;
        }

        // create child
        std::unique_ptr<ObjectBase> child = manager.createObject(childHeader.m_id);
        if (!child)
        {
            return DecodeResult::INVALID_DATA;
        }

        // try to decode child
        ByteArrayReader childReader(reader, offset, childHeader.m_size);
        DecodeResult childResult = child->decode(manager, childReader);
        if (childResult != DecodeResult::SUCCESS)
        {
            return childResult;
        }

        appendChild(std::move(child));

        offset += childHeader.m_size;
    }

    // TODO verify children

    return DecodeResult::SUCCESS;
}

ObjectBase::EncodeResult ContainerObject::encodePayload(
        ByteArrayWriter& writer) const
{
    EncodeResult result = EncodeResult::SUCCESS;

    for (int i = 0; i < m_children.size(); ++i)
    {
        result = m_children.at(i)->encodeInternal(writer);
        if (result != EncodeResult::SUCCESS)
        {
            break;
        }
    }

    return result;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
