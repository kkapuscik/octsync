//-----------------------------------------------------------------------------
/// \file
/// Server - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../Server.hpp"
#include "ServerConnection.hpp"
#include "SslServerSocket.hpp"

// library includes
#include <QtCore/QThread>
#include <QtCore/QFile>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QNetworkInterface>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

Server::Server(
        QObject* parent) :
                QObject(parent)
{
    m_serverSocket = new SslServerSocket(this);
    connect(m_serverSocket, &QTcpServer::newConnection, this, &Server::acceptNewConnection);
    connect(m_serverSocket, &QTcpServer::acceptError, this, &Server::processAcceptError);
}

void Server::setObjectFactoryManager(
        QPointer<ObjectFactoryManager> factoryManager)
{
    m_factoryManager = factoryManager;
}

Server::~Server()
{
    // 'free' connection threads
    // TODO
}

void Server::setMode(
        const bool secure,
        QSslCertificate sslCertificate,
        QSslKey sslKey)
{
    m_serverSocket->setMode(secure, sslCertificate, sslKey);
}

void Server::enable()
{
    if (m_serverSocket->listen())
    {
        qDebug() << "Server running on port: " << m_serverSocket->serverPort();
    }
}

void Server::disable()
{
    m_serverSocket->close();
}

QUrl Server::getUrl() const
{
    QUrl url;

    if (m_serverSocket->isSecure())
    {
        url.setScheme("sbop"); // TODO: extract this schemes to some general constants
    }
    else
    {
        url.setScheme("bop");
    }
    url.setHost("localhost");
    url.setPort(m_serverSocket->serverPort());

    return url;
}

void Server::processAcceptError(
        QAbstractSocket::SocketError socketError)
{
    qDebug() << "Server::processAcceptError()" << socketError;

    // TODO
}

void Server::acceptNewConnection()
{
    QTcpSocket* clientSocket = m_serverSocket->nextPendingConnection();
    if (clientSocket)
    {
        ServerConnection* newConnection = new ServerConnection(clientSocket, this);

        // store connection/thread to stop them on disable()/destructor() call
        m_connections.insert(newConnection);

        // TODO limit number of connections using:
        // m_serverSocket->resumeAccepting();
        // m_serverSocket->pauseAccepting();
    }
    else
    {
        qDebug() << "Server failed to accept connection";
    }
}

void Server::connectionFinished(
        ServerConnection* connection)
{
    qDebug() << "Connection finished";

    m_connections.remove(connection);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
