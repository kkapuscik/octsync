//-----------------------------------------------------------------------------
/// \file
/// ByteArrayReader - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ByteArrayReader.hpp"

// library includes
#include <commons/FatalException.hpp>
#include <commons/OutOfBoundsException.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Commons::FatalException;
using OCTsync::Commons::OutOfBoundsException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ByteArrayReader::ByteArrayReader(
        const ByteArray& byteArray) :
                m_byteArray(byteArray)
{
    Init(0, m_byteArray.getSize(), 0, m_byteArray.getSize());
}

ByteArrayReader::ByteArrayReader(
        const ByteArray& byteArray,
        const int offset) :
                m_byteArray(byteArray)
{
    Init(offset, m_byteArray.getSize() - offset, 0, m_byteArray.getSize());
}

ByteArrayReader::ByteArrayReader(
        const ByteArray& byteArray,
        int offset,
        int length) :
                m_byteArray(byteArray)
{
    Init(offset, length, 0, m_byteArray.getSize());
}

ByteArrayReader::ByteArrayReader(
        const ByteArrayReader& reader) :
                m_byteArray(reader.m_byteArray)
{
    Init(0, reader.m_rangeLength, reader.m_rangeOffset, reader.m_rangeLength);
}

ByteArrayReader::ByteArrayReader(
        const ByteArrayReader& reader,
        int offset) :
                m_byteArray(reader.m_byteArray)
{
    Init(offset, reader.m_rangeLength - offset, reader.m_rangeOffset, reader.m_rangeLength);
}

ByteArrayReader::ByteArrayReader(
        const ByteArrayReader& reader,
        int offset,
        int length) :
                m_byteArray(reader.m_byteArray)
{
    Init(offset, length, reader.m_rangeOffset, reader.m_rangeLength);
}

void ByteArrayReader::Init(
        const int relativeRangeOffset,
        const int relativeRangeLength,
        const int parentRangeOffset,
        const int parentRangeLength)
{
    if (relativeRangeOffset < 0)
    {
        throw FatalException("Negative offset");
    }
    if (relativeRangeLength < 0)
    {
        throw FatalException("Negative length");
    }

    // calculate reader range
    m_rangeOffset = relativeRangeOffset + parentRangeOffset;
    m_rangeLength = relativeRangeLength;

    // check if fits inside parent range
    if (m_rangeOffset + m_rangeLength > parentRangeOffset + parentRangeLength)
    {
        throw OutOfBoundsException("Reader range outside parent range",
                m_rangeOffset + m_rangeLength > parentRangeOffset + parentRangeLength);
    }

    // check against array
    if (m_rangeOffset > m_byteArray.getSize())
    {
        throw OutOfBoundsException("Range start outside array", m_rangeOffset,
                m_byteArray.getSize());
    }
    if (m_rangeOffset + m_rangeLength > m_byteArray.getSize())
    {
        throw OutOfBoundsException("Range end outside array", m_rangeOffset + m_rangeLength,
                m_byteArray.getSize());
    }

    m_position = 0;
}

bool ByteArrayReader::hasMoreData() const
{
    return m_position < m_rangeLength;
}

void ByteArrayReader::setPosition(
        const int position)
{
    if (position < 0)
    {
        throw OutOfBoundsException("Negative position", position);
    }
    if (position > m_rangeLength)
    {
        throw OutOfBoundsException("Position outside reader range", position, m_rangeLength);
    }

    m_position = position;
}

void ByteArrayReader::skip(
        const int bytes)
{
    if (bytes < 0)
    {
        throw FatalException("Negative byte count");
    }

    setPosition(m_position + bytes);
}

quint8 ByteArrayReader::getNextByte()
{
    if (m_position >= m_rangeLength)
    {
        throw OutOfBoundsException("Position outside reader range", m_position, m_rangeLength);
    }

    return m_byteArray.get(m_rangeOffset + m_position++);
}

BoxId ByteArrayReader::decodeBoxId()
{
    const char char1 = static_cast<char>(getNextByte());
    const char char2 = static_cast<char>(getNextByte());
    const char char3 = static_cast<char>(getNextByte());
    const char char4 = static_cast<char>(getNextByte());

    return BoxId(char1, char2, char3, char4);
}

BoxType ByteArrayReader::decodeBoxType()
{
    const quint8 typeByte = getNextByte();

    return BoxType(typeByte);
}

quint8 ByteArrayReader::decodeUint8()
{
    return getNextByte();
}

quint16 ByteArrayReader::decodeUint16()
{
    const quint8 byte1 = getNextByte();
    const quint8 byte2 = getNextByte();

    // big endian
    quint16 value;
    value = ((quint16) byte1) << 8;
    value |= ((quint16) byte2) << 0;

    return value;
}

quint32 ByteArrayReader::decodeUint32()
{
    const quint8 byte1 = getNextByte();
    const quint8 byte2 = getNextByte();
    const quint8 byte3 = getNextByte();
    const quint8 byte4 = getNextByte();

    // big endian
    quint32 value;
    value = ((quint32) byte1) << 24;
    value |= ((quint32) byte2) << 16;
    value |= ((quint32) byte3) << 8;
    value |= ((quint32) byte4) << 0;

    return value;
}

quint64 ByteArrayReader::decodeUint64()
{
    const quint8 byte1 = getNextByte();
    const quint8 byte2 = getNextByte();
    const quint8 byte3 = getNextByte();
    const quint8 byte4 = getNextByte();
    const quint8 byte5 = getNextByte();
    const quint8 byte6 = getNextByte();
    const quint8 byte7 = getNextByte();
    const quint8 byte8 = getNextByte();

    // big endian
    quint64 value;
    value = ((quint64) byte1) << 56;
    value |= ((quint64) byte2) << 48;
    value |= ((quint64) byte3) << 40;
    value |= ((quint64) byte4) << 32;
    value |= ((quint64) byte5) << 24;
    value |= ((quint64) byte6) << 16;
    value |= ((quint64) byte7) << 8;
    value |= ((quint64) byte8) << 0;

    return value;
}

qint8 ByteArrayReader::decodeInt8()
{
    return static_cast<qint8>(decodeUint8());
}

qint16 ByteArrayReader::decodeInt16()
{
    return static_cast<qint16>(decodeUint16());
}

qint32 ByteArrayReader::decodeInt32()
{
    return static_cast<qint32>(decodeUint32());
}

qint64 ByteArrayReader::decodeInt64()
{
    return static_cast<qint64>(decodeUint64());
}

QByteArray ByteArrayReader::decodeByteArray(
        const int size)
{
    Q_ASSERT(size >= 0);

    QByteArray retVal;

    retVal.resize(size);

    for (int i = 0; i < size; ++i)
    {
        retVal[i] = getNextByte();
    }

    return retVal;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
