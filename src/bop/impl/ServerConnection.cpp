//-----------------------------------------------------------------------------
/// \file
/// ServerConnection - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ServerConnection.hpp"
#include "Server.hpp"

// library includes
#include <QtCore/QThread>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ServerConnection::ServerConnection(
        QTcpSocket* const clientSocket,
        QPointer<Server> server) :
                QObject(server),
                m_state(State::READING),
                m_server(server),
                m_socket(clientSocket),
                m_writeOffset(0)
{
    clientSocket->setParent(this);

    // connect thread signals
    connect(m_socket, &QTcpSocket::disconnected, this, &ServerConnection::onSocketDisconnect);
    connect(m_socket, &QTcpSocket::bytesWritten, this, &ServerConnection::onSocketBytesWritten);
    connect(m_socket, &QTcpSocket::readChannelFinished, this,
            &ServerConnection::onSocketReadChannelFinished);
    connect(m_socket, &QTcpSocket::readyRead, this, &ServerConnection::onSocketReadyRead);

    void (QTcpSocket::*errorMethod)(
            QAbstractSocket::SocketError) = &QTcpSocket::error;
    connect(m_socket, errorMethod, this, &ServerConnection::onSocketError);

    // connect the extra signals
    connect(this, &ServerConnection::resumeRead, this, &ServerConnection::onSocketReadyRead,
            Qt::QueuedConnection);
    connect(this, &ServerConnection::resumeWrite, this, &ServerConnection::onSocketReadyWrite,
            Qt::QueuedConnection);
}

ServerConnection::State ServerConnection::getState() const
{
    return m_state;
}

void ServerConnection::setState(
        const State newState)
{
    if (m_state != newState)
    {
        Q_ASSERT(m_state != State::TERMINATED);

        m_state = newState;

        switch (m_state)
        {
            case State::READING:
                emit resumeRead();
                break;
            case State::WRITING:
                emit resumeWrite();
                break;
            case State::TERMINATED:
                terminate();
                break;
            default:
                Q_ASSERT(0);
                break;
        }
    }
}

void ServerConnection::onSocketDisconnect()
{
    qDebug() << "onSocketDisconnect()";

    // TODO: is that correct? What if there is data to read?
    setState(State::TERMINATED);
}

void ServerConnection::onSocketBytesWritten(
        qint64 bytes)
{
    qDebug() << "onSocketBytesWritten() " << bytes;

    onSocketReadyWrite();
}

void ServerConnection::processAllDataWritten()
{
    m_writeData.clear();
    m_writeOffset = 0;

    m_currentResponse.reset();

    setState(State::READING);
}

void ServerConnection::onSocketReadyWrite()
{
    qDebug() << "ServerConnection::onSocketReadyWrite()";

    while (getState() == State::WRITING)
    {
        const qint64 MAX_BYTES_TO_WRITE = 16 * 1024;

        const qint64 bytesToWrite = m_socket->bytesToWrite();
        const qint64 writeSpaceLeft = MAX_BYTES_TO_WRITE - bytesToWrite;

        if (writeSpaceLeft > 0)
        {
            qDebug() << "- write space left = " << writeSpaceLeft;

            qint64 bytesLeftToWrite = m_writeData.getSize() - m_writeOffset;
            if (bytesLeftToWrite > writeSpaceLeft)
            {
                bytesLeftToWrite = writeSpaceLeft;
            }

            qint64 bytesWritten = m_socket->write(m_writeData.getConstData() + m_writeOffset,
                    bytesLeftToWrite);

            qDebug() << "- requested = " << bytesLeftToWrite << " written = " << bytesWritten;

            if (bytesWritten < 0)
            {
                setState(State::TERMINATED);
            }
            else if (bytesWritten == 0)
            {
                // strange
                break;
            }
            else
            {
                m_writeOffset += bytesWritten;

                if (m_writeOffset == m_writeData.getSize())
                {
                    processAllDataWritten();
                }
            }
        }
        else
        {
            break;
        }
    }
}

void ServerConnection::onSocketReadChannelFinished()
{
    qDebug() << "onSocketReadChannelFinished()";
}

void ServerConnection::onSocketReadyRead()
{
    qDebug() << "ServerConnection::onSocketReadyRead()";

    // TODO: clean up this function, it seems to be over-complicated

    // wait until response is sent
    while (getState() == State::READING)
    {
        ProcessingResult result = ProcessingResult::CONTINUE;

        while (result == ProcessingResult::CONTINUE)
        {
            if (!m_currentRequest)
            {
                result = readObjectId();
            }
            else
            {
                result = readObject();
            }
        }

        if (result == ProcessingResult::ERROR)
        {
            setState(State::TERMINATED);
        }
        else if (result == ProcessingResult::TERMINATE)
        {
            break;
        }
    }
}

ServerConnection::ProcessingResult ServerConnection::readObjectId()
{
    const int BOX_ID_SIZE = 4;

    int bytesToRead = BOX_ID_SIZE - m_readData.getSize();

    if (bytesToRead == 0)
    {
        const BoxId tmpId = ByteArrayReader(m_readData).decodeBoxId();

        if (!tmpId.isValid())
        {
            qDebug() << "The ID read is invalid";
            return ProcessingResult::ERROR;
        }

        qDebug() << "ID deserialized";

        if (!m_server->m_factoryManager)
        {
            qDebug() << "No factory manager available";
            return ProcessingResult::ERROR;
        }

        m_currentRequest = m_server->m_factoryManager->createObject(tmpId);
        if (!m_currentRequest)
        {
            qDebug() << "Cannot create object";
            return ProcessingResult::ERROR;
        }

        qDebug() << "Object created";

        return ProcessingResult::CONTINUE;
    }
    else
    {
        return readMoreData(bytesToRead);
    }
}

ServerConnection::ProcessingResult ServerConnection::readObject()
{
    if (!m_server->m_factoryManager)
    {
        qDebug() << "No factory manager available";
        return ProcessingResult::ERROR;
    }

    // There shall be an object now - try to read all the contents
    // needed to decode the object
    int bytesNeeded;
    const ObjectBase::DecodeResult decodeResult = m_currentRequest->decode(
            *m_server->m_factoryManager, m_readData, bytesNeeded);
    switch (decodeResult)
    {
        case ObjectBase::DecodeResult::SUCCESS:
        {
            // we do not need the data any longer
            m_readData.clear();

            qDebug() << "Decode success";

            return processObject();
        }

        case ObjectBase::DecodeResult::NOT_ENOUGH_DATA:
        {
            qDebug() << "Object need more data. Needed: " << bytesNeeded << " have: "
                    << m_readData.getSize();
            int bytesToRead = bytesNeeded - m_readData.getSize();

            return readMoreData(bytesToRead);
        }

        default:
            qDebug() << "Object decode error: " << decodeResult;
            break;
    };

    return ProcessingResult::ERROR;
}

ServerConnection::ProcessingResult ServerConnection::readMoreData(
        const int bytes)
{
    int bytesToRead = bytes;

    while ((bytesToRead > 0) && (m_socket->bytesAvailable() > 0))
    {
        // TODO: optimize
        QByteArray newData = m_socket->read(bytesToRead);

        qDebug() << "readMoreData() requested: " << bytes << ", read: " << newData.size();

        m_readData.append(newData);

        bytesToRead -= newData.size();
    }

    // continue only if all bytes were read
    return (bytesToRead > 0) ? ProcessingResult::TERMINATE : ProcessingResult::CONTINUE;
}

bool ServerConnection::hasDataToWrite()
{
    return m_writeOffset < m_writeData.getSize();
}

bool ServerConnection::prepareDataToSend()
{
    Q_ASSERT(m_writeData.getSize() == 0);
    Q_ASSERT(m_writeOffset == 0);

    Q_ASSERT(m_currentResponse.get());

    if (m_currentResponse->encode(m_writeData) == ObjectBase::EncodeResult::SUCCESS)
    {
        return true;
    }
    else
    {
        m_writeData.clear();
        return false;
    }
}

ServerConnection::ProcessingResult ServerConnection::processObject()
{
    Q_ASSERT(m_currentRequest);
    Q_ASSERT(!m_currentResponse);

    if (m_server)
    {
        m_currentResponse = m_server->processRequest(*m_currentRequest);
    }
    else
    {
        qDebug() << "Cannot process request. Server no longer available.";
    }

    // we do not need the request object any longer
    m_currentRequest.reset();

    if (m_currentResponse)
    {
        qDebug() << "Object processed. Response will be sent to client";

        if (prepareDataToSend())
        {
            qDebug() << "Scheduling write resume";

            setState(State::WRITING);

            // send before reading the rest
            return ProcessingResult::TERMINATE;
        }
        else
        {
            qDebug() << "Response preparation failed";

            return ProcessingResult::ERROR;
        }
    }
    else
    {
        qDebug() << "Object processing failed";

        // TODO: change to 'protocol error' response?
        return ProcessingResult::ERROR;
    }
}

void ServerConnection::terminate()
{
    if (m_server)
    {
        m_server->connectionFinished(this);
    }

    deleteLater();
}

void ServerConnection::onSocketError(
        QAbstractSocket::SocketError socketError)
{
    qDebug() << "ServerConnection::onSocketError " << socketError << " "
            << m_socket->errorString();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
