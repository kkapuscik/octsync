//-----------------------------------------------------------------------------
/// \file
/// ClientConnection - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ClientConnection.hpp"
#include "ClientConnectionPrivate.hpp"

// library includes
// (none)

// system includes
#include <algorithm>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ClientConnection::ClientConnection(
        QPointer<ObjectFactoryManager> objectFactoryManager,
        const QUrl& url,
        QObject* parent) :
                QObject(parent),
                m_state(State::DISCONNECTED)
{
    m_private = new ClientConnectionPrivate(url, this);
    m_private->setObjectFactoryManager(objectFactoryManager);

    connect(m_private, &ClientConnectionPrivate::connected, this,
            &ClientConnection::onConnected);
    connect(m_private, &ClientConnectionPrivate::connected, this,
            &ClientConnection::onDisconnected);
    connect(m_private, &ClientConnectionPrivate::objectSendFinished, this,
            &ClientConnection::onObjectSendFinished);
    connect(m_private, &ClientConnectionPrivate::objectReceived, this,
            &ClientConnection::onObjectReceived);
}

const QUrl& ClientConnection::getUrl() const
{
    return m_private->getUrl();
}

ClientConnection::State ClientConnection::getState() const
{
    return m_state;
}

void ClientConnection::connectToServer()
{
    if (getState() == State::DISCONNECTED)
    {
        setState(State::CONNECTING);

        m_private->connectToServer();
    }
}

void ClientConnection::disconnectFromServer()
{
    if (getState() != State::DISCONNECTED)
    {
        setState(State::DISCONNECTED);

        m_private->disconnectFromServer();
    }
}

void ClientConnection::sendObject(
        ConstObjectSharedPtr object)
{
    Q_ASSERT(object);

    // TODO: maybe there shall be a limit on queue size

    m_sendQueue.push_back(object);

    // initiate write asynchronously
    // (caller shall not receive error signal synchronously)
    if (getState() != State::DISCONNECTED)
    {
        QTimer::singleShot(0, this, SLOT( setObjectToSend() ));
    }
}

void ClientConnection::cancelSendObject(
        ConstObjectSharedPtr object)
{
    remove(m_sendQueue.begin(), m_sendQueue.end(), object);
}

void ClientConnection::setState(
        State newState)
{
    m_state = newState;

    emit stateChanged(getUrl(), newState);
}

void ClientConnection::setObjectToSend()
{
    // check if there is a connection and if something could be sent immediately
    if (getState() != State::DISCONNECTED)
    {
        if (!m_private->isSendingObject())
        {
            if (!m_sendQueue.empty())
            {
                ConstObjectSharedPtr object = m_sendQueue.front();

                m_private->setObjectToSend(object);
            }
        }
    }
}

void ClientConnection::onConnected()
{
    if (getState() == State::CONNECTING)
    {
        setState(State::CONNECTED);

        setObjectToSend();
    }
}

void ClientConnection::onDisconnected()
{
    if (getState() == State::CONNECTED)
    {
        setState(State::CONNECTING);
    }
}

void ClientConnection::onObjectSendFinished(
        ConstObjectSharedPtr object,
        Error error)
{
    if (getState() != State::DISCONNECTED)
    {
        // remove object from queue
        if (!m_sendQueue.empty())
        {
            ConstObjectSharedPtr queuedObject = m_sendQueue.front();
            if (queuedObject == object)
            {
                m_sendQueue.pop_front();
            }
        }

        // notify listeners
        emit objectSendFinished(getUrl(), object, error);

        // set next object to send
        setObjectToSend();
    }
}

void ClientConnection::onObjectReceived(
        ConstObjectSharedPtr object)
{
    if (getState() != State::DISCONNECTED)
    {
        emit objectReceived(getUrl(), object);
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
