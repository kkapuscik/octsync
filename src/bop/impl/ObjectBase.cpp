//-----------------------------------------------------------------------------
/// \file
/// ObjectBase - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../ObjectBase.hpp"
#include "ByteArray.hpp"
#include "ObjectFactoryManager.hpp"
#include <commons/OutOfBoundsException.hpp>

// library includes
// (none)

// system includes
// (none)

// using clauses
using OCTsync::Commons::OutOfBoundsException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ObjectBase::ObjectBase(
        const BoxId& id,
        const BoxType& type) :
                m_id(id),
                m_type(type)
{
    Q_ASSERT(id.isValid());
    Q_ASSERT(type.isValid());
}

ObjectBase::~ObjectBase()
{
    // nothing to do
}

ObjectBase::DecodeResult ObjectBase::decodeHeader(
        ByteArrayReader& reader,
        BoxHeader& header)
{
    // check if the header is complete
    if (reader.getSize() < HEADER_SIZE)
    {
        return DecodeResult::INVALID_DATA;
    }

    // there is a header - extract size
    reader.setPosition(4 + 1 + 3);
    header.m_size = reader.decodeUint32();

    // check against minimum size
    if (header.m_size < HEADER_SIZE)
    {
        return DecodeResult::INVALID_DATA;
    }

    // check against maximum size
    if (header.m_size > MAX_BOP_SIZE)
    {
        return DecodeResult::SIZE_TOO_BIG;
    }

    // check if whole object is complete
    if (reader.getSize() < header.m_size)
    {
        return DecodeResult::INVALID_DATA;
    }

    // reset reader position
    reader.setPosition(0);

    // - decode id
    header.m_id = reader.decodeBoxId();
    if (!header.m_id.isValid())
    {
        return DecodeResult::INVALID_DATA;
    }

    // - decode type
    header.m_type = reader.decodeBoxType();
    if (!header.m_type.isValid())
    {
        return DecodeResult::INVALID_DATA;
    }

    // - decode reserved bytes (ignore)
    reader.skip(3);

    // - decode size (ignore, already extracted)
    reader.skip(4);

    return DecodeResult::SUCCESS;
}

ObjectBase::DecodeResult ObjectBase::decodeSize(
        const ByteArray& bytes,
        int& bytesNeeded)
{
    // check if the header is complete
    if (bytes.getSize() < HEADER_SIZE)
    {
        bytesNeeded = HEADER_SIZE;
        return DecodeResult::NOT_ENOUGH_DATA;
    }

    // construct the reader
    ByteArrayReader reader(bytes);

    // there is a header - check if all required data is available
    reader.setPosition(4 + 1 + 3);
    quint32 boxSize = reader.decodeUint32();
    if (bytes.getSize() < boxSize)
    {
        bytesNeeded = boxSize;
        return DecodeResult::NOT_ENOUGH_DATA;
    }

    return DecodeResult::SUCCESS;
}


ObjectBase::DecodeResult ObjectBase::decode(
        ObjectFactoryManager& manager,
        const ByteArray& bytes,
        int& bytesNeeded)
{
    ObjectBase::DecodeResult result = DecodeResult::SUCCESS;

    do
    {
        try
        {
            // decode size, check number of bytes needed
            result = decodeSize(bytes, bytesNeeded);
            if (result != DecodeResult::SUCCESS)
            {
                break;
            }

            // decode the object
            ByteArrayReader objectReader(bytes);
            result = decode(manager, objectReader);
            if (result != DecodeResult::SUCCESS)
            {
                if (result == DecodeResult::NOT_ENOUGH_DATA)
                {
                    Q_ASSERT(0);
                    result = DecodeResult::INVALID_DATA;
                }
                break;
            }

            // check decoded object
            if (!isValid())
            {
                result = DecodeResult::INVALID_DATA;
            }
        }
        catch (OutOfBoundsException& exc)
        {
            result = DecodeResult::INVALID_DATA;
        }
    } while (false);

    return result;
}

ObjectBase::DecodeResult ObjectBase::decode(
        ObjectFactoryManager& manager,
        ByteArrayReader& reader)
{
    BoxHeader header;

    DecodeResult headerResult = decodeHeader(reader, header);
    if (headerResult != DecodeResult::SUCCESS)
    {
        return headerResult;
    }

    // there is a whole object - extract base elements
    if ((header.m_id != m_id) || (header.m_type != m_type))
    {
        return DecodeResult::INVALID_DATA;
    }

    // - skip the reserved (not used now)
    // - the size is already decoded
    // - decode optional bytes (not supported now)

    // - decode payload bytes
    ByteArrayReader payloadReader(reader, HEADER_SIZE);
    DecodeResult payloadResult = decodePayload(manager, payloadReader);
    if (payloadResult != DecodeResult::SUCCESS)
    {
        return payloadResult;
    }

    // everything done
    return DecodeResult::SUCCESS;
}

ObjectBase::EncodeResult ObjectBase::encode(
        ByteArray& bytes) const
{
    EncodeResult result = EncodeResult::INVALID_DATA;

    if (isValid())
    {
        ByteArrayWriter writer(bytes);
        result = encodeInternal(writer);
    }

    return result;
}

ObjectBase::EncodeResult ObjectBase::encodeInternal(
        ByteArrayWriter& writer) const
{
    // get number of bytes before encoding
    quint32 startSize = writer.getSize();

    // append base elements
    // - encode id
    writer.appendBoxId(m_id);

    // - encode type
    writer.appendBoxType(m_type);

    // - encode reserved
    writer.appendUint8(0);
    writer.appendUint8(0);
    writer.appendUint8(0);

    // - encode size (fake)
    writer.appendUint32(0);

    // - encode optional bytes (not supported now)

    // - encode payload
    EncodeResult payloadResult = encodePayload(writer);
    if (payloadResult != EncodeResult::SUCCESS)
    {
        return payloadResult;
    }

    // get number of bytes after encoding
    quint32 endSize = writer.getSize();

    // - encode size (correct)
    quint32 bopSize = endSize - startSize;
    writer.overwriteUint32(startSize + 8, bopSize);

    // all done
    return EncodeResult::SUCCESS;
}

//-----------------------------------------------------------------------------

QDebug operator<<(
        QDebug dbg,
        const ObjectBase::EncodeResult& result)
{
    switch (result)
    {
        case ObjectBase::EncodeResult::SUCCESS:
            dbg.nospace() << "EncodeResult::SUCCESS";
            break;
        case ObjectBase::EncodeResult::INVALID_DATA:
            dbg.nospace() << "EncodeResult::INVALID_DATA";
            break;
        default:
            dbg.nospace() << "EncodeResult::???";
            break;
    }

    return dbg.space();
}

QDebug operator<<(
        QDebug dbg,
        const ObjectBase::DecodeResult& result)
{
    switch (result)
    {
        case ObjectBase::DecodeResult::SUCCESS:
            dbg.nospace() << "DecodeResult::SUCCESS";
            break;
        case ObjectBase::DecodeResult::NOT_ENOUGH_DATA:
            dbg.nospace() << "DecodeResult::NOT_ENOUGH_DATA";
            break;
        case ObjectBase::DecodeResult::SIZE_TOO_BIG:
            dbg.nospace() << "DecodeResult::SIZE_TOO_BIG";
            break;
        case ObjectBase::DecodeResult::INVALID_DATA:
            dbg.nospace() << "DecodeResult::INVALID_DATA";
            break;
        default:
            dbg.nospace() << "DecodeResult::???";
            break;
    }

    return dbg.space();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
