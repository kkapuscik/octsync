//-----------------------------------------------------------------------------
/// \file
/// SslServerSocket - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2014 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SslServerSocket
#define INCLUDED_OCTsync_SslServerSocket

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QSslSocket>
#include <QtNetwork/QSslConfiguration>
#include <QtNetwork/QSslKey>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

/// Tcp Server socket with SSL functionality
///
class SslServerSocket : public QTcpServer
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    bool m_secure;
    QSslCertificate m_sslCertificate;
    QSslKey m_sslKey;


// Methods (public, protected, private)
public:
    explicit SslServerSocket(
            QObject* const parent = nullptr);

    virtual ~SslServerSocket();

    bool isSecure() const;

    void setMode(
            const bool secure,
            QSslCertificate sslCertificate,
            QSslKey sslKey);

protected:
    virtual void incomingConnection(
            const qintptr handle);

private:
    QTcpSocket* createConnectionSocket(
            const qintptr handle);

    QTcpSocket* createSecureConnectionSocket(
            const qintptr handle);

    QTcpSocket* createPlainConnectionSocket(
            const qintptr handle);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SslServerSocket*/
