//-----------------------------------------------------------------------------
/// \file
/// BoxId - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../BoxId.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

BoxId::BoxId() :
                m_id(0)
{
    // nothing to do
}

BoxId::BoxId(
        const char c1,
        const char c2,
        const char c3,
        const char c4) :
                m_id(0)
{
    m_id |= (c1 << 24);
    m_id |= (c2 << 16);
    m_id |= (c3 << 8);
    m_id |= (c4 << 0);
}

void BoxId::toCharArray(char* const buffer) const
{
    char c1 = (m_id >> 24) & 0xFF;
    char c2 = (m_id >> 16) & 0xFF;
    char c3 = (m_id >> 8) & 0xFF;
    char c4 = (m_id >> 0) & 0xFF;

    buffer[0] = c1;
    buffer[1] = c2;
    buffer[2] = c3;
    buffer[3] = c4;
}

bool BoxId::isValid() const
{
    char c1 = (m_id >> 24) & 0xFF;
    char c2 = (m_id >> 16) & 0xFF;
    char c3 = (m_id >> 8) & 0xFF;
    char c4 = (m_id >> 0) & 0xFF;

    return isValid(c1) && isValid(c2) && isValid(c3) && isValid(c4);
}

bool BoxId::isValid(
        const char c) const
{
    if (c >= '0' && c <= '9')
    {
        return true;
    }
    else if (c >= 'A' && c <= 'Z')
    {
        return true;
    }
    return false;
}

bool operator !=(
        const BoxId& box1,
        const BoxId& box2)
{
    return box1.m_id != box2.m_id;
}

bool operator ==(
        const BoxId& box1,
        const BoxId& box2)
{
    return box1.m_id == box2.m_id;
}

bool operator <(
        const BoxId& box1,
        const BoxId& box2)
{
    return box1.m_id < box2.m_id;
}

bool operator >(
        const BoxId& box1,
        const BoxId& box2)
{
    return box1.m_id > box2.m_id;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
