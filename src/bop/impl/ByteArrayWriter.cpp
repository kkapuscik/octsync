//-----------------------------------------------------------------------------
/// \file
/// ByteArrayWriter - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ByteArrayWriter.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

void ByteArrayWriter::appendBoxId(
        const BoxId& id)
{
    char idChars[4];

    id.toCharArray(idChars);

    m_byteArray.append(idChars, 4);
}

void ByteArrayWriter::appendBoxType(
        const BoxType& type)
{
    appendUint8(type.getType());
}

void ByteArrayWriter::appendInt8(
        const qint8 value)
{
    appendUint8(static_cast<quint8>(value));
}

void ByteArrayWriter::appendInt16(
        const qint16 value)
{
    appendUint16(static_cast<quint16>(value));
}

void ByteArrayWriter::appendInt32(
        const qint32 value)
{
    appendUint32(static_cast<quint32>(value));
}

void ByteArrayWriter::appendInt64(
        const qint64 value)
{
    appendUint64(static_cast<quint64>(value));
}

void ByteArrayWriter::appendUint8(
        const quint8 value)
{
    m_byteArray.append(value);
}

void ByteArrayWriter::appendUint16(
        const quint16 value)
{
    quint8 byte1 = (value >> 8) & 0xFF;
    quint8 byte2 = (value >> 0) & 0xFF;

    // big endian
    appendUint8(byte1);
    appendUint8(byte2);
}

void ByteArrayWriter::appendUint32(
        const quint32 value)
{
    quint8 byte1 = (value >> 24) & 0xFF;
    quint8 byte2 = (value >> 16) & 0xFF;
    quint8 byte3 = (value >> 8) & 0xFF;
    quint8 byte4 = (value >> 0) & 0xFF;

    // big endian
    appendUint8(byte1);
    appendUint8(byte2);
    appendUint8(byte3);
    appendUint8(byte4);
}

void ByteArrayWriter::appendUint64(
        const quint64 value)
{
    quint8 byte1 = (value >> 56) & 0xFF;
    quint8 byte2 = (value >> 48) & 0xFF;
    quint8 byte3 = (value >> 40) & 0xFF;
    quint8 byte4 = (value >> 32) & 0xFF;
    quint8 byte5 = (value >> 24) & 0xFF;
    quint8 byte6 = (value >> 16) & 0xFF;
    quint8 byte7 = (value >> 8) & 0xFF;
    quint8 byte8 = (value >> 0) & 0xFF;

    // big endian
    appendUint8(byte1);
    appendUint8(byte2);
    appendUint8(byte3);
    appendUint8(byte4);
    appendUint8(byte5);
    appendUint8(byte6);
    appendUint8(byte7);
    appendUint8(byte8);
}

void ByteArrayWriter::append(
        const QByteArray& array)
{
    m_byteArray.append(array);
}

void ByteArrayWriter::overwriteUint32(
        const int offset,
        const quint32 value)
{
    Q_ASSERT(offset + 4 <= getSize());

    quint8 byte1 = (value >> 24) & 0xFF;
    quint8 byte2 = (value >> 16) & 0xFF;
    quint8 byte3 = (value >> 8) & 0xFF;
    quint8 byte4 = (value >> 0) & 0xFF;

    // big endian
    m_byteArray.set(offset + 0, byte1);
    m_byteArray.set(offset + 1, byte2);
    m_byteArray.set(offset + 2, byte3);
    m_byteArray.set(offset + 3, byte4);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
