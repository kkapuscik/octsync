//-----------------------------------------------------------------------------
/// \file
/// ObjectReader - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ObjectReader.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

ObjectReader::ObjectReader()
{
    // nothing to do
}

void ObjectReader::reset()
{
    m_object.reset();
    m_buffer.clear();
}

ConstObjectSharedPtr ObjectReader::takeObject()
{
    ConstObjectSharedPtr result = m_object;

    reset();

    return result;
}


ObjectReader::ReadResult ObjectReader::read(
        QTcpSocket& socket,
        ObjectFactoryManager& factoryManager)
{
    if (!m_object)
    {
        const ReadResult result = readObjectId(socket, factoryManager);
        if (result != ReadResult::COMPLETE)
        {
            return result;
        }
    }

    return readObject(socket, factoryManager);
}

ObjectReader::ReadResult ObjectReader::readObjectId(
        QTcpSocket& socket,
        ObjectFactoryManager& factoryManager)
{
    const int BOX_ID_SIZE = 4;
    int bytesToRead = BOX_ID_SIZE - m_buffer.getSize();
    if (bytesToRead > 0)
    {
        const ReadResult result = readMoreData(socket, bytesToRead);
        if (result != ReadResult::COMPLETE)
        {
            return result;
        }
    }

    const BoxId tmpId = ByteArrayReader(m_buffer).decodeBoxId();
    if (!tmpId.isValid())
    {
        qDebug() << "The ID read is invalid";
        return ReadResult::ERROR;
    }

    qDebug() << "ID deserialized";

    m_object = factoryManager.createObject(tmpId);
    if (!m_object)
    {
        qDebug() << "Cannot create object";
        return ReadResult::ERROR;
    }

    qDebug() << "Object created";

    return ReadResult::COMPLETE;
}

ObjectReader::ReadResult ObjectReader::readObject(
        QTcpSocket& socket,
        ObjectFactoryManager& factoryManager)
{
    Q_ASSERT(m_object);

    bool retryDecode;
    do
    {
        retryDecode = false;

        // There shall be an object now - try to read all the contents
        // needed to decode the object
        int bytesNeeded;
        const ObjectBase::DecodeResult decodeResult = m_object->decode(factoryManager,
                m_buffer, bytesNeeded);
        switch (decodeResult)
        {
            case ObjectBase::DecodeResult::SUCCESS:
            {
                // we do not need the data any longer
                m_buffer.clear();

                qDebug() << "Decode success";

                return ReadResult::COMPLETE;
            }

            case ObjectBase::DecodeResult::NOT_ENOUGH_DATA:
            {
                qDebug() << "Object need more data. Needed: " << bytesNeeded << " have: "
                        << m_buffer.getSize();

                const int bytesToRead = bytesNeeded - m_buffer.getSize();

                const ReadResult result = readMoreData(socket, bytesToRead);
                if (result == ReadResult::COMPLETE)
                {
                    // retry decode
                    retryDecode = true;
                }
                else
                {
                    return result;
                }
                break;
            }

            default:
                qDebug() << "Object decode error: " << decodeResult;
                break;
        };
    } while (retryDecode);

    return ReadResult::ERROR;
}

ObjectReader::ReadResult ObjectReader::readMoreData(
        QTcpSocket& socket,
        const int byteCount)
{
    int bytesToRead = byteCount;

    while ((bytesToRead > 0) && (socket.bytesAvailable() > 0))
    {
        // TODO: optimize
        QByteArray newData = socket.read(bytesToRead);

        qDebug() << "readMoreData() requested: " << byteCount << ", read: " << newData.size();

        m_buffer.append(newData);

        bytesToRead -= newData.size();
    }

    // continue only if all bytes were read
    return (bytesToRead > 0) ? ReadResult::INCOMPLETE : ReadResult::COMPLETE;
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
