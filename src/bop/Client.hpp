//-----------------------------------------------------------------------------
/// \file
/// Client - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Bop_Client
#define INCLUDED_OCTsync_Bop_Client

//-----------------------------------------------------------------------------

// local includes
#include "ObjectBase.hpp"
#include "ObjectFactoryManager.hpp"
#include "ClientConnection.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QPointer>
#include <QtCore/QMap>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
/// \todo Hide some of the implementation in private class
class Client : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QMap<QUrl, ClientConnection*> ConnectionMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QPointer<ObjectFactoryManager> m_factoryManager;
    ConnectionMap m_connections;

// Methods (public, protected, private)
public:
    Client(
            QPointer<ObjectFactoryManager> factoryManager,
            QObject* parent = nullptr);

    void abortConnection(
            const QUrl& url);

    ClientConnection* getConnection(
            const QUrl& url,
            const bool createFlag = true);

    bool sendObject(
            const QUrl& url,
            const bool createFlag,
            ConstObjectSharedPtr object);

private:
    void onConnectionDestroyed(
            QObject* obj);

signals:
    void connectionStateChanged(
            QUrl url,
            ClientConnection::State newState);

    void objectReceived(
            QUrl url,
            ConstObjectSharedPtr object);

    void objectSendFinished(
            QUrl url,
            ConstObjectSharedPtr object,
            Error error);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Bop_Client*/
