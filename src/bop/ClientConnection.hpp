//-----------------------------------------------------------------------------
/// \file
/// ClientConnection - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ClientConnection
#define INCLUDED_OCTsync_ClientConnection

//-----------------------------------------------------------------------------

// local includes
#include "Error.hpp"
#include "ObjectBase.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QUrl>

// system includes
#include <memory>
#include <deque>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
class ClientConnectionPrivate;

/// TODO: Class documentation
///
class ClientConnection : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    /// State of the connection.
    enum class State
    {
        /// Connection was not started.
        DISCONNECTED,
        /// Connected is currently being established.
        CONNECTING,
        /// Connection exists.
        CONNECTED
    };

private:
    typedef std::deque<ConstObjectSharedPtr> ObjectFifo;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    State m_state;
    ObjectFifo m_sendQueue;
    ClientConnectionPrivate* m_private;

// Methods (public, protected, private)
public:
    ClientConnection(
            QPointer<ObjectFactoryManager> objectFactoryManager,
            const QUrl& url,
            QObject* parent = nullptr);

    const QUrl& getUrl() const;

    State getState() const;

    void connectToServer();

    void disconnectFromServer();

    void sendObject(
            ConstObjectSharedPtr object);

    void cancelSendObject(
            ConstObjectSharedPtr object);

private slots:
    void setObjectToSend();

private:
    void setState(
            State newState);

    void onConnected();

    void onDisconnected();

    void onObjectSendFinished(
            ConstObjectSharedPtr object,
            Error error);

    void onObjectReceived(
            ConstObjectSharedPtr object);

signals:
    void stateChanged(
            QUrl url,
            State newState);

    void objectSendFinished(
            QUrl url,
            ConstObjectSharedPtr object,
            Error error);

    void objectReceived(
            QUrl url,
            ConstObjectSharedPtr object);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ClientConnection*/
