//-----------------------------------------------------------------------------
/// \file
/// ObjectFactory - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ObjectFactory
#define INCLUDED_OCTsync_ObjectFactory

//-----------------------------------------------------------------------------

// local includes
#include "ObjectBase.hpp"

// library includes
#include <QtCore/QObject>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace Bop
{

// forward references in namespace
// (none)

/// BOP object factory interface.
///
class ObjectFactory : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    ObjectFactory(
            QObject* parent = nullptr) :
                    QObject(parent)
    {
        // nothing to do
    }

    virtual std::unique_ptr<ObjectBase> createObject(
            const BoxId& id) = 0;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ObjectFactory*/
