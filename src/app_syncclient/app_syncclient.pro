#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T20:57:58
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = app_syncclient
TEMPLATE = app
CONFIG += c++11

SOURCES += \
    impl/AsyncTask.cpp \
    impl/AsyncTaskQueue.cpp \
    impl/CopyTask.cpp \
    impl/DeleteTask.cpp \
    impl/FileInfo.cpp \
    impl/FileTreeItem.cpp \
    impl/FileTreeModel.cpp \
    impl/ManualSyncWidget.cpp \
    impl/ReadDirTask.cpp \
    impl/SafeRenameTask.cpp \
    impl/ServerDevice.cpp \
    impl/ShareLocator.cpp \
    impl/ShareSelectDialog.cpp \
    impl/SharesTreeView.cpp \
    impl/SharesUpdater.cpp \
    impl/SharesWidget.cpp \
    impl/ShareTreeModel.cpp \
    impl/SyncAlgorithm.cpp \
    impl/SyncAlgorithmManual.cpp \
    impl/SyncAlgorithmMirror.cpp \
    impl/SyncAlgorithmTwoSided.cpp \
    impl/SyncClientApplication.cpp \
    impl/SyncContext.cpp \
    impl/SynchronizeTask.cpp \
    impl/SyncMode.cpp \
    impl/SyncPoint.cpp \
    impl/SyncPointEditDialog.cpp \
    impl/SyncPointInfoWidget.cpp \
    impl/SyncPointListModel.cpp \
    impl/SyncPointListView.cpp \
    impl/SyncPointsData.cpp \
    impl/SyncShare.cpp \
    impl/SyncsInfoWidget.cpp \
    impl/SyncsListWidget.cpp \
    impl/SyncsRepository.cpp \
    impl/SyncsWidget.cpp \
    impl/app_syncclient_main.cpp \
    impl/SyncClientEngine.cpp \
    impl/SyncClientWindow.cpp

HEADERS  += \
    namespace.hpp \
    impl/AsyncTask.hpp \
    impl/AsyncTaskQueue.hpp \
    impl/CopyTask.hpp \
    impl/DeleteTask.hpp \
    impl/FileInfo.hpp \
    impl/FileTreeItem.hpp \
    impl/FileTreeModel.hpp \
    impl/ItemTypes.hpp \
    impl/ManualSyncWidget.hpp \
    impl/ReadDirTask.hpp \
    impl/SafeRenameTask.hpp \
    impl/ServerDevice.hpp \
    impl/ShareLocator.hpp \
    impl/ShareSelectDialog.hpp \
    impl/SharesTreeView.hpp \
    impl/SharesUpdater.hpp \
    impl/SharesWidget.hpp \
    impl/ShareTreeModel.hpp \
    impl/SyncAlgorithm.hpp \
    impl/SyncAlgorithmManual.hpp \
    impl/SyncAlgorithmMirror.hpp \
    impl/SyncAlgorithmTwoSided.hpp \
    impl/SyncClientApplication.hpp \
    impl/SyncContext.hpp \
    impl/SynchronizeTask.hpp \
    impl/SyncMode.hpp \
    impl/SyncOp.hpp \
    impl/SyncPoint.hpp \
    impl/SyncPointEditDialog.hpp \
    impl/SyncPointInfoWidget.hpp \
    impl/SyncPointListModel.hpp \
    impl/SyncPointListView.hpp \
    impl/SyncPointsData.hpp \
    impl/SyncShare.hpp \
    impl/SyncsInfoWidget.hpp \
    impl/SyncsListWidget.hpp \
    impl/SyncsRepository.hpp \
    impl/SyncsWidget.hpp \
    impl/SyncClientEngine.hpp \
    impl/SyncClientWindow.hpp

RESOURCES += \
    ../../data/octsync.qrc

CONFIG += mobility
MOBILITY =

OTHER_FILES += \
    CMakeLists.txt

INCLUDEPATH += $$PWD/../

win32-g++: {
    message("Building for WIN G++")
} else:unix: {
    message("Building for UNIX")
} else {
    error("Unsupported platform configuration")
}

# add library COMMONS
DEPENDPATH += $$PWD/../commons
LIBS += -L$$OUT_PWD/../commons/ -lcommons
PRE_TARGETDEPS += $$OUT_PWD/../commons/libcommons.a

# add library LDP
DEPENDPATH += $$PWD/../ldp
LIBS += -L$$OUT_PWD/../ldp/ -lldp
PRE_TARGETDEPS += $$OUT_PWD/../ldp/libldp.a

# add library BOPRPC
DEPENDPATH += $$PWD/../boprpc
LIBS += -L$$OUT_PWD/../boprpc/ -lboprpc
PRE_TARGETDEPS += $$OUT_PWD/../boprpc/libboprpc.a

# add library BOP
DEPENDPATH += $$PWD/../bop
LIBS += -L$$OUT_PWD/../bop/ -lbop
PRE_TARGETDEPS += $$OUT_PWD/../bop/libbop.a

# Add library syncserver
DEPENDPATH += $$PWD/../syncserver
LIBS += -L$$OUT_PWD/../syncserver/ -lsyncserver
PRE_TARGETDEPS += $$OUT_PWD/../syncserver/libsyncserver.a

# add library UI
DEPENDPATH += $$PWD/../ui
LIBS += -L$$OUT_PWD/../ui/ -lui
PRE_TARGETDEPS += $$OUT_PWD/../ui/libui.a
