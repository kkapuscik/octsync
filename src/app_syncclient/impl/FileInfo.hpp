//-----------------------------------------------------------------------------
/// \file
/// FileInfo - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_AppSyncClient_FileInfo
#define INCLUDED_OCTsync_AppSyncClient_FileInfo

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QSharedData>
#include <QtCore/QString>
#include <QtCore/QDateTime>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class FileInfo
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    enum class Type
    {
        FILE,
        DIRECTORY,
        OTHER
    };

private:
    class FileInfoData : public QSharedData
    {
    public:
        QString m_name;
        Type m_type;
        qint64 m_size;
        QDateTime m_time;

        FileInfoData()
        {
            m_type = Type::OTHER;
            m_size = 0;
        }
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QSharedDataPointer<FileInfoData> m_data;

// Methods (public, protected, private)
public:
    FileInfo();

    bool isValid() const
    {
        return !m_data->m_name.isEmpty() && (m_data->m_size >= 0) && (m_data->m_time.isValid());
    }

    void setName(
            const QString& name)
    {
        m_data->m_name = name;
    }

    const QString& getName() const
    {
        return m_data->m_name;
    }

    void setType(
            const Type type)
    {
        m_data->m_type = type;
    }

    Type getType() const
    {
        return m_data->m_type;
    }

    void setSize(
            const qint64 size)
    {
        Q_ASSERT(size >= 0);

        m_data->m_size = size;
    }

    qint64 getSize() const
    {
        return m_data->m_size;
    }

    void setMTime(
            const QDateTime& time)
    {
        m_data->m_time = time;
    }

    QDateTime getMTime() const
    {
        return m_data->m_time;
    }
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_AppSyncClient_FileInfo*/
