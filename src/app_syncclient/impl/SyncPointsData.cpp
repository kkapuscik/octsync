//-----------------------------------------------------------------------------
/// \file
/// SyncPointsData - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncPointsData.hpp"
#include "SyncsRepository.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncPointsData::SyncPointsData(
        QObject* parent) :
                QObject(parent)
{
    // Nothing to do
}

bool SyncPointsData::load(
        QSettings& settings)
{
	m_syncs.removeAllSyncs();

	// Try to read the name
	settings.beginGroup("data");
	const int size = settings.beginReadArray("syncs");
	for (int i = 0; i < size; ++i)
	{
		settings.setArrayIndex(i);

		SyncPoint loadedSyncPoint;

		// read uuid, generate if invalid / does not exist
	    Commons::Uuid pointUuid = Commons::Uuid::parse(settings.value("uuid").toString());
	    if (!pointUuid.isValid())
	    {
	        pointUuid = Commons::Uuid::generate();
	    }
	    loadedSyncPoint.setUuid(pointUuid);

		loadedSyncPoint.setName(settings.value("name").toString());

		SyncMode syncMode;
		if (syncMode.fromInt(settings.value("mode").toInt()))
		{
			loadedSyncPoint.setMode(syncMode);
		}
		else
		{
			// In case of error during conversion
			// set mode to MANUAL (this should be safe).
			loadedSyncPoint.setMode(SyncMode::MANUAL);
		}
		loadedSyncPoint.setLastSyncTime(settings.value("last_sync").toDateTime());

		ShareLocator shareLocatorA(settings.value("server_a_id").toUuid(),
								   settings.value("share_a_id").toUuid());

		loadedSyncPoint.setShareLocatorA(shareLocatorA);

		ShareLocator shareLocatorB(settings.value("server_b_id").toUuid(),
							       settings.value("share_b_id").toUuid());

		loadedSyncPoint.setShareLocatorB(shareLocatorB);

		if (!loadedSyncPoint.isValid())
		{
			emit changed();
			return false;
		}

		m_syncs.addSync(loadedSyncPoint);
	}

    settings.endArray();
    settings.endGroup();

    emit changed();

    return true;
}

bool SyncPointsData::save(
        QSettings& settings)
{
	// First get all sync names
	auto const syncIds = m_syncs.getSyncIds();

	settings.beginGroup("data");
	settings.beginWriteArray("syncs", syncIds.length());

	for (int i = 0; i < syncIds.length(); ++i)
	{
		settings.setArrayIndex(i);

		const SyncPoint syncPoint = m_syncs.getSync(syncIds.at(i));
	    settings.setValue("uuid",        syncPoint.getUuid().toString());
		settings.setValue("name",        syncPoint.getName());
		settings.setValue("mode",        syncPoint.getMode().toInt());
		settings.setValue("last_sync",   syncPoint.getLastSyncTime());
		settings.setValue("server_a_id", syncPoint.getShareLocatorA().getServerId().toString());
		settings.setValue("share_a_id",  syncPoint.getShareLocatorA().getShareId().toString());
		settings.setValue("server_b_id", syncPoint.getShareLocatorB().getServerId().toString());
		settings.setValue("share_b_id",  syncPoint.getShareLocatorB().getShareId().toString());
	}

    return true;
}

const SyncsRepository& SyncPointsData::getSyncPoints() const
{
    return m_syncs;
}

SyncPoint SyncPointsData::getSync(
        const Commons::Uuid& uuid) const
{
	return m_syncs.getSync(uuid);
}

void SyncPointsData::addSync(
        const SyncPoint& sync)
{
	if (m_syncs.addSync(sync))
	{
		emit changed();
	}
}

void SyncPointsData::modifySync(
        const SyncPoint& sync)
{
	if (m_syncs.modifySync(sync))
	{
		emit changed();
	}
}

void SyncPointsData::removeSync(
        const SyncPoint& sync)
{
	if (m_syncs.removeSync(sync))
	{
		emit changed();
	}
}

void SyncPointsData::removeAllSyncs()
{
	m_syncs.removeAllSyncs();

	emit changed();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
