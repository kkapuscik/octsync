//-----------------------------------------------------------------------------
/// \file
/// ManualSyncWidget - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ManualSyncWidget
#define INCLUDED_OCTsync_ManualSyncWidget

//-----------------------------------------------------------------------------

// local includes
#include "SyncContext.hpp"

// library includes
#include <QtWidgets/QWidget>

// system includes
// (none)

// using clauses
// (none)

// forward references
class QTreeView;
class QComboBox;
class QAction;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ManualSyncWidget : public QWidget
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncContext* m_syncContext;
    QTreeView* m_treeView;
    QComboBox* m_comboBoxAlgorithm;
    QAction* m_actionBuildTree;
    QAction* m_actionAnalyze;
    QAction* m_actionSynchronize;

// Methods (public, protected, private)
public:
    ManualSyncWidget(
            SyncContext* syncContext,
            QWidget* parent = nullptr);

private:
    void onTreeContextMenuRequested(
            const QPoint& pos);

    void createActions();
    void createWidgets();
    void connectActions();

    void onBuildTreeRequested();
    void onBuildTreeFinished();

    void onAnalysisRequested();

    void onSyncRequested();
    void onSyncFinished();;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ManualSyncWidget*/
