//-----------------------------------------------------------------------------
/// \file
/// SyncsInfoWidget - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncsInfoWidget.hpp"
#include "SyncsListWidget.hpp"

// library includes
#include <QtWidgets/QSplitter>
#include <QtWidgets/QLayout>
#include <QtWidgets/QGridLayout>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncsInfoWidget::SyncsInfoWidget(
        QWidget* parent) :
                QWidget(parent)
{
    QGridLayout* mainlayout = new QGridLayout(this);

    m_share1 = new QLabel("Share1:");

    m_hostName1 = new QLabel("Host name 1");

    m_shareName1 = new QLabel("Share name 1");

    m_changeShare1 = new QPushButton("Change share");

    // Add elements to the grid layout
    mainlayout->addWidget(m_share1, 0, 0);
    mainlayout->addWidget(m_hostName1, 0, 1);
    mainlayout->addWidget(m_shareName1, 0, 2);
    mainlayout->addWidget(m_changeShare1, 0, 3);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
