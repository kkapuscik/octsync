//-----------------------------------------------------------------------------
/// \file
/// FileTreeItem - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_FileTreeItem
#define INCLUDED_OCTsync_FileTreeItem

//-----------------------------------------------------------------------------

// local includes
#include "FileInfo.hpp"
#include "SyncOp.hpp"

// library includes
#include <QtCore/QString>
#include <QtGui/QStandardItemModel>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class FileTreeItem
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QString m_name;
    SyncOp m_syncOp;
    QString m_path;
    FileInfo m_infoA;
    FileInfo m_infoB;

    QStandardItem* m_nameItem;
    QStandardItem* m_operationItem;
    QStandardItem* m_infoSizeItemA;
    QStandardItem* m_infoSizeItemB;
    QStandardItem* m_dateItemA;
    QStandardItem* m_dateItemB;

// Methods (public, protected, private)
public:
    FileTreeItem(
            const QString& name,
            const QString& path);

    FileInfo::Type getType() const;

    const FileInfo& getInfoA() const
    {
        return m_infoA;
    }

    const FileInfo& getInfoB() const
    {
        return m_infoB;
    }

    SyncOpSet getValidOps() const;

    SyncOp getSyncOp() const
    {
        return m_syncOp;
    }

    void setSyncOp(
            const SyncOp op);

    // TODO: set share name?
    void setRootInfo();

    void updateInfoA(
            const FileInfo& info);

    void updateInfoB(
            const FileInfo& info);

    const QString& getPath() const;

    QStandardItem* getModelItem() const;

    QList<QStandardItem*> getRowItems() const;

    QModelIndex getItemIndex() const;

private:
    void updateCommonInfo();

    void updateData(
            const FileInfo& info,
            QStandardItem* const infoSizeItem,
            QStandardItem* const dateItem);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_FileTreeItem*/
