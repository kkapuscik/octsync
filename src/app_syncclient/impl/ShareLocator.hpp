//-----------------------------------------------------------------------------
/// \file
/// ShareLocator - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ShareLocator
#define INCLUDED_OCTsync_ShareLocator

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <commons/Uuid.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ShareLocator
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_serverId;
    Commons::Uuid m_shareId;

// Methods (public, protected, private)
public:
    ShareLocator();

    ShareLocator(
            const Commons::Uuid& serverId,
            const Commons::Uuid& shareId);

    bool isValid() const;

    const Commons::Uuid& getServerId() const;

    const Commons::Uuid& getShareId() const;

    void setLocator(
            const Commons::Uuid& serverId,
            const Commons::Uuid& shareId);
};

bool operator ==(
        const ShareLocator& locator1,
        const ShareLocator& locator2);

bool operator !=(
        const ShareLocator& locator1,
        const ShareLocator& locator2);

} // end of namespace
} // end of namespace


//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ShareLocator*/
