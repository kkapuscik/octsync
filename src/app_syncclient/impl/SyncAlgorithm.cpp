//-----------------------------------------------------------------------------
/// \file
/// SyncAlgorithm - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncAlgorithm.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

bool SyncAlgorithm::isSameFile(
        const FileInfo& info1,
        const FileInfo& info2) const
{
    bool sameFile = false;

    if (info1.isValid() && info2.isValid())
    {
        if (info1.getSize() == info2.getSize())
        {
            if (qAbs(info1.getMTime().msecsTo(info2.getMTime())) <= MAX_TIME_DIFF_MS)
            {
                sameFile = true;
            }
        }
    }

    return sameFile;
}

qint64 SyncAlgorithm::getTimeDiff(
        const FileInfo& info1,
        const FileInfo& info2) const
{
    Q_ASSERT(info1.isValid() && info2.isValid());

    const qint64 diff = info1.getMTime().toMSecsSinceEpoch() - info2.getMTime().toMSecsSinceEpoch();
    if (qAbs(diff) <= MAX_TIME_DIFF_MS)
    {
        return 0;
    }
    else
    {
        return diff;
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
