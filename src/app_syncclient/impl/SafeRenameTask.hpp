//-----------------------------------------------------------------------------
/// \file
/// SafeRenameTask - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SafeRenameTask
#define INCLUDED_OCTsync_SafeRenameTask

//-----------------------------------------------------------------------------

// local includes
#include "AsyncTask.hpp"
#include "ServerDevice.hpp"

// library includes
#include <QtCore/QPointer>
#include <commons/Uuid.hpp>
#include <boprpc/Executor.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
/// TODO: Change name to SafeRenameFileTask
class SafeRenameTask : public AsyncTask
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_serverId;
    Commons::Uuid m_shareId;
    QString m_path;

    bool m_result;
    QString m_resultPath;

// Methods (public, protected, private)
public:
    SafeRenameTask(
            SyncClientEngine* engine,
            const Commons::Uuid& serverId,
            const Commons::Uuid& shareId,
            const QString& path,
            QObject* parent = nullptr);

    bool getResult() const
    {
        return m_result;
    }

    QString getResultPath() const
    {
        Q_ASSERT(getResult());

        return m_resultPath;
    }

protected:
    virtual void run();

private:
    void onRenameFinished(
            QPointer<BopRpc::Executor> executor);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SafeRenameTask*/
