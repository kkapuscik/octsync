//-----------------------------------------------------------------------------
/// \file
/// SyncAlgorithmTwoSided - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncAlgorithmTwoSided.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

void SyncAlgorithmTwoSided::processItem(
        FileTreeItem& item)
{
    const FileInfo& infoA = item.getInfoA();
    const FileInfo& infoB = item.getInfoB();

    item.setSyncOp(SyncOp::NONE);

    if (item.getType() == FileInfo::Type::FILE)
    {
        if (infoA.isValid() && infoB.isValid())
        {
            // TODO: isSameFile compares without checking the contents - to be modified

            if (!isSameFile(infoA, infoB))
            {
                if (getTimeDiff(infoA, infoB) < 0)
                {
                    // time(A) < time(B) -> copy B to A

                    if (m_safeMode)
                    {
                        item.setSyncOp(SyncOp::COPY_B_TO_A_WITH_BACKUP);
                    }
                    else
                    {
                        item.setSyncOp(SyncOp::COPY_B_TO_A);
                    }
                }
                else
                {
                    // time(A) > time(B) -> copy A to B

                    if (m_safeMode)
                    {
                        item.setSyncOp(SyncOp::COPY_A_TO_B_WITH_BACKUP);
                    }
                    else
                    {
                        item.setSyncOp(SyncOp::COPY_A_TO_B);
                    }
                }
            }
        }
        else if (infoA.isValid())
        {
            item.setSyncOp(SyncOp::COPY_A_TO_B);
        }
        else if (infoB.isValid())
        {
            item.setSyncOp(SyncOp::COPY_B_TO_A);
        }
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
