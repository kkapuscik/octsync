//-----------------------------------------------------------------------------
/// \file
/// FileInfo - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "FileInfo.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

FileInfo::FileInfo() :
                m_data(new FileInfoData())
{
    // nothing to do
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
