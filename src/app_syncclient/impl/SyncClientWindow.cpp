//-----------------------------------------------------------------------------
/// \file
/// SyncClientWindow - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncClientWindow.hpp"
#include "SharesWidget.hpp"
#include "SyncsWidget.hpp"
#include "SyncPointEditDialog.hpp"
#include "SyncContext.hpp"
#include "ManualSyncWidget.hpp"

// library includes
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QAction>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QTabWidget>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncClientWindow::SyncClientWindow(
        SyncClientEngine* engine,
        QWidget* parent) :
                QMainWindow(parent),
                m_engine(engine)
{
    Q_ASSERT(engine);

    setWindowTitle("SyncClient");
    setWindowIcon(QIcon(":/icons/octsyncclient"));

    createActions();
    createWidgets();
    createMainMenu();

    resize(800, 600);
    move(QApplication::desktop()->screen()->rect().center() - rect().center());

    connectActions();
    setUpModels();
}

void SyncClientWindow::setUpModels()
{
    m_sharesWidget->setEngine(m_engine);
    m_syncsWidget->setEngine(m_engine);
}

void SyncClientWindow::createActions()
{
    m_actionAppExit = new QAction(this);
    m_actionAppExit->setText(tr("E&xit"));
    m_actionAppExit->setIcon(QIcon(":/icons/app_exit"));

    m_actionAppAbout = new QAction(this);
    m_actionAppAbout->setText(tr("&About"));
    m_actionAppAbout->setIcon(QIcon(":/icons/app_about"));

    m_actionAppAboutQt = new QAction(this);
    m_actionAppAboutQt->setText(tr("About &Qt"));
    m_actionAppAboutQt->setIcon(QIcon(":/icons/app_about_qt"));

    m_actionSyncManual = new QAction(this);
    m_actionSyncManual->setText(tr("Manual Sync..."));
    m_actionSyncManual->setIcon(QIcon(":/icons/manual_sync"));
}

void SyncClientWindow::createWidgets()
{
    // create main tab widget
    m_mainTabWidget = new QTabWidget(this);
    m_mainTabWidget->setTabPosition(QTabWidget::North);

    // create tab widgets
    m_sharesWidget = new SharesWidget(m_mainTabWidget);
    m_syncsWidget = new SyncsWidget(m_mainTabWidget);

    // create tabs
    m_mainTabWidget->addTab(m_sharesWidget, QIcon(":/icons/tab_shares"), tr("Shares"));
    m_mainTabWidget->addTab(m_syncsWidget, QIcon(":/icons/tab_syncpoints"), tr("SyncPoints"));

    setCentralWidget(m_mainTabWidget);
}

void SyncClientWindow::createMainMenu()
{
    QMenu* menuFile;
    QMenu* menuHelp;

    menuFile = new QMenu(this);
    menuFile->setTitle(tr("&File"));
    menuFile->addAction(m_actionSyncManual);
    menuFile->addSeparator();
    menuFile->addAction(m_actionAppExit);

    menuHelp = new QMenu(this);
    menuHelp->setTitle(tr("&Help"));
    menuHelp->addAction(m_actionAppAbout);
    menuHelp->addAction(m_actionAppAboutQt);

    menuBar()->addMenu(menuFile);
    menuBar()->addMenu(menuHelp);
}

void SyncClientWindow::connectActions()
{
    connect(m_actionAppExit, &QAction::triggered, this, &SyncClientWindow::close);
    connect(m_actionAppAbout, &QAction::triggered, this, &SyncClientWindow::showAppAboutDialog);
    connect(m_actionAppAboutQt, &QAction::triggered, this, &SyncClientWindow::showQtAboutDialog);
    connect(m_actionSyncManual, &QAction::triggered, this, &SyncClientWindow::startManualSync);

    connect(m_syncsWidget, &SyncsWidget::manualSyncRequested, this, &SyncClientWindow::performManualSync);

}

void SyncClientWindow::showAppAboutDialog()
{
    QMessageBox::about(this, tr("Sync Client - About"),
            tr("Sync Client v0.2\n\n"
               "Copyright (C) 2014 OCTaedr Software\n"
               "- Krzysztof Kapuscik\n"
               "- Michal Kowal\n\n"
               "All Rights Reserved"));
}

void SyncClientWindow::showQtAboutDialog()
{
    QMessageBox::aboutQt(this, tr("Sync Client (Qt)"));
}

void SyncClientWindow::startManualSync()
{
    SyncPointEditDialog dialog(m_engine, SyncPointEditDialog::Mode::MANUAL_SYNC_POINT);

    if (dialog.exec() == QDialog::Accepted)
    {
        const SyncPoint syncPoint = dialog.getSyncPoint();
        Q_ASSERT(syncPoint.isValid());

        if (syncPoint.isValid())
        {
            performManualSync(syncPoint);
        }
    }
}

void SyncClientWindow::performManualSync(
        const SyncPoint& syncPoint)
{
    SyncContext* syncContext = m_engine->createSyncContext(syncPoint);
    Q_ASSERT(syncContext);

    ManualSyncWidget* msyncWidget = new ManualSyncWidget(syncContext, m_mainTabWidget);

    m_mainTabWidget->addTab(msyncWidget, QIcon(":/icons/tab_synchronize"),
            tr("Sync: ") + syncPoint.getName());
    m_mainTabWidget->setCurrentWidget(msyncWidget);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
