//-----------------------------------------------------------------------------
/// \file
/// SyncPointEditDialog - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncPointEditDialog.hpp"
#include "ShareSelectDialog.hpp"

// library includes
#include <QtWidgets/QAction>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLayout>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QMessageBox>
#include <ui/ActionButton.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::ActionButton;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncPointEditDialog::SyncPointEditDialog(
        SyncClientEngine* engine,
        const Mode mode,
        QWidget* parent,
        Qt::WindowFlags flags) :
                QDialog(parent, flags),
                m_dialogMode(mode),
                m_engine(engine)
{
    setSizeGripEnabled(true);
    resize(600, 450);

    createActions();
    createWidgets();
    connectActions();

    setSyncPoint(SyncPoint());
}

void SyncPointEditDialog::createActions()
{
    m_actionSelectShareA = new QAction(this);
    m_actionSelectShareA->setText("Select...");

    m_actionSelectShareB = new QAction(this);
    m_actionSelectShareB->setText("Select...");

    m_actionDialogOk = new QAction(this);
    m_actionDialogOk->setText("OK");

    m_actionDialogCancel = new QAction(this);
    m_actionDialogCancel->setText("Cancel");
}

void SyncPointEditDialog::createWidgets()
{
    m_name = new QLineEdit();
    m_syncMode = new QComboBox(this);
    for (auto mode : SyncMode::getAllModes())
    {
        m_syncMode->addItem(mode.toString(), mode.toInt());
    }

    m_serverNameA = new QLabel(this);
    m_shareNameA = new QLabel(this);
    m_serverIdA = new QLabel(this);
    m_shareIdA = new QLabel(this);

    m_serverNameB = new QLabel(this);
    m_shareNameB = new QLabel(this);
    m_serverIdB = new QLabel(this);
    m_shareIdB = new QLabel(this);

    QFormLayout* const formLayout = new QFormLayout();
    formLayout->addRow(tr("Name"), m_name);
    formLayout->addRow(tr("Mode"), m_syncMode);
    formLayout->addRow(tr("Share A"), new ActionButton(m_actionSelectShareA));
    formLayout->addRow(tr("- server name"), m_serverNameA);
    formLayout->addRow(tr("- share name"), m_shareNameA);
    formLayout->addRow(tr("- server id"), m_serverIdA);
    formLayout->addRow(tr("- share id"), m_shareIdA);
    formLayout->addRow(tr("Share B"), new ActionButton(m_actionSelectShareB));
    formLayout->addRow(tr("- server name"), m_serverNameB);
    formLayout->addRow(tr("- share name"), m_shareNameB);
    formLayout->addRow(tr("- server id"), m_serverIdB);
    formLayout->addRow(tr("- share id"), m_shareIdB);

    ActionButton* const dialogOkButton = new ActionButton(m_actionDialogOk, this);
    ActionButton* const dialogCancelButton = new ActionButton(m_actionDialogCancel, this);

    QHBoxLayout* const dialogButtonsLayout = new QHBoxLayout();
    dialogButtonsLayout->addSpacing(10);
    dialogButtonsLayout->addWidget(dialogOkButton);
    dialogButtonsLayout->addStretch(1);
    dialogButtonsLayout->addWidget(dialogCancelButton);
    dialogButtonsLayout->addSpacing(10);

    QVBoxLayout* const mainLayout = new QVBoxLayout(this);
    mainLayout->setSpacing(15);
    mainLayout->addLayout(formLayout);
    mainLayout->addLayout(dialogButtonsLayout);

    dialogOkButton->setDefault(true);

    if (m_dialogMode == Mode::MANUAL_SYNC_POINT)
    {
        m_name->setReadOnly(true);

        m_name->setEnabled(false);
        m_syncMode->setEnabled(false);
    }
}

void SyncPointEditDialog::connectActions()
{
    connect(m_actionDialogOk, &QAction::triggered, this, &SyncPointEditDialog::onDialogOk);
    connect(m_actionDialogCancel, &QAction::triggered, this, &QDialog::reject);
    connect(m_actionSelectShareA, &QAction::triggered, this, &SyncPointEditDialog::onSelectShareA);
    connect(m_actionSelectShareB, &QAction::triggered, this, &SyncPointEditDialog::onSelectShareB);

    connect(m_name, &QLineEdit::textChanged, this, &SyncPointEditDialog::onNameChanged);
    connect(m_syncMode, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &SyncPointEditDialog::onModeChanged);
}

void SyncPointEditDialog::onNameChanged(
        const QString& text)
{
    m_syncPoint.setName(text);
}

void SyncPointEditDialog::onModeChanged(
        int index)
{
    if (index >= 0)
    {
        const int intMode = m_syncMode->itemData(index).toInt();

        SyncMode mode;
        if (mode.fromInt(intMode))
        {
            m_syncPoint.setMode(mode);
        }
    }
}

void SyncPointEditDialog::onDialogOk()
{
    if (m_syncPoint.isValid())
    {
        accept();
    }
    else
    {
        QMessageBox::information(this, tr("Invalid data"),
                tr("Please enter valid sync point data."));
    }
}

void SyncPointEditDialog::onSelectShareA()
{
    ShareSelectDialog dialog(m_engine);

    if (dialog.exec() == QDialog::Accepted)
    {
        m_syncPoint.setShareLocatorA(dialog.getShareLocator());

        updateValues();
    }
}

void SyncPointEditDialog::onSelectShareB()
{
    ShareSelectDialog dialog(m_engine);

    if (dialog.exec() == QDialog::Accepted)
    {
        m_syncPoint.setShareLocatorB(dialog.getShareLocator());

        updateValues();
    }
}

void SyncPointEditDialog::updateValues()
{
    m_name->setText(m_syncPoint.getName());

    const int mode = m_syncPoint.getMode().toInt();
    const int index = m_syncMode->findData(mode);
    if (index >= 0)
    {
        m_syncMode->setCurrentIndex(index);
    }

    if (m_syncPoint.getShareLocatorA().isValid())
    {
        const auto& serverId = m_syncPoint.getShareLocatorA().getServerId();
        const auto& shareId = m_syncPoint.getShareLocatorA().getShareId();

        m_serverNameA->setText(m_engine->getServerName(serverId));
        m_shareNameA->setText(m_engine->getShareName(serverId, shareId));
        m_serverIdA->setText(serverId.toString());
        m_shareIdA->setText(shareId.toString());
    }
    else
    {
        m_serverNameA->setText("");
        m_shareNameA->setText("");
        m_serverIdA->setText("");
        m_shareIdA->setText("");
    }

    if (m_syncPoint.getShareLocatorB().isValid())
    {
        const auto& serverId = m_syncPoint.getShareLocatorB().getServerId();
        const auto& shareId = m_syncPoint.getShareLocatorB().getShareId();

        m_serverNameB->setText(m_engine->getServerName(serverId));
        m_shareNameB->setText(m_engine->getShareName(serverId, shareId));
        m_serverIdB->setText(serverId.toString());
        m_shareIdB->setText(shareId.toString());
    }
    else
    {
        m_serverNameB->setText("");
        m_shareNameB->setText("");
        m_serverIdB->setText("");
        m_shareIdB->setText("");
    }
}

void SyncPointEditDialog::setSyncPoint(
        const SyncPoint& syncPoint)
{
    m_syncPoint = syncPoint;
    if (m_dialogMode == Mode::MANUAL_SYNC_POINT)
    {
        m_syncPoint.setName("Manual");
        m_syncPoint.setMode(SyncMode::MANUAL);
    }

    updateValues();
}

SyncPoint SyncPointEditDialog::getSyncPoint() const
{
    return m_syncPoint;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
