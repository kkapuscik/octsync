#include "SyncClientApplication.hpp"

using OCTsync::AppSyncClient::SyncClientApplication;

int main(
        int argc,
        char** argv)
{
    return SyncClientApplication(argc, argv).exec();
}
