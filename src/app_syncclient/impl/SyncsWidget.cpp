//-----------------------------------------------------------------------------
/// \file
/// SyncsWidget - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncsWidget.hpp"
#include "SyncsListWidget.hpp"
#include "SyncPointInfoWidget.hpp"
#include "SyncPointListView.hpp"

// library includes
#include <QtWidgets/QSplitter>
#include <QtWidgets/QLayout>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncsWidget::SyncsWidget(
        QWidget* parent) :
                QWidget(parent)
{
    // Splitter which divides list of sync points from sync point information
    QSplitter* mainLayoutSplitter = new QSplitter(Qt::Vertical, this);

    // Prepare widgets
    m_syncsListWidget = new SyncsListWidget(mainLayoutSplitter);
    m_syncPointInfoWidget = new SyncPointInfoWidget(SyncPointInfoWidget::Mode::FULL_INFO,
            mainLayoutSplitter);

    // Add widgets
    mainLayoutSplitter->addWidget(m_syncsListWidget);
    mainLayoutSplitter->addWidget(m_syncPointInfoWidget);

    mainLayoutSplitter->setChildrenCollapsible(false);

    mainLayoutSplitter->setStretchFactor(mainLayoutSplitter->indexOf(m_syncsListWidget), 1);
    mainLayoutSplitter->setStretchFactor(mainLayoutSplitter->indexOf(m_syncPointInfoWidget), 0);

    // Add widgets to the vertical layout
    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(mainLayoutSplitter);

    connect(m_syncsListWidget, &SyncsListWidget::manualSyncRequested, this,
            &SyncsWidget::manualSyncRequested);
    connect(m_syncsListWidget, &SyncsListWidget::currentChanged, m_syncPointInfoWidget,
            &SyncPointInfoWidget::setSyncPoint);
}

void SyncsWidget::setEngine(
        SyncClientEngine* const engine)
{
    m_syncsListWidget->setEngine(engine);
    m_syncPointInfoWidget->setEngine(engine);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
