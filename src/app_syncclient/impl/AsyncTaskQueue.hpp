//-----------------------------------------------------------------------------
/// \file
/// AsyncTaskQueue - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_AsyncTaskQueue
#define INCLUDED_OCTsync_AsyncTaskQueue

//-----------------------------------------------------------------------------

// local includes
#include "AsyncTask.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QPointer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class AsyncTaskQueue : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QList<AsyncTask*> TaskList;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    TaskList m_tasks;
    bool m_autoStartNext;

// Methods (public, protected, private)
public:
    AsyncTaskQueue(
            QObject* parent = nullptr);

    void setAutoStartNext(
            bool enabled);

    void appendTask(
            AsyncTask* task);

    bool hasMoreTasks() const;

private:
    void onTaskFinished();

    void startNextTask();

signals:
    void taskFinished(
            QPointer<AsyncTask> task);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_AsyncTaskQueue*/
