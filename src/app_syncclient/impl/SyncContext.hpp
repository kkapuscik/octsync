//-----------------------------------------------------------------------------
/// \file
/// SyncContext - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncContext
#define INCLUDED_OCTsync_SyncContext

//-----------------------------------------------------------------------------

// local includes
#include "SyncPoint.hpp"
#include "AsyncTaskQueue.hpp"
#include "SyncOp.hpp"
#include "SyncMode.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QAbstractItemModel>
#include <QtCore/QPointer>

// system includes
#include <memory>
#include <map>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class FileTreeModel;
class FileTreeItem;
class ServerDevice;

/// TODO: Class documentation
///
class SyncContext : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    enum Role
    {
        ROLE_PATH = Qt::UserRole
    };

private:
    enum class State
    {
        IDLE,
        BUILD_TREE,
        SYNCHRONIZE
    };

    typedef std::shared_ptr<FileTreeItem> ItemPtr;
    typedef std::map<QString, ItemPtr> ItemMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncClientEngine* m_engine;

    State m_state;

    SyncPoint m_syncPoint;

    FileTreeModel* m_fileTreeModel;

    AsyncTaskQueue* m_buildTreeQueue;

    AsyncTaskQueue* m_synchronizeQueue;

    ItemMap m_items;

// Methods (public, protected, private)
public:
    SyncContext(
            SyncClientEngine* engine,
            const SyncPoint& syncPoint,
            QObject* parent = nullptr);

    virtual ~SyncContext();

    const SyncPoint& getSyncPoint() const
    {
        return m_syncPoint;
    }

    QAbstractItemModel* getFileTreeModel() const;

    SyncOpSet getValidSyncOps(
            const QString& path) const;

    void setSyncOp(
            const QString& path,
            const SyncOp op);

    bool startTreeBuild();

    void applyAlgorithm(
            const SyncMode mode);

    bool startSynchronization();

private:
    void onBuildTreeTaskFinished(
            QPointer<AsyncTask> task);

    void onSynchronizeTaskFinished(
            QPointer<AsyncTask> task);

    ItemPtr createRootItem();

    ItemPtr createItem(
            const QString& name,
            const QString& path);

    ItemPtr getItem(
            ItemPtr parent,
            const QString& name,
            const QString& path);

signals:
    void treeBuildFinished();
    void syncFinished();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncContext*/
