//-----------------------------------------------------------------------------
/// \file
/// ManualSyncWidget - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ManualSyncWidget.hpp"
#include "SyncOp.hpp"

// library includes
#include <QtWidgets/QTreeView>
#include <QtWidgets/QLayout>
#include <QtWidgets/QMenu>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QAction>
#include <ui/ActionButton.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::ActionButton;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

ManualSyncWidget::ManualSyncWidget(
        SyncContext* syncContext,
        QWidget* parent) :
                QWidget(parent),
                m_syncContext(syncContext)
{
    createActions();
    createWidgets();
    connectActions();

    m_treeView->setModel(m_syncContext->getFileTreeModel());
}

void ManualSyncWidget::createActions()
{
    m_actionBuildTree = new QAction(this);
    m_actionBuildTree->setText(tr("Get Files"));

    m_actionAnalyze = new QAction(this);
    m_actionAnalyze->setText(tr("Analyze"));

    m_actionSynchronize = new QAction(this);
    m_actionSynchronize->setText(tr("Synchronize"));
}

void ManualSyncWidget::createWidgets()
{
    m_comboBoxAlgorithm = new QComboBox(this);
    for (auto mode : SyncMode::getAllModes())
    {
        m_comboBoxAlgorithm->addItem(mode.toString(), mode.toInt());
    }

    m_treeView = new QTreeView(this);
    m_treeView->setContextMenuPolicy(Qt::CustomContextMenu);

    auto buttonsLayout = new QHBoxLayout();
    buttonsLayout->setSpacing(5);
    buttonsLayout->addWidget(new ActionButton(m_actionBuildTree));
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(m_comboBoxAlgorithm);
    buttonsLayout->addWidget(new ActionButton(m_actionAnalyze));
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(new ActionButton(m_actionSynchronize));

    auto mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(buttonsLayout);
    mainLayout->addWidget(m_treeView);
}

void ManualSyncWidget::connectActions()
{
    connect(m_treeView, &QTreeView::customContextMenuRequested, this,
            &ManualSyncWidget::onTreeContextMenuRequested);

    connect(m_actionBuildTree, &QAction::triggered, this, &ManualSyncWidget::onBuildTreeRequested);
    connect(m_syncContext, &SyncContext::treeBuildFinished, this,
            &ManualSyncWidget::onBuildTreeFinished);

    connect(m_actionAnalyze, &QAction::triggered, this, &ManualSyncWidget::onAnalysisRequested);

    connect(m_actionSynchronize, &QAction::triggered, this, &ManualSyncWidget::onSyncRequested);
    connect(m_syncContext, &SyncContext::syncFinished, this, &ManualSyncWidget::onSyncFinished);
}

void ManualSyncWidget::onTreeContextMenuRequested(
        const QPoint& pos)
{
    const QModelIndex itemIndex = m_treeView->indexAt(pos);
    if (itemIndex.isValid())
    {
        const QModelIndex dataIndex = m_treeView->model()->index(itemIndex.row(), 0,
                itemIndex.parent());

        const QString path =
                m_treeView->model()->data(dataIndex, SyncContext::ROLE_PATH).toString();
        const SyncOpSet validOps = m_syncContext->getValidSyncOps(path);
        if (validOps)
        {
            QMenu popupMenu;

            QAction* actionNone = nullptr;
            QAction* actionCopyAB = nullptr;
            QAction* actionCopyBA = nullptr;
            QAction* actionSafeAB = nullptr;
            QAction* actionSafeBA = nullptr;
            QAction* actionDeleteA = nullptr;
            QAction* actionDeleteB = nullptr;

            actionNone = popupMenu.addAction(QIcon(":/icons/op_none"), tr("No Action"));
            if (validOps.isSet(SyncOp::COPY_A_TO_B))
            {
                actionCopyAB = popupMenu.addAction(QIcon(":/icons/op_copy_a_to_b"),
                        tr("Copy A->B"));
            }
            if (validOps.isSet(SyncOp::COPY_B_TO_A))
            {
                actionCopyBA = popupMenu.addAction(QIcon(":/icons/op_copy_b_to_a"),
                        tr("Copy B->A"));
            }
            if (validOps.isSet(SyncOp::COPY_A_TO_B_WITH_BACKUP))
            {
                actionSafeAB = popupMenu.addAction(QIcon(":/icons/op_copy_a_to_b_safe"),
                        tr("Copy A->B w/Backup"));
            }
            if (validOps.isSet(SyncOp::COPY_B_TO_A_WITH_BACKUP))
            {
                actionSafeBA = popupMenu.addAction(QIcon(":/icons/op_copy_b_to_a_safe"),
                        tr("Copy B->A w/Backup"));
            }
            if (validOps.isSet(SyncOp::DELETE_A))
            {
                actionDeleteA = popupMenu.addAction(QIcon(":/icons/op_delete_a"), tr("Delete A"));
            }
            if (validOps.isSet(SyncOp::DELETE_B))
            {
                actionDeleteB = popupMenu.addAction(QIcon(":/icons/op_delete_b"), tr("Delete B"));
            }

            QAction* actionSelected = popupMenu.exec(m_treeView->mapToGlobal(pos), nullptr);
            if (actionSelected == nullptr)
            {
                // ignore
            }
            else if (actionSelected == actionNone)
            {
                m_syncContext->setSyncOp(path, SyncOp::NONE);
            }
            else if (actionSelected == actionCopyAB)
            {
                m_syncContext->setSyncOp(path, SyncOp::COPY_A_TO_B);
            }
            else if (actionSelected == actionCopyBA)
            {
                m_syncContext->setSyncOp(path, SyncOp::COPY_B_TO_A);
            }
            else if (actionSelected == actionSafeAB)
            {
                m_syncContext->setSyncOp(path, SyncOp::COPY_A_TO_B_WITH_BACKUP);
            }
            else if (actionSelected == actionSafeBA)
            {
                m_syncContext->setSyncOp(path, SyncOp::COPY_B_TO_A_WITH_BACKUP);
            }
            else if (actionSelected == actionDeleteA)
            {
                m_syncContext->setSyncOp(path, SyncOp::DELETE_A);
            }
            else if (actionSelected == actionDeleteB)
            {
                m_syncContext->setSyncOp(path, SyncOp::DELETE_B);
            }
        }
    }
}

void ManualSyncWidget::onBuildTreeRequested()
{
    if (m_syncContext->startTreeBuild())
    {
        m_actionBuildTree->setEnabled(false);
        m_actionAnalyze->setEnabled(false);
        m_actionSynchronize->setEnabled(false);
    }
}

void ManualSyncWidget::onBuildTreeFinished()
{
    m_actionBuildTree->setEnabled(true);
    m_actionAnalyze->setEnabled(true);
    m_actionSynchronize->setEnabled(true);

    m_treeView->expandAll();
    m_treeView->resizeColumnToContents(0);
}

void ManualSyncWidget::onAnalysisRequested()
{
    const int modeIndex = m_comboBoxAlgorithm->currentIndex();
    const int intMode = m_comboBoxAlgorithm->itemData(modeIndex).toInt();

    SyncMode mode;
    if (mode.fromInt(intMode))
    {
        m_syncContext->applyAlgorithm(mode);
    }
}

void ManualSyncWidget::onSyncRequested()
{
    if (m_syncContext->startSynchronization())
    {
        m_actionBuildTree->setEnabled(false);
        m_actionAnalyze->setEnabled(false);
        m_actionSynchronize->setEnabled(false);
    }
}

void ManualSyncWidget::onSyncFinished()
{
    m_actionBuildTree->setEnabled(true);
    m_actionAnalyze->setEnabled(true);
    m_actionSynchronize->setEnabled(true);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
