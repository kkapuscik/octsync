//-----------------------------------------------------------------------------
/// \file
/// SyncPointListModel - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncPointListModel
#define INCLUDED_OCTsync_SyncPointListModel

//-----------------------------------------------------------------------------

// local includes
#include "ServerDevice.hpp"

// library includes
#include <QtGui/QStandardItemModel>

// system includes
#include <memory>
#include <map>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class SyncPointsData;
class SyncPoint;

/// TODO: Class documentation
///
class SyncPointListModel : public QStandardItemModel
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncPointsData* m_syncData;

// Methods (public, protected, private)
public:
    explicit SyncPointListModel(SyncPointsData* syncData,
            QObject *parent = nullptr);

    void addSyncPoint(
            const SyncPoint& syncPoint);

    void removeSyncPoint(
            const SyncPoint& syncPoint);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncPointListModel*/
