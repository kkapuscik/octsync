//-----------------------------------------------------------------------------
/// \file
/// SyncShare - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncShare
#define INCLUDED_OCTsync_SyncShare

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QString>
#include <commons/Uuid.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncShare
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_uuid;
    QString m_name;
    QString m_description;
    bool m_readOnly;

// Methods (public, protected, private)
public:
    SyncShare();

    bool isValid() const;

    Commons::Uuid getUuid() const
    {
        return m_uuid;
    }

    void setUuid(
            const Commons::Uuid& uuid)
    {
        m_uuid = uuid;
    }

    QString getName() const
    {
        return m_name;
    }

    void setName(
            const QString& name)
    {
        m_name = name;
    }

    QString getDescription() const
    {
        return m_description;
    }

    void setDescription(
            const QString& description)
    {
        m_description = description;
    }

    bool isReadOnly() const
    {
        return m_readOnly;
    }

    void setReadOnly(
            const bool readOnly)
    {
        m_readOnly = readOnly;
    }

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncShare*/
