//-----------------------------------------------------------------------------
/// \file
/// ReadDirTask - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ReadDirTask
#define INCLUDED_OCTsync_ReadDirTask

//-----------------------------------------------------------------------------

// local includes
#include "AsyncTask.hpp"
#include "FileInfo.hpp"

// library includes
#include <QtCore/QPointer>
#include <QtCore/QList>
#include <boprpc/Executor.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class ServerDevice;

/// TODO: Class documentation
///
class ReadDirTask : public AsyncTask
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const int MAX_READ_DIR_COUNT = 30;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_serverId;
    Commons::Uuid m_shareId;
    QString m_path;

    quint32 m_dirId;

    bool m_taskSuccess;
    QList<FileInfo> m_infos;

// Methods (public, protected, private)
public:
    ReadDirTask(
            SyncClientEngine* engine,
            const Commons::Uuid& serverId,
            const Commons::Uuid& shareId,
            const QString& path,
            QObject* parent = nullptr);

    const Commons::Uuid& getServerId() const
    {
        return m_serverId;
    }

    const Commons::Uuid& getShareId() const
    {
        return m_shareId;
    }

    const QString& getPath() const
    {
        return m_path;
    }

    bool getResult() const
    {
        return m_taskSuccess;
    }

    QList<FileInfo> getInfos() const
    {
        return m_infos;
    }

protected:
    virtual void run();

private:
    void requestOpenDir();

    void onOpenDirFinished(
            QPointer<BopRpc::Executor> executor);

    void requestReadDir();

    void onReadDirFinished(
            QPointer<BopRpc::Executor> executor);

    void requestCloseDir();

    void onCloseDirFinished(
            QPointer<BopRpc::Executor> executor);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ReadDirTask*/
