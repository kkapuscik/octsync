//-----------------------------------------------------------------------------
/// \file
/// SyncsRepository - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncsRepository.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncsRepository::SyncsRepository()
{
    // nothing to do
}

bool SyncsRepository::addSync(
        const SyncPoint& sync)
{
    if (!sync.isValid())
    {
        return false;
    }

    if (hasSync(sync.getUuid()))
    {
        return false;
    }

    m_syncs.insert(sync.getUuid(), sync);

    return true;
}

bool SyncsRepository::modifySync(
        const SyncPoint& sync)
{
    if (!sync.isValid())
    {
        return false;
    }

    if (!hasSync(sync.getUuid()))
    {
        return false;
    }

    m_syncs.insert(sync.getUuid(), sync);

    return true;
}

bool SyncsRepository::removeSync(
        const SyncPoint& sync)
{
    if (hasSync(sync.getUuid()))
    {
    	m_syncs.remove(sync.getUuid());
        return true;
    }
    else
    {
        return false;
    }
}

void SyncsRepository::removeAllSyncs()
{
	m_syncs.clear();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
