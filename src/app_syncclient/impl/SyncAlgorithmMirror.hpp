//-----------------------------------------------------------------------------
/// \file
/// SyncAlgorithmMirror - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncAlgorithmMirror
#define INCLUDED_OCTsync_SyncAlgorithmMirror

//-----------------------------------------------------------------------------

// local includes
#include "SyncAlgorithm.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncAlgorithmMirror : public SyncAlgorithm
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    enum class Direction
    {
        A_TO_B,
        B_TO_A
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Direction m_direction;
    bool m_safeMode;

// Methods (public, protected, private)
public:
    SyncAlgorithmMirror(
            const Direction direction,
            const bool safeMode) :
                    SyncAlgorithm(),
                    m_direction(direction),
                    m_safeMode(safeMode)
    {
        // nothing to do
    }

    virtual void processItem(
            FileTreeItem& item);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncAlgorithmMirror*/
