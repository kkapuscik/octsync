//-----------------------------------------------------------------------------
/// \file
/// SyncClientApplication - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncClientApplication.hpp"

// library includes
#include <QtWidgets/QMenu>
#include <QtWidgets/QAction>
#include <QtCore/QtGlobal>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

// #define USE_TRAY_ICON

//-----------------------------------------------------------------------------

static void initResources()
{
    Q_INIT_RESOURCE(octsync);
}

namespace OCTsync
{
namespace AppSyncClient
{

SyncClientApplication::SyncClientApplication(
        int& argc,
        char** argv) :
                QApplication(argc, argv)
{
    m_engine = new SyncClientEngine(this);

    m_mainWindow.reset(new SyncClientWindow(m_engine));

#ifdef USE_TRAY_ICON
    m_trayContextMenu.reset(new QMenu());
    QAction* trayShowWindowAction = m_trayContextMenu->addAction(tr("&Show"));
    connect(trayShowWindowAction, &QAction::triggered, m_mainWindow.get(), &QMainWindow::show);
    m_trayContextMenu->addSeparator();
    QAction* trayQuitAppAction = m_trayContextMenu->addAction(tr("&Quit"));
    connect(trayQuitAppAction, &QAction::triggered, &QApplication::quit);

    m_trayIcon = new QSystemTrayIcon(this);
    connect(m_trayIcon, &QSystemTrayIcon::activated, this,
            &SyncClientApplication::onTrayIconActivated);
    connect(m_trayIcon, &QSystemTrayIcon::messageClicked, m_mainWindow.get(), &QMainWindow::show);
#else
    m_trayIcon = nullptr;
#endif
}

int SyncClientApplication::exec()
{
    initResources();

    m_engine->start();

    if (m_trayIcon)
    {
        m_trayIcon->show();
        m_trayIcon->setContextMenu(m_trayContextMenu.get());
    }

    m_mainWindow->show();

    return QApplication::exec();
}

void SyncClientApplication::onTrayIconActivated(
        QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
        case QSystemTrayIcon::Trigger:
            m_mainWindow->show();
            break;
        default:
            break;
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
