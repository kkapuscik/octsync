//-----------------------------------------------------------------------------
/// \file
/// SyncAlgorithmManual - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncAlgorithmManual.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

void SyncAlgorithmManual::processItem(
        FileTreeItem& item)
{
    item.setSyncOp(SyncOp::NONE);
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
