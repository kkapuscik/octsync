//-----------------------------------------------------------------------------
/// \file
/// FileTreeItem - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "FileTreeItem.hpp"
#include "SyncContext.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

FileTreeItem::FileTreeItem(
        const QString& name,
        const QString& path) :
                m_name(name),
                m_path(path)
{
    m_nameItem = new QStandardItem();
    m_nameItem->setEditable(false);
    m_nameItem->setText(name);
    m_nameItem->setData(path, SyncContext::ROLE_PATH);

    m_operationItem = new QStandardItem();
    m_operationItem->setEditable(false);
    setSyncOp(SyncOp::NONE);

    m_infoSizeItemA = new QStandardItem();
    m_infoSizeItemA->setEditable(false);

    m_infoSizeItemB = new QStandardItem();
    m_infoSizeItemB->setEditable(false);

    m_dateItemA = new QStandardItem();
    m_dateItemA->setEditable(false);

    m_dateItemB = new QStandardItem();
    m_dateItemB->setEditable(false);
}

FileInfo::Type FileTreeItem::getType() const
{
    FileInfo::Type type = FileInfo::Type::OTHER;

    // check if both exists
    if (m_infoA.isValid() && m_infoB.isValid())
    {
        if (m_infoA.getType() == m_infoB.getType())
        {
            type = m_infoA.getType();
        }
    }
    else if (m_infoA.isValid())
    {
        type = m_infoA.getType();
    }
    else if (m_infoB.isValid())
    {
        type = m_infoB.getType();
    }

    return type;
}

SyncOpSet FileTreeItem::getValidOps() const
{
    SyncOpSet validOps;

    if (getType() == FileInfo::Type::FILE)
    {
        // check if both exists
        if (m_infoA.isValid() && m_infoB.isValid())
        {
            validOps.set(SyncOp::COPY_A_TO_B);
            validOps.set(SyncOp::COPY_B_TO_A);
            validOps.set(SyncOp::COPY_A_TO_B_WITH_BACKUP);
            validOps.set(SyncOp::COPY_B_TO_A_WITH_BACKUP);
            validOps.set(SyncOp::DELETE_A);
            validOps.set(SyncOp::DELETE_B);
        }
        else if (m_infoA.isValid())
        {
            validOps.set(SyncOp::DELETE_A);
            validOps.set(SyncOp::COPY_A_TO_B);
        }
        else if (m_infoB.isValid())
        {
            validOps.set(SyncOp::DELETE_B);
            validOps.set(SyncOp::COPY_B_TO_A);
        }
    }

    return validOps;
}

void FileTreeItem::setSyncOp(
        const SyncOp op)
{
    if (op != SyncOp::NONE)
    {
        Q_ASSERT(getValidOps().isSet(op));
    }

    m_syncOp = op;

    switch (m_syncOp)
    {
        case SyncOp::NONE:
            m_operationItem->setIcon(QIcon(":/icons/op_none"));
            break;
        case SyncOp::COPY_A_TO_B:
            m_operationItem->setIcon(QIcon(":/icons/op_copy_a_to_b"));
            break;
        case SyncOp::COPY_B_TO_A:
            m_operationItem->setIcon(QIcon(":/icons/op_copy_b_to_a"));
            break;
        case SyncOp::COPY_A_TO_B_WITH_BACKUP:
            m_operationItem->setIcon(QIcon(":/icons/op_copy_a_to_b_safe"));
            break;
        case SyncOp::COPY_B_TO_A_WITH_BACKUP:
            m_operationItem->setIcon(QIcon(":/icons/op_copy_b_to_a_safe"));
            break;
        case SyncOp::DELETE_A:
            m_operationItem->setIcon(QIcon(":/icons/op_delete_a"));
            break;
        case SyncOp::DELETE_B:
            m_operationItem->setIcon(QIcon(":/icons/op_delete_b"));
            break;
        default:
            Q_ASSERT(0);
            break;
    }
    // (SyncOp::NONE)

}

// TODO: set share name?
void FileTreeItem::setRootInfo()
{
    m_infoSizeItemA->setText("Share A");
    m_infoSizeItemB->setText("Share B");
}

void FileTreeItem::updateCommonInfo()
{
    FileInfo::Type type = FileInfo::Type::OTHER;

    if (m_infoA.isValid() && m_infoB.isValid())
    {
        if (m_infoA.getType() == m_infoB.getType())
        {
            type = m_infoA.getType();
        }
        else
        {
            // TODO
        }
    }
    else if (m_infoA.isValid())
    {
        type = m_infoA.getType();
    }
    else if (m_infoB.isValid())
    {
        type = m_infoB.getType();
    }

    switch (type)
    {
        case FileInfo::Type::DIRECTORY:
            m_nameItem->setIcon(QIcon(":/icons/item_dir"));
            break;
        case FileInfo::Type::FILE:
            m_nameItem->setIcon(QIcon(":/icons/item_file"));
            break;
        case FileInfo::Type::OTHER:
        default:
            m_nameItem->setIcon(QIcon(":/icons/item_other"));
            break;
    }
}

void FileTreeItem::updateData(
        const FileInfo& info,
        QStandardItem* const infoSizeItem,
        QStandardItem* const dateItem)
{
    Q_ASSERT(infoSizeItem);
    Q_ASSERT(dateItem);

    if (info.isValid())
    {
        switch (info.getType())
        {
            case FileInfo::Type::DIRECTORY:
                infoSizeItem->setText("Dir");
                break;
            case FileInfo::Type::FILE:
                infoSizeItem->setText(QString("%1").arg(info.getSize()));
                break;
            case FileInfo::Type::OTHER:
            default:
                infoSizeItem->setText("Other");
                break;
        }

        // dateItem->setText(info.getMTime().toString());
        dateItem->setText(info.getMTime().toString("yyyy-MM-dd hh:mm:ss"));
    }
    else
    {
        infoSizeItem->setText("");
        dateItem->setText("");
    }
}

void FileTreeItem::updateInfoA(
        const FileInfo& info)
{
    m_infoA = info;

    updateData(info, m_infoSizeItemA, m_dateItemA);
    updateCommonInfo();
}

void FileTreeItem::updateInfoB(
        const FileInfo& info)
{
    m_infoB = info;

    updateData(info, m_infoSizeItemB, m_dateItemB);
    updateCommonInfo();
}

const QString& FileTreeItem::getPath() const
{
    return m_path;
}

QStandardItem* FileTreeItem::getModelItem() const
{
    return m_nameItem;
}

QList<QStandardItem*> FileTreeItem::getRowItems() const
{
    return QList<QStandardItem*>() << m_nameItem << m_operationItem << m_infoSizeItemA
            << m_infoSizeItemB << m_dateItemA << m_dateItemB;
}

QModelIndex FileTreeItem::getItemIndex() const
{
    return m_nameItem->index();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
