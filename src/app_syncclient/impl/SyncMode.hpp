//-----------------------------------------------------------------------------
/// \file
/// SyncMode - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncMode
#define INCLUDED_OCTsync_SyncMode

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QString>
#include <QtCore/QList>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: documentation
///
class SyncMode
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    static SyncMode MANUAL;
    static SyncMode FULL_TWO_SIDED;
    static SyncMode SAFE_TWO_SIDED;
    static SyncMode MIRROR_A_TO_B;
    static SyncMode MIRROR_B_TO_A;
    static SyncMode NEW_ONLY_A_TO_B;
    static SyncMode NEW_ONLY_B_TO_A;

private:
    /// TODO: Class documentation
    ///
    enum Mode
    {
        /// Manual synchronization only.
        MODE_MANUAL = 0,

        /// Perform full synchronization of A and B.
        MODE_FULL_TWO_SIDED = 10,

        /// Copy/update files between A and B, no deletions.
        MODE_SAFE_TWO_SIDED = 11,

        /// Make B a 1:1 copy of A.
        MODE_MIRROR_A_TO_B = 20,

        /// Make A a 1:1 copy of B.
        MODE_MIRROR_B_TO_A = 21,

        /// Copy/update files from A to B, no deletions.
        MODE_NEW_ONLY_A_TO_B = 30,

        /// Copy/update files from B to A, no deletions.
        MODE_NEW_ONLY_B_TO_A = 31
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
public:
    static QList<SyncMode> getAllModes();

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Mode m_mode;

// Methods (public, protected, private)
public:
    SyncMode() :
                    m_mode(MODE_MANUAL)
    {
        // nothing to do
    }

    int toInt() const;

    bool fromInt(
            int value);

    QString toString() const;

    friend bool operator ==(
            const SyncMode& mode1,
            const SyncMode& mode2)
    {
        return mode1.m_mode == mode2.m_mode;
    }

    friend bool operator !=(
            const SyncMode& mode1,
            const SyncMode& mode2)
    {
        return mode1.m_mode != mode2.m_mode;
    }

private:
    SyncMode(
            const Mode mode) :
                    m_mode(mode)
    {
        // nothing to do
    }
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncMode*/
