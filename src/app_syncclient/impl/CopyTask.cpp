//-----------------------------------------------------------------------------
/// \file
/// CopyTask - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "CopyTask.hpp"

// library includes
#include <syncserver/OpenFileExecutor.hpp>
#include <syncserver/CloseFileExecutor.hpp>
#include <syncserver/ReadFileExecutor.hpp>
#include <syncserver/WriteFileExecutor.hpp>
#include <boprpc/BadArgumentException.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::Executor;
using OCTsync::SyncServer::OpenFileExecutor;
using OCTsync::SyncServer::CloseFileExecutor;
using OCTsync::SyncServer::ReadFileExecutor;
using OCTsync::SyncServer::WriteFileExecutor;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

CopyTask::CopyTask(
        SyncClientEngine* engine,
        const Commons::Uuid& srcServer,
        const Commons::Uuid& srcShareId,
        const Commons::Uuid& dstServer,
        const Commons::Uuid& dstShareId,
        const QString& path,
        const qint64 mtime,
        QObject* parent) :
                AsyncTask(engine, parent),
                m_srcServer(srcServer),
                m_srcShareId(srcShareId),
                m_dstServer(dstServer),
                m_dstShareId(dstShareId),
                m_path(path),
                m_mtime(mtime),
                m_result(false),
                m_srcFileOpened(false),
                m_dstFileOpened(false),
                m_srcFileId(0),
                m_dstFileId(0)
{
    // nothing to do
}

void CopyTask::run()
{
    requestOpenRead();
}

void CopyTask::requestOpenRead()
{
    OpenFileExecutor* executor = new OpenFileExecutor(this);

    setTarget(executor, m_srcServer, true);

    connect(executor, &Executor::finished, this, &CopyTask::onOpenReadFinished);

    executor->setShareId(m_srcShareId);
    executor->setPath(m_path);
    executor->setMode("Read");
    executor->setMTime(0);

    executor->execute();
}

void CopyTask::onOpenReadFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        if (executor->getErrorCode() == 0)
        {
            OpenFileExecutor* const openFileExecutor =
                    static_cast<OpenFileExecutor*>(executor.data());

            try
            {
                m_srcFileId = openFileExecutor->getFileId();
                m_srcFileOpened = true;
                success = true;
            }
            catch (BadArgumentException& e)
            {
                qDebug() << e.getMessage();
            }
        }
    }

    if (success)
    {
        requestOpenWrite();
    }
    else
    {
        requestCloseRead();
    }

    executor->deleteLater();
}

void CopyTask::requestOpenWrite()
{
    OpenFileExecutor* executor = new OpenFileExecutor(this);

    setTarget(executor, m_dstServer, true);

    connect(executor, &Executor::finished, this, &CopyTask::onOpenWriteFinished);

    executor->setShareId(m_dstShareId);
    executor->setPath(m_path);
    executor->setMode("Write");
    executor->setMTime(m_mtime);
    executor->execute();
}

void CopyTask::onOpenWriteFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        if (executor->getErrorCode() == 0)
        {
            OpenFileExecutor* const openFileExecutor =
                    static_cast<OpenFileExecutor*>(executor.data());

            try
            {
                m_dstFileId = openFileExecutor->getFileId();
                m_dstFileOpened = true;
                success = true;
            }
            catch (BadArgumentException& e)
            {
                qDebug() << e.getMessage();
            }
        }
    }

    if (success)
    {
        requestReadData();
    }
    else
    {
        requestCloseRead();
    }

    executor->deleteLater();
}

void CopyTask::requestReadData()
{
    ReadFileExecutor* executor = new ReadFileExecutor(this);

    setTarget(executor, m_srcServer, true);

    connect(executor, &Executor::finished, this, &CopyTask::onReadDataFinished);

    executor->setFileId(m_srcFileId);
    executor->setMaxSize(MAX_CHUNK_SIZE);
    executor->execute();
}

void CopyTask::onReadDataFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;
    QByteArray data;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        if (executor->getErrorCode() == 0)
        {
            ReadFileExecutor* const readFileExecutor =
                    static_cast<ReadFileExecutor*>(executor.data());

            try
            {
                data = readFileExecutor->getData();
                success = true;
            }
            catch (BadArgumentException& e)
            {
                qDebug() << e.getMessage();
            }
        }
    }

    if (success)
    {
        if (data.size() > 0)
        {
            requestWriteData(data);
        }
        else
        {
            m_result = true;
            requestCloseRead();
        }
    }
    else
    {
        requestCloseRead();
    }

    executor->deleteLater();
}

void CopyTask::requestWriteData(
        const QByteArray& bytes)
{
    WriteFileExecutor* executor = new WriteFileExecutor(this);

    setTarget(executor, m_dstServer, true);

    connect(executor, &Executor::finished, this, &CopyTask::onWriteDataFinished);

    executor->setFileId(m_dstFileId);
    executor->setData(bytes);
    executor->execute();
}

void CopyTask::onWriteDataFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;
    QByteArray data;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        if (executor->getErrorCode() == 0)
        {
            success = true;
        }
    }

    if (success)
    {
        requestReadData();
    }
    else
    {
        requestCloseRead();
    }

    executor->deleteLater();
}

void CopyTask::requestCloseRead()
{
    if (m_srcFileOpened)
    {
        CloseFileExecutor* executor = new CloseFileExecutor(this);

        setTarget(executor, m_srcServer, true);

        connect(executor, &Executor::finished, this, &CopyTask::onCloseReadFinished);

        executor->setFileId(m_srcFileId);
        executor->execute();
    }
    else
    {
        requestCloseWrite();
    }
}

void CopyTask::onCloseReadFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;
    QByteArray data;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        if (executor->getErrorCode() != 0)
        {
            m_result = false;
        }
    }

    requestCloseWrite();

    executor->deleteLater();
}

void CopyTask::requestCloseWrite()
{
    if (m_dstFileOpened)
    {
        CloseFileExecutor* executor = new CloseFileExecutor(this);

        setTarget(executor, m_dstServer, true);

        connect(executor, &Executor::finished, this, &CopyTask::onCloseWriteFinished);

        executor->setFileId(m_dstFileId);
        executor->execute();
    }
    else
    {
        notifyFinished();
    }
}

void CopyTask::onCloseWriteFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;
    QByteArray data;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        if (executor->getErrorCode() != 0)
        {
            m_result = false;
        }
    }

    notifyFinished();

    executor->deleteLater();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
