//-----------------------------------------------------------------------------
/// \file
/// CopyTask - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_CopyTask
#define INCLUDED_OCTsync_CopyTask

//-----------------------------------------------------------------------------

// local includes
#include "AsyncTask.hpp"
#include "ServerDevice.hpp"

// library includes
#include <QtCore/QByteArray>
#include <commons/Uuid.hpp>
#include <boprpc/Executor.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class CopyTask : public AsyncTask
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const qint32 MAX_CHUNK_SIZE = 16 * 1024;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_srcServer;
    Commons::Uuid m_srcShareId;
    Commons::Uuid m_dstServer;
    Commons::Uuid m_dstShareId;
    QString m_path;
    qint64 m_mtime;

    bool m_result;

    bool m_srcFileOpened;
    bool m_dstFileOpened;
    quint32 m_srcFileId;
    quint32 m_dstFileId;

// Methods (public, protected, private)
public:
    CopyTask(
            SyncClientEngine* engine,
            const Commons::Uuid& srcServer,
            const Commons::Uuid& srcShareId,
            const Commons::Uuid& dstServer,
            const Commons::Uuid& dstShareId,
            const QString& path,
            const qint64 mtime,
            QObject* parent = nullptr);

    bool getResult() const
    {
        return m_result;
    }

protected:
    virtual void run();

private:
    void requestOpenRead();

    void onOpenReadFinished(
            QPointer<BopRpc::Executor> executor);

    void requestOpenWrite();

    void onOpenWriteFinished(
            QPointer<BopRpc::Executor> executor);

    void requestReadData();

    void onReadDataFinished(
            QPointer<BopRpc::Executor> executor);

    void requestWriteData(
            const QByteArray& bytes);

    void onWriteDataFinished(
            QPointer<BopRpc::Executor> executor);

    void requestCloseRead();

    void onCloseReadFinished(
            QPointer<BopRpc::Executor> executor);

    void requestCloseWrite();

    void onCloseWriteFinished(
            QPointer<BopRpc::Executor> executor);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_CopyTask*/
