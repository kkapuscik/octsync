//-----------------------------------------------------------------------------
/// \file
/// FileTreeModel - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "FileTreeModel.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

FileTreeModel::FileTreeModel(
        QObject* parent) :
                QStandardItemModel(parent)
{
    QStringList headerLabels;
    headerLabels << tr("Name") << tr("Operation") << tr("Info/Size A") << tr("Info/Size B")
            << tr("Date A") << tr("Date B");
    setHorizontalHeaderLabels(headerLabels);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
