//-----------------------------------------------------------------------------
/// \file
/// SynchronizeTask - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SynchronizeTask
#define INCLUDED_OCTsync_SynchronizeTask

//-----------------------------------------------------------------------------

// local includes
#include "AsyncTask.hpp"
#include "ServerDevice.hpp"
#include "SyncOp.hpp"
#include "SafeRenameTask.hpp"
#include "CopyTask.hpp"
#include "DeleteTask.hpp"

// library includes
#include <QtCore/QPointer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SynchronizeTask : public AsyncTask
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_serverIdA;
    Commons::Uuid m_shareIdA;
    Commons::Uuid m_serverIdB;
    Commons::Uuid m_shareIdB;
    QString m_path;
    qint64 m_mtimeA;
    qint64 m_mtimeB;
    SyncOp m_syncOp;

    bool m_result;

    QPointer<SafeRenameTask> m_safeRenameTask;
    QPointer<CopyTask> m_backupCopyTask;
    QPointer<DeleteTask> m_deleteTask;
    QPointer<CopyTask> m_copyTask;

// Methods (public, protected, private)
public:
    SynchronizeTask(
            SyncClientEngine* engine,
            const Commons::Uuid& serverIdA,
            const Commons::Uuid& shareIdA,
            const Commons::Uuid& serverIdB,
            const Commons::Uuid& shareIdB,
            const QString& path,
            const qint64 mtimeA,
            const qint64 mtimeB,
            const SyncOp syncOp,
            QObject* parent = nullptr);

protected:
    virtual void run();

private:
    void requestSafeRename(
            const Commons::Uuid& serverId,
            const Commons::Uuid& shareId,
            const QString& path);

    void requestBackupCopy(
            const Commons::Uuid& srcServerId,
            const Commons::Uuid& srcShareId,
            const Commons::Uuid& dstServerId,
            const Commons::Uuid& dstShareId,
            const QString& path,
            const qint64 mtime);

    void requestCopy(
            const Commons::Uuid& srcServerId,
            const Commons::Uuid& srcShareId,
            const Commons::Uuid& dstServerId,
            const Commons::Uuid& dstShareId,
            const QString& path,
            const qint64 mtime);

    void requestDelete(
            const Commons::Uuid& serverId,
            const Commons::Uuid& shareId,
            const QString& path);

    void onSafeRenameFinished();

    void onBackupCopyFinished();

    void onCopyFinished();

    void onDeleteFinished();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SynchronizeTask*/
