//-----------------------------------------------------------------------------
/// \file
/// AsyncTaskQueue - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "AsyncTaskQueue.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

AsyncTaskQueue::AsyncTaskQueue(
        QObject* parent) :
                QObject(parent),
                m_autoStartNext(false)
{
    // nothing to do
}

void AsyncTaskQueue::setAutoStartNext(
        bool enabled)
{
    m_autoStartNext = enabled;

    if (m_autoStartNext)
    {
        startNextTask();
    }
}

void AsyncTaskQueue::appendTask(
        AsyncTask* task)
{
    task->markScheduled(this);

    m_tasks.append(task);

    if (m_autoStartNext)
    {
        startNextTask();
    }
}

bool AsyncTaskQueue::hasMoreTasks() const
{
    return !m_tasks.empty();
}

void AsyncTaskQueue::startNextTask()
{
    if (m_tasks.empty())
    {
        return;
    }

    AsyncTask* task = m_tasks.front();

    Q_ASSERT(task->getQueue() == this);

    if (task->getState() == AsyncTask::State::SCHEDULED)
    {
        connect(task, &AsyncTask::finished, this, &AsyncTaskQueue::onTaskFinished);
        task->start();
    }
}

void AsyncTaskQueue::onTaskFinished()
{
    Q_ASSERT(!m_tasks.empty());

    // get first task in queue (and remove it)
    AsyncTask* task = m_tasks.front();
    m_tasks.pop_front();

    // check if that is the task that finished
    Q_ASSERT(sender() == task);
    Q_ASSERT(task->getQueue() == this);

    // disconnect from task
    disconnect(task, &AsyncTask::finished, this, &AsyncTaskQueue::onTaskFinished);

    // notify listeners about task finished
    emit taskFinished(QPointer<AsyncTask>(task));

    if (m_autoStartNext)
    {
        // start next task if present
        startNextTask();
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
