//-----------------------------------------------------------------------------
/// \file
/// SharesWidget - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SharesWidget
#define INCLUDED_OCTsync_SharesWidget

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QMainWindow>

// system includes
// (none)

// using clauses
// (none)

// forward references
class QStackedWidget;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class ShareInfoWidget;
class ServerInfoWidget;
class SharesTreeView;
class SyncClientEngine;

/// TODO: Class documentation
///
class SharesWidget : public QWidget
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SharesTreeView* m_sharesTreeView;

// Methods (public, protected, private)
public:
    explicit SharesWidget(
            QWidget* parent = nullptr);

    void setEngine(
            SyncClientEngine* const engine);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SharesWidget*/
