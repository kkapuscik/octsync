//-----------------------------------------------------------------------------
/// \file
/// ShareTreeModel - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ShareTreeModel.hpp"
#include "SyncClientEngine.hpp"

// library includes
#include <QtCore/QSet>
#include <syncserver/Share.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Commons::Uuid;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

class ShareTreeModelItem
{
protected:
    QStandardItem* m_nameItem;
    QStandardItem* m_typeItem;
    QStandardItem* m_statusItem;

public:
    ShareTreeModelItem()
    {
        m_nameItem = new QStandardItem();
        m_nameItem->setEditable(false);

        m_typeItem = new QStandardItem();
        m_typeItem->setEditable(false);

        m_statusItem = new QStandardItem();
        m_statusItem->setEditable(false);
    }

    QStandardItem* getModelItem() const
    {
        return m_nameItem;
    }

    QList<QStandardItem*> getRowItems() const
    {
        return QList<QStandardItem*>() << m_nameItem << m_typeItem << m_statusItem;
    }

    QModelIndex getItemIndex() const
    {
        return m_nameItem->index();
    }
};

class ShareItem : public ShareTreeModelItem
{
private:
    bool m_isValid;

public:
    ShareItem(
            ServerDevice* const device,
            const SyncShare& share) :
                    m_isValid(false)
    {
        update(share);

        m_nameItem->setIcon(QIcon(":/icons/network_share"));

        m_nameItem->setData(SyncClientEngine::ITEM_TYPE_SHARE, SyncClientEngine::ROLE_ITEM_TYPE);
        m_nameItem->setData(device->getUuid().toQUuid(), SyncClientEngine::ROLE_SERVER_ID);
        m_nameItem->setData(share.getUuid().toQUuid(), SyncClientEngine::ROLE_SHARE_ID);
    }

    void update(
            const SyncShare& share)
    {
        m_nameItem->setText(share.getName());
        m_typeItem->setText(
                QString("Share %1").arg(share.isReadOnly() ? "Read/Only" : "Read/Write"));
        m_statusItem->setText(share.getDescription());
    }

    void invalidate()
    {
        m_isValid = false;
    }

    void markValid()
    {
        m_isValid = true;
    }

    bool isValid() const
    {
        return m_isValid;
    }
};

class DeviceItem : public ShareTreeModelItem
{
public:
    typedef std::map<Uuid, std::unique_ptr<ShareItem>> ShareItemMap;

private:
    ShareItemMap m_shareItems;

public:
    DeviceItem(
            ServerDevice* const device)
    {
        m_nameItem->setIcon(QIcon(":/icons/network_device"));

        m_nameItem->setText(device->getName());
        m_typeItem->setText("Server");
        m_statusItem->setText(device->getUrl().toString());

        m_nameItem->setData(SyncClientEngine::ITEM_TYPE_SERVER, SyncClientEngine::ROLE_ITEM_TYPE);
        m_nameItem->setData(device->getUuid().toQUuid(), SyncClientEngine::ROLE_SERVER_ID);
    }

    ShareItemMap& getShareItems()
    {
        return m_shareItems;
    }
};

ShareTreeModel::ShareTreeModel(
        QObject* parent) :
                QStandardItemModel(parent)
{
    QStringList headerLabels;
    headerLabels << tr("Name") << tr("Type") << tr("Description");
    setHorizontalHeaderLabels(headerLabels);
}

ShareTreeModel::~ShareTreeModel()
{
    // defined to workaround problem with unique_ptr deleter
    // which requires undefined sizeof(DeviceItem) in header
    // (in default destructor)
}

void ShareTreeModel::addDevice(
        ServerDevice* const device)
{
    std::unique_ptr<DeviceItem> deviceItem(new DeviceItem(device));

    QStandardItem* const parentItem = invisibleRootItem();
    parentItem->appendRow(deviceItem->getRowItems());

    m_deviceItems.insert(std::make_pair(device, std::move(deviceItem)));

    connect(device, &ServerDevice::sharesUpdated, this, &ShareTreeModel::onDeviceSharesUpdated);
    connect(device, &QObject::destroyed, this, &ShareTreeModel::onDeviceDeleted);
}

void ShareTreeModel::removeDevice(
        ServerDevice* const device)
{
    auto itemIter = m_deviceItems.find(device);
    if (itemIter != m_deviceItems.end())
    {
        QModelIndex index = itemIter->second->getItemIndex();
        removeRow(index.row(), index.parent());

        m_deviceItems.erase(itemIter);
    }
}

void ShareTreeModel::onDeviceDeleted(
        QObject* object)
{
    removeDevice(static_cast<ServerDevice*>(object));
}

void ShareTreeModel::onDeviceSharesUpdated(
        QPointer<ServerDevice> device)
{
    // if the event is coming from deleted device - ignore it
    if (!device)
    {
        return;
    }

    // process event
    auto itemIter = m_deviceItems.find(device);
    if (itemIter != m_deviceItems.end())
    {
        // find device item
        DeviceItem* const deviceItem = itemIter->second.get();
        Q_ASSERT(deviceItem);

        // get new set of shares
        const QList<SyncShare> shares = device->getShares();

        // get existing share items
        DeviceItem::ShareItemMap& shareItems = deviceItem->getShareItems();

        // mark all items invalid
        for (auto& itemEntry : shareItems)
        {
            itemEntry.second->invalidate();
        }

        // update / add items
        for (const SyncShare& share : shares)
        {
            // check if we already have share item
            auto shareItemIter = shareItems.find(share.getUuid());
            if (shareItemIter != shareItems.end())
            {
                // update contents
                shareItemIter->second->update(share);
                shareItemIter->second->markValid();
            }
            else
            {
                // create new item
                std::unique_ptr<ShareItem> newShareItem(new ShareItem(device, share));
                newShareItem->markValid();

                // append to model
                deviceItem->getModelItem()->appendRow(newShareItem->getRowItems());

                // remember the item
                shareItems.insert(std::make_pair(share.getUuid(), std::move(newShareItem)));
            }
        }

        for (auto shareItemIter = shareItems.begin(), endIter = shareItems.end();
                shareItemIter != endIter; )
        {
            if (!shareItemIter->second->isValid())
            {
                QModelIndex index = shareItemIter->second->getItemIndex();
                removeRow(index.row(), index.parent());

                shareItemIter = shareItems.erase(shareItemIter);
            }
            else
            {
                ++shareItemIter;
            }
        }
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
