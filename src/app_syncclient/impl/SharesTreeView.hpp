//-----------------------------------------------------------------------------
/// \file
/// SharesTreeView - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SharesTreeView
#define INCLUDED_OCTsync_SharesTreeView

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QTreeView>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SharesTreeView : public QTreeView
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    explicit SharesTreeView(
            QWidget* parent = nullptr);

private:
    void onItemExpanded(
            const QModelIndex& index);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SharesTreeView*/
