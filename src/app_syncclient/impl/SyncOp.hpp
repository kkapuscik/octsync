//-----------------------------------------------------------------------------
/// \file
/// SyncOp - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncOp
#define INCLUDED_OCTsync_SyncOp

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
enum class SyncOp
    : quint32
    {
        NONE = 0,
    COPY_A_TO_B = 1 << 0,
    COPY_B_TO_A = 1 << 1,
    COPY_A_TO_B_WITH_BACKUP = 1 << 2,
    COPY_B_TO_A_WITH_BACKUP = 1 << 3,
    DELETE_A = 1 << 4,
    DELETE_B = 1 << 5
};

/// TODO: Class documentation
///
class SyncOpSet
{
private:
    quint32 m_flags;

public:
    SyncOpSet() :
                    m_flags(0)
    {
        // nothing to do
    }

    void clear()
    {
        m_flags = 0;
    }

    void clear(
            const SyncOp op)
    {
        m_flags &= ~static_cast<quint32>(op);
    }

    void set(
            const SyncOp op)
    {
        m_flags |= static_cast<quint32>(op);
    }

    bool isSet(
            const SyncOp op) const
    {
        return m_flags & static_cast<quint32>(op);
    }

    operator bool() const
    {
        return m_flags != 0;
    }
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncOp*/
