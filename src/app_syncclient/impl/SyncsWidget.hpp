//-----------------------------------------------------------------------------
/// \file
/// SyncsWidget - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncsWidget
#define INCLUDED_OCTsync_SyncsWidget

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QMainWindow>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class SyncClientEngine;
class SyncPoint;
class SyncsListWidget;
class SyncPointInfoWidget;

/// TODO: Class documentation
///
class SyncsWidget : public QWidget
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncsListWidget* m_syncsListWidget;
    SyncPointInfoWidget* m_syncPointInfoWidget;

// Methods (public, protected, private)
public:
    explicit SyncsWidget(
            QWidget* parent = nullptr);

    void setEngine(
            SyncClientEngine* const engine);

signals:
    void manualSyncRequested(
            const SyncPoint& syncPoint);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncsWidget*/
