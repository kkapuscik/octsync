//-----------------------------------------------------------------------------
/// \file
/// ShareSelectDialog - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ShareSelectDialog
#define INCLUDED_OCTsync_ShareSelectDialog

//-----------------------------------------------------------------------------

// local includes
#include "SyncClientEngine.hpp"
#include "ShareLocator.hpp"

// library includes
#include <QtWidgets/QDialog>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QAction>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ShareSelectDialog : public QDialog
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QTreeView* m_sharesTree;

    QAction* m_actionDialogOk;
    QAction* m_actionDialogCancel;

    ShareLocator m_currentLocator;

// Methods (public, protected, private)
public:
    explicit ShareSelectDialog(
            SyncClientEngine* engine,
            QWidget* parent = nullptr);

    ShareLocator getShareLocator() const;

private:
    void createActions();

    void createWidgets();

    void connectActions();

    void onDialogOk();

    void onCurrentChanged(
            const QModelIndex& current,
            const QModelIndex& previous);

    void onItemExpanded(
            const QModelIndex& index);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ShareSelectDialog*/

