//-----------------------------------------------------------------------------
/// \file
/// SyncsListWidget - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncsListWidget
#define INCLUDED_OCTsync_SyncsListWidget

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QWidget>

// system includes
// (none)

// using clauses
// (none)

// forward references
class QAction;
class QListView;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class SyncPointListView;
class SyncClientEngine;
class SyncPoint;

/// TODO: Class documentation
///
class SyncsListWidget : public QWidget
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)mainLayoutSplitter

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncClientEngine* m_engine;

    QAction* m_actionCreateSync;
    QAction* m_actionEditSync;
    QAction* m_actionDeleteSync;
    QAction* m_actionManualSync;

    QListView* m_syncListView;

// Methods (public, protected, private)
public:
    explicit SyncsListWidget(
            QWidget* parent = nullptr);

    void setEngine(
            SyncClientEngine* const engine);

private:
    void createActions();
    void createWidgets();
    void connectActions();

    void onListCurrentChanged(
            const QModelIndex& current,
            const QModelIndex& previous);

    void onCreateSyncClicked();
    void onEditSyncClicked();
    void onDeleteSyncClicked();
    void onManualSyncClicked();

signals:
    void manualSyncRequested(
            const SyncPoint& syncPoint);

    void currentChanged(
            const SyncPoint& syncPoint);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncsListWidget*/
