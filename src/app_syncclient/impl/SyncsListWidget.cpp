//-----------------------------------------------------------------------------
/// \file
/// SyncsListWidget - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncsListWidget.hpp"
#include "SyncPointEditDialog.hpp"
#include "SyncClientEngine.hpp"

// library includes
#include <QtWidgets/QLayout>
#include <QtWidgets/QAction>
#include <QtWidgets/QListView>
#include <ui/ActionButton.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::ActionButton;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncsListWidget::SyncsListWidget(
        QWidget* parent) :
                QWidget(parent)
{
    createActions();
    createWidgets();
    connectActions();
}

void SyncsListWidget::setEngine(
        SyncClientEngine* const engine)
{
    m_engine = engine;

    m_syncListView->setModel(engine->getSyncListModel());

    connect(m_syncListView->selectionModel(), &QItemSelectionModel::currentChanged, this,
            &SyncsListWidget::onListCurrentChanged);
}

void SyncsListWidget::createActions()
{
    m_actionCreateSync = new QAction(this);
    m_actionCreateSync->setText(tr("Create"));

    m_actionEditSync = new QAction(this);
    m_actionEditSync->setText(tr("Edit"));
    //m_actionEditSync->setEnabled(false);

    m_actionDeleteSync = new QAction(this);
    m_actionDeleteSync->setText(tr("Delete"));
    //m_actionDeleteSync->setEnabled(false);

    m_actionManualSync = new QAction(this);
    m_actionManualSync->setText(tr("Manual Sync"));
    //m_actionManualSync->setEnabled(false);
}

void SyncsListWidget::createWidgets()
{
    QVBoxLayout* buttonsLayout = new QVBoxLayout();
    buttonsLayout->setSpacing(5);
    buttonsLayout->addWidget(new ActionButton(m_actionCreateSync));
    buttonsLayout->addWidget(new ActionButton(m_actionEditSync));
    buttonsLayout->addWidget(new ActionButton(m_actionDeleteSync));
    buttonsLayout->addSpacing(5);
    buttonsLayout->addWidget(new ActionButton(m_actionManualSync));
    buttonsLayout->addStretch(1);

    m_syncListView = new QListView(this);

    QHBoxLayout* mainLayout = new QHBoxLayout(this);
    mainLayout->setSpacing(10);
    mainLayout->addWidget(m_syncListView);
    mainLayout->addLayout(buttonsLayout);
}

void SyncsListWidget::connectActions()
{
    connect(m_actionCreateSync, &QAction::triggered, this, &SyncsListWidget::onCreateSyncClicked);
    connect(m_actionEditSync, &QAction::triggered, this, &SyncsListWidget::onEditSyncClicked);
    connect(m_actionDeleteSync, &QAction::triggered, this, &SyncsListWidget::onDeleteSyncClicked);
    connect(m_actionManualSync, &QAction::triggered, this, &SyncsListWidget::onManualSyncClicked);

    // TODO:
    // Add possibility to enable other buttons when there is something selected
    // on the ListView.
}

void SyncsListWidget::onListCurrentChanged(
        const QModelIndex& current,
        const QModelIndex& /*previous*/)
{
    SyncPoint currentSyncPoint;

    const QUuid syncId = current.data(SyncClientEngine::ROLE_SYNC_ID).toUuid();
    if (!syncId.isNull())
    {
        currentSyncPoint = m_engine->getSyncPoint(syncId);
    }

    emit currentChanged(currentSyncPoint);
}

void SyncsListWidget::onCreateSyncClicked()
{
    SyncPointEditDialog dialog(m_engine, SyncPointEditDialog::Mode::STORED_SYNC_POINT);

    if (dialog.exec() == QDialog::Accepted)
    {
        SyncPoint syncPoint = dialog.getSyncPoint();
        Q_ASSERT(syncPoint.isValid());

        if (syncPoint.isValid())
        {
            m_engine->addSyncPoint(syncPoint);
        }
    }
}

void SyncsListWidget::onEditSyncClicked()
{
    const QUuid syncId = m_syncListView->selectionModel()->currentIndex().data(
            SyncClientEngine::ROLE_SYNC_ID).toUuid();
    if (!syncId.isNull())
    {
        SyncPoint modifiedSyncPoint = m_engine->getSyncPoint(syncId);

        SyncPointEditDialog dialog(m_engine, SyncPointEditDialog::Mode::STORED_SYNC_POINT);

        dialog.setSyncPoint(modifiedSyncPoint);

        if (dialog.exec() == QDialog::Accepted)
        {
            SyncPoint syncPoint = dialog.getSyncPoint();
            Q_ASSERT(syncPoint.isValid());

            if (syncPoint.isValid())
            {
                m_engine->removeSyncPoint(modifiedSyncPoint);
                m_engine->addSyncPoint(syncPoint);
            }
        }
    }
}

void SyncsListWidget::onDeleteSyncClicked()
{
    // TODO:
    // Can it be done better?
    const QUuid syncId = m_syncListView->selectionModel()->currentIndex().data(
            SyncClientEngine::ROLE_SYNC_ID).toUuid();
    if (!syncId.isNull())
    {
        SyncPoint syncPoint = m_engine->getSyncPoint(syncId);
        m_engine->removeSyncPoint(syncPoint);
    }
}

void SyncsListWidget::onManualSyncClicked()
{
    const QUuid syncId = m_syncListView->selectionModel()->currentIndex().data(
            SyncClientEngine::ROLE_SYNC_ID).toUuid();
    if (!syncId.isNull())
    {
        const SyncPoint syncPoint = m_engine->getSyncPoint(syncId);

        emit manualSyncRequested(syncPoint);
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
