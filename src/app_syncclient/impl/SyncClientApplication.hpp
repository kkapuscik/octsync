//-----------------------------------------------------------------------------
/// \file
/// SyncClientApplication - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncClientApplication
#define INCLUDED_OCTsync_SyncClientApplication

//-----------------------------------------------------------------------------

// local includes
#include "SyncClientWindow.hpp"
#include "SyncClientEngine.hpp"

// library includes
#include <QtWidgets/QApplication>
#include <QtWidgets/QSystemTrayIcon>
#include <QtWidgets/QMenu>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncClientApplication : public QApplication
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    std::unique_ptr<SyncClientWindow> m_mainWindow;
    QSystemTrayIcon* m_trayIcon;
    SyncClientEngine* m_engine;
    std::unique_ptr<QMenu> m_trayContextMenu;

// Methods (public, protected, private)
public:
    SyncClientApplication(
            int& argc,
            char** argv);

    int exec();

private:
    void onTrayIconActivated(
            QSystemTrayIcon::ActivationReason reason);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncClientApplication*/
