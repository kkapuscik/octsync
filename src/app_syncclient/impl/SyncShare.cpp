//-----------------------------------------------------------------------------
/// \file
/// SyncShare - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncShare.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncShare::SyncShare() :
                m_readOnly(false)
{
    // nothing to do
}

bool SyncShare::isValid() const
{
    return m_uuid.isValid() && !m_name.isEmpty();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
