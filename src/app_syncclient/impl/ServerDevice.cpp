//-----------------------------------------------------------------------------
/// \file
/// ServerDevice - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ServerDevice.hpp"
#include "SharesUpdater.hpp"

// library includes
#include <QtCore/QTimer>
#include <boprpc/Protocol.hpp>
#include <boprpc/Executor.hpp>
#include <boprpc/Client.hpp>
#include <boprpc/Service.hpp>
#include <boprpc/ListServicesExecutor.hpp>
#include <boprpc/GetDeviceInfoExecutor.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::BadArgumentException;
using OCTsync::BopRpc::Service;
using OCTsync::BopRpc::Executor;
using OCTsync::BopRpc::Client;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

const QString ServerDevice::URL_SCHEME_BOP("bop");
const QString ServerDevice::URL_SCHEME_SBOP("sbop");

ServerDevice::ServerDevice(
        QPointer<BopRpc::Client> client,
        QObject* parent) :
                QObject(parent),
                m_client(client),
                m_state(State::UNINITIALIZED),
                m_syncServiceId(Service::INVALID_SERVICE_ID)
{
    m_sharesUpdater = new SharesUpdater(client, this);

    connect(m_sharesUpdater, &SharesUpdater::updateFinished, this,
            &ServerDevice::onSharesUpdateFinished);
    connect(m_sharesUpdater, &SharesUpdater::updateFailed, this,
            &ServerDevice::onSharesUpdateFailed);
}

bool ServerDevice::isValid() const
{
    return m_ldpDevice.isValid();
}

void ServerDevice::setState(
        const State newState)
{
    if (m_state != newState)
    {
        Q_ASSERT(newState > m_state);

        m_state = newState;

        emit stateChanged(QPointer<ServerDevice>(this), m_state);
    }
}

void ServerDevice::setLdpDevice(
        const Ldp::Device& ldpDevice)
{
    Q_ASSERT(ldpDevice.isValid());

    if (getState() == State::UNINITIALIZED)
    {
        m_ldpDevice = ldpDevice;
        if (m_name.isEmpty())
        {
            m_name = m_ldpDevice.getUrl().toString();
        }

        setState(State::INITIALIZED);
    }
    else
    {
        Q_ASSERT(0);
    }
}

void ServerDevice::invalidate()
{
    setState(State::INVALID);
}

void ServerDevice::requestRpcValidate()
{
    if (!m_client)
    {
        setState(State::INVALID);
        return;
    }

    if (getState() == State::INITIALIZED)
    {
        if (checkDeviceUrlScheme())
        {
            Executor* executor = new BopRpc::ListServicesExecutor(this);

            executor->setClient(m_client);
            executor->setUrl(m_ldpDevice.getUrl());

            connect(executor, &Executor::finished, this,
                    &ServerDevice::onValidateExecutionFinished);

            executor->execute();
        }
        else
        {
            setState(State::INVALID);
        }
    }
    else
    {
        Q_ASSERT(0);
    }
}

void ServerDevice::onValidateExecutionFinished(
        QPointer<Executor> executor)
{
    qDebug() << "ServerDevice::onValidateExecutionFinished() " << executor;

    if (executor)
    {
        if (executor->getState() == Executor::State::SUCCEEDED)
        {
            try
            {
                BopRpc::ListServicesExecutor* commandExecutor =
                        static_cast<BopRpc::ListServicesExecutor*>(executor.data());

                bool syncServiceFound = false;
                bool rpcServiceFound = false;
                quint32 rpcServiceId = Service::INVALID_SERVICE_ID;
                quint32 syncServiceId = Service::INVALID_SERVICE_ID;

                const int serviceCount = commandExecutor->getListedServiceCount();

                qDebug() << "Services count: " << serviceCount;

                for (int i = 0; i < serviceCount; ++i)
                {
                    const quint32 serviceId = commandExecutor->getListedServiceId(i);
                    const QString serviceName = commandExecutor->getListedServiceName(i);

                    qDebug() << "Service " << i;
                    qDebug() << "- id " << serviceId;
                    qDebug() << "- name " << serviceName;

                    if (serviceName == "RPC") // TODO: add constant
                    {
                        if (!rpcServiceFound)
                        {
                            rpcServiceId = serviceId;
                            rpcServiceFound = true;
                        }
                        else
                        {
                            throw BadArgumentException("Duplicated RPC service"); // TODO: use diff. exception
                        }
                    }
                    else if (serviceName == "Sync")
                    {
                        if (!syncServiceFound)
                        {
                            syncServiceId = serviceId;
                            syncServiceFound = true;
                        }
                        else
                        {
                            throw BadArgumentException("Duplicated Sync service"); // TODO: diff exc.
                        }
                    }
                }

                if (!rpcServiceFound)
                {
                    throw BadArgumentException("Required RPC service not found"); // TODO: diff exc.
                }
                if (rpcServiceId != 0)
                {
                    throw BadArgumentException("RPC service is not service 0"); // TODO: diff exc.
                }
                if (!syncServiceFound)
                {
                    throw BadArgumentException("Required Sync service not found"); // TODO: diff exc.
                }

                if (getState() == State::INITIALIZED)
                {
                    m_syncServiceId = syncServiceId;
                    m_sharesUpdater->setServiceInfo(m_ldpDevice.getUrl(), syncServiceId);

                    setState(State::BOPRPC_VALIDATED);
                }
            }
            catch (BadArgumentException& e)
            {
                qDebug() << e.getMessage();
                setState(State::INVALID);
            }
        }
        else
        {
            setState(State::INVALID);
        }

        executor->deleteLater();
    }
}

void ServerDevice::requestNameUpdate()
{
    if (!m_client)
    {
        setState(State::INVALID);
        return;
    }

    if (getState() == State::BOPRPC_VALIDATED)
    {
        if (checkDeviceUrlScheme())
        {
            Executor* executor = new BopRpc::GetDeviceInfoExecutor(this);

            executor->setClient(m_client);
            executor->setUrl(m_ldpDevice.getUrl());

            connect(executor, &Executor::finished, this, &ServerDevice::onNameUpdateFinished);

            executor->execute();
        }
        else
        {
            setState(State::INVALID);
        }
    }
    else
    {
        Q_ASSERT(0);
    }
}

void ServerDevice::onNameUpdateFinished(
        QPointer<Executor> executor)
{
    qDebug() << "ServerDevice::onNameUpdateFinished() " << executor;

    if (executor)
    {
        if (executor->getState() == Executor::State::SUCCEEDED)
        {
            try
            {
                BopRpc::GetDeviceInfoExecutor* commandExecutor =
                        static_cast<BopRpc::GetDeviceInfoExecutor*>(executor.data());

                m_name = commandExecutor->getDeviceName();

                qDebug() << "Device name: " << m_name;

                if (getState() == State::BOPRPC_VALIDATED)
                {
                    setState(State::READY);

                    requestSharesUpdate();
                }
            }
            catch (BadArgumentException& e)
            {
                qDebug() << e.getMessage();
                setState(State::INVALID);
            }
        }
        else
        {
            setState(State::INVALID);
        }

        executor->deleteLater();
    }
}

void ServerDevice::requestSharesUpdate()
{
    m_sharesUpdater->requestUpdate();
}

void ServerDevice::onSharesUpdateFinished(
        QList<SyncShare> shares)
{
    // TODO: check if there was a change
    // TODO: - sort by UUID
    // TOOD: - compare all properties

    // store new set of shares
    m_shares = shares;

    // notify about the change
    emit sharesUpdated(QPointer<ServerDevice>(this));

    // repeat shares update
    QTimer::singleShot(SHARES_UPDATE_DELAY_MS, this, SLOT(requestSharesUpdate()));
}

void ServerDevice::onSharesUpdateFailed()
{
    bool needToNotify = false;

    // remove shares (no known due to failure)
    if (!m_shares.isEmpty())
    {
        m_shares.clear();
        needToNotify = true;
    }

    // notify about the change
    if (needToNotify)
    {
        emit sharesUpdated(QPointer<ServerDevice>(this));
    }

    // repeat after a delay
    QTimer::singleShot(SHARES_UPDATE_FAILED_DELAY_MS, this, SLOT(requestSharesUpdate()));
}

QString ServerDevice::getShareName(
        const Commons::Uuid& shareId) const
{
    for (const auto& share : m_shares)
    {
        if (share.getUuid() == shareId)
        {
            return share.getName();
        }
    }

    return tr("Unknown");
}

bool ServerDevice::checkDeviceUrlScheme()
{
    return (m_ldpDevice.getUrl().scheme() == URL_SCHEME_BOP) ||
           (m_ldpDevice.getUrl().scheme() == URL_SCHEME_SBOP);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
