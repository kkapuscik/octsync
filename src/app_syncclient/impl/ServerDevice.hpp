//-----------------------------------------------------------------------------
/// \file
/// ServerDevice - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ServerDevice
#define INCLUDED_OCTsync_ServerDevice

//-----------------------------------------------------------------------------

// local includes
#include "SyncShare.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QList>
#include <ldp/Device.hpp>
#include <boprpc/Executor.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class SharesUpdater;

/// TODO: Class documentation
///
class ServerDevice : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const QString URL_SCHEME_BOP;
    static const QString URL_SCHEME_SBOP;

    static const int SHARES_UPDATE_DELAY_MS = 30 * 1000;
    static const int SHARES_UPDATE_FAILED_DELAY_MS = 20 * 1000;

// Types (public, protected, private)
public:
    enum class State
    {
        UNINITIALIZED,
        INITIALIZED,
        BOPRPC_VALIDATED,
        READY,

        INVALID
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QPointer<BopRpc::Client> m_client;
    State m_state;
    Ldp::Device m_ldpDevice;
    QString m_name;
    quint32 m_syncServiceId;
    SharesUpdater* m_sharesUpdater;
    QList<SyncShare> m_shares;

// Methods (public, protected, private)
public:
    ServerDevice(
            QPointer<BopRpc::Client> client,
            QObject* parent = nullptr);

    QPointer<BopRpc::Client> getClient() const
    {
        return m_client;
    }

    quint32 getSyncServiceId() const
    {
        return m_syncServiceId;
    }

    State getState() const
    {
        return m_state;
    }

    QString getName() const
    {
        return m_name;
    }

    Commons::Uuid getUuid() const
    {
        return m_ldpDevice.getUuid();
    }

    QUrl getUrl() const
    {
        return m_ldpDevice.getUrl();
    }

    bool isValid() const;

    void setLdpDevice(
            const Ldp::Device& ldpDevice);

    void invalidate();

    void requestRpcValidate();

    void requestNameUpdate();

    QList<SyncShare> getShares() const
    {
        return m_shares;
    }

    QString getShareName(
            const Commons::Uuid& shareId) const;

public slots:
    void requestSharesUpdate();

private:
    void setState(
            const State newState);

    void onValidateExecutionFinished(
            QPointer<BopRpc::Executor> executor);

    void onNameUpdateFinished(
            QPointer<BopRpc::Executor> executor);

    void onSharesUpdateFinished(
            QList<SyncShare> shares);

    void onSharesUpdateFailed();

    bool checkDeviceUrlScheme();

signals:
    void stateChanged(
            QPointer<ServerDevice> device,
            State state);

    void sharesUpdated(
            QPointer<ServerDevice> device);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ServerDevice*/
