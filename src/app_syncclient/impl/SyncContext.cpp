//-----------------------------------------------------------------------------
/// \file
/// SyncContext - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncContext.hpp"
#include "FileTreeModel.hpp"
#include "FileTreeItem.hpp"
#include "ReadDirTask.hpp"
#include "ServerDevice.hpp"
#include "SyncAlgorithm.hpp"
#include "SyncAlgorithmManual.hpp"
#include "SyncAlgorithmTwoSided.hpp"
#include "SyncAlgorithmMirror.hpp"
#include "SynchronizeTask.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncContext::SyncContext(
        SyncClientEngine* engine,
        const SyncPoint& syncPoint,
        QObject* parent) :
                QObject(parent),
                m_engine(engine),
                m_state(State::IDLE),
                m_syncPoint(syncPoint)
{
    m_buildTreeQueue = new AsyncTaskQueue(this);
    m_buildTreeQueue->setAutoStartNext(true);
    connect(m_buildTreeQueue, &AsyncTaskQueue::taskFinished, this,
            &SyncContext::onBuildTreeTaskFinished);

    m_synchronizeQueue = new AsyncTaskQueue(this);
    m_synchronizeQueue->setAutoStartNext(true);
    connect(m_synchronizeQueue, &AsyncTaskQueue::taskFinished, this,
            &SyncContext::onSynchronizeTaskFinished);

    m_fileTreeModel = new FileTreeModel(this);
}

SyncContext::~SyncContext()
{
    // nothing to do
}

SyncContext::ItemPtr SyncContext::createItem(
        const QString& name,
        const QString& path)
{
    return ItemPtr(new FileTreeItem(name, path));
}

SyncContext::ItemPtr SyncContext::createRootItem()
{
    ItemPtr item = createItem("(root)", "");

    item->setRootInfo();

    m_items.insert(std::make_pair(item->getPath(), item));

    m_fileTreeModel->invisibleRootItem()->appendRow(item->getRowItems());

    return item;
}

SyncContext::ItemPtr SyncContext::getItem(
        SyncContext::ItemPtr parent,
        const QString& name,
        const QString& path)
{
    auto itemIter = m_items.find(path);
    if (itemIter != m_items.end())
    {
        return itemIter->second;
    }
    else
    {
        Q_ASSERT(parent);

        ItemPtr item = createItem(name, path);
        m_items.insert(std::make_pair(item->getPath(), item));

        QStandardItem* stdItem = parent->getModelItem();
        stdItem->appendRow(item->getRowItems());

        return item;
    }
}

QAbstractItemModel* SyncContext::getFileTreeModel() const
{
    return m_fileTreeModel;
}

SyncOpSet SyncContext::getValidSyncOps(
        const QString& path) const
{
    auto itemIter = m_items.find(path);
    if (itemIter != m_items.end())
    {
        return itemIter->second->getValidOps();
    }
    else
    {
        return SyncOpSet();
    }
}

void SyncContext::setSyncOp(
        const QString& path,
        const SyncOp op)
{
    auto itemIter = m_items.find(path);
    if (itemIter != m_items.end())
    {
        itemIter->second->setSyncOp(op);
    }
}

bool SyncContext::startSynchronization()
{
    Q_ASSERT(m_state == State::IDLE);

    bool anyTaskScheduled = false;
    for (ItemMap::value_type& item : m_items)
    {
        const ItemPtr itemPtr = item.second;

        AsyncTask* const synchronizeTask = new SynchronizeTask(m_engine,
                m_syncPoint.getShareLocatorA().getServerId(),
                m_syncPoint.getShareLocatorA().getShareId(),
                m_syncPoint.getShareLocatorB().getServerId(),
                m_syncPoint.getShareLocatorB().getShareId(),
                itemPtr->getPath(),
                itemPtr->getInfoA().getMTime().toMSecsSinceEpoch(),
                itemPtr->getInfoB().getMTime().toMSecsSinceEpoch(),
                itemPtr->getSyncOp(),
                this);

        synchronizeTask->setAutoDelete();

        m_synchronizeQueue->appendTask(synchronizeTask);
        anyTaskScheduled = true;
    }

    if (anyTaskScheduled)
    {
        m_state = State::SYNCHRONIZE;
    }

    // TODO: change, notify about success
    return anyTaskScheduled;
}

bool SyncContext::startTreeBuild()
{
    Q_ASSERT(m_state == State::IDLE);

    /* clear current items */
    m_fileTreeModel->removeRows(0, m_fileTreeModel->rowCount());
    m_items.clear();

    /* create root element */
    (void) createRootItem();

    /* build tree */
    m_state = State::BUILD_TREE;

    AsyncTask* readRootDirTaskA = new ReadDirTask(m_engine,
            m_syncPoint.getShareLocatorA().getServerId(),
            m_syncPoint.getShareLocatorA().getShareId(), "", this);
    AsyncTask* readRootDirTaskB = new ReadDirTask(m_engine,
            m_syncPoint.getShareLocatorB().getServerId(),
            m_syncPoint.getShareLocatorB().getShareId(), "", this);

    readRootDirTaskA->setAutoDelete();
    readRootDirTaskB->setAutoDelete();

    m_buildTreeQueue->appendTask(readRootDirTaskA);
    m_buildTreeQueue->appendTask(readRootDirTaskB);

    return true;
}

void SyncContext::onSynchronizeTaskFinished(
        QPointer<AsyncTask> task)
{
    SynchronizeTask* const synchronizeTask = static_cast<SynchronizeTask*>(task.data());
    Q_ASSERT(synchronizeTask);

//    if (synchronizeTask->getResult())
//    {
//    // TODO
//    }

    if (!m_synchronizeQueue->hasMoreTasks())
    {
        m_state = State::IDLE;
        emit syncFinished();
    }
}

void SyncContext::onBuildTreeTaskFinished(
        QPointer<AsyncTask> task)
{
    ReadDirTask* const readDirTask = static_cast<ReadDirTask*>(task.data());
    Q_ASSERT(readDirTask);

    if (readDirTask->getResult())
    {
        QList<FileInfo> infos = readDirTask->getInfos();

        for (FileInfo& info : infos)
        {
            const QString parentPath = readDirTask->getPath();
            const QString itemPath = parentPath + "/" + info.getName();

            ItemPtr parent = getItem(nullptr, QString::null, readDirTask->getPath());
            ItemPtr item = getItem(parent, info.getName(), itemPath);

            //  update info
            if (readDirTask->getShareId() == m_syncPoint.getShareLocatorA().getShareId())
            {
                qDebug() << "updateInfoA: " << info.getName();
                item->updateInfoA(info);
            }
            else
            {
                qDebug() << "updateInfoB: " << info.getName();
                item->updateInfoB(info);
            }

            // if item is a directory - request recursively
            if (info.getType() == FileInfo::Type::DIRECTORY)
            {
                AsyncTask* const subDirReadTask = new ReadDirTask(m_engine,
                        readDirTask->getServerId(),
                        readDirTask->getShareId(), itemPath, this);
                subDirReadTask->setAutoDelete();

                m_buildTreeQueue->appendTask(subDirReadTask);
            }
        }
    }
    else
    {
        // TODO
    }

    if (!m_buildTreeQueue->hasMoreTasks())
    {
        m_state = State::IDLE;
        emit treeBuildFinished();
    }
}

void SyncContext::applyAlgorithm(
        const SyncMode mode)
{
    Q_ASSERT(m_state == State::IDLE);

    if (m_items.empty())
    {
        return;
    }

    std::unique_ptr<SyncAlgorithm> algorithm;

    // TODO: checking if algorithm is applicable (read only shares!)

    if (mode == SyncMode::MANUAL)
    {
        algorithm.reset(new SyncAlgorithmManual());
    }
    else if (mode == SyncMode::FULL_TWO_SIDED)
    {
        algorithm.reset(new SyncAlgorithmTwoSided(false));
    }
    else if (mode == SyncMode::SAFE_TWO_SIDED)
    {
        algorithm.reset(new SyncAlgorithmTwoSided(true));
    }
    else if (mode == SyncMode::MIRROR_A_TO_B)
    {
        algorithm.reset(new SyncAlgorithmMirror(SyncAlgorithmMirror::Direction::A_TO_B, false));
    }
    else if (mode == SyncMode::MIRROR_B_TO_A)
    {
        algorithm.reset(new SyncAlgorithmMirror(SyncAlgorithmMirror::Direction::B_TO_A, false));
    }
    else if (mode == SyncMode::NEW_ONLY_A_TO_B)
    {
        algorithm.reset(new SyncAlgorithmMirror(SyncAlgorithmMirror::Direction::A_TO_B, true));
    }
    else if (mode == SyncMode::NEW_ONLY_B_TO_A)
    {
        algorithm.reset(new SyncAlgorithmMirror(SyncAlgorithmMirror::Direction::B_TO_A, true));
    }

    if (algorithm)
    {
        for (ItemMap::value_type& item : m_items)
        {
            algorithm->processItem(*item.second);
        }
    }
    else
    {
        Q_ASSERT(0);
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
