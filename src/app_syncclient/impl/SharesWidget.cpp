//-----------------------------------------------------------------------------
/// \file
/// SharesWidget - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SharesWidget.hpp"
#include "SharesTreeView.hpp"
#include "SyncClientEngine.hpp"

// library includes
#include <QtWidgets/QLayout>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SharesWidget::SharesWidget(
        QWidget* parent) :
                QWidget(parent)
{
    // in future this widget may be extended to add refresh & other buttons

    // create shares tree widget
    m_sharesTreeView = new SharesTreeView(this);

    // create layout
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(m_sharesTreeView);
}

void SharesWidget::setEngine(
        SyncClientEngine* const engine)
{
    m_sharesTreeView->setModel(engine->getShareTreeModel());
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
