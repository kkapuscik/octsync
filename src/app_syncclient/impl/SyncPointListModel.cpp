//-----------------------------------------------------------------------------
/// \file
/// SyncPointListModel - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncPointListModel.hpp"
#include "SyncClientEngine.hpp"
#include "SyncsRepository.hpp"
#include "SyncPointsData.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
using OCTsync::Commons::Uuid;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncPointListModel::SyncPointListModel(
        SyncPointsData* syncData,
        QObject* parent)
    : QStandardItemModel(parent),
      m_syncData(syncData)
{
    // Nothing to do
}

void SyncPointListModel::addSyncPoint(
        const SyncPoint& syncPoint)
{
    QStandardItem* const parentItem = invisibleRootItem();

    QStandardItem* nameItem = new QStandardItem();
    nameItem->setEditable(false);
    nameItem->setText(syncPoint.getName());
    nameItem->setData(syncPoint.getUuid().toQUuid(), SyncClientEngine::ROLE_SYNC_ID);

    parentItem->appendRow(nameItem);

    m_syncData->addSync(syncPoint);
}

void SyncPointListModel::removeSyncPoint(
        const SyncPoint& syncPoint)
{
    m_syncData->removeSync(syncPoint);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
