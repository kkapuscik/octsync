//-----------------------------------------------------------------------------
/// \file
/// SyncAlgorithmMirror - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncAlgorithmMirror.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

void SyncAlgorithmMirror::processItem(
        FileTreeItem& item)
{
    const FileInfo& infoA = item.getInfoA();
    const FileInfo& infoB = item.getInfoB();

    item.setSyncOp(SyncOp::NONE);

    // TODO: isSameFile compares without checking the contents - to be modified

    if (item.getType() == FileInfo::Type::FILE)
    {
        if (m_direction == Direction::A_TO_B)
        {
            if (infoA.isValid())
            {
                if (infoB.isValid())
                {
                    if (!isSameFile(infoA, infoB))
                    {
                        item.setSyncOp(
                                m_safeMode ? SyncOp::COPY_A_TO_B_WITH_BACKUP : SyncOp::COPY_A_TO_B);
                    }
                }
                else
                {
                    item.setSyncOp(SyncOp::COPY_A_TO_B);
                }
            }
            else
            {
                if (!m_safeMode && infoB.isValid())
                {
                    item.setSyncOp(SyncOp::DELETE_B);
                }
            }
        }
        else // B_TO_A
        {
            if (infoB.isValid())
            {
                if (infoA.isValid())
                {
                    if (!isSameFile(infoA, infoB))
                    {
                        item.setSyncOp(
                                m_safeMode ? SyncOp::COPY_B_TO_A_WITH_BACKUP : SyncOp::COPY_B_TO_A);
                    }
                }
                else
                {
                    item.setSyncOp(SyncOp::COPY_B_TO_A);
                }
            }
            else
            {
                if (!m_safeMode && infoA.isValid())
                {
                    item.setSyncOp(SyncOp::DELETE_A);
                }
            }
        }
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
