//-----------------------------------------------------------------------------
/// \file
/// AsyncTask - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_AsyncTask
#define INCLUDED_OCTsync_AsyncTask

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <boprpc/Executor.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class SyncClientEngine;
class AsyncTaskQueue;

/// TODO: Class documentation
///
class AsyncTask : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    enum class State
    {
        PENDING,
        SCHEDULED,
        STARTED,
        FINISHED
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncClientEngine* m_engine;
    State m_state;
    AsyncTaskQueue* m_queue;

// Methods (public, protected, private)
public:
    AsyncTask(
            SyncClientEngine* engine,
            QObject* parent = nullptr);

    void setAutoDelete();

    void markScheduled(
            AsyncTaskQueue* queue);

    void start();

    // TODO: seems to be a hack... fix somehow
    void startStandalone();

    AsyncTaskQueue* getQueue() const
    {
        return m_queue;
    }

    State getState() const
    {
        return m_state;
    }

protected:
    virtual void run() = 0;

    void notifyFinished();

    SyncClientEngine* getEngine() const
    {
        return m_engine;
    }

    void setTarget(
            BopRpc::Executor* executor,
            const Commons::Uuid& serverId,
            const bool syncCall);

private slots:
    void startAsync();

signals:
    void finished();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_AsyncTask*/
