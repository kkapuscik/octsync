//-----------------------------------------------------------------------------
/// \file
/// ShareTreeModel - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ShareTreeModel
#define INCLUDED_OCTsync_ShareTreeModel

//-----------------------------------------------------------------------------

// local includes
#include "ServerDevice.hpp"

// library includes
#include <QtGui/QStandardItemModel>
#include <QtCore/QPointer>
#include <commons/Map.hpp>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class DeviceItem;

/// TODO: Class documentation
///
class ShareTreeModel : public QStandardItemModel
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef Commons::Map<ServerDevice*, std::unique_ptr<DeviceItem>> DeviceItemMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    DeviceItemMap m_deviceItems;

// Methods (public, protected, private)
public:
    explicit ShareTreeModel(
            QObject *parent = nullptr);

    virtual ~ShareTreeModel();

    // TODO: remove the pointers
    void addDevice(
            ServerDevice* const device);

    void removeDevice(
            ServerDevice* const device);

private:
    void onDeviceDeleted(
            QObject* object);

    void onDeviceSharesUpdated(
            QPointer<ServerDevice> device);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ShareTreeModel*/
