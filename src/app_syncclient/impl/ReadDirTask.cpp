//-----------------------------------------------------------------------------
/// \file
/// ReadDirTask - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ReadDirTask.hpp"
#include "ServerDevice.hpp"

// library includes
#include <boprpc/Executor.hpp>
#include <boprpc/BadArgumentException.hpp>
#include <syncserver/OpenDirExecutor.hpp>
#include <syncserver/ReadDirExecutor.hpp>
#include <syncserver/CloseDirExecutor.hpp>

// system includes
// (none)

// using clauses
using OCTsync::SyncServer::OpenDirExecutor;
using OCTsync::SyncServer::ReadDirExecutor;
using OCTsync::SyncServer::CloseDirExecutor;
using OCTsync::BopRpc::Executor;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

ReadDirTask::ReadDirTask(
        SyncClientEngine* engine,
        const Commons::Uuid& serverId,
        const Commons::Uuid& shareId,
        const QString& path,
        QObject* parent) :
                AsyncTask(engine, parent),
                m_serverId(serverId),
                m_shareId(shareId),
                m_path(path),
                m_dirId(0),
                m_taskSuccess(false)
{
    // nothing to do
}

void ReadDirTask::run()
{
    requestOpenDir();
}

void ReadDirTask::requestOpenDir()
{
    OpenDirExecutor* executor = new OpenDirExecutor(this);

    setTarget(executor, m_serverId, true);

    connect(executor, &Executor::finished, this, &ReadDirTask::onOpenDirFinished);

    executor->setShareId(m_shareId);
    executor->setPath(m_path);
    executor->execute();
}

void ReadDirTask::onOpenDirFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        OpenDirExecutor* const openDirExecutor = static_cast<OpenDirExecutor*>(executor.data());

        try
        {
            m_dirId = openDirExecutor->getDirId();
            success = true;
        }
        catch (BadArgumentException& e)
        {
            qDebug() << e.getMessage();
        }
    }

    if (success)
    {
        requestReadDir();
    }
    else
    {
        // finished - no success
        notifyFinished();
    }

    executor->deleteLater();
}

void ReadDirTask::requestReadDir()
{
    ReadDirExecutor* executor = new ReadDirExecutor(this);

    setTarget(executor, m_serverId, true);

    connect(executor, &Executor::finished, this, &ReadDirTask::onReadDirFinished);

    executor->setDirId(m_dirId);
    executor->setOffset(m_infos.size());
    executor->setMaxCount(MAX_READ_DIR_COUNT);
    executor->execute();
}

void ReadDirTask::onReadDirFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;
    bool allRead = false;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        ReadDirExecutor* const readDirExecutor = static_cast<ReadDirExecutor*>(executor.data());

        // TODO: check error code

        try
        {
            const quint32 count = readDirExecutor->getCount();
            const quint32 total = readDirExecutor->getTotal();

            for (quint32 i = 0; i < count; ++i)
            {
                FileInfo info;

                info.setName(readDirExecutor->getItemName(i));
                info.setSize(readDirExecutor->getItemSize(i));
                info.setMTime(readDirExecutor->getItemMTime(i));

                const QString type = readDirExecutor->getItemType(i);
                if (type == "Dir")
                {
                    info.setType(FileInfo::Type::DIRECTORY);
                }
                else if (type == "File")
                {
                    info.setType(FileInfo::Type::FILE);
                }
                else
                {
                    info.setType(FileInfo::Type::OTHER);
                }

                m_infos.append(info);
            }

            success = true;
            allRead = (static_cast<quint32>(m_infos.size()) == total);
        }
        catch (BadArgumentException& e)
        {
            qDebug() << e.getMessage();
        }
    }

    if (success)
    {
        if (allRead)
        {
            // mark success
            m_taskSuccess = true;

            // close directory before notify
            requestCloseDir();
        }
        else
        {
            // continue reading
            requestReadDir();
        }
    }
    else
    {
        // still marked as failure - close directory before notify
        requestCloseDir();
    }

    executor->deleteLater();
}

void ReadDirTask::requestCloseDir()
{
    CloseDirExecutor* executor = new CloseDirExecutor(this);

    setTarget(executor, m_serverId, true);

    connect(executor, &Executor::finished, this, &ReadDirTask::onCloseDirFinished);

    executor->setDirId(m_dirId);
    executor->execute();
}

void ReadDirTask::onCloseDirFinished(
        QPointer<BopRpc::Executor> executor)
{
    Q_ASSERT(executor);

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        CloseDirExecutor* const closeDirExecutor = static_cast<CloseDirExecutor*>(executor.data());

        // TODO check response
    }
    else
    {
        m_taskSuccess = false;
    }

    // finished
    notifyFinished();

    executor->deleteLater();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
