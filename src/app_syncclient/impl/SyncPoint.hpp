//-----------------------------------------------------------------------------
/// \file
/// SyncPoint - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncPoint
#define INCLUDED_OCTsync_SyncPoint

//-----------------------------------------------------------------------------

// local includes
#include "ShareLocator.hpp"
#include "SyncMode.hpp"

// library includes
#include <QtCore/QDateTime>
#include <QtCore/QSharedData>
#include <QtNetwork/QTcpSocket>

#include <commons/Uuid.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// Descriptor of a single sync point.
///
/// Objects of this class are used to describe single synchronization points.
class SyncPoint
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    struct SyncPointData : public QSharedData
    {
        Commons::Uuid m_uuid;
        QString m_name;
        SyncMode m_mode;
        QDateTime m_lastSyncTime;
        ShareLocator m_shareLocatorA;
        ShareLocator m_shareLocatorB;

        SyncPointData() :
                        m_uuid(Commons::Uuid::generate()),
                        m_mode(SyncMode::MANUAL)
        {
            // nothing to do
        }
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QSharedDataPointer<SyncPointData> m_data;

// Methods (public, protected, private)
public:
    SyncPoint();

    bool isValid() const;

    const Commons::Uuid& getUuid() const
    {
        return m_data->m_uuid;
    }

    const QString& getName() const
    {
        return m_data->m_name;
    }

    SyncMode getMode() const
    {
        return m_data->m_mode;
    }

    const QDateTime& getLastSyncTime() const
    {
        return m_data->m_lastSyncTime;
    }

    const ShareLocator& getShareLocatorA() const
    {
        return m_data->m_shareLocatorA;
    }

    const ShareLocator& getShareLocatorB() const
    {
        return m_data->m_shareLocatorB;
    }

    void setUuid(
            const Commons::Uuid& uuid)
    {
        m_data->m_uuid = uuid;
    }

    void setName(
            const QString& name)
    {
        m_data->m_name = name;
    }

    void setMode(
            const SyncMode mode)
    {
        m_data->m_mode = mode;
    }

    void setLastSyncTime(
            const QDateTime lastSyncTime = QDateTime())
    {
        m_data->m_lastSyncTime = lastSyncTime;
    }

    void setShareLocatorA(
            const ShareLocator& shareLocator)
    {
        m_data->m_shareLocatorA = shareLocator;
    }

    void setShareLocatorB(
            const ShareLocator& shareLocator)
    {
        m_data->m_shareLocatorB = shareLocator;
    }

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncPoint*/
