//-----------------------------------------------------------------------------
/// \file
/// ShareLocator - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ShareLocator.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

ShareLocator::ShareLocator()
{
    // nothing to do
}

ShareLocator::ShareLocator(
        const Commons::Uuid& serverId,
        const Commons::Uuid& shareId)
{
    setLocator(serverId, shareId);
}

bool ShareLocator::isValid() const
{
    return m_serverId.isValid() && m_shareId.isValid();
}

const Commons::Uuid& ShareLocator::getServerId() const
{
    return m_serverId;
}

const Commons::Uuid& ShareLocator::getShareId() const
{
    return m_shareId;
}

void ShareLocator::setLocator(
        const Commons::Uuid& serverId,
        const Commons::Uuid& shareId)
{
    m_serverId = serverId;
    m_shareId = shareId;
}

bool operator ==(
        const ShareLocator& locator1,
        const ShareLocator& locator2)
{
    if (locator1.getServerId() != locator2.getServerId())
    {
        return false;
    }

    if (locator1.getShareId() != locator2.getShareId())
    {
        return false;
    }

    return true;
}

bool operator !=(
        const ShareLocator& locator1,
        const ShareLocator& locator2)
{
    return !(locator1 == locator2);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
