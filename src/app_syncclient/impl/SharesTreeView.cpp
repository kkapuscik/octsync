//-----------------------------------------------------------------------------
/// \file
/// SharesTreeView - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SharesTreeView.hpp"

// library includes
#include <QtCore/QStringList>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SharesTreeView::SharesTreeView(
        QWidget* parent) :
                QTreeView(parent)
{
    setAllColumnsShowFocus(true);
    connect(this, &QTreeView::expanded, this, &SharesTreeView::onItemExpanded);
}

void SharesTreeView::onItemExpanded(
        const QModelIndex& /*index*/)
{
    resizeColumnToContents(0);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
