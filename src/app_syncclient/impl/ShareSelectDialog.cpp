//-----------------------------------------------------------------------------
/// \file
/// ShareSelectDialog - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ShareSelectDialog.hpp"

// library includes
#include <QtWidgets/QLayout>
#include <ui/ActionButton.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Ui::ActionButton;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

ShareSelectDialog::ShareSelectDialog(
        SyncClientEngine* engine,
        QWidget* parent) :
                QDialog(parent)
{
    setSizeGripEnabled(true);
    resize(600, 300);

    createActions();
    createWidgets();

    // must be set before connecting to selection model
    m_sharesTree->setModel(engine->getShareTreeModel());

    connectActions();

    // force selection recalculation
    onCurrentChanged(m_sharesTree->currentIndex(), m_sharesTree->currentIndex());
}

void ShareSelectDialog::createActions()
{
    m_actionDialogOk = new QAction(this);
    m_actionDialogOk->setText(tr("OK"));

    m_actionDialogCancel = new QAction(this);
    m_actionDialogCancel->setText(tr("Cancel"));
}

void ShareSelectDialog::createWidgets()
{
    m_sharesTree = new QTreeView();
    m_sharesTree->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_sharesTree->setSelectionMode(QAbstractItemView::SingleSelection);

    ActionButton* dialogOkButton = new ActionButton(m_actionDialogOk);
    ActionButton* dialogCancelButton = new ActionButton(m_actionDialogCancel);

    QHBoxLayout* const dialogButtonsLayout = new QHBoxLayout();
    dialogButtonsLayout->addSpacing(10);
    dialogButtonsLayout->addWidget(dialogOkButton);
    dialogButtonsLayout->addStretch(1);
    dialogButtonsLayout->addWidget(dialogCancelButton);
    dialogButtonsLayout->addSpacing(10);

    QVBoxLayout* const mainLayout = new QVBoxLayout(this);
    mainLayout->setSpacing(15);
    mainLayout->addWidget(m_sharesTree);
    mainLayout->addLayout(dialogButtonsLayout);

    dialogOkButton->setDefault(true);
}

void ShareSelectDialog::connectActions()
{
    connect(m_actionDialogOk, &QAction::triggered, this, &ShareSelectDialog::onDialogOk);
    connect(m_actionDialogCancel, &QAction::triggered, this, &QDialog::reject);

    connect(m_sharesTree->selectionModel(), &QItemSelectionModel::currentChanged, this,
            &ShareSelectDialog::onCurrentChanged);
    connect(m_sharesTree, &QTreeView::expanded, this, &ShareSelectDialog::onItemExpanded);
}

void ShareSelectDialog::onItemExpanded(
        const QModelIndex& /*index*/)
{
    m_sharesTree->resizeColumnToContents(0);
}

void ShareSelectDialog::onCurrentChanged(
        const QModelIndex& current,
        const QModelIndex& /*previous*/)
{
    m_currentLocator = ShareLocator();

    if (current.isValid())
    {
        const QModelIndex dataIndex = m_sharesTree->model()->index(current.row(), 0,
                current.parent());

        const QAbstractItemModel* const model = m_sharesTree->model();

        int itemType = model->data(dataIndex, SyncClientEngine::ROLE_ITEM_TYPE).toInt();
        if (itemType == SyncClientEngine::ITEM_TYPE_SHARE)
        {
            const QUuid serverId = model->data(dataIndex, SyncClientEngine::ROLE_SERVER_ID).toUuid();
            const QUuid shareId = model->data(dataIndex, SyncClientEngine::ROLE_SHARE_ID).toUuid();

            m_currentLocator.setLocator(serverId, shareId);
        }
    }

    m_actionDialogOk->setEnabled(m_currentLocator.isValid());
}

void ShareSelectDialog::onDialogOk()
{
    if (m_currentLocator.isValid())
    {
        accept();
    }
}

ShareLocator ShareSelectDialog::getShareLocator() const
{
    return m_currentLocator;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
