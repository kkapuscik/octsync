//-----------------------------------------------------------------------------
/// \file
/// AsyncTask - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "AsyncTask.hpp"
#include "SyncClientEngine.hpp"

// library includes
#include <QtCore/QTimer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

AsyncTask::AsyncTask(
        SyncClientEngine* engine,
        QObject* parent) :
                QObject(parent),
                m_engine(engine),
                m_state(State::PENDING),
                m_queue(nullptr)
{
    Q_ASSERT(m_engine);
}

void AsyncTask::setTarget(
        BopRpc::Executor* executor,
        const Commons::Uuid& serverId,
        const bool syncCall)
{
    Q_ASSERT(executor);

    m_engine->setTarget(executor, serverId, syncCall);
}

void AsyncTask::setAutoDelete()
{
    connect(this, &AsyncTask::finished, this, &QObject::deleteLater);
}

void AsyncTask::markScheduled(
        AsyncTaskQueue* queue)
{
    Q_ASSERT(m_state == State::PENDING);
    Q_ASSERT(!m_queue);
    Q_ASSERT(queue);

    m_state = State::SCHEDULED;
    m_queue = queue;
}

void AsyncTask::start()
{
    Q_ASSERT(m_state == State::SCHEDULED);
    Q_ASSERT(m_queue);

    m_state = State::STARTED;

    QTimer::singleShot(0, this, SLOT( startAsync() ));
}

void AsyncTask::startStandalone()
{
    Q_ASSERT(m_state == State::PENDING);
    Q_ASSERT(!m_queue);

    m_state = State::STARTED;

    QTimer::singleShot(0, this, SLOT( startAsync() ));
}

void AsyncTask::startAsync()
{
    run();
}

void AsyncTask::notifyFinished()
{
    m_state = State::FINISHED;

    emit finished();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
