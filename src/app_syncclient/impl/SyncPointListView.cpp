//-----------------------------------------------------------------------------
/// \file
/// SharesTreeView - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncPointListView.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncPointListView::SyncPointListView(
        QWidget* parent) :
                QListView(parent)
{
    // Nothing to do
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
