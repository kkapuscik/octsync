//-----------------------------------------------------------------------------
/// \file
/// DeleteTask - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "DeleteTask.hpp"

// library includes
#include <syncserver/DeleteFileExecutor.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::Executor;
using OCTsync::SyncServer::DeleteFileExecutor;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

DeleteTask::DeleteTask(
        SyncClientEngine* engine,
        const Commons::Uuid& serverId,
        const Commons::Uuid& shareId,
        const QString& path,
        QObject* parent) :
                AsyncTask(engine, parent),
                m_serverId(serverId),
                m_shareId(shareId),
                m_path(path),
                m_result(false)
{
    // nothing to do
}

void DeleteTask::run()
{
    DeleteFileExecutor* executor = new DeleteFileExecutor(this);

    setTarget(executor, m_serverId, true);

    connect(executor, &Executor::finished, this, &DeleteTask::onDeleteFinished);

    executor->setShareId(m_shareId);
    executor->setPath(m_path);
    executor->execute();
}

void DeleteTask::onDeleteFinished(
        QPointer<Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        if (executor->getErrorCode() == 0)
        {
            m_result = true;
        }
    }

    notifyFinished();

    executor->deleteLater();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
