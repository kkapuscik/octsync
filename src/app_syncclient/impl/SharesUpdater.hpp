//-----------------------------------------------------------------------------
/// \file
/// SharesUpdater - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SharesUpdater
#define INCLUDED_OCTsync_SharesUpdater

//-----------------------------------------------------------------------------

// local includes
#include "SyncShare.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QPointer>
#include <QtCore/QList>
#include <boprpc/Client.hpp>
#include <syncserver/GetSharesExecutor.hpp>
#include <syncserver/GetShareInfoExecutor.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SharesUpdater : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QPointer<BopRpc::Client> m_client;
    QUrl m_url;
    quint32 m_serviceId;

    SyncServer::GetSharesExecutor* m_getSharesExecutor;
    SyncServer::GetShareInfoExecutor* m_getShareInfoExecutor;

    QList<Commons::Uuid> m_shareIds;
    QList<SyncShare> m_shareInfos;

// Methods (public, protected, private)
public:
    SharesUpdater(
            QPointer<BopRpc::Client> client,
            QObject* parent = nullptr);

    void setServiceInfo(
            const QUrl& url,
            const quint32 serviceId);

    void requestUpdate();

private:
    void onGetSharesFinished(
            QPointer<BopRpc::Executor> executor);

    void onGetShareInfoFinished(
            QPointer<BopRpc::Executor> executor);

    void getNextShareInfo();

    void notifyResults(
            const bool success);

signals:
    void updateFinished(
            QList<SyncShare> shares);

    void updateFailed();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SharesUpdater*/
