//-----------------------------------------------------------------------------
/// \file
/// SyncPointInfoWidget - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncPointInfoWidget.hpp"

// library includes
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLayout>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncPointInfoWidget::SyncPointInfoWidget(
        const Mode mode,
        QWidget* parent) :
                QWidget(parent)
{
    m_uuid = createLineEdit(this);
    m_name = createLineEdit(this);
    m_mode = createLineEdit(this);
    m_lastSyncTime = createLineEdit(this);

    m_serverNameA = createLineEdit(this);
    m_shareNameA = createLineEdit(this);
    m_serverIdA = createLineEdit(this);
    m_shareIdA = createLineEdit(this);

    m_serverNameB = createLineEdit(this);
    m_shareNameB = createLineEdit(this);
    m_serverIdB = createLineEdit(this);
    m_shareIdB = createLineEdit(this);

    auto nameLayoutA = layoutRowWidget(m_serverNameA, m_shareNameA);
    auto idLayoutA = layoutRowWidget(m_serverIdA, m_shareIdA);
    auto nameLayoutB = layoutRowWidget(m_serverNameB, m_shareNameB);
    auto idLayoutB = layoutRowWidget(m_serverIdB, m_shareIdB);

    auto mainLayout = new QFormLayout(this);

    switch (mode)
    {
        case Mode::FULL_INFO:
            mainLayout->addRow(tr("UUID"), m_uuid);
            mainLayout->addRow(tr("Name"), m_name);
            mainLayout->addRow(tr("Mode"), m_mode);
            mainLayout->addRow(tr("Last sync"), m_lastSyncTime);
            mainLayout->addRow(tr("Share A"), nameLayoutA);
            mainLayout->addRow(tr("Identifiers A"), idLayoutA);
            mainLayout->addRow(tr("Share B"), nameLayoutB);
            mainLayout->addRow(tr("Identifiers B"), idLayoutB);
            break;
        case Mode::SHARES_INFO:
            mainLayout->addRow(tr("Name"), m_name);
            mainLayout->addRow(tr("Share A"), nameLayoutA);
            mainLayout->addRow(tr("Ids. A"), idLayoutA);
            mainLayout->addRow(tr("Share B"), nameLayoutB);
            mainLayout->addRow(tr("Ids. B"), idLayoutB);
            break;
        default:
            Q_ASSERT(0);
            break;
    }
}

QLineEdit* SyncPointInfoWidget::createLineEdit(
        QWidget* const parent)
{
    auto const lineEdit = new QLineEdit(parent);
    lineEdit->setReadOnly(true);
    return lineEdit;
}

QLayout* SyncPointInfoWidget::layoutRowWidget(
        QLineEdit* const lineEdit1,
        QLineEdit* const lineEdit2)
{
    auto const boxLayout = new QHBoxLayout();
    boxLayout->setSpacing(5);
    boxLayout->addWidget(lineEdit1);
    boxLayout->addWidget(lineEdit2);
    return boxLayout;
}

void SyncPointInfoWidget::setEngine(
        QPointer<SyncClientEngine> engine)
{
    m_engine = engine;
    setSyncPoint(m_syncPoint);
}

void SyncPointInfoWidget::setSyncPoint(
        const SyncPoint& syncPoint)
{
    m_syncPoint = syncPoint;

    QString syncPointUuid;
    QString syncPointName;
    QString modeString;
    QString lastSyncTimeString;
    QString serverNameA;
    QString shareNameA;
    QString serverIdStrA;
    QString shareIdStrA;
    QString serverNameB;
    QString shareNameB;
    QString serverIdStrB;
    QString shareIdStrB;

    if (syncPoint.isValid())
    {
        const Commons::Uuid& serverIdA = syncPoint.getShareLocatorA().getServerId();
        const Commons::Uuid& serverIdB = syncPoint.getShareLocatorB().getServerId();

        const Commons::Uuid& shareIdA = syncPoint.getShareLocatorA().getShareId();
        const Commons::Uuid& shareIdB = syncPoint.getShareLocatorB().getShareId();

        syncPointUuid = syncPoint.getUuid().toString();
        syncPointName = syncPoint.getName();
        modeString = syncPoint.getMode().toString();
        lastSyncTimeString = syncPoint.getLastSyncTime().toString();

        serverIdStrA = serverIdA.toString();
        shareIdStrA = shareIdA.toString();
        serverIdStrB = serverIdB.toString();
        shareIdStrB = shareIdB.toString();

        if (m_engine)
        {
            serverNameA = m_engine->getServerName(serverIdA);
            serverNameB = m_engine->getServerName(serverIdB);
            shareNameA = m_engine->getShareName(serverIdA, shareIdA);
            shareNameB = m_engine->getShareName(serverIdB, shareIdB);

            // TODO: refresh
        }
    }

    m_uuid->setText(syncPointUuid);
    m_name->setText(syncPointName);
    m_mode->setText(modeString);
    m_lastSyncTime->setText(lastSyncTimeString);

    m_serverNameA->setText(serverNameA);
    m_shareNameA->setText(shareNameA);
    m_serverIdA->setText(serverIdStrA);
    m_shareIdA->setText(shareIdStrA);

    m_serverNameB->setText(serverNameB);
    m_shareNameB->setText(shareNameB);
    m_serverIdB->setText(serverIdStrB);
    m_shareIdB->setText(shareIdStrB);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
