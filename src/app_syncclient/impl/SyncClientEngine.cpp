//-----------------------------------------------------------------------------
/// \file
/// SyncClientEngine - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncClientEngine.hpp"
#include "ShareTreeModel.hpp"
#include "SyncPointListModel.hpp"
#include "SyncPointsData.hpp"

// library includes
#include <bop/ObjectFactoryManager.hpp>
#include <boprpc/RpcObjectFactory.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::Client;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

const QString SyncClientEngine::OPTION_NAME_SERVER_CFGFILE("-syncscfg=");
const QString SyncClientEngine::ORGANIZATION_NAME("OCTaedr");
const QString SyncClientEngine::APPLICATION_NAME("SyncClient");

SyncClientEngine::SyncClientEngine(
        QObject* parent) :
                QObject(parent)
{
    m_shareTreeModel = new ShareTreeModel(this);

    m_syncData = new SyncPointsData(this);

    m_syncPointListModel = new SyncPointListModel(m_syncData, this);

    m_ldpClient = Ldp::Client(Commons::Uuid::generate());

    m_ldpMonitor = new Ldp::Monitor(m_ldpClient, this);

    connect(m_ldpMonitor, &Ldp::Monitor::deviceAdded, this, &SyncClientEngine::onLdpDeviceAdded);
    connect(m_ldpMonitor, &Ldp::Monitor::deviceRemoved, this, &SyncClientEngine::onLdpDeviceRemoved);

    m_rpcClient = new BopRpc::Client(this);
}

void SyncClientEngine::setTarget(
        BopRpc::Executor* executor,
        const Commons::Uuid& serverId,
        const bool syncCall)
{
    Q_ASSERT(executor);

    auto serverIter = m_serverDevices.cfind(serverId);
    if (serverIter != m_serverDevices.end())
    {
        executor->setClient(QPointer<Client>(m_rpcClient));
        executor->setUrl(serverIter->second->getUrl());
        if (syncCall)
        {
            executor->setServiceId(serverIter->second->getSyncServiceId());
        }
    }
}

void SyncClientEngine::processArguments(
        QStringList arguments)
{
    QString configFileName;

    for (QString arg : arguments)
    {
        if (arg.startsWith(OPTION_NAME_SERVER_CFGFILE))
        {
            configFileName = arg.mid(OPTION_NAME_SERVER_CFGFILE.length());
        }
    }

    if (!configFileName.isEmpty())
    {
        m_configFileName = configFileName;
    }
}

void SyncClientEngine::start()
{
    std::unique_ptr<QSettings> settings = createSettingsObject();

    connect(m_syncData, &SyncPointsData::changed, this, &SyncClientEngine::onSyncPointsDataChanged);

    if (settings)
    {
        if (!m_syncData->load(*settings))
        {
            // TOOD:
            // Error handling
        }
    }
    else
    {
        // TODO:
        // What should be done here?
    }

    m_ldpMonitor->enable();
}

QAbstractItemModel* SyncClientEngine::getShareTreeModel() const
{
    return m_shareTreeModel;
}

QAbstractItemModel* SyncClientEngine::getSyncListModel() const
{
    return m_syncPointListModel;
}

SyncContext* SyncClientEngine::createSyncContext(
        const SyncPoint& syncPoint)
{
    return new SyncContext(this, syncPoint, this);
}

void SyncClientEngine::onLdpDeviceAdded(
        Ldp::Device ldpDevice)
{
    qDebug() << "SyncClientEngine::onLdpDeviceAdded()" << ldpDevice.getUrl();

    auto deviceIter = m_serverDevices.find(ldpDevice.getUuid());
    if (deviceIter == m_serverDevices.end())
    {
        ServerDevice* newDevice = new ServerDevice(QPointer<Client>(m_rpcClient), this);
        connect(newDevice, &ServerDevice::stateChanged, this, &SyncClientEngine::onDeviceStateChanged);

        m_serverDevices.insert(std::make_pair(ldpDevice.getUuid(), newDevice));

        newDevice->setLdpDevice(ldpDevice);
    }
    else
    {
        qDebug() << "Fatal: device with same UUID already added" << ldpDevice.getUuid().toString()
                << ' ' << ldpDevice.getUrl();
    }
}

void SyncClientEngine::onLdpDeviceRemoved(
        Ldp::Device ldpDevice)
{
    qDebug() << "SyncClientEngine::onLdpDeviceRemoved()" << ldpDevice.getUrl();

    auto deviceIter = m_serverDevices.find(ldpDevice.getUuid());
    if (deviceIter != m_serverDevices.end())
    {
        auto device = deviceIter->second;

        m_serverDevices.erase(deviceIter);

        device->invalidate();

        device->deleteLater();
    }
}

void SyncClientEngine::onDeviceStateChanged(
        QPointer<ServerDevice> device,
        ServerDevice::State state)
{
    if (!device)
    {
        return;
    }

    switch (state)
    {
        case ServerDevice::State::INITIALIZED:
            device->requestRpcValidate();
            break;

        case ServerDevice::State::BOPRPC_VALIDATED:
            device->requestNameUpdate();
            break;

        case ServerDevice::State::READY:
            addValidDevice(device.data());
            break;

        case ServerDevice::State::INVALID:
            removeValidDevice(device.data());
            break;

        default:
            Q_ASSERT(0);
            break;
    }
}

void SyncClientEngine::addValidDevice(
        ServerDevice* const device)
{
    m_shareTreeModel->addDevice(device);
    // TODO
}

void SyncClientEngine::removeValidDevice(
        ServerDevice* const device)
{
    m_shareTreeModel->removeDevice(device);
    // TODO
}

std::unique_ptr<QSettings> SyncClientEngine::createSettingsObject()
{
    if (m_configFileName.isEmpty())
    {
        return std::unique_ptr<QSettings>(
                new QSettings(QSettings::IniFormat, QSettings::UserScope, ORGANIZATION_NAME,
                        APPLICATION_NAME, this));
    }
    else
    {
        return std::unique_ptr<QSettings>(
                new QSettings(m_configFileName, QSettings::IniFormat, this));
    }
}

void SyncClientEngine::onSyncPointsDataChanged()
{
    std::unique_ptr<QSettings> settings = createSettingsObject();

    if (settings)
    {
        if (!m_syncData->save(*settings))
        {
            // TODO
        }
    }
    else
    {
        // TODO
    }

    m_syncPointListModel->clear();

    const auto syncList = m_syncData->getSyncPoints().getSyncIds();

    for (const auto& syncId : syncList)
    {
        const SyncPoint syncPoint = m_syncData->getSyncPoints().getSync(syncId);
        addSyncPoint(syncPoint);
    }
}

void SyncClientEngine::addSyncPoint(
        const SyncPoint& syncPoint)
{
    SyncPoint sync = syncPoint;
    m_syncPointListModel->addSyncPoint(sync);
}

void SyncClientEngine::removeSyncPoint(
        const SyncPoint& syncPoint)
{
    SyncPoint sync = syncPoint;
    m_syncPointListModel->removeSyncPoint(sync);
}

SyncPoint SyncClientEngine::getSyncPoint(
        const Commons::Uuid& syncId)
{
    return m_syncData->getSync(syncId);
}

ServerDevice* SyncClientEngine::getServer(
        const Commons::Uuid& serverId) const
{
    auto serverIter = m_serverDevices.cfind(serverId);
    if (serverIter != m_serverDevices.end())
    {
        return serverIter->second;
    }
    else
    {
        return nullptr;
    }
}

QString SyncClientEngine::getServerName(
        const Commons::Uuid& serverId) const
{
    const auto server = getServer(serverId);
    if (server)
    {
        return server->getName();
    }
    return tr("Unknown");
}

QString SyncClientEngine::getShareName(
        const Commons::Uuid& serverId,
        const Commons::Uuid& shareId) const
{
    const auto server = getServer(serverId);
    if (server)
    {
        return server->getShareName(shareId);
    }
    return tr("Unknown");
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
