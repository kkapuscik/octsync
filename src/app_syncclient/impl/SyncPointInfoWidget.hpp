//-----------------------------------------------------------------------------
/// \file
/// SyncPointInfoWidget - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncPointInfoWidget
#define INCLUDED_OCTsync_SyncPointInfoWidget

//-----------------------------------------------------------------------------

// local includes
#include "SyncClientEngine.hpp"
#include "SyncPoint.hpp"

// library includes
#include <QtWidgets/QWidget>
#include <QtCore/QPointer>
#include <QtWidgets/QLineEdit>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncPointInfoWidget : public QWidget
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    enum class Mode
    {
        FULL_INFO,
        SHARES_INFO
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QPointer<SyncClientEngine> m_engine;
    SyncPoint m_syncPoint;

    QLineEdit* m_uuid;
    QLineEdit* m_name;
    QLineEdit* m_mode;
    QLineEdit* m_lastSyncTime;

    QLineEdit* m_serverNameA;
    QLineEdit* m_shareNameA;
    QLineEdit* m_serverIdA;
    QLineEdit* m_shareIdA;

    QLineEdit* m_serverNameB;
    QLineEdit* m_shareNameB;
    QLineEdit* m_serverIdB;
    QLineEdit* m_shareIdB;

// Methods (public, protected, private)
public:
    SyncPointInfoWidget(
            const Mode mode,
            QWidget* parent = nullptr);

    void setEngine(
            QPointer<SyncClientEngine> engine);

    void setSyncPoint(
            const SyncPoint& syncPoint);

private:
    QLineEdit* createLineEdit(
            QWidget* const parent);

    QLayout* layoutRowWidget(
            QLineEdit* const lineEdit1,
            QLineEdit* const lineEdit2);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncPointInfoWidget*/
