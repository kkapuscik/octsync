//-----------------------------------------------------------------------------
/// \file
/// SyncMode - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncMode.hpp"

// library includes
#include <QtCore/QtGlobal>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncMode SyncMode::MANUAL(MODE_MANUAL);
SyncMode SyncMode::FULL_TWO_SIDED(MODE_FULL_TWO_SIDED);
SyncMode SyncMode::SAFE_TWO_SIDED(MODE_SAFE_TWO_SIDED);
SyncMode SyncMode::MIRROR_A_TO_B(MODE_MIRROR_A_TO_B);
SyncMode SyncMode::MIRROR_B_TO_A(MODE_MIRROR_B_TO_A);
SyncMode SyncMode::NEW_ONLY_A_TO_B(MODE_NEW_ONLY_A_TO_B);
SyncMode SyncMode::NEW_ONLY_B_TO_A(MODE_NEW_ONLY_B_TO_A);

QList<SyncMode> SyncMode::getAllModes()
{
    QList<SyncMode> allModes;

    allModes.append(MANUAL);
    allModes.append(FULL_TWO_SIDED);
    allModes.append(SAFE_TWO_SIDED);
    allModes.append(MIRROR_A_TO_B);
    allModes.append(MIRROR_B_TO_A);
    allModes.append(NEW_ONLY_A_TO_B);
    allModes.append(NEW_ONLY_B_TO_A);

    return allModes;
}

QString SyncMode::toString() const
{
    switch (m_mode)
    {
        case MODE_MANUAL:
            return QT_TR_NOOP("Manual");
        case MODE_FULL_TWO_SIDED:
            return QT_TR_NOOP("Full Two-sided");
        case MODE_SAFE_TWO_SIDED:
            return QT_TR_NOOP("Safe Two-sided");
        case MODE_MIRROR_A_TO_B:
            return QT_TR_NOOP("Mirror A->B");
        case MODE_MIRROR_B_TO_A:
            return QT_TR_NOOP("Mirror B->A");
        case MODE_NEW_ONLY_A_TO_B:
            return QT_TR_NOOP("New A->B");
        case MODE_NEW_ONLY_B_TO_A:
            return QT_TR_NOOP("New B->A");
        default:
            return QT_TR_NOOP("-unknown-");
    }
}

int SyncMode::toInt() const
{
    return m_mode;
}

bool SyncMode::fromInt(
        const int value)
{
    for (SyncMode mode : getAllModes())
    {
        if (mode.toInt() == value)
        {
            m_mode = mode.m_mode;
            return true;
        }
    }

    return false;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
