//-----------------------------------------------------------------------------
/// \file
/// SharesUpdater - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SharesUpdater.hpp"

// library includes
#include <boprpc/Service.hpp>
#include <boprpc/Executor.hpp>
#include <boprpc/BadArgumentException.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Commons::Uuid;
using OCTsync::BopRpc::Service;
using OCTsync::BopRpc::Executor;
using OCTsync::BopRpc::BadArgumentException;
using OCTsync::SyncServer::GetSharesExecutor;
using OCTsync::SyncServer::GetShareInfoExecutor;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SharesUpdater::SharesUpdater(
        QPointer<BopRpc::Client> client,
        QObject* parent) :
                QObject(parent),
                m_client(client),
                m_serviceId(Service::INVALID_SERVICE_ID),
                m_getSharesExecutor(nullptr),
                m_getShareInfoExecutor(nullptr)
{
    // nothing to do
}

void SharesUpdater::setServiceInfo(
        const QUrl& url,
        const quint32 serviceId)
{
    m_url = url;
    m_serviceId = serviceId;
}

void SharesUpdater::requestUpdate()
{
    // update is already in progress?
    if (m_getSharesExecutor || m_getShareInfoExecutor)
    {
        return;
    }

    // reset the state
    m_shareIds.clear();
    m_shareInfos.clear();

    // try to get shares
    m_getSharesExecutor = new GetSharesExecutor(this);

    m_getSharesExecutor->setClient(m_client);
    m_getSharesExecutor->setUrl(m_url);
    m_getSharesExecutor->setServiceId(m_serviceId);

    connect(m_getSharesExecutor, &Executor::finished, this, &SharesUpdater::onGetSharesFinished);

    m_getSharesExecutor->execute();
}

void SharesUpdater::onGetSharesFinished(
        QPointer<Executor> executor)
{
    if (executor)
    {
        Q_ASSERT(executor == m_getSharesExecutor);

        if (executor->getState() == Executor::State::SUCCEEDED)
        {
            try
            {
                m_shareIds = m_getSharesExecutor->getShareIds();

                getNextShareInfo();
            }
            catch (BadArgumentException& e)
            {
                qDebug() << e.getMessage();
                notifyResults(false);
            }
        }
        else
        {
            notifyResults(false);
        }

        m_getSharesExecutor->deleteLater();
        m_getSharesExecutor = nullptr;
    }
}

void SharesUpdater::getNextShareInfo()
{
    if (m_shareInfos.size() < m_shareIds.size())
    {
        const Uuid uuid = m_shareIds.at(m_shareInfos.size());

        Q_ASSERT(!m_getShareInfoExecutor);

        m_getShareInfoExecutor = new GetShareInfoExecutor(this);

        m_getShareInfoExecutor->setClient(m_client);
        m_getShareInfoExecutor->setUrl(m_url);
        m_getShareInfoExecutor->setServiceId(m_serviceId);

        connect(m_getShareInfoExecutor, &Executor::finished, this,
                &SharesUpdater::onGetShareInfoFinished);

        m_getShareInfoExecutor->setShareId(uuid);
        m_getShareInfoExecutor->execute();
    }
    else
    {
        notifyResults(true);
    }
}

void SharesUpdater::onGetShareInfoFinished(
        QPointer<Executor> executor)
{
    if (executor)
    {
        bool success = false;

        Q_ASSERT(executor == m_getShareInfoExecutor);

        if (executor->getState() == Executor::State::SUCCEEDED)
        {
            try
            {
                SyncShare newShare;

                newShare.setUuid(m_shareIds.at(m_shareInfos.size()));
                newShare.setName(m_getShareInfoExecutor->getShareName());
                newShare.setDescription(m_getShareInfoExecutor->getShareDescription());
                newShare.setReadOnly(m_getShareInfoExecutor->isShareReadOnly());

                m_shareInfos.append(newShare);

                success = true;
            }
            catch (BadArgumentException& e)
            {
                qDebug() << e.getMessage();
            }
        }

        m_getShareInfoExecutor->deleteLater();
        m_getShareInfoExecutor = nullptr;

        if (success)
        {
            getNextShareInfo();
        }
        else
        {
            notifyResults(false);
        }
    }
}

void SharesUpdater::notifyResults(
        const bool success)
{
    qDebug() << "SharesUpdater::notifyResults(" << success << ")";

    for (int i = 0; i < m_shareIds.size(); ++i)
    {
        qDebug() << "Share " << i << ":";
        if (i < m_shareInfos.size())
        {
            qDebug() << "- id      : " << m_shareInfos.at(i).getUuid().toString();
            qDebug() << "- name    : " << m_shareInfos.at(i).getName();
            qDebug() << "- descrip.: " << m_shareInfos.at(i).getDescription();
            qDebug() << "- readonly: " << m_shareInfos.at(i).isReadOnly();
        }
    }

    if (success)
    {
        emit updateFinished(m_shareInfos);
    }
    else
    {
        emit updateFailed();
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
