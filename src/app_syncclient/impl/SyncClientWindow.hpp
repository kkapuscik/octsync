//-----------------------------------------------------------------------------
/// \file
/// SyncClientWindow - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_AppSyncClient_SyncClientWindow
#define INCLUDED_OCTsync_AppSyncClient_SyncClientWindow

//-----------------------------------------------------------------------------

// local includes
#include "SyncClientEngine.hpp"

// library includes
#include <QtWidgets/QMainWindow>

// system includes
// (none)

// using clauses
// (none)

// forward references
class QAction;
class QTabWidget;

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class SharesWidget;
class SyncsWidget;

/// TODO: Class documentation
///
class SyncClientWindow : public QMainWindow
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncClientEngine* m_engine;

    QAction* m_actionAppExit;
    QAction* m_actionAppAbout;
    QAction* m_actionAppAboutQt;
    QAction* m_actionSyncManual;

    QTabWidget* m_mainTabWidget;

    SharesWidget* m_sharesWidget;
    SyncsWidget* m_syncsWidget;

// Methods (public, protected, private)
public:
    SyncClientWindow(
            SyncClientEngine* engine,
            QWidget* parent = nullptr);

private:
    void createActions();
    void createWidgets();
    void createMainMenu();

    void connectActions();

    void setUpModels();

    void showAppAboutDialog();
    void showQtAboutDialog();
    void startManualSync();

    void performManualSync(
            const SyncPoint& syncPoint);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_AppSyncClient_SyncClientWindow*/
