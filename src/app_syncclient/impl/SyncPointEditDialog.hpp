//-----------------------------------------------------------------------------
/// \file
/// SyncPointEditDialog - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncPointEditDialog
#define INCLUDED_OCTsync_SyncPointEditDialog

//-----------------------------------------------------------------------------

// local includes
#include "SyncClientEngine.hpp"
#include "SyncPoint.hpp"

// library includes
#include <QtCore/QPointer>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncPointEditDialog : public QDialog
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    enum class Mode
    {
        STORED_SYNC_POINT,
        MANUAL_SYNC_POINT
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Mode m_dialogMode;

    SyncClientEngine* m_engine;
    SyncPoint m_syncPoint;

    QLineEdit* m_name;
    QComboBox* m_syncMode;

    QLabel* m_serverNameA;
    QLabel* m_shareNameA;
    QLabel* m_serverIdA;
    QLabel* m_shareIdA;

    QLabel* m_serverNameB;
    QLabel* m_shareNameB;
    QLabel* m_serverIdB;
    QLabel* m_shareIdB;

    QAction* m_actionSelectShareA;
    QAction* m_actionSelectShareB;
    QAction* m_actionDialogOk;
    QAction* m_actionDialogCancel;

// Methods (public, protected, private)
public:
    SyncPointEditDialog(
            SyncClientEngine* engine,
            const Mode mode,
            QWidget* parent = nullptr,
            Qt::WindowFlags flags = 0);

    void setSyncPoint(
            const SyncPoint& syncPoint);

    SyncPoint getSyncPoint() const;

private:
    void createActions();
    void createWidgets();
    void connectActions();

    void onDialogOk();

    void onSelectShareA();
    void onSelectShareB();

    void onNameChanged(
            const QString& text);

    void onModeChanged(
            int index);

    void updateValues();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncPointEditDialog*/
