//-----------------------------------------------------------------------------
/// \file
/// SyncPointsData - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncPointsData
#define INCLUDED_OCTsync_SyncPointsData

//-----------------------------------------------------------------------------

// local includes
#include "SyncsRepository.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QSettings>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class SyncPoint;

/// TODO: Class documentation
///
class SyncPointsData : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncsRepository m_syncs;

// Methods (public, protected, private)
public:
    SyncPointsData(
            QObject* parent = nullptr);

    bool load(
            QSettings& settings);

    bool save(
            QSettings& settings);

    const SyncsRepository& getSyncPoints() const;

    SyncPoint getSync(
            const Commons::Uuid& uuid) const;

    void addSync(
            const SyncPoint& sync);

    void modifySync(
            const SyncPoint& sync);

    void removeSync(
            const SyncPoint& sync);

    void removeAllSyncs();

signals:
    void changed();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncPointsData*/
