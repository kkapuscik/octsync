//-----------------------------------------------------------------------------
/// \file
/// SyncAlgorithmManual - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncAlgorithmManual
#define INCLUDED_OCTsync_SyncAlgorithmManual

//-----------------------------------------------------------------------------

// local includes
#include "SyncAlgorithm.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncAlgorithmManual : public SyncAlgorithm
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    SyncAlgorithmManual() :
                    SyncAlgorithm()
    {
        // nothing to do
    }

    virtual void processItem(
            FileTreeItem& item);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncAlgorithmManual*/
