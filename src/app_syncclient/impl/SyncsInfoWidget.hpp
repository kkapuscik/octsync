//-----------------------------------------------------------------------------
/// \file
/// SyncsInfoWidget - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncsInfoWidget
#define INCLUDED_OCTsync_SyncsInfoWidget

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncsInfoWidget : public QWidget
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QLabel* m_share1;
    QLabel* m_hostName1;
    QLabel* m_shareName1;
    QPushButton* m_changeShare1;

// Methods (public, protected, private)
public:
    explicit SyncsInfoWidget(
            QWidget* parent = nullptr);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncsInfoWidget*/
