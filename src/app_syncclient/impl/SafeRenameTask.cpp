//-----------------------------------------------------------------------------
/// \file
/// SafeRenameTask - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SafeRenameTask.hpp"

// library includes
#include <syncserver/SafeRenameFileExecutor.hpp>
#include <boprpc/Executor.hpp>
#include <boprpc/BadArgumentException.hpp>

// system includes
// (none)

// using clauses
using OCTsync::SyncServer::SafeRenameFileExecutor;
using OCTsync::BopRpc::Executor;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SafeRenameTask::SafeRenameTask(
        SyncClientEngine* engine,
        const Commons::Uuid& serverId,
        const Commons::Uuid& shareId,
        const QString& path,
        QObject* parent) :
                AsyncTask(engine, parent),
                m_serverId(serverId),
                m_shareId(shareId),
                m_path(path),
                m_result(false)
{
    // nothing to do
}

void SafeRenameTask::run()
{
    SafeRenameFileExecutor* executor = new SafeRenameFileExecutor(this);

    setTarget(executor, m_serverId, true);

    connect(executor, &Executor::finished, this, &SafeRenameTask::onRenameFinished);

    executor->setShareId(m_shareId);
    executor->setPath(m_path);
    executor->execute();
}

void SafeRenameTask::onRenameFinished(
        QPointer<BopRpc::Executor> executor)
{
    Q_ASSERT(executor);

    bool success = false;

    if (executor->getState() == Executor::State::SUCCEEDED)
    {
        if (executor->getErrorCode() == 0)
        {
            SafeRenameFileExecutor* const renameExecutor =
                    static_cast<SafeRenameFileExecutor*>(executor.data());

            try
            {
                m_resultPath = renameExecutor->getSafePath();
                if (!m_resultPath.isEmpty())
                {
                    m_result = true;
                }
            }
            catch (BadArgumentException& e)
            {
                qDebug() << e.getMessage();
            }
        }
    }

    notifyFinished();

    executor->deleteLater();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
