//-----------------------------------------------------------------------------
/// \file
/// SyncAlgorithm - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncAlgorithm
#define INCLUDED_OCTsync_SyncAlgorithm

//-----------------------------------------------------------------------------

// local includes
#include "FileTreeItem.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncAlgorithm
{
// Static constants (public, protected, private)
private:
    static const qint64 MAX_TIME_DIFF_MS = 1500;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    SyncAlgorithm()
    {
        // nothing to do
    }

    virtual ~SyncAlgorithm()
    {
        // nothing to do
    }

    virtual void processItem(
            FileTreeItem& item) = 0;

protected:
    bool isSameFile(
            const FileInfo& info1,
            const FileInfo& info2) const;

    qint64 getTimeDiff(
            const FileInfo& info1,
            const FileInfo& info2) const;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncAlgorithm*/
