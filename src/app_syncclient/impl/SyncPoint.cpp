//-----------------------------------------------------------------------------
/// \file
/// SyncPoint - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncPoint.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SyncPoint::SyncPoint() :
        m_data(new SyncPointData())
{
    // nothing to do
}

bool SyncPoint::isValid() const
{
    bool valid = false;

    do {
        if (!m_data->m_uuid.isValid())
        {
            break;
        }

        // name must be specified
        if (m_data->m_name.isEmpty())
        {
            break;
        }

        // share A must be defined
        if (!m_data->m_shareLocatorA.isValid())
        {
            break;
        }

        // share B must be defined
        if (!m_data->m_shareLocatorB.isValid())
        {
            break;
        }

        // shares must be different
        if (m_data->m_shareLocatorA == m_data->m_shareLocatorB)
        {
            break;
        }

        valid = true;
    } while (0);

    return valid;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
