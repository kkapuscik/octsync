//-----------------------------------------------------------------------------
/// \file
/// SynchronizeTask - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SynchronizeTask.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

SynchronizeTask::SynchronizeTask(
        SyncClientEngine* engine,
        const Commons::Uuid& serverIdA,
        const Commons::Uuid& shareIdA,
        const Commons::Uuid& serverIdB,
        const Commons::Uuid& shareIdB,
        const QString& path,
        const qint64 mtimeA,
        const qint64 mtimeB,
        const SyncOp syncOp,
        QObject* parent) :
                AsyncTask(engine, parent),
                m_serverIdA(serverIdA),
                m_shareIdA(shareIdA),
                m_serverIdB(serverIdB),
                m_shareIdB(shareIdB),
                m_path(path),
                m_mtimeA(mtimeA),
                m_mtimeB(mtimeB),
                m_syncOp(syncOp),
                m_result(false)
{
    // nothing to do
}

void SynchronizeTask::run()
{
    switch (m_syncOp)
    {
        case SyncOp::NONE:
            notifyFinished();
            break;

        case SyncOp::COPY_A_TO_B_WITH_BACKUP:
            requestSafeRename(m_serverIdB, m_shareIdB, m_path);
            break;

        case SyncOp::COPY_B_TO_A_WITH_BACKUP:
            requestSafeRename(m_serverIdA, m_shareIdA, m_path);
            break;

        case SyncOp::COPY_A_TO_B:
            requestCopy(m_serverIdA, m_shareIdA, m_serverIdB, m_shareIdB, m_path, m_mtimeA);
            break;

        case SyncOp::COPY_B_TO_A:
            requestCopy(m_serverIdB, m_shareIdB, m_serverIdA, m_shareIdA, m_path, m_mtimeB);
            break;

        case SyncOp::DELETE_A:
            requestDelete(m_serverIdA, m_shareIdA, m_path);
            break;

        case SyncOp::DELETE_B:
            requestDelete(m_serverIdB, m_shareIdB, m_path);
            break;

        default:
            Q_ASSERT(0);
            notifyFinished();
            break;
    }
}

void SynchronizeTask::requestSafeRename(
        const Commons::Uuid& serverId,
        const Commons::Uuid& shareId,
        const QString& path)
{
    // TODO: there shall be name negotiation between src & dst to avoid conflict
    //       between backup name and existing files
    m_safeRenameTask = new SafeRenameTask(getEngine(), serverId, shareId, path, this);
    connect(m_safeRenameTask.data(), &AsyncTask::finished, this,
            &SynchronizeTask::onSafeRenameFinished);
    m_safeRenameTask->startStandalone();
}

void SynchronizeTask::onSafeRenameFinished()
{
    Q_ASSERT(m_safeRenameTask);

    if (m_safeRenameTask->getResult())
    {
        const QString backupPath = m_safeRenameTask->getResultPath();
        Q_ASSERT(!backupPath.isEmpty());

        if (m_syncOp == SyncOp::COPY_A_TO_B_WITH_BACKUP)
        {
            requestBackupCopy(m_serverIdB, m_shareIdB, m_serverIdA, m_shareIdA,
                              backupPath, m_mtimeB);
        }
        else if (m_syncOp == SyncOp::COPY_B_TO_A_WITH_BACKUP)
        {
            requestBackupCopy(m_serverIdA, m_shareIdA, m_serverIdB, m_shareIdB,
                              backupPath, m_mtimeA);
        }
        else
        {
            Q_ASSERT(0);
        }
    }
    else
    {
        // notify about failure
        notifyFinished();
    }
}

void SynchronizeTask::requestBackupCopy(
        const Commons::Uuid& srcServerId,
        const Commons::Uuid& srcShareId,
        const Commons::Uuid& dstServerId,
        const Commons::Uuid& dstShareId,
        const QString& path,
        const qint64 mtime)
{
    m_backupCopyTask = new CopyTask(getEngine(), srcServerId, srcShareId, dstServerId, dstShareId,
            path, mtime, this);
    connect(m_backupCopyTask.data(), &AsyncTask::finished, this,
            &SynchronizeTask::onBackupCopyFinished);
    m_backupCopyTask->startStandalone();
}

void SynchronizeTask::onBackupCopyFinished()
{
    Q_ASSERT(m_backupCopyTask);

    if (m_backupCopyTask->getResult())
    {
        if (m_syncOp == SyncOp::COPY_A_TO_B_WITH_BACKUP)
        {
            requestCopy(m_serverIdA, m_shareIdA, m_serverIdB, m_shareIdB, m_path, m_mtimeA);
        }
        else if (m_syncOp == SyncOp::COPY_B_TO_A_WITH_BACKUP)
        {
            requestCopy(m_serverIdB, m_shareIdB, m_serverIdA, m_shareIdA, m_path, m_mtimeB);
        }
        else
        {
            Q_ASSERT(0);
        }
    }
    else
    {
        // notify about failure
        notifyFinished();
    }
}

void SynchronizeTask::requestDelete(
        const Commons::Uuid& serverId,
        const Commons::Uuid& shareId,
        const QString& path)
{
    m_deleteTask = new DeleteTask(getEngine(), serverId, shareId, path, this);
    connect(m_deleteTask.data(), &AsyncTask::finished, this, &SynchronizeTask::onDeleteFinished);
    m_deleteTask->startStandalone();
}

void SynchronizeTask::onDeleteFinished()
{
    Q_ASSERT(m_deleteTask);

    if (m_deleteTask->getResult())
    {
        m_result = true;
    }
    notifyFinished();
}

void SynchronizeTask::requestCopy(
        const Commons::Uuid& srcServerId,
        const Commons::Uuid& srcShareId,
        const Commons::Uuid& dstServerId,
        const Commons::Uuid& dstShareId,
        const QString& path,
        const qint64 mtime)
{
    m_copyTask = new CopyTask(getEngine(), srcServerId, srcShareId, dstServerId, dstShareId, path,
                              mtime, this);
    connect(m_copyTask.data(), &AsyncTask::finished, this, &SynchronizeTask::onCopyFinished);
    m_copyTask->startStandalone();
}

void SynchronizeTask::onCopyFinished()
{
    Q_ASSERT(m_copyTask);

    if (m_copyTask->getResult())
    {
        m_result = true;
    }
    notifyFinished();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
