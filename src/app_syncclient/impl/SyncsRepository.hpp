//-----------------------------------------------------------------------------
/// \file
/// SyncsRepository - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncsRepository
#define INCLUDED_OCTsync_SyncsRepository

//-----------------------------------------------------------------------------

// local includes
#include "SyncPoint.hpp"

// library includes
#include <QtCore/QMap>
#include <QtCore/QList>
#include <QtNetwork/QTcpSocket>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncsRepository
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QMap<Commons::Uuid, SyncPoint> SyncsCollection;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    SyncsCollection m_syncs;

// Methods (public, protected, private)
public:
    SyncsRepository();

    int getSyncsCount() const
    {
        return m_syncs.size();
    }

    bool hasSync(
            const Commons::Uuid& uuid) const
    {
        return m_syncs.contains(uuid);
    }

    SyncPoint getSync(
            const Commons::Uuid& uuid) const
    {
        return m_syncs.value(uuid);
    }

    QList<Commons::Uuid> getSyncIds() const
    {
        return m_syncs.keys();
    }

    bool addSync(
            const SyncPoint& sync);

    bool modifySync(
            const SyncPoint& sync);

    bool removeSync(
            const SyncPoint& sync);

    void removeAllSyncs();
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncsRepository*/
