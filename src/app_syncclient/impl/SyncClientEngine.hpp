//-----------------------------------------------------------------------------
/// \file
/// SyncClientEngine - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_AppSyncClient_SyncClientEngine
#define INCLUDED_OCTsync_AppSyncClient_SyncClientEngine

//-----------------------------------------------------------------------------

// local includes
#include "ServerDevice.hpp"
#include "SyncContext.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QAbstractItemModel>
#include <QtCore/QSettings>
#include <ldp/Monitor.hpp>
#include <boprpc/Client.hpp>
#include <commons/Map.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncClient
{

// forward references in namespace
class ShareTreeModel;
class SyncPointsData;
class SyncPointListModel;

/// TODO: Class documentation
///
class SyncClientEngine : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const QString ORGANIZATION_NAME;
    static const QString APPLICATION_NAME;
    static const QString OPTION_NAME_SERVER_CFGFILE;

// Types (public, protected, private)
public:
    enum Role : int
    {
        ROLE_ITEM_TYPE = Qt::UserRole,
        ROLE_SERVER_ID,
        ROLE_SHARE_ID,
        ROLE_SYNC_ID
    };

    enum ItemType : int
    {
        ITEM_TYPE_INVALID = 0,
        ITEM_TYPE_SERVER,
        ITEM_TYPE_SHARE
    };

private:
    typedef Commons::Map<Commons::Uuid, ServerDevice*> ServerDeviceMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Ldp::Client m_ldpClient;
    Ldp::Monitor* m_ldpMonitor;
    BopRpc::Client* m_rpcClient;

    ServerDeviceMap m_serverDevices;

    ShareTreeModel* m_shareTreeModel;

    SyncPointListModel* m_syncPointListModel;

    SyncPointsData* m_syncData;

    QString m_configFileName;

// Methods (public, protected, private)
public:
    SyncClientEngine(
            QObject* parent = nullptr);

    void setTarget(
            BopRpc::Executor* executor,
            const Commons::Uuid& serverId,
            const bool syncCall);

    void processArguments(
            QStringList arguments);

    void start();

    QAbstractItemModel* getShareTreeModel() const;

    QAbstractItemModel* getSyncListModel() const;

    SyncContext* createSyncContext(
            const SyncPoint& syncPoint);

    void addSyncPoint(
            const SyncPoint& syncPoint);

    void removeSyncPoint(
            const SyncPoint& syncPoint);

    SyncPoint getSyncPoint(
            const Commons::Uuid& syncId);

    ServerDevice* getServer(
            const Commons::Uuid& serverId) const;

    QString getServerName(
            const Commons::Uuid& serverId) const;

    QString getShareName(
            const Commons::Uuid& serverId,
            const Commons::Uuid& shareId) const;

private:
    void addValidDevice(
            ServerDevice* const device);

    void removeValidDevice(
            ServerDevice* const device);

    void onLdpDeviceAdded(
            Ldp::Device ldpDevice);

    void onLdpDeviceRemoved(
            Ldp::Device ldpDevice);

    void onDeviceStateChanged(
            QPointer<ServerDevice> device,
            ServerDevice::State state);

    void onSyncPointsDataChanged();

    std::unique_ptr<QSettings> createSettingsObject();
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_AppSyncClient_SyncClientEngine*/
