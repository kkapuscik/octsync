//-----------------------------------------------------------------------------
/// \file
/// SyncError - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncError
#define INCLUDED_OCTsync_SyncError

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <boprpc/RpcError.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// Error codes for Sync actions.
///
class SyncError : public BopRpc::RpcError
{
private:
    // forbid creating instances
    SyncError();

public:
    enum
    {
        /// Invalid share identifier given as argument.
        INVALID_SHARE_ID = SERVICE_SPECIFIC_BASE,
        BAD_HANDLE,
        ITEM_NOT_FOUND,
        ACCESS_DENIED,
        READ_ERROR,
        WRITE_ERROR
    };
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncError*/
