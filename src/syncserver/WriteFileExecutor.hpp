//-----------------------------------------------------------------------------
/// \file
/// WriteFileExecutor - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_WriteFileExecutor
#define INCLUDED_OCTsync_WriteFileExecutor

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <boprpc/Executor.hpp>
#include <boprpc/BadArgumentException.hpp>
#include <QtCore/QString>
#include <QtCore/QByteArray>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class WriteFileExecutor : public BopRpc::Executor
{
Q_OBJECT

// Static constants (public, protected, private)
public:
    static const QString COMMAND_NAME;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    WriteFileExecutor(
            QObject* parent = nullptr);

    void setFileId(
            const quint32 fileId);

    void setData(
            const QByteArray& data);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_WriteFileExecutor*/
