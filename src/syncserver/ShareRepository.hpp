//-----------------------------------------------------------------------------
/// \file
/// ShareRepository - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ShareRepository
#define INCLUDED_OCTsync_ShareRepository

//-----------------------------------------------------------------------------

// local includes
#include "Share.hpp"

// library includes
#include <QtCore/QMap>
#include <QtCore/QList>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ShareRepository
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QMap<Commons::Uuid, Share> ShareCollection;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    ShareCollection m_shares;

// Methods (public, protected, private)
public:
    ShareRepository();

    int getShareCount() const
    {
        return m_shares.size();
    }

    bool hasShare(
            const Commons::Uuid& uuid) const
    {
        return m_shares.contains(uuid);
    }

    Share getShare(
            const Commons::Uuid& uuid) const
    {
        return m_shares.value(uuid);
    }

    QList<Commons::Uuid> getShareKeys() const
    {
        return m_shares.keys();
    }

    bool addShare(
            const Share& share);

    bool modifyShare(
            const Share& share);

    bool removeShare(
            const Commons::Uuid& uuid);

    void removeAllShares();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ShareRepository*/
