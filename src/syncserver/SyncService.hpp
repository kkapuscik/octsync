//-----------------------------------------------------------------------------
/// \file
/// SyncService - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncService
#define INCLUDED_OCTsync_SyncService

//-----------------------------------------------------------------------------

// local includes
#include "ServerData.hpp"

// library includes
#include <boprpc/Service.hpp>
#include <boprpc/ServiceCommand.hpp>
#include <QtCore/QPointer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncService : public BopRpc::Service
{
Q_OBJECT

    // Static constants (public, protected, private)
private:
    static const QString SERVICE_NAME;

    // Types (public, protected, private)
private:
    typedef BopRpc::TypedServiceCommand<SyncService> SvcCmd;

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QPointer<ServerData> m_data;

// Methods (public, protected, private)
public:
    SyncService(
            QObject* parent = nullptr);

    void setServerData(
            QPointer<ServerData> data);

private:
    void addCommand(
            const QString& name,
            SvcCmd::Callback callback);

    quint32 cmdGetShares(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdGetShareInfo(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdOpenDir(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdReadDir(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdCloseDir(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdDeleteFile(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdSafeRenameFile(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdOpenFile(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdCloseFile(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdReadFile(
            const BopRpc::Command& command,
            BopRpc::Result& result);

    quint32 cmdWriteFile(
            const BopRpc::Command& command,
            BopRpc::Result& result);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncService*/
