#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T19:34:22
#
#-------------------------------------------------

QT       -= gui

TARGET = syncserver
TEMPLATE = lib
CONFIG += staticlib c++11

HEADERS += \
    CloseDirExecutor.hpp \
    CloseFileExecutor.hpp \
    DeleteFileExecutor.hpp \
    GetShareInfoExecutor.hpp \
    GetSharesExecutor.hpp \
    namespace.hpp \
    OpenDirExecutor.hpp \
    OpenFileExecutor.hpp \
    ReadDirExecutor.hpp \
    ReadFileExecutor.hpp \
    SafeRenameFileExecutor.hpp \
    ServerData.hpp \
    Share.hpp \
    ShareRepository.hpp \
    SyncError.hpp \
    SyncService.hpp \
    WriteFileExecutor.hpp \
    impl/Directory.hpp \
    impl/File.hpp

OTHER_FILES += \
    CMakeLists.txt

SOURCES += \
    impl/CloseDirExecutor.cpp \
    impl/CloseFileExecutor.cpp \
    impl/DeleteFileExecutor.cpp \
    impl/Directory.cpp \
    impl/File.cpp \
    impl/GetShareInfoExecutor.cpp \
    impl/GetSharesExecutor.cpp \
    impl/OpenDirExecutor.cpp \
    impl/OpenFileExecutor.cpp \
    impl/ReadDirExecutor.cpp \
    impl/ReadFileExecutor.cpp \
    impl/SafeRenameFileExecutor.cpp \
    impl/ServerData.cpp \
    impl/Share.cpp \
    impl/ShareRepository.cpp \
    impl/SyncService.cpp \
    impl/WriteFileExecutor.cpp

INCLUDEPATH += $$PWD/..
