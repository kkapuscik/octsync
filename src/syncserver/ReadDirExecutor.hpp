//-----------------------------------------------------------------------------
/// \file
/// ReadDirExecutor - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ReadDirExecutor
#define INCLUDED_OCTsync_ReadDirExecutor

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <boprpc/Executor.hpp>
#include <boprpc/BadArgumentException.hpp>
#include <QtCore/QDateTime>
#include <QtCore/QString>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ReadDirExecutor : public BopRpc::Executor
{
Q_OBJECT

// Static constants (public, protected, private)
public:
    static const QString COMMAND_NAME;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    ReadDirExecutor(
            QObject* parent = nullptr);

    void setDirId(
            const quint32 dirId);

    void setOffset(
            const quint32 offset);

    void setMaxCount(
            const quint32 maxCount);

    quint32 getCount() const
            throw (BopRpc::BadArgumentException);

    quint32 getTotal() const
            throw (BopRpc::BadArgumentException);

    QString getItemName(
            const int index) const
                    throw (BopRpc::BadArgumentException);

    qint64 getItemSize(
            const int index) const
                    throw (BopRpc::BadArgumentException);

    QString getItemType(
            const int index) const
                    throw (BopRpc::BadArgumentException);

    QDateTime getItemMTime(
            const int index) const
                    throw (BopRpc::BadArgumentException);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ReadDirExecutor*/
