//-----------------------------------------------------------------------------
/// \file
/// File - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "File.hpp"

// library includes
#ifdef __linux__
    #include <sys/time.h>
#else
#include <sys/utime.h>
#include <time.h>
#endif
#include <QtCore/QDebug>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

File::File(
        const quint32 handle,
        const QString& path,
        const QString& mode,
        const qint64 mtime) :
                m_handle(handle),
                m_path(path),
                m_file(path),
                m_mode(Mode::INVALID),
                m_mtime(mtime)
{
    if (mode == "Read")
    {
        if (m_file.open(QIODevice::ReadOnly))
        {
            m_mode = Mode::READ;
        }
    }
    else if (mode == "Write")
    {
        if (m_file.open(QIODevice::WriteOnly | QIODevice::Truncate))
        {
            m_mode = Mode::WRITE;
        }
    }
    else
    {
        Q_ASSERT(0);
    }
}

File::~File()
{
    invalidate();
}

bool File::isValid() const
{
    return m_mode != Mode::INVALID;
}

bool File::seek(
        const qint64 pos)
{
    bool result = false;

    if (isValid())
    {
        if (m_file.seek(pos))
        {
            result = true;
        }
        else
        {
            invalidate();
        }
    }

    return result;
}

bool File::read(
        QByteArray& data,
        const quint32 maxSize)
{
    bool result = false;

    if (isValid() && (m_mode == Mode::READ))
    {
        data = m_file.read(maxSize);
        if (m_file.error() == QFileDevice::NoError)
        {
            result = true;
        }
        else
        {
            invalidate();
        }
    }

    return result;
}

bool File::write(
        const QByteArray& data)
{
    bool result = false;

    if (isValid() && (m_mode == Mode::WRITE))
    {
        (void) m_file.write(data);
        if (m_file.error() == QFileDevice::NoError)
        {
            result = true;
        }
        else
        {
            invalidate();
        }
    }

    return result;
}

void File::invalidate()
{
    if (m_mode != Mode::INVALID)
    {
        m_file.close();

        if (m_mode == Mode::WRITE)
        {
#ifdef __linux__
            struct timeval tv[2];

            tv[0].tv_sec = m_mtime / 1000;
            tv[0].tv_usec = (m_mtime % 1000) * 1000;
            tv[1] = tv[0];

            // ignore result?
            int result = ::utimes(m_path.toUtf8().constData(), tv);

            qDebug() << "File " << m_path << " setMtime(" << m_mtime << ") => " << result;
#else
            time_t mytime = m_mtime / 1000;

            struct _utimbuf timebuf;

            timebuf.actime = mytime;
            timebuf.modtime = mytime;

            _utime(m_path.toUtf8().constData(), &timebuf );
#if 0
            if (hfile = OpenFile(strFile2Touch, &info, OF_WRITE) != -1) {
                 printf("File Open..Setting Time\n");
                 result=SetFileTime((HANDLE)hfile, (LPFILETIME)
     &ft,LPFILETIME)&ft, LPFILETIME)&ft);
               if (result)
               reportError("Time Stamp Error");
                    else
               printf("Time Stamp Error\n");
            }

          CloseHandle((HANDLE) hfile);
            f = SetFileTime(hFile,           // Sets last-write time of the file
                (LPFILETIME) NULL,           // to the converted current system time
                (LPFILETIME) NULL,
                &ft);
#endif

#endif
        }

        m_mode = Mode::INVALID;
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
