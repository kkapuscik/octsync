//-----------------------------------------------------------------------------
/// \file
/// OpenDirExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "OpenDirExecutor.hpp"

// library includes
#include <boprpc/TypeMismatchException.hpp>
#include <boprpc/BadArgumentException.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::TypeMismatchException;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString OpenDirExecutor::COMMAND_NAME("OpenDir");

OpenDirExecutor::OpenDirExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void OpenDirExecutor::setShareId(
        const Commons::Uuid& shareId)
{
    getCommand().setArgument("ShareId", shareId);
}

void OpenDirExecutor::setPath(
        const QString& path)
{
    getCommand().setArgument("Path", path);
}

quint32 OpenDirExecutor::getDirId() const
        throw (BopRpc::BadArgumentException)
{
    try
    {
        return getResult().getArgument("DirId").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: DirId");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
