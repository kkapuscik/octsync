//-----------------------------------------------------------------------------
/// \file
/// ServerData - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../ServerData.hpp"

// library includes
#include <QtCore/QDir>
#include <QtCore/QDebug>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

ServerData::ServerData(
        QObject* parent) :
                QObject(parent),
                m_nextHandle(0),
                m_serverSecure(false),
                m_serverEnabled(false)
{
    setField("serverUuid", m_serverUuid, Commons::Uuid::generate(), false);
}

Commons::Uuid ServerData::getServerUuid() const
{
    qDebug() << "getServerUuid()" << m_serverUuid.toString();

    return m_serverUuid;
}

void ServerData::setServerUuid(
        const Commons::Uuid& uuid)
{
    setField("serverUuid", m_serverUuid, uuid, true);
}

QString ServerData::getSllCertificatePath() const
{
    qDebug() << "getSllCertificatePath()" << m_serverSslCertPath;

    return m_serverSslCertPath;
}

void ServerData::setSslCertificatePath(
        const QString& path)
{
    setField("serverSslCert", m_serverSslCertPath, path, true);
}

QString ServerData::getSslKeyPath() const
{
    qDebug() << "getSslKeyPath()" << m_serverSslKeyPath;

    return m_serverSslKeyPath;
}

void ServerData::setSslKeyPath(
        const QString& path)
{
    setField("serverSslKey", m_serverSslKeyPath, path, true);
}

bool ServerData::isServerEnabled() const
{
    qDebug() << "isServerEnabled()" << m_serverEnabled;

    return m_serverEnabled;
}

void ServerData::setServerEnabled(
        const bool enabled)
{
    setField("serverEnabled", m_serverEnabled, enabled, true);
}

bool ServerData::isServerSecure() const
{
    qDebug() << "isServerSecure()" << m_serverSecure;

    return m_serverSecure;
}

void ServerData::setServerSecure(
        const bool secure)
{
    setField("serverSecure", m_serverSecure, secure, true);
}

bool ServerData::load(
        QSettings& settings)
{
    // remove all shares
    m_shares.removeAllShares();

    // try to read the UUID
    settings.beginGroup("server");
    const auto serverIdStr = settings.value("uuid").toString();
    const auto serverUuid = Commons::Uuid::parse(serverIdStr);
    if (!serverUuid.isValid())
    {
        emit changed();
        return false;
    }
    setField("serverUuid", m_serverUuid, serverUuid, false);
    setField("serverSecure", m_serverSecure, settings.value("secure", false).toBool(), false);
    setField("serverEnabled", m_serverEnabled, settings.value("enabled", false).toBool(), false);
    setField("serverSslCert", m_serverSslCertPath, settings.value("sslcert", QString()).toString(), false);
    setField("serverSslKey",  m_serverSslKeyPath, settings.value("sslkey", QString()).toString(), false);
    settings.endGroup();

    // and try to load these stored
    settings.beginGroup("data");
    const int size = settings.beginReadArray("shares");
    for (int i = 0; i < size; ++i)
    {
        settings.setArrayIndex(i);

        Share loadedShare;

        loadedShare.setUuid(settings.value("uuid").toUuid());
        loadedShare.setName(settings.value("name").toString());
        loadedShare.setDescription(settings.value("description").toString());
        loadedShare.setRootDir(QDir(settings.value("directory").toString()));
        loadedShare.setReadOnly(settings.value("readonly").toBool());

        if (!loadedShare.isValid())
        {
            emit changed();
            return false;
        }

        m_shares.addShare(loadedShare);
    }
    settings.endArray();
    settings.endGroup();

    emit changed();
    return true;
}

bool ServerData::save(
        QSettings& settings)
{
    if (!settings.isWritable())
    {
        return false;
    }

    // try to read the UUID
    settings.beginGroup("server");
    settings.setValue("uuid", getServerUuid().toString());
    settings.setValue("secure", m_serverSecure);
    settings.setValue("enabled", m_serverEnabled);
    settings.setValue("sslcert", m_serverSslCertPath);
    settings.setValue("sslkey", m_serverSslKeyPath);
    settings.endGroup();

    // get all shares (identifiers)
    QList<Commons::Uuid> sharesIds = m_shares.getShareKeys();

    // and try to load these stored
    settings.beginGroup("data");
    settings.beginWriteArray("shares", sharesIds.length());
    for (int i = 0; i < sharesIds.length(); ++i)
    {
        settings.setArrayIndex(i);

        const Share share = m_shares.getShare(sharesIds.at(i));

        settings.setValue("uuid", share.getUuid().toString());
        settings.setValue("name", share.getName());
        settings.setValue("description", share.getDescription());
        settings.setValue("directory", share.getRootDir().path());
        settings.setValue("readonly", share.isReadOnly());
    }
    settings.endArray();
    settings.endGroup();
    settings.sync();

    return true;
}

Share ServerData::getShare(
        const Commons::Uuid& uuid) const
{
    return m_shares.getShare(uuid);
}

void ServerData::addShare(
        const Share& share)
{
    if (m_shares.addShare(share))
    {
        emit changed();
    }
}

void ServerData::modifyShare(
        const Share& share)
{
    if (m_shares.modifyShare(share))
    {
        emit changed();
    }
}

void ServerData::removeShare(
        const Commons::Uuid& uuid)
{
    if (m_shares.removeShare(uuid))
    {
        emit changed();
    }
}

void ServerData::removeAllShares()
{
    m_shares.removeAllShares();

    emit changed();
}

quint32 ServerData::getNextHandle()
{
    quint32 handle = 0;

    for (;;)
    {
        handle = m_nextHandle++;

        if (m_directories.find(handle) != m_directories.end())
        {
            continue;
        }

        if (m_files.find(handle) != m_files.end())
        {
            continue;
        }

        break;
    }

    return handle;
}

ServerData::DirectoryConstPtr ServerData::openDirectory(
        const Commons::Uuid& shareUuid,
        const QString& path)
{
    const Share share = m_shares.getShare(shareUuid);
    if (!share.isValid())
    {
        return nullptr;
    }

    const QFileInfo fileInfo(share.getRootDir().path() + "/" + path);

    qDebug() << "openDirectory() " << fileInfo.filePath();

    if (!fileInfo.isDir())
    {
        return nullptr;
    }

    const QDir dir(fileInfo.filePath());

    qDebug() << "openDirectory() " << dir.path();

    const QFileInfoList fileInfos = dir.entryInfoList(
            QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot, QDir::Name);

    DirectoryPtr newDirectory(new Directory(getNextHandle(), dir, fileInfos));

    m_directories.insert(std::make_pair(newDirectory->getHandle(), newDirectory));

    return newDirectory;
}

ServerData::DirectoryConstPtr ServerData::getDirectory(
        const quint32 dirId)
{
    DirectoryMap::iterator iter = m_directories.find(dirId);
    if (iter != m_directories.end())
    {
        return iter->second;
    }
    else
    {
        return nullptr;
    }
}

bool ServerData::closeDirectory(
        const quint32 dirId)
{
    DirectoryMap::iterator iter = m_directories.find(dirId);
    if (iter != m_directories.end())
    {
        m_directories.erase(iter);
        return true;
    }
    else
    {
        return false;
    }
}

QString ServerData::getSafePath(
        const Commons::Uuid& shareUuid,
        const QString& filePath) const
{
    const Share share = m_shares.getShare(shareUuid);
    if (!share.isValid())
    {
        return nullptr;
    }

    Q_ASSERT(hasFile(shareUuid, filePath));

    const QString sharePath = share.getRootDir().path() + "/";

    const QFileInfo baseInfo(sharePath + filePath);
    const QString path = baseInfo.path();
    const QString baseName = baseInfo.baseName();
    QString suffix = baseInfo.completeSuffix();
    if (!suffix.isEmpty())
    {
        suffix = "." + suffix;
    }

    QString safePath;
    for (int i = 0;; ++i)
    {
        const QFileInfo safeInfo(QString("%1/%2_%3%4").arg(path).arg(baseName).arg(i).arg(suffix));

        if (!safeInfo.exists())
        {
            safePath = safeInfo.filePath();
            break;
        }
    }

    return safePath.mid(sharePath.length());
}

bool ServerData::hasFile(
        const Commons::Uuid& shareUuid,
        const QString& path) const
{
    const Share share = m_shares.getShare(shareUuid);
    if (!share.isValid())
    {
        return nullptr;
    }

    const QFileInfo fileInfo(share.getRootDir().path() + "/" + path);

    if (fileInfo.isFile() && !fileInfo.isSymLink())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool ServerData::deleteFile(
        const Commons::Uuid& shareUuid,
        const QString& path) const
{
    const Share share = m_shares.getShare(shareUuid);
    if (!share.isValid())
    {
        return nullptr;
    }

    Q_ASSERT(hasFile(shareUuid, path));

    return QFile::remove(share.getRootDir().path() + "/" + path);
}

bool ServerData::renameFile(
        const Commons::Uuid& shareUuid,
        const QString& oldName,
        const QString& newName) const
{
    const Share share = m_shares.getShare(shareUuid);
    if (!share.isValid())
    {
        return nullptr;
    }

    Q_ASSERT(hasFile(shareUuid, oldName));

    const QString oldPath = share.getRootDir().path() + "/" + oldName;
    const QString newPath = share.getRootDir().path() + "/" + newName;

    return QFile::rename(oldPath, newPath);
}

ServerData::FileConstPtr ServerData::openFile(
        const Commons::Uuid& shareUuid,
        const QString& path,
        const QString& mode,
        const qint64 mtime)
{
    const Share share = m_shares.getShare(shareUuid);
    if (!share.isValid())
    {
        return nullptr;
    }

    if ((mode != "Read") && (mode != "Write"))
    {
        return nullptr;
    }

    const QFileInfo fileInfo(share.getRootDir().path() + "/" + path);

    qDebug() << "openFile() " << fileInfo.filePath();

    if (fileInfo.exists() && !fileInfo.isFile())
    {
        return nullptr;
    }

    // TODO: hardcore zapaua
    fileInfo.dir().mkpath(".");

    FilePtr newFile(new File(getNextHandle(), fileInfo.filePath(), mode, mtime));
    if (!newFile->isValid())
    {
        return nullptr;
    }

    qDebug() << "openFile() " << fileInfo.filePath();

    m_files.insert(std::make_pair(newFile->getHandle(), newFile));

    return newFile;
}

bool ServerData::closeFile(
        const quint32 fileId)
{
    FileMap::iterator iter = m_files.find(fileId);
    if (iter != m_files.end())
    {
        m_files.erase(iter);
        return true;
    }
    else
    {
        return false;
    }
}

ServerData::FilePtr ServerData::getFile(
        const quint32 fileId)
{
    FileMap::iterator iter = m_files.find(fileId);
    if (iter != m_files.end())
    {
        return iter->second;
    }
    else
    {
        return nullptr;
    }
}

template <class T>
void ServerData::setField(
        const QString& fieldName,
        T& field,
        const T& value,
        const bool emitChange)
{
    if (field != value)
    {
        field = value;

        qDebug() << "setField(" << fieldName << "," << value << ")";

        if (emitChange)
        {
            emit changed();
        }
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
