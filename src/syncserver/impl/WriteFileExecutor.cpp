//-----------------------------------------------------------------------------
/// \file
/// WriteFileExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "WriteFileExecutor.hpp"

// library includes
#include <boprpc/TypeMismatchException.hpp>
#include <boprpc/BadArgumentException.hpp>
#include <boprpc/Variant.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::TypeMismatchException;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString WriteFileExecutor::COMMAND_NAME("WriteFile");

WriteFileExecutor::WriteFileExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void WriteFileExecutor::setFileId(
        const quint32 dirId)
{
    getCommand().setArgument("FileId", dirId);
}

void WriteFileExecutor::setData(
        const QByteArray& data)
{
    getCommand().setArgument("Data", data);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
