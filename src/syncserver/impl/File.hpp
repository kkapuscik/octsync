//-----------------------------------------------------------------------------
/// \file
/// File - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_File
#define INCLUDED_OCTsync_File

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QFile>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class File
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    enum class Mode
    {
        READ,
        WRITE,
        INVALID
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    quint32 m_handle;
    QString m_path;
    QFile m_file;
    Mode m_mode;
    qint64 m_mtime;

// Methods (public, protected, private)
public:
    File(
            const quint32 handle,
            const QString& path,
            const QString& mode,
            const qint64 mtime);

    ~File();

    bool isValid() const;

    quint32 getHandle() const
    {
        return m_handle;
    }

    bool seek(const qint64 pos);

    bool read(
            QByteArray& data,
            const quint32 maxSize);

    bool write(
            const QByteArray& data);

private:
    void invalidate();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_File*/
