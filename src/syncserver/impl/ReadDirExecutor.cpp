//-----------------------------------------------------------------------------
/// \file
/// ReadDirExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ReadDirExecutor.hpp"

// library includes
#include <boprpc/TypeMismatchException.hpp>
#include <boprpc/BadArgumentException.hpp>
#include <boprpc/Variant.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::TypeMismatchException;
using OCTsync::BopRpc::BadArgumentException;
using OCTsync::BopRpc::VariantArray;
using OCTsync::BopRpc::VariantMap;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString ReadDirExecutor::COMMAND_NAME("ReadDir");

ReadDirExecutor::ReadDirExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void ReadDirExecutor::setDirId(
        const quint32 dirId)
{
    getCommand().setArgument("DirId", dirId);
}

void ReadDirExecutor::setOffset(
        const quint32 offset)
{
    getCommand().setArgument("Offset", offset);
}

void ReadDirExecutor::setMaxCount(
        const quint32 maxCount)
{
    getCommand().setArgument("MaxCount", maxCount);
}

quint32 ReadDirExecutor::getCount() const
        throw (BadArgumentException)
{
    try
    {
        return getResult().getArgument("Count").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Count");
    }
}

quint32 ReadDirExecutor::getTotal() const
        throw (BadArgumentException)
{
    try
    {
        return getResult().getArgument("Total").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Count");
    }
}

QString ReadDirExecutor::getItemName(
        const int index) const
                throw (BopRpc::BadArgumentException)
{
    try
    {
        const VariantArray infos = getResult().getArgument("Infos").getVariantArray();
        if (index < 0 || index >= infos.size())
        {
            throw BadArgumentException("Invalid index");
        }

        const VariantMap info = infos.at(index).getVariantMap();

        return info.value("Name").getString();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Infos");
    }
}

qint64 ReadDirExecutor::getItemSize(
        const int index) const
                throw (BopRpc::BadArgumentException)
{
    try
    {
        const VariantArray infos = getResult().getArgument("Infos").getVariantArray();
        if (index < 0 || index >= infos.size())
        {
            throw BadArgumentException("Invalid index");
        }

        const VariantMap info = infos.at(index).getVariantMap();

        return info.value("Size").getInt64();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Infos");
    }
}

QString ReadDirExecutor::getItemType(
        const int index) const
                throw (BopRpc::BadArgumentException)
{
    try
    {
        const VariantArray infos = getResult().getArgument("Infos").getVariantArray();
        if (index < 0 || index >= infos.size())
        {
            throw BadArgumentException("Invalid index");
        }

        const VariantMap info = infos.at(index).getVariantMap();

        return info.value("Type").getString();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Infos");
    }
}

QDateTime ReadDirExecutor::getItemMTime(
        const int index) const
                throw (BopRpc::BadArgumentException)
{
    try
    {
        const VariantArray infos = getResult().getArgument("Infos").getVariantArray();
        if (index < 0 || index >= infos.size())
        {
            throw BadArgumentException("Invalid index");
        }

        const VariantMap info = infos.at(index).getVariantMap();

        QDateTime date;
        date.setMSecsSinceEpoch(info.value("MTime").getInt64());
        return date;
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Infos");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
