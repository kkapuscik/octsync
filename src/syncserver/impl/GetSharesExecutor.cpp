//-----------------------------------------------------------------------------
/// \file
/// GetSharesExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "GetSharesExecutor.hpp"

// library includes
#include <commons/Uuid.hpp>
#include <boprpc/Executor.hpp>
#include <boprpc/BadArgumentException.hpp>
#include <boprpc/Variant.hpp>
#include <boprpc/TypeMismatchException.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::Executor;
using OCTsync::BopRpc::BadArgumentException;
using OCTsync::Commons::Uuid;
using OCTsync::BopRpc::Variant;
using OCTsync::BopRpc::TypeMismatchException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString GetSharesExecutor::COMMAND_NAME("GetShares");

GetSharesExecutor::GetSharesExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

QList<Uuid> GetSharesExecutor::getShareIds() const
        throw (BadArgumentException)
{
    try
    {
        QList<Uuid> result;

        const BopRpc::VariantArray commands = getResult().getArgument("ShareIds").getVariantArray();
        for (const Variant& value : commands)
        {
            result.append(value.getUuid());
        }

        return result;
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: ShareIds");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
