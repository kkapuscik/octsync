//-----------------------------------------------------------------------------
/// \file
/// Share - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Share.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

Share::Share() :
                m_rootDir(QDir::home().path() + QDir::separator() + "new-share"),
                m_readOnly(false)
{
    // nothing to do
}

bool Share::isValid() const
{
    return m_uuid.isValid() && !m_name.isEmpty() && !m_rootDir.absolutePath().isEmpty();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
