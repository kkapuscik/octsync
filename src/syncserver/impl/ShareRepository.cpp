//-----------------------------------------------------------------------------
/// \file
/// ShareRepository - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ShareRepository.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

ShareRepository::ShareRepository()
{
    // nothing to do
}

bool ShareRepository::addShare(
        const Share& share)
{
    if (!share.isValid())
    {
        return false;
    }

    if (hasShare(share.getUuid()))
    {
        return false;
    }

    m_shares.insert(share.getUuid(), share);

    return true;
}

bool ShareRepository::modifyShare(
        const Share& share)
{
    if (!share.isValid())
    {
        return false;
    }

    if (!hasShare(share.getUuid()))
    {
        return false;
    }

    m_shares.insert(share.getUuid(), share);

    return true;
}

bool ShareRepository::removeShare(
        const Commons::Uuid& uuid)
{
    if (hasShare(uuid))
    {
        m_shares.remove(uuid);
        return true;
    }
    else
    {
        return false;
    }
}

void ShareRepository::removeAllShares()
{
    m_shares.clear();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
