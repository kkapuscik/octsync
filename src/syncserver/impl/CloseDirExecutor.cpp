//-----------------------------------------------------------------------------
/// \file
/// CloseDirExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "CloseDirExecutor.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString CloseDirExecutor::COMMAND_NAME("CloseDir");

CloseDirExecutor::CloseDirExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void CloseDirExecutor::setDirId(
        const quint32 dirId)
{
    getCommand().setArgument("DirId", dirId);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
