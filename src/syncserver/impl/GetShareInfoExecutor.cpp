//-----------------------------------------------------------------------------
/// \file
/// GetShareInfoExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "GetShareInfoExecutor.hpp"

// library includes
#include <boprpc/Executor.hpp>
#include <boprpc/TypeMismatchException.hpp>
#include <boprpc/BadArgumentException.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::Executor;
using OCTsync::BopRpc::TypeMismatchException;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString GetShareInfoExecutor::COMMAND_NAME("GetShareInfo");

GetShareInfoExecutor::GetShareInfoExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void GetShareInfoExecutor::setShareId(
        const Commons::Uuid& shareId)
{
    getCommand().setArgument("ShareId", shareId);
}

QString GetShareInfoExecutor::getShareName() const
        throw (BopRpc::BadArgumentException)
{
    try
    {
        return getResult().getArgument("Name").getString();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Name");
    }
}

QString GetShareInfoExecutor::getShareDescription() const
        throw (BopRpc::BadArgumentException)
{
    try
    {
        return getResult().getArgument("Description").getString();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Description");
    }
}

bool GetShareInfoExecutor::isShareReadOnly() const
        throw (BopRpc::BadArgumentException)
{
    try
    {
        return getResult().getArgument("ReadOnly").getBool();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: ReadOnly");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
