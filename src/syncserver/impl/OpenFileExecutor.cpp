//-----------------------------------------------------------------------------
/// \file
/// OpenFileExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "OpenFileExecutor.hpp"

// library includes
#include <boprpc/TypeMismatchException.hpp>
#include <boprpc/BadArgumentException.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::TypeMismatchException;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString OpenFileExecutor::COMMAND_NAME("OpenFile");

OpenFileExecutor::OpenFileExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void OpenFileExecutor::setShareId(
        const Commons::Uuid& shareId)
{
    getCommand().setArgument("ShareId", shareId);
}

void OpenFileExecutor::setPath(
        const QString& path)
{
    getCommand().setArgument("Path", path);
}

void OpenFileExecutor::setMode(
        const QString& mode)
{
    getCommand().setArgument("Mode", mode);
}

void OpenFileExecutor::setMTime(
        const qint64 mtime)
{
    getCommand().setArgument("MTime", mtime);
}

quint32 OpenFileExecutor::getFileId() const
        throw (BopRpc::BadArgumentException)
{
    try
    {
        return getResult().getArgument("FileId").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: FileId");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
