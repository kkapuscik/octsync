//-----------------------------------------------------------------------------
/// \file
/// ReadFileExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ReadFileExecutor.hpp"

// library includes
#include <boprpc/TypeMismatchException.hpp>
#include <boprpc/BadArgumentException.hpp>
#include <boprpc/Variant.hpp>

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::TypeMismatchException;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString ReadFileExecutor::COMMAND_NAME("ReadFile");

ReadFileExecutor::ReadFileExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void ReadFileExecutor::setFileId(
        const quint32 dirId)
{
    getCommand().setArgument("FileId", dirId);
}

void ReadFileExecutor::setMaxSize(
        const quint32 maxBytes)
{
    getCommand().setArgument("MaxSize", maxBytes);
}

QByteArray ReadFileExecutor::getData() const
        throw (BadArgumentException)
{
    try
    {
        return getResult().getArgument("Data").getByteArray();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Data");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
