//-----------------------------------------------------------------------------
/// \file
/// Directory - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Directory
#define INCLUDED_OCTsync_Directory

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QtGlobal>
#include <QtCore/QDir>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Directory
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    quint32 m_handle;
    QDir m_dir;
    QFileInfoList m_fileInfos;

// Methods (public, protected, private)
public:
    Directory(
            const quint32 handle,
            const QDir& dir,
            const QFileInfoList& fileInfos);

    quint32 getHandle() const
    {
        return m_handle;
    }

    const QDir& getDir() const
    {
        return m_dir;
    }

    const QFileInfoList& getFileInfos() const
    {
        return m_fileInfos;
    }
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Directory*/
