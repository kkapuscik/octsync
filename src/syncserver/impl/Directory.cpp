//-----------------------------------------------------------------------------
/// \file
/// Directory - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Directory.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

Directory::Directory(
        const quint32 handle,
        const QDir& dir,
        const QFileInfoList& fileInfos) :
                m_handle(handle),
                m_dir(dir),
                m_fileInfos(fileInfos)
{
    // nothing to do
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
