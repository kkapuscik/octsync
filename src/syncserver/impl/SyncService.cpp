//-----------------------------------------------------------------------------
/// \file
/// SyncService - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../SyncService.hpp"
#include "../SyncError.hpp"

// library includes
#include <QtCore/QDateTime>
#include <boprpc/RpcError.hpp>
#include <boprpc/TypeMismatchException.hpp>
#include <boprpc/RpcError.hpp>

// system includes
// (none)

// using clauses
using OCTsync::Commons::Uuid;
using OCTsync::BopRpc::TypeMismatchException;
using OCTsync::BopRpc::RpcError;
using OCTsync::BopRpc::VariantArray;
using OCTsync::BopRpc::Command;
using OCTsync::BopRpc::Result;
using OCTsync::BopRpc::VariantMap;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString SyncService::SERVICE_NAME("Sync");

SyncService::SyncService(
        QObject* parent) :
                BopRpc::Service(SERVICE_NAME, parent)
{
    addCommand("GetShares", &SyncService::cmdGetShares);
    addCommand("GetShareInfo", &SyncService::cmdGetShareInfo);

    addCommand("OpenDir", &SyncService::cmdOpenDir);
    addCommand("ReadDir", &SyncService::cmdReadDir);
    addCommand("CloseDir", &SyncService::cmdCloseDir);

    addCommand("DeleteFile", &SyncService::cmdDeleteFile);
    addCommand("SafeRenameFile", &SyncService::cmdSafeRenameFile);

    addCommand("OpenFile", &SyncService::cmdOpenFile);
    addCommand("CloseFile", &SyncService::cmdCloseFile);
    addCommand("ReadFile", &SyncService::cmdReadFile);
    addCommand("WriteFile", &SyncService::cmdWriteFile);
}

void SyncService::setServerData(
        QPointer<ServerData> data)
{
    m_data = data;
}

void SyncService::addCommand(
        const QString& name,
        SvcCmd::Callback callback)
{
    std::unique_ptr<SvcCmd> commandGetDeviceInfo(new SvcCmd());
    commandGetDeviceInfo->setName(name);
    commandGetDeviceInfo->setCallback(this, callback);
    Service::addCommand(std::move(commandGetDeviceInfo));
}

quint32 SyncService::cmdGetShares(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    try
    {
        if (command.getArgumentCount() != 0)
        {
            return BopRpc::RpcError::INVALID_ARGUMENTS;
        }
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    const QList<Uuid> shareUuids = m_data->getShares().getShareKeys();
    VariantArray shareIds;
    for (const Uuid& uuid : shareUuids)
    {
        shareIds.append(uuid);
    }

    result.setArgument("ShareIds", shareIds);

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdGetShareInfo(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    Uuid shareUuid;

    try
    {
        if (command.getArgumentCount() != 1)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        shareUuid = command.getArgument("ShareId").getUuid();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    if (!m_data->getShares().hasShare(shareUuid))
    {
        return SyncError::INVALID_SHARE_ID;
    }

    const Share share = m_data->getShares().getShare(shareUuid);
    result.setArgument("Name", share.getName());
    result.setArgument("Description", share.getDescription());
    result.setArgument("ReadOnly", share.isReadOnly());

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdOpenDir(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    Uuid shareUuid;
    QString dirPath;

    try
    {
        if (command.getArgumentCount() != 2)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        shareUuid = command.getArgument("ShareId").getUuid();
        dirPath = command.getArgument("Path").getString();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    if (!m_data->getShares().hasShare(shareUuid))
    {
        return SyncError::INVALID_SHARE_ID;
    }

    // TODO: shall there be hasDirectory?

    ServerData::DirectoryConstPtr directory = m_data->openDirectory(shareUuid, dirPath);
    if (!directory)
    {
        return SyncError::ITEM_NOT_FOUND;
    }

    result.setArgument("DirId", directory->getHandle());

    qDebug() << "SyncService::cmdOpenDir() dirId = " << directory->getHandle();

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdReadDir(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    quint32 dirId = 0;
    quint32 offset = 0;
    quint32 maxCount = 0;

    try
    {
        if (command.getArgumentCount() != 3)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        dirId = command.getArgument("DirId").getUint32();
        offset = command.getArgument("Offset").getUint32();
        maxCount = command.getArgument("MaxCount").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    qDebug() << "SyncService::cmdReadDir() dirId = " << dirId;

    ServerData::DirectoryConstPtr directory = m_data->getDirectory(dirId);
    if (!directory)
    {
        return SyncError::BAD_HANDLE;
    }

    // get file info array
    const QFileInfoList& fileInfos = directory->getFileInfos();
    const quint32 totalCount = fileInfos.size();
    quint32 returnedCount = maxCount;

    // if requested range is outside array - reduce array to 0 elements
    if (offset >= fileInfos.size())
    {
        offset = fileInfos.size();
        returnedCount = 0;
    }

    // calculate number of elements after given offset
    const quint32 itemsLeft = totalCount - offset;

    // limit number of elements returned to existing
    if (returnedCount > itemsLeft)
    {
        returnedCount = itemsLeft;
    }

    // TODO: convert to option?
    const quint32 RETURNED_COUNT_LIMIT = 10;

    // limit number of elements returned - general limit
    if (returnedCount > RETURNED_COUNT_LIMIT)
    {
        returnedCount = RETURNED_COUNT_LIMIT;
    }

    // prepare the output
    VariantArray resultInfos;
    for (quint32 index = 0; index < returnedCount; ++index)
    {
        // get file info array
        const QFileInfo& info = fileInfos.at(offset + index);

        VariantMap infoData;

        const QString name = info.fileName();
        const qint64 mtime = info.lastModified().toMSecsSinceEpoch();
        qint64 size = 0;
        QString type("Unknown");

        if (info.isDir() && !info.isSymLink())
        {
            type = "Dir";
        }
        else if (info.isFile() && !info.isSymLink())
        {
            type = "File";
            size = info.size();
        }
        else
        {
            type = "Other";
        }

        infoData.insert("Name", name);
        infoData.insert("Type", type);
        infoData.insert("Size", size);
        infoData.insert("MTime", mtime);

        resultInfos.append(infoData);

        try
        {
            qDebug() << "SyncService::cmdReadDir() " << infoData.value("Name").getString()
                        << infoData.value("Size").getInt64() << infoData.value("Type").getString();
        }
        catch (TypeMismatchException& e)
        {
            qDebug() << e.getMessage();
        }
    }

    result.setArgument("Infos", resultInfos);
    result.setArgument("Count", returnedCount);
    result.setArgument("Total", totalCount);

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdCloseDir(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    quint32 dirId = 0;

    try
    {
        if (command.getArgumentCount() != 1)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        dirId = command.getArgument("DirId").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    qDebug() << "SyncService::cmdCloseDir() dirId = " << dirId;

    if (!m_data->closeDirectory(dirId))
    {
        return SyncError::BAD_HANDLE;
    }

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdDeleteFile(
        const BopRpc::Command& command,
        BopRpc::Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    Uuid shareUuid;
    QString filePath;

    try
    {
        if (command.getArgumentCount() != 2)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        shareUuid = command.getArgument("ShareId").getUuid();
        filePath = command.getArgument("Path").getString();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    if (!m_data->getShares().hasShare(shareUuid))
    {
        return SyncError::INVALID_SHARE_ID;
    }

    if (!m_data->hasFile(shareUuid, filePath))
    {
        return SyncError::ITEM_NOT_FOUND;
    }

    if (!m_data->deleteFile(shareUuid, filePath))
    {
        // TODO: proper error
        return SyncError::ACCESS_DENIED;
    }

    qDebug() << "SyncService::cmdDeleteFile()";

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdSafeRenameFile(
        const BopRpc::Command& command,
        BopRpc::Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    Uuid shareUuid;
    QString filePath;

    try
    {
        if (command.getArgumentCount() != 2)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        shareUuid = command.getArgument("ShareId").getUuid();
        filePath = command.getArgument("Path").getString();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    if (!m_data->getShares().hasShare(shareUuid))
    {
        return SyncError::INVALID_SHARE_ID;
    }

    if (!m_data->hasFile(shareUuid, filePath))
    {
        return SyncError::ITEM_NOT_FOUND;
    }

    const QString safePath = m_data->getSafePath(shareUuid, filePath);

    if (!m_data->renameFile(shareUuid, filePath, safePath))
    {
        // TODO: proper error
        return SyncError::ACCESS_DENIED;
    }

    result.setArgument("SafePath", safePath);

    qDebug() << "SyncService::cmdSafeRenameFile() " << safePath;

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdOpenFile(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    Uuid shareUuid;
    QString dirPath;
    QString mode;
    qint64 mtime;

    try
    {
        if (command.getArgumentCount() != 4)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        shareUuid = command.getArgument("ShareId").getUuid();
        dirPath = command.getArgument("Path").getString();
        mode = command.getArgument("Mode").getString();
        mtime = command.getArgument("MTime").getInt64();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    if (!m_data->getShares().hasShare(shareUuid))
    {
        return SyncError::INVALID_SHARE_ID;
    }

    // TODO: shall there be hasFile?

    ServerData::FileConstPtr file = m_data->openFile(shareUuid, dirPath, mode, mtime);
    if (!file)
    {
        return SyncError::ITEM_NOT_FOUND;
    }

    result.setArgument("FileId", file->getHandle());

    qDebug() << "SyncService::cmdOpenFile() dirId = " << file->getHandle();

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdCloseFile(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    quint32 fileId = 0;

    try
    {
        if (command.getArgumentCount() != 1)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        fileId = command.getArgument("FileId").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    qDebug() << "SyncService::cmdCloseFile() fileId = " << fileId;

    if (!m_data->closeFile(fileId))
    {
        return SyncError::BAD_HANDLE;
    }

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdReadFile(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    quint32 fileId = 0;
    quint32 maxSize = 0;

    try
    {
        if (command.getArgumentCount() !=2)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        fileId = command.getArgument("FileId").getUint32();
        maxSize = command.getArgument("MaxSize").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    qDebug() << "SyncService::cmdReadFile() fileId = " << fileId;

    ServerData::FilePtr file = m_data->getFile(fileId);
    if (!file)
    {
        return SyncError::BAD_HANDLE;
    }

    // get file info array
    QByteArray data;
    if (!file->read(data, maxSize))
    {
        return SyncError::READ_ERROR;
    }

    result.setArgument("Data", data);

    return RpcError::SUCCESS;
}

quint32 SyncService::cmdWriteFile(
        const Command& command,
        Result& result)
{
    if (!m_data)
    {
        return RpcError::INTERNAL_ERROR;
    }

    quint32 fileId = 0;
    QByteArray data;

    try
    {
        if (command.getArgumentCount() !=2)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        fileId = command.getArgument("FileId").getUint32();
        data = command.getArgument("Data").getByteArray();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    qDebug() << "SyncService::cmdWriteFile() fileId = " << fileId;

    ServerData::FilePtr file = m_data->getFile(fileId);
    if (!file)
    {
        return SyncError::BAD_HANDLE;
    }

    // get file info array
    if (!file->write(data))
    {
        return SyncError::READ_ERROR;
    }

    return RpcError::SUCCESS;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
