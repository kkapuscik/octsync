//-----------------------------------------------------------------------------
/// \file
/// SafeRenameFileExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SafeRenameFileExecutor.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
using OCTsync::BopRpc::TypeMismatchException;
using OCTsync::BopRpc::BadArgumentException;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString SafeRenameFileExecutor::COMMAND_NAME("SafeRenameFile");

SafeRenameFileExecutor::SafeRenameFileExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void SafeRenameFileExecutor::setShareId(
        const Commons::Uuid& shareId)
{
    getCommand().setArgument("ShareId", shareId);
}

void SafeRenameFileExecutor::setPath(
        const QString& path)
{
    getCommand().setArgument("Path", path);
}

QString SafeRenameFileExecutor::getSafePath() const
        throw (BopRpc::BadArgumentException)
{
    try
    {
        return getResult().getArgument("SafePath").getString();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: SafePath");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
