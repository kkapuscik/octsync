//-----------------------------------------------------------------------------
/// \file
/// CloseFileExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "CloseFileExecutor.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString CloseFileExecutor::COMMAND_NAME("CloseFile");

CloseFileExecutor::CloseFileExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void CloseFileExecutor::setFileId(
        const quint32 dirId)
{
    getCommand().setArgument("FileId", dirId);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
