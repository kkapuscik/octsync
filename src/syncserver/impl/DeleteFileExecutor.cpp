//-----------------------------------------------------------------------------
/// \file
/// DeleteFileExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "DeleteFileExecutor.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

const QString DeleteFileExecutor::COMMAND_NAME("DeleteFile");

DeleteFileExecutor::DeleteFileExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
}

void DeleteFileExecutor::setShareId(
        const Commons::Uuid& shareId)
{
    getCommand().setArgument("ShareId", shareId);
}

void DeleteFileExecutor::setPath(
        const QString& path)
{
    getCommand().setArgument("Path", path);
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
