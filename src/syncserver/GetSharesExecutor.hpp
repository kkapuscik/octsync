//-----------------------------------------------------------------------------
/// \file
/// GetSharesExecutor - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_GetSharesExecutor
#define INCLUDED_OCTsync_GetSharesExecutor

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QList>
#include <commons/Uuid.hpp>
#include <boprpc/Executor.hpp>
#include <boprpc/BadArgumentException.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class GetSharesExecutor : public BopRpc::Executor
{
    Q_OBJECT

// Static constants (public, protected, private)
public:
    static const QString COMMAND_NAME;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    GetSharesExecutor(
            QObject* parent = nullptr);

    QList<Commons::Uuid> getShareIds() const
            throw (BopRpc::BadArgumentException);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_GetSharesExecutor*/
