//-----------------------------------------------------------------------------
/// \file
/// GetShareInfoExecutor - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_GetShareInfoExecutor
#define INCLUDED_OCTsync_GetShareInfoExecutor

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <boprpc/Executor.hpp>
#include <boprpc/BadArgumentException.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class GetShareInfoExecutor : public BopRpc::Executor
{
Q_OBJECT

// Static constants (public, protected, private)
public:
    static const QString COMMAND_NAME;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    GetShareInfoExecutor(
            QObject* parent = nullptr);

    void setShareId(
            const Commons::Uuid& shareId);

    QString getShareName() const
            throw (BopRpc::BadArgumentException);

    QString getShareDescription() const
            throw (BopRpc::BadArgumentException);

    bool isShareReadOnly() const
            throw (BopRpc::BadArgumentException);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_GetShareInfoExecutor*/
