//-----------------------------------------------------------------------------
/// \file
/// ServerData - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ServerData
#define INCLUDED_OCTsync_ServerData

//-----------------------------------------------------------------------------

// local includes
#include "ShareRepository.hpp"
#include "impl/Directory.hpp"
#include "impl/File.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QSettings>

// system includes
#include <map>
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ServerData : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
public:
    typedef std::shared_ptr<const Directory> DirectoryConstPtr;
    typedef std::shared_ptr<const File> FileConstPtr;
    typedef std::shared_ptr<File> FilePtr;

private:
    typedef std::shared_ptr<Directory> DirectoryPtr;
    typedef std::map<quint32, DirectoryPtr> DirectoryMap;
    typedef std::map<quint32, FilePtr> FileMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_serverUuid;

    ShareRepository m_shares;

    DirectoryMap m_directories;
    FileMap m_files;
    quint32 m_nextHandle;
    bool m_serverSecure;
    bool m_serverEnabled;
    QString m_serverSslCertPath;
    QString m_serverSslKeyPath;

// Methods (public, protected, private)
public:
    ServerData(
            QObject* parent = nullptr);

    Commons::Uuid getServerUuid() const;

    void setServerUuid(
            const Commons::Uuid& uuid);

    QString getSllCertificatePath() const;

    void setSslCertificatePath(
            const QString& path);

    QString getSslKeyPath() const;

    void setSslKeyPath(
            const QString& path);

    bool isServerEnabled() const;

    void setServerEnabled(
            const bool enabled);

    bool isServerSecure() const;

    void setServerSecure(
            const bool secure);

    bool load(
            QSettings& settings);

    bool save(
            QSettings& settings);

    const ShareRepository& getShares() const
    {
        return m_shares;
    }

    Share getShare(
            const Commons::Uuid& uuid) const;

    void addShare(
            const Share& share);

    void modifyShare(
            const Share& share);

    void removeShare(
            const Commons::Uuid& uuid);

    void removeAllShares();

    DirectoryConstPtr openDirectory(
            const Commons::Uuid& shareUuid,
            const QString& path);

    DirectoryConstPtr getDirectory(
            const quint32 dirId);

    bool closeDirectory(
            const quint32 dirId);

    QString getSafePath(
            const Commons::Uuid& shareUuid,
            const QString& path) const;

    bool hasFile(
            const Commons::Uuid& shareUuid,
            const QString& path) const;

    bool deleteFile(
            const Commons::Uuid& shareUuid,
            const QString& path) const;

    bool renameFile(
            const Commons::Uuid& shareUuid,
            const QString& oldName,
            const QString& newName) const;

    FileConstPtr openFile(
            const Commons::Uuid& shareUuid,
            const QString& path,
            const QString& mode,
            const qint64 mtime);

    bool closeFile(
            const quint32 fileId);

    FilePtr getFile(
            const quint32 fileId);

private:
    quint32 getNextHandle();

    template <class T>
    void setField(
            const QString& fieldName,
            T& field,
            const T& value,
            const bool emitChange);

    void setServerUuidInternal(
            const Commons::Uuid& uuid,
            const bool emitChange);

    void setServerSecureInternal(
            const bool secure,
            const bool emitChange);

    void setServerEnabledInternal(
            const bool enabled,
            const bool emitChange);

signals:
    void changed();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ServerData*/
