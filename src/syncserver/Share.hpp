//-----------------------------------------------------------------------------
/// \file
/// Share - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Share
#define INCLUDED_OCTsync_Share

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <commons/Uuid.hpp>
#include <QtCore/QDir>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace SyncServer
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Share
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Commons::Uuid m_uuid;
    QString m_name;
    QString m_description;
    QDir m_rootDir;
    bool m_readOnly;

// Methods (public, protected, private)
public:
    Share();

    bool isValid() const;

    Commons::Uuid getUuid() const
    {
        return m_uuid;
    }

    void setUuid(
            const Commons::Uuid& uuid)
    {
        m_uuid = uuid;
    }

    QString getName() const
    {
        return m_name;
    }

    void setName(
            const QString& name)
    {
        m_name = name;
    }

    QString getDescription() const
    {
        return m_description;
    }

    void setDescription(
            const QString& description)
    {
        m_description = description;
    }

    QDir getRootDir() const
    {
        return m_rootDir;
    }

    void setRootDir(
            const QDir& dir)
    {
        m_rootDir = dir;
    }

    bool isReadOnly() const
    {
        return m_readOnly;
    }

    void setReadOnly(
            const bool readOnly)
    {
        m_readOnly = readOnly;
    }
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Share*/
