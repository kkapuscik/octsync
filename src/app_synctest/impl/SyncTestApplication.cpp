//-----------------------------------------------------------------------------
/// \file
/// SyncTestApplication - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "SyncTestApplication.hpp"

// library includes
#include <QtWidgets/QMenu>
#include <QtWidgets/QAction>
#include <QtCore/QtGlobal>
#include <ui/LogManager.hpp>

// system includes
// (none)

// using clauses
using OCTsync::AppSyncServer::SyncServerEngine;
using OCTsync::AppSyncClient::SyncClientEngine;
using OCTsync::AppSyncServer::SyncServerWindow;
using OCTsync::AppSyncClient::SyncClientWindow;
using OCTsync::Ui::LogManager;

// forward references
// (none)

//-----------------------------------------------------------------------------

static void initResources()
{
    Q_INIT_RESOURCE(octsync);
}

namespace OCTsync
{
namespace AppSyncTest
{

SyncTestApplication::SyncTestApplication(
        int& argc,
        char** argv) :
                QApplication(argc, argv)
{
    m_serverEngine = new SyncServerEngine(this);
    m_serverEngine->processArguments(arguments());
    m_clientEngine = new SyncClientEngine(this);

    m_serverWindow.reset(new SyncServerWindow(m_serverEngine));
    m_clientWindow.reset(new SyncClientWindow(m_clientEngine));

    LogManager* const logManager = LogManager::getInstance();

    logManager->initialize();

    QApplication::connect(logManager, &LogManager::messageLogged,
                          m_serverWindow.get(), &SyncServerWindow::addLogMessage);
}

int SyncTestApplication::exec()
{
    initResources();

    m_serverEngine->start();
    m_clientEngine->start();

    m_serverWindow->show();
    m_clientWindow->show();

    return QApplication::exec();
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
