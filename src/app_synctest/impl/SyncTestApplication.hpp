//-----------------------------------------------------------------------------
/// \file
/// SyncTestApplication - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_SyncTestApplication
#define INCLUDED_OCTsync_SyncTestApplication

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtWidgets/QApplication>

// system includes
#include <memory>
#include <app_syncclient/impl/SyncClientWindow.hpp>
#include <app_syncserver/impl/SyncServerWindow.hpp>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace AppSyncTest
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class SyncTestApplication : public QApplication
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    std::unique_ptr<AppSyncServer::SyncServerWindow> m_serverWindow;
    std::unique_ptr<AppSyncClient::SyncClientWindow> m_clientWindow;
    AppSyncServer::SyncServerEngine* m_serverEngine;
    AppSyncClient::SyncClientEngine* m_clientEngine;

// Methods (public, protected, private)
public:
    SyncTestApplication(
            int& argc,
            char** argv);

    int exec();

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_SyncTestApplication*/
