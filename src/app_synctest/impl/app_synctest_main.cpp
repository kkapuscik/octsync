#include "SyncTestApplication.hpp"

using OCTsync::AppSyncTest::SyncTestApplication;

int main(
        int argc,
        char** argv)
{
    return SyncTestApplication(argc, argv).exec();
}
