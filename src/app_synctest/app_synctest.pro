#-------------------------------------------------
#
# Project created by QtCreator 2014-10-02T18:51:36
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = app_synctest
TEMPLATE = app
CONFIG += mobility c++11

SOURCES += \
    impl/app_synctest_main.cpp \
    impl/SyncTestApplication.cpp \
    ../app_syncserver/impl/LogView.cpp \
    ../app_syncserver/impl/ShareEditDialog.cpp \
    ../app_syncserver/impl/SharesTable.cpp \
    ../app_syncserver/impl/SharesTableModel.cpp \
    ../app_syncserver/impl/SharesView.cpp \
    ../app_syncserver/impl/SyncServerApplication.cpp \
    ../app_syncserver/impl/SyncServerEngine.cpp \
    ../app_syncserver/impl/SyncServerWindow.cpp \
    ../app_syncclient/impl/AsyncTask.cpp \
    ../app_syncclient/impl/AsyncTaskQueue.cpp \
    ../app_syncclient/impl/CopyTask.cpp \
    ../app_syncclient/impl/DeleteTask.cpp \
    ../app_syncclient/impl/FileInfo.cpp \
    ../app_syncclient/impl/FileTreeItem.cpp \
    ../app_syncclient/impl/FileTreeModel.cpp \
    ../app_syncclient/impl/ManualSyncWidget.cpp \
    ../app_syncclient/impl/ReadDirTask.cpp \
    ../app_syncclient/impl/SafeRenameTask.cpp \
    ../app_syncclient/impl/ServerDevice.cpp \
    ../app_syncclient/impl/ShareLocator.cpp \
    ../app_syncclient/impl/ShareSelectDialog.cpp \
    ../app_syncclient/impl/SharesTreeView.cpp \
    ../app_syncclient/impl/SharesUpdater.cpp \
    ../app_syncclient/impl/SharesWidget.cpp \
    ../app_syncclient/impl/ShareTreeModel.cpp \
    ../app_syncclient/impl/SyncAlgorithm.cpp \
    ../app_syncclient/impl/SyncAlgorithmManual.cpp \
    ../app_syncclient/impl/SyncAlgorithmMirror.cpp \
    ../app_syncclient/impl/SyncAlgorithmTwoSided.cpp \
    ../app_syncclient/impl/SyncClientApplication.cpp \
    ../app_syncclient/impl/SyncClientEngine.cpp \
    ../app_syncclient/impl/SyncClientWindow.cpp \
    ../app_syncclient/impl/SyncContext.cpp \
    ../app_syncclient/impl/SynchronizeTask.cpp \
    ../app_syncclient/impl/SyncMode.cpp \
    ../app_syncclient/impl/SyncPoint.cpp \
    ../app_syncclient/impl/SyncPointEditDialog.cpp \
    ../app_syncclient/impl/SyncPointInfoWidget.cpp \
    ../app_syncclient/impl/SyncPointListModel.cpp \
    ../app_syncclient/impl/SyncPointListView.cpp \
    ../app_syncclient/impl/SyncPointsData.cpp \
    ../app_syncclient/impl/SyncShare.cpp \
    ../app_syncclient/impl/SyncsInfoWidget.cpp \
    ../app_syncclient/impl/SyncsListWidget.cpp \
    ../app_syncclient/impl/SyncsRepository.cpp \
    ../app_syncclient/impl/SyncsWidget.cpp \
    ../app_syncserver/impl/SecuritySettingsDialog.cpp

HEADERS  += \
    namespace.hpp \
    impl/SyncTestApplication.hpp \
    ../app_syncserver/impl/LogView.hpp \
    ../app_syncserver/impl/ShareEditDialog.hpp \
    ../app_syncserver/impl/SharesTable.hpp \
    ../app_syncserver/impl/SharesTableModel.hpp \
    ../app_syncserver/impl/SharesView.hpp \
    ../app_syncserver/impl/SyncServerApplication.hpp \
    ../app_syncserver/impl/SyncServerEngine.hpp \
    ../app_syncserver/impl/SyncServerWindow.hpp \
    ../app_syncclient/impl/AsyncTask.hpp \
    ../app_syncclient/impl/AsyncTaskQueue.hpp \
    ../app_syncclient/impl/CopyTask.hpp \
    ../app_syncclient/impl/DeleteTask.hpp \
    ../app_syncclient/impl/FileInfo.hpp \
    ../app_syncclient/impl/FileTreeItem.hpp \
    ../app_syncclient/impl/FileTreeModel.hpp \
    ../app_syncclient/impl/ItemTypes.hpp \
    ../app_syncclient/impl/ManualSyncWidget.hpp \
    ../app_syncclient/impl/ReadDirTask.hpp \
    ../app_syncclient/impl/SafeRenameTask.hpp \
    ../app_syncclient/impl/ServerDevice.hpp \
    ../app_syncclient/impl/ShareLocator.hpp \
    ../app_syncclient/impl/ShareSelectDialog.hpp \
    ../app_syncclient/impl/SharesTreeView.hpp \
    ../app_syncclient/impl/SharesUpdater.hpp \
    ../app_syncclient/impl/SharesWidget.hpp \
    ../app_syncclient/impl/ShareTreeModel.hpp \
    ../app_syncclient/impl/SyncAlgorithm.hpp \
    ../app_syncclient/impl/SyncAlgorithmManual.hpp \
    ../app_syncclient/impl/SyncAlgorithmMirror.hpp \
    ../app_syncclient/impl/SyncAlgorithmTwoSided.hpp \
    ../app_syncclient/impl/SyncClientApplication.hpp \
    ../app_syncclient/impl/SyncClientEngine.hpp \
    ../app_syncclient/impl/SyncClientWindow.hpp \
    ../app_syncclient/impl/SyncContext.hpp \
    ../app_syncclient/impl/SynchronizeTask.hpp \
    ../app_syncclient/impl/SyncMode.hpp \
    ../app_syncclient/impl/SyncOp.hpp \
    ../app_syncclient/impl/SyncPoint.hpp \
    ../app_syncclient/impl/SyncPointEditDialog.hpp \
    ../app_syncclient/impl/SyncPointInfoWidget.hpp \
    ../app_syncclient/impl/SyncPointListModel.hpp \
    ../app_syncclient/impl/SyncPointListView.hpp \
    ../app_syncclient/impl/SyncPointsData.hpp \
    ../app_syncclient/impl/SyncShare.hpp \
    ../app_syncclient/impl/SyncsInfoWidget.hpp \
    ../app_syncclient/impl/SyncsListWidget.hpp \
    ../app_syncclient/impl/SyncsRepository.hpp \
    ../app_syncclient/impl/SyncsWidget.hpp \
    ../app_syncserver/impl/SecuritySettingsDialog.hpp

RESOURCES += \
    ../../data/octsync.qrc

MOBILITY =

OTHER_FILES += \
    CMakeLists.txt

INCLUDEPATH += $$PWD/..

# add library COMMONS
DEPENDPATH += $$PWD/../commons
LIBS += -L$$OUT_PWD/../commons/ -lcommons
PRE_TARGETDEPS += $$OUT_PWD/../commons/libcommons.a

# add library LDP
DEPENDPATH += $$PWD/../ldp
LIBS += -L$$OUT_PWD/../ldp/ -lldp
PRE_TARGETDEPS += $$OUT_PWD/../ldp/libldp.a

# add library BOPRPC
DEPENDPATH += $$PWD/../boprpc
LIBS += -L$$OUT_PWD/../boprpc/ -lboprpc
PRE_TARGETDEPS += $$OUT_PWD/../boprpc/libboprpc.a

# add library BOP
DEPENDPATH += $$PWD/../bop
LIBS += -L$$OUT_PWD/../bop/ -lbop
PRE_TARGETDEPS += $$OUT_PWD/../bop/libbop.a

# Add library syncserver
DEPENDPATH += $$PWD/../syncserver
LIBS += -L$$OUT_PWD/../syncserver/ -lsyncserver
PRE_TARGETDEPS += $$OUT_PWD/../syncserver/libsyncserver.a

# add library UI
DEPENDPATH += $$PWD/../ui
LIBS += -L$$OUT_PWD/../ui/ -lui
PRE_TARGETDEPS += $$OUT_PWD/../ui/libui.a
