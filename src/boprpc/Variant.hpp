//-----------------------------------------------------------------------------
/// \file
/// Variant - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Variant
#define INCLUDED_OCTsync_Variant

//-----------------------------------------------------------------------------

// local includes
#include "VariantType.hpp"
#include "TypeMismatchException.hpp"

// library includes
#include <QtCore/QtGlobal>
#include <QtCore/QString>
#include <QtCore/QByteArray>
#include <QtCore/QSharedData>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <commons/Uuid.hpp>

// system includes
#include <cstring>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
class Variant;

/// TODO
typedef QList<Variant> VariantArray;
typedef QMap<QString,Variant> VariantMap;

/// Object for storing values of misc. types.
///
class Variant
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    class Data : public QSharedData
    {
    public:
        /// Object to store numeric values of misc. types.
        union NumericValue
        {
            qint8 m_int8;
            qint16 m_int16;
            qint32 m_int32;
            qint64 m_int64;
            quint8 m_uint8;
            quint16 m_uint16;
            quint32 m_uint32;
            quint64 m_uint64;
            bool m_bool;
        };

        VariantType m_type;
        NumericValue m_numeric;
        QString m_string;
        Commons::Uuid m_uuid;
        QByteArray m_byteArray;
        VariantArray m_variantArray;
        VariantMap m_variantMap;

        Data()
        {
            reset();
        }

        void reset()
        {
            m_type = VariantType::INVALID;
            std::memset(&m_numeric, 0x0, sizeof(m_numeric));
            m_string.clear();
            m_byteArray.clear();
            m_uuid = Commons::Uuid();
        }
    };

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QSharedDataPointer<Data> m_data;

// Methods (public, protected, private)
public:
    Variant() :
                    m_data(new Data())
    {
        // nothing to do
    }

    template<class T>
    Variant(
            const T& value) :
                    m_data(new Data())
    {
        setValue(value);
    }

    bool isValid() const
    {
        return m_data->m_type != VariantType::INVALID;
    }

    void reset()
    {
        m_data->reset();
    }

    bool is(
            const VariantType& type) const
    {
        return getType() == type;
    }

    VariantType getType() const
    {
        return m_data->m_type;
    }

    void setValue(
            const char* const value)
    {
        set(VariantType::STRING, m_data->m_string, QString(value));
    }

    void setValue(
            const qint8 value)
    {
        set(VariantType::INT8, m_data->m_numeric.m_int8, value);
    }

    void setValue(
            const qint16 value)
    {
        set(VariantType::INT16, m_data->m_numeric.m_int16, value);
    }

    void setValue(
            const qint32 value)
    {
        set(VariantType::INT32, m_data->m_numeric.m_int32, value);
    }

    void setValue(
            const qint64 value)
    {
        set(VariantType::INT64, m_data->m_numeric.m_int64, value);
    }

    void setValue(
            const quint8 value)
    {
        set(VariantType::UINT8, m_data->m_numeric.m_uint8, value);
    }

    void setValue(
            const quint16 value)
    {
        set(VariantType::UINT16, m_data->m_numeric.m_uint16, value);
    }

    void setValue(
            const quint32 value)
    {
        set(VariantType::UINT32, m_data->m_numeric.m_uint32, value);
    }

    void setValue(
            const quint64 value)
    {
        set(VariantType::UINT64, m_data->m_numeric.m_uint64, value);
    }

    void setValue(
            const bool value)
    {
        set(VariantType::BOOL, m_data->m_numeric.m_bool, value);
    }

    void setValue(
            const QString& value)
    {
        set(VariantType::STRING, m_data->m_string, value);
    }

    void setValue(
            const Commons::Uuid& value)
    {
        set(VariantType::UUID, m_data->m_uuid, value);
    }

    void setValue(
            const QByteArray& value)
    {
        set(VariantType::BYTEARRAY, m_data->m_byteArray, value);
    }

    void setValue(
            const VariantArray& value)
    {
        set(VariantType::VARIANTARRAY, m_data->m_variantArray, value);
    }

    void setValue(
            const VariantMap& value)
    {
        set(VariantType::VARIANTMAP, m_data->m_variantMap, value);
    }

    qint8 getInt8() const
            throw (TypeMismatchException)
    {
        return get(VariantType::INT8, m_data->m_numeric.m_int8);
    }

    qint16 getInt16() const
            throw (TypeMismatchException)
    {
        return get(VariantType::INT16, m_data->m_numeric.m_int16);
    }

    qint32 getInt32() const
            throw (TypeMismatchException)
    {
        return get(VariantType::INT32, m_data->m_numeric.m_int32);
    }

    qint64 getInt64() const
            throw (TypeMismatchException)
    {
        return get(VariantType::INT64, m_data->m_numeric.m_int64);
    }

    quint8 getUint8() const
            throw (TypeMismatchException)
    {
        return get(VariantType::UINT8, m_data->m_numeric.m_uint8);
    }

    quint16 getUint16() const
            throw (TypeMismatchException)
    {
        return get(VariantType::UINT16, m_data->m_numeric.m_uint16);
    }

    quint32 getUint32() const
            throw (TypeMismatchException)
    {
        return get(VariantType::UINT32, m_data->m_numeric.m_uint32);
    }

    quint64 getUint64() const
            throw (TypeMismatchException)
    {
        return get(VariantType::UINT64, m_data->m_numeric.m_uint64);
    }

    bool getBool() const
            throw (TypeMismatchException)
    {
        return get(VariantType::BOOL, m_data->m_numeric.m_bool);
    }

    QString getString() const
            throw (TypeMismatchException)
    {
        return get(VariantType::STRING, m_data->m_string);
    }

    Commons::Uuid getUuid() const
            throw (TypeMismatchException)
    {
        return get(VariantType::UUID, m_data->m_uuid);
    }

    QByteArray getByteArray() const
            throw (TypeMismatchException)
    {
        return get(VariantType::BYTEARRAY, m_data->m_byteArray);
    }

    VariantArray getVariantArray() const
    {
        return get(VariantType::VARIANTARRAY, m_data->m_variantArray);
    }

    VariantMap getVariantMap() const
    {
        return get(VariantType::VARIANTMAP, m_data->m_variantMap);
    }

    bool getValue(
            qint8& retValue) const
    {
        return get(retValue, VariantType::INT8, m_data->m_numeric.m_int8);
    }

    bool getValue(
            qint16& retValue) const
    {
        return get(retValue, VariantType::INT16, m_data->m_numeric.m_int16);
    }

    bool getValue(
            qint32& retValue) const
    {
        return get(retValue, VariantType::INT32, m_data->m_numeric.m_int32);
    }

    bool getValue(
            qint64& retValue) const
    {
        return get(retValue, VariantType::INT64, m_data->m_numeric.m_int64);
    }

    bool getValue(
            quint8& retValue) const
    {
        return get(retValue, VariantType::UINT8, m_data->m_numeric.m_uint8);
    }

    bool getValue(
            quint16& retValue) const
    {
        return get(retValue, VariantType::UINT16, m_data->m_numeric.m_uint16);
    }

    bool getValue(
            quint32& retValue) const
    {
        return get(retValue, VariantType::UINT32, m_data->m_numeric.m_uint32);
    }

    bool getValue(
            quint64& retValue) const
    {
        return get(retValue, VariantType::UINT64, m_data->m_numeric.m_uint64);
    }

    bool getValue(
            bool& retValue) const
    {
        return get(retValue, VariantType::BOOL, m_data->m_numeric.m_bool);
    }

    bool getValue(
            QString& retValue) const
    {
        return get(retValue, VariantType::STRING, m_data->m_string);
    }

    bool getValue(
            Commons::Uuid& retValue) const
    {
        return get(retValue, VariantType::UUID, m_data->m_uuid);
    }

    bool getValue(
            QByteArray& retValue) const
    {
        return get(retValue, VariantType::BYTEARRAY, m_data->m_byteArray);
    }

    bool getArray(VariantArray& retValue) const
    {
        return get(retValue, VariantType::VARIANTARRAY, m_data->m_variantArray);
    }

    bool getMap(VariantMap& retValue) const
    {
        return get(retValue, VariantType::VARIANTMAP, m_data->m_variantMap);
    }

private:
    template<class T>
    T get(
            const VariantType type,
            const T& value) const
                    throw (TypeMismatchException)
    {
        if (m_data->m_type == type)
        {
            return value;
        }
        else
        {
            throw TypeMismatchException(type, m_data->m_type);
        }
    }

    template<class T>
    bool get(
            T& retval,
            const VariantType type,
            const T& value) const
    {
        const bool result = (m_data->m_type == type);
        if (result)
        {
            retval = value;
        }
        return result;
    }

    template<class T>
    void set(
            const VariantType type,
            T& storage,
            const T& value)
    {
        reset();
        m_data->m_type = type;
        storage = value;
    }

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Variant*/
