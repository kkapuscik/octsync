//-----------------------------------------------------------------------------
/// \file
/// ListCommandsExecutor - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ListCommandsExecutor
#define INCLUDED_OCTsync_ListCommandsExecutor

//-----------------------------------------------------------------------------

// local includes
#include "Executor.hpp"
#include "BadArgumentException.hpp"

// library includes
#include <QtCore/QStringList>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ListCommandsExecutor : public Executor
{
Q_OBJECT

// Static constants (public, protected, private)
public:
    static const QString COMMAND_NAME;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    ListCommandsExecutor(
            QObject* parent = nullptr);

    void setListedServiceId(
            const quint32 serviceId);

    QStringList getCommands() const
            throw (BadArgumentException);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ListCommandsExecutor*/
