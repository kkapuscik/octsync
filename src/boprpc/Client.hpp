//-----------------------------------------------------------------------------
/// \file
/// Client - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_RpcClient
#define INCLUDED_OCTsync_RpcClient

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <bop/ObjectFactoryManager.hpp>
#include <bop/Client.hpp>
#include <bop/ObjectBase.hpp>

// system includes
#include <memory>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
class Executor;

/// TODO: Class documentation
///
class Client : public QObject
{
Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
private:
    typedef QMap<quint32,QPointer<Executor>> ExecutorMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Bop::ObjectFactoryManager* m_objectFactoryManager;
    Bop::Client* m_bopClient;
    ExecutorMap m_executors;

// Methods (public, protected, private)
public:
    Client(
            QObject* parent = nullptr);

    void execute(
            Executor* executor);

private:
    void onObjectReceived(
            QUrl url,
            Bop::ConstObjectSharedPtr object);

    void onObjectSendFinished(
            QUrl url,
            Bop::ConstObjectSharedPtr object,
            Bop::Error error);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_RpcClient*/
