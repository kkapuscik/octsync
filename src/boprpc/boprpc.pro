#-------------------------------------------------
#
# Project created by QtCreator 2014-10-01T19:17:13
#
#-------------------------------------------------

QT       -= gui

TARGET = boprpc
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    impl/Client.cpp \
    impl/Command.cpp \
    impl/DataDecoder.cpp \
    impl/DataEncoder.cpp \
    impl/Device.cpp \
    impl/Executor.cpp \
    impl/GetDeviceInfoExecutor.cpp \
    impl/ListCommandsExecutor.cpp \
    impl/ListServicesExecutor.cpp \
    impl/Protocol.cpp \
    impl/Request.cpp \
    impl/Response.cpp \
    impl/Result.cpp \
    impl/RpcObjectFactory.cpp \
    impl/RpcService.cpp \
    impl/Service.cpp \
    impl/Variant.cpp

HEADERS += \
    BadArgumentException.hpp \
    Client.hpp \
    Command.hpp \
    Device.hpp \
    Executor.hpp \
    GetDeviceInfoExecutor.hpp \
    ListCommandsExecutor.hpp \
    ListServicesExecutor.hpp \
    namespace.hpp \
    Protocol.hpp \
    Request.hpp \
    Response.hpp \
    Result.hpp \
    RpcError.hpp \
    RpcObjectFactory.hpp \
    Service.hpp \
    ServiceCommand.hpp \
    TypeMismatchException.hpp \
    Variant.hpp \
    VariantArray.hpp \
    VariantType.hpp \
    impl/DataDecoder.hpp \
    impl/DataEncoder.hpp \
    impl/DecodeErrorException.hpp \
    impl/EncodeErrorException.hpp \
    impl/RpcService.hpp
unix {
    target.path = /usr/lib
    INSTALLS += target
}

OTHER_FILES += \
    CMakeLists.txt

INCLUDEPATH += $$PWD/..
