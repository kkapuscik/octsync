//-----------------------------------------------------------------------------
/// \file
/// Service - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Service
#define INCLUDED_OCTsync_Service

//-----------------------------------------------------------------------------

// local includes
#include "ServiceCommand.hpp"

// library includes
#include <QtCore/QObject>

// system includes
#include <memory>
#include <map>

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
class Command;
class Result;

/// TODO: Class documentation
///
class Service : public QObject
{
Q_OBJECT

    friend class RpcService;

// Static constants (public, protected, private)
public:
    static const quint32 INVALID_SERVICE_ID = 0xFFFFFFFF;

// Types (public, protected, private)
private:
    typedef std::map<QString, std::unique_ptr<ServiceCommand>> CommandMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    const QString m_name;
    CommandMap m_commands;

// Methods (public, protected, private)
public:
    Service(
            const QString& name,
            QObject* parent = nullptr);

    const QString& getName() const
    {
        return m_name;
    }

    bool hasCommand(
            const QString& name) const;

    quint32 processRequest(
            const Command& command,
            Result& result);

protected:
    // NOTE: given command object is NOT copied. It shall exist as long
    // as the service exists.
    void addCommand(
            std::unique_ptr<ServiceCommand> command);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Service*/
