//-----------------------------------------------------------------------------
/// \file
/// ListServicesExecutor - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ListServicesExecutor
#define INCLUDED_OCTsync_ListServicesExecutor

//-----------------------------------------------------------------------------

// local includes
#include "Executor.hpp"
#include "BadArgumentException.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class ListServicesExecutor : public Executor
{
Q_OBJECT

// Static constants (public, protected, private)
public:
    static const QString COMMAND_NAME;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    ListServicesExecutor(
            QObject* parent = nullptr);

    int getListedServiceCount() const
            throw (BadArgumentException);

    QString getListedServiceName(
            const int index) const
                    throw (BadArgumentException);

    quint32 getListedServiceId(
            const int index) const
                    throw (BadArgumentException);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ListServicesExecutor*/
