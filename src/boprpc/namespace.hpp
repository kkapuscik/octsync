//-----------------------------------------------------------------------------
/// \file
/// Namespace definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

namespace OCTsync
{

/// OCT BOP protocol implementation.
namespace Bop
{

}

}
