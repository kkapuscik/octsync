//-----------------------------------------------------------------------------
/// \file
/// TypeMismatchException - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_TypeMismatchException
#define INCLUDED_OCTsync_TypeMismatchException

//-----------------------------------------------------------------------------

// local includes
#include "VariantType.hpp"

// library includes
#include <commons/OctException.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// Type mismatch exception.
///
/// Exception thrown when type mismatch occurred.
///
class TypeMismatchException : public Commons::OctException
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    const VariantType m_expected;
    const VariantType m_given;

// Methods (public, protected, private)
public:
    /// Constructs the exception
    ///
    /// \param message
    ///     Exception message.
    TypeMismatchException(
            const VariantType expected,
            const VariantType given) :
                    Commons::OctException(createMessage(expected, given)),
                    m_expected(expected),
                    m_given(given)
    {
        // nothing to do
    }

private:
    static QString createMessage(
            const VariantType expected,
            const VariantType given)
    {
        return QString("Type mismatch. Expected: ") + typeToString(expected) + "; Actual: "
                + typeToString(given);
    }

    static QString typeToString(
            const VariantType type)
    {
        switch (type)
        {
            case VariantType::INVALID:
                return "Invalid";
            case VariantType::INT8:
                return "INT8";
            case VariantType::INT16:
                return "INT16";
            case VariantType::INT32:
                return "INT32";
            case VariantType::INT64:
                return "INT64";
            case VariantType::UINT8:
                return "UINT8";
            case VariantType::UINT16:
                return "UINT16";
            case VariantType::UINT32:
                return "UINT32";
            case VariantType::UINT64:
                return "UINT64";
            case VariantType::STRING:
                return "String";
            case VariantType::BYTEARRAY:
                return "ByteArray";
            case VariantType::VARIANTARRAY:
                return "VariantArray";
            case VariantType::VARIANTMAP:
                return "VariantMap";
            default:
                return "-unknown-";
        }
    }
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_TypeMismatchException*/
