//-----------------------------------------------------------------------------
/// \file
/// RpcObjectFactory - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_RpcObjectFactory
#define INCLUDED_OCTsync_RpcObjectFactory

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <bop/ObjectFactory.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class RpcObjectFactory : public Bop::ObjectFactory
{
    Q_OBJECT

// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    RpcObjectFactory(
            QObject* parent = nullptr) :
                    ObjectFactory(parent)
    {
        // nothing to do
    }

    virtual std::unique_ptr<Bop::ObjectBase> createObject(
            const Bop::BoxId& id);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_RpcObjectFactory*/
