//-----------------------------------------------------------------------------
/// \file
/// Protocol - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Protocol
#define INCLUDED_OCTsync_Protocol

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <bop/ContainerObject.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
class Request;
class Response;
class Command;
class Result;

/// TODO: Class documentation
///
class Protocol : public Bop::ContainerObject
{
// Static constants (public, protected, private)
public:
    static const Bop::BoxId BOX_ID;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    Protocol();

    bool isRequest() const;

    bool isResponse() const;

    Request* getRequest() const;

    Command* getCommand() const;

    Response* getResponse() const;

    Result* getResult() const;

    void setRequest(
            std::unique_ptr<Request> request,
            std::unique_ptr<Command> command);

    void setResponse(
            std::unique_ptr<Response> response,
            std::unique_ptr<Result> result);

protected:
    virtual bool validateChildren() const;
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Protocol*/
