//-----------------------------------------------------------------------------
/// \file
/// Device - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Device
#define INCLUDED_OCTsync_Device

//-----------------------------------------------------------------------------

// local includes
#include "Service.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include <QtCore/QMap>
#include <bop/Server.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
class RpcService;

/// TODO: Class documentation
///
class Device : public Bop::Server
{
Q_OBJECT

    // TODO: clean this up somehow
    friend class RpcService;

// Static constants (public, protected, private)
private:
    static const QString DEFAULT_NAME;

// Types (public, protected, private)
private:
    typedef QMap<quint32, QPointer<Service>> ServiceMap;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    ServiceMap m_services;
    QString m_deviceName;

// Methods (public, protected, private)
public:
    Device(
            QObject* parent);

    void setName(
            const QString& name)
    {
        m_deviceName = name;
    }

    QString getName() const
    {
        return m_deviceName;
    }

    void addService(
            const quint32 serviceId,
            QPointer<Service> service);

protected:
    virtual std::unique_ptr<Bop::ObjectBase> processRequest(
            const Bop::ObjectBase& request);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Device*/
