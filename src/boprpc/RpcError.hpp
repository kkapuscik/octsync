//-----------------------------------------------------------------------------
/// \file
/// RpcError - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_RpcError
#define INCLUDED_OCTsync_RpcError

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// Error codes for RPC actions.
///
class RpcError
{
private:
    RpcError();

public:
    // TODO: documentation
    enum
    {
        SUCCESS                     =   0,

        PROTOCOL_SPECIFIC_BASE      =   1,

        INTERNAL_ERROR              =   PROTOCOL_SPECIFIC_BASE + 0,
        UNKNOWN_SERVICE             =   PROTOCOL_SPECIFIC_BASE + 1,
        UNKNOWN_ACTION              =   PROTOCOL_SPECIFIC_BASE + 2,
        INVALID_ARGUMENTS           =   PROTOCOL_SPECIFIC_BASE + 3,

        SERVICE_SPECIFIC_BASE       =   1000,

        VENDOR_SPECIFIC_BASE        =   10000
    };
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_RpcError*/
