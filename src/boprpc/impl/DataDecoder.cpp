//-----------------------------------------------------------------------------
/// \file
/// DataDecoder - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "DataDecoder.hpp"
#include "DecodeErrorException.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

DataDecoder::DataDecoder(
        Bop::ByteArrayReader& reader) :
                m_reader(reader)
{
    // nothing to do
}

bool DataDecoder::hasMoreData() const
{
    return m_reader.hasMoreData();
}

QString DataDecoder::decodeString()
{
    Variant variant = decodeItem();

    if (variant.is(VariantType::STRING))
    {
        return variant.getString();
    }
    else
    {
        throw DecodeErrorException("Invalid type");
    }
}

VariantMap DataDecoder::decodeVariantMap()
{
    Variant variant = decodeItem();

    if (variant.is(VariantType::VARIANTMAP))
    {
        return variant.getVariantMap();
    }
    else
    {
        throw DecodeErrorException("Invalid type");
    }
}

Variant DataDecoder::decodeItem()
{
    Variant retval;

    try
    {
        const VariantType type = static_cast<VariantType>(m_reader.decodeUint8());

        switch (type)
        {
            case VariantType::INT8:
                retval.setValue(m_reader.decodeInt8());
                break;
            case VariantType::INT16:
                retval.setValue(m_reader.decodeInt16());
                break;
            case VariantType::INT32:
                retval.setValue(m_reader.decodeInt32());
                break;
            case VariantType::INT64:
                retval.setValue(m_reader.decodeInt64());
                break;
            case VariantType::UINT8:
                retval.setValue(m_reader.decodeUint8());
                break;
            case VariantType::UINT16:
                retval.setValue(m_reader.decodeUint16());
                break;
            case VariantType::UINT32:
                retval.setValue(m_reader.decodeUint32());
                break;
            case VariantType::UINT64:
                retval.setValue(m_reader.decodeUint64());
                break;
            case VariantType::BOOL:
            {
                const quint8 val = m_reader.decodeUint8();
                retval.setValue(val != 0 ? true : false);
                break;
            }
            case VariantType::STRING:
            {
                const quint32 size = m_reader.decodeUint32();
                const QByteArray chars = m_reader.decodeByteArray(size);
                retval.setValue(QString::fromUtf8(chars));
                break;
            }
            case VariantType::UUID:
            {
                const quint32 size = m_reader.decodeUint32();
                const QByteArray chars = m_reader.decodeByteArray(size);
                const QString uuidString = QString::fromUtf8(chars);
                retval.setValue(Commons::Uuid::parse(uuidString));
                break;
            }
            case VariantType::BYTEARRAY:
            {
                const quint32 size = m_reader.decodeUint32();
                const QByteArray bytes = m_reader.decodeByteArray(size);
                retval.setValue(bytes);
                break;
            }
            case VariantType::VARIANTARRAY:
            {
                VariantArray array;
                const quint32 size = m_reader.decodeUint32();
                for (quint32 i = 0; i < size; ++i)
                {
                    array.append(decodeItem());
                }
                retval.setValue(array);
                break;
            }
            case VariantType::VARIANTMAP:
            {
                VariantMap array;
                const quint32 size = m_reader.decodeUint32();
                for (quint32 i = 0; i < size; ++i)
                {
                    const QString key = decodeString();
                    const Variant value = decodeItem();
                    array.insert(key, value);
                }
                retval.setValue(array);
                break;
            }
            default:
                throw DecodeErrorException("Invalid type");
        }
    }
    catch (Commons::OutOfBoundsException& ex)
    {
        throw DecodeErrorException("Not enough data");
    }

    return retval;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
