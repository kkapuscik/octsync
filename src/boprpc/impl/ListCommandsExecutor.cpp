//-----------------------------------------------------------------------------
/// \file
/// ListCommandsExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ListCommandsExecutor.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const QString ListCommandsExecutor::COMMAND_NAME("ListCommands");

ListCommandsExecutor::ListCommandsExecutor(
        QObject* parent) :
                Executor(parent)
{
    setServiceId(BopRpc::Executor::RPC_SERVICE_ID);
    setCommandName(COMMAND_NAME);
}

void ListCommandsExecutor::setListedServiceId(
        const quint32 serviceId)
{
    getCommand().setArgument("ServiceId", serviceId);
}

QStringList ListCommandsExecutor::getCommands() const
        throw (BadArgumentException)
{
    try
    {
        QStringList result;

        const BopRpc::VariantArray commands = getResult().getArgument("Commands").getVariantArray();
        for (const Variant& value : commands)
        {
            result.append(value.getString());
        }

        return result;
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Commands");
    }
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
