//-----------------------------------------------------------------------------
/// \file
/// ListServicesExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "ListServicesExecutor.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const QString ListServicesExecutor::COMMAND_NAME("ListServices");

ListServicesExecutor::ListServicesExecutor(
        QObject* parent) :
                Executor(parent)
{
    setCommandName(COMMAND_NAME);
    setServiceId(BopRpc::Executor::RPC_SERVICE_ID);
}

int ListServicesExecutor::getListedServiceCount() const
        throw (BadArgumentException)
{
    try
    {
        const BopRpc::VariantArray services = getResult().getArgument("Services").getVariantArray();

        return services.size();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Services");
    }
}

QString ListServicesExecutor::getListedServiceName(
        const int index) const
                throw (BadArgumentException)
{
    try
    {
        const BopRpc::VariantArray services = getResult().getArgument("Services").getVariantArray();

        if ((index < 0) || (index >= services.size()))
        {
            throw BadArgumentException("Invalid index");
        }

        BopRpc::VariantMap serviceDescriptor = services.at(index).getVariantMap();

        return serviceDescriptor.value("Name").getString();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Services");
    }
}

quint32 ListServicesExecutor::getListedServiceId(
        const int index) const
                throw (BadArgumentException)
{
    try
    {
        const BopRpc::VariantArray services = getResult().getArgument("Services").getVariantArray();

        if ((index < 0) || (index >= services.size()))
        {
            throw BadArgumentException("Invalid index");
        }

        BopRpc::VariantMap serviceDescriptor = services.at(index).getVariantMap();

        return serviceDescriptor.value("Id").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Services");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
