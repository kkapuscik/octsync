//-----------------------------------------------------------------------------
/// \file
/// Device - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../Device.hpp"
#include "../RpcObjectFactory.hpp"
#include "../Protocol.hpp"
#include "../Request.hpp"
#include "../Command.hpp"
#include "../Response.hpp"
#include "../Result.hpp"
#include "../RpcError.hpp"
#include "RpcService.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const QString Device::DEFAULT_NAME("BOPRPC");

Device::Device(
        QObject* parent) :
                Bop::Server(parent),
                m_deviceName(DEFAULT_NAME)
{
    auto factoryManager = new Bop::ObjectFactoryManager(this);
    factoryManager->addObjectFactory(new RpcObjectFactory(factoryManager));

    setObjectFactoryManager(factoryManager);

    addService(0, new RpcService(this));
}

void Device::addService(
        const quint32 serviceId,
        QPointer<Service> service)
{
    Q_ASSERT(service);
    Q_ASSERT(!m_services.contains(serviceId));

    m_services.insert(serviceId, service);
}

std::unique_ptr<Bop::ObjectBase> Device::processRequest(
        const Bop::ObjectBase& requestObject)
{
    if (requestObject.getId() == Protocol::BOX_ID)
    {
        const Protocol& requestProtocol = static_cast<const Protocol&>(requestObject);

        if (requestProtocol.isRequest())
        {
            auto request = requestProtocol.getRequest();
            auto command = requestProtocol.getCommand();

            if (request && command)
            {
                const quint32 serviceId = request->getServiceId();
                const quint32 context = request->getContext();

                quint32 errorCode = 0;

                /* create result objects */
                std::unique_ptr<Result> result(new Result());

                /* find the service & process request if possible */
                auto service = m_services.value(serviceId);
                if (service)
                {
                    try
                    {
                        errorCode = service->processRequest(*command, *result);
                    }
                    catch (...)
                    {
                        errorCode = RpcError::INTERNAL_ERROR;
                    }
                }
                else
                {
                    errorCode = RpcError::UNKNOWN_SERVICE;
                }

                /* create response object */
                std::unique_ptr<Response> response(new Response());
                response->setContext(context);
                response->setErrorCode(errorCode);

                /* clear result in case of error */
                if (errorCode != 0)
                {
                    result->clearAllArguments();
                }

                /* clear protocol object for response */
                std::unique_ptr<Protocol> responseProtocol(new Protocol());
                responseProtocol->setResponse(std::move(response), std::move(result));

                return std::move(responseProtocol);
            }
        }
    }

    return nullptr;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
