//-----------------------------------------------------------------------------
/// \file
/// Protocol - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../Protocol.hpp"
#include "../Request.hpp"
#include "../Response.hpp"
#include "../Command.hpp"
#include "../Result.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const Bop::BoxId Protocol::BOX_ID('B', 'R', 'P', 'C');

Protocol::Protocol() :
                ContainerObject(Protocol::BOX_ID)
{
    // nothing to do
}

bool Protocol::validateChildren() const
{
    return isRequest() || isResponse();
}

bool Protocol::isRequest() const
{
    bool result = false;

    if (getChildCount() == 2)
    {
        auto childId0 = getChild(0)->getId();
        auto childId1 = getChild(1)->getId();

        if ((childId0 == Request::BOX_ID) && (childId1 == Command::BOX_ID))
        {
            result = true;
        }
    }

    return result;
}

bool Protocol::isResponse() const
{
    bool result = false;

    if (getChildCount() == 2)
    {
        auto childId0 = getChild(0)->getId();
        auto childId1 = getChild(1)->getId();

        if ((childId0 == Response::BOX_ID) && (childId1 == Result::BOX_ID))
        {
            result = true;
        }
    }

    return result;
}

Request* Protocol::getRequest() const
{
    Request* request = nullptr;

    if (isRequest())
    {
        request = static_cast<Request*>(getChild(0));
    }

    return request;
}

Command* Protocol::getCommand() const
{
    Command* command = nullptr;

    if (isRequest())
    {
        command = static_cast<Command*>(getChild(1));
    }

    return command;
}

Response* Protocol::getResponse() const
{
    Response* response = nullptr;

    if (isResponse())
    {
        response = static_cast<Response*>(getChild(0));
    }

    return response;
}

Result* Protocol::getResult() const
{
    Result* result = nullptr;

    if (isResponse())
    {
        result = static_cast<Result*>(getChild(1));
    }

    return result;
}

void Protocol::setRequest(
        std::unique_ptr<Request> request,
        std::unique_ptr<Command> command)
{
    Q_ASSERT(request);
    Q_ASSERT(command);

    deleteAllChildren();
    appendChild(std::move(request));
    appendChild(std::move(command));
}

void Protocol::setResponse(
        std::unique_ptr<Response> response,
        std::unique_ptr<Result> result)
{
    Q_ASSERT(response);
    Q_ASSERT(result);

    deleteAllChildren();
    appendChild(std::move(response));
    appendChild(std::move(result));
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
