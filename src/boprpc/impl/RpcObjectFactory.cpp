//-----------------------------------------------------------------------------
/// \file
/// RpcObjectFactory - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "RpcObjectFactory.hpp"
#include "../Protocol.hpp"
#include "../Request.hpp"
#include "../Command.hpp"
#include "../Result.hpp"
#include "../Response.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

std::unique_ptr<Bop::ObjectBase> RpcObjectFactory::createObject(
        const Bop::BoxId& id)
{
    if (id == Protocol::BOX_ID)
    {
        return std::unique_ptr<Bop::ObjectBase>(new Protocol());
    }
    else if (id == Request::BOX_ID)
    {
        return std::unique_ptr<Bop::ObjectBase>(new Request());
    }
    else if (id == Response::BOX_ID)
    {
        return std::unique_ptr<Bop::ObjectBase>(new Response());
    }
    else if (id == Command::BOX_ID)
    {
        return std::unique_ptr<Bop::ObjectBase>(new Command());
    }
    else if (id == Result::BOX_ID)
    {
        return std::unique_ptr<Bop::ObjectBase>(new Result());
    }

    return nullptr;
}

}// end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
