//-----------------------------------------------------------------------------
/// \file
/// Service - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Service.hpp"
#include "../RpcError.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

Service::Service(
        const QString& name,
        QObject* parent) :
                QObject(parent),
                m_name(name)
{
    // nothing to do
}

void Service::addCommand(
        std::unique_ptr<ServiceCommand> command)
{
    Q_ASSERT(command);
    Q_ASSERT(!hasCommand(command->getName()));

    m_commands.insert(std::make_pair(command->getName(), std::move(command)));
}

bool Service::hasCommand(
        const QString& name) const
{
    return m_commands.find(name) != m_commands.end();
}

quint32 Service::processRequest(
        const Command& command,
        Result& result)
{
    quint32 errorCode = RpcError::UNKNOWN_ACTION;

    auto cmdIter = m_commands.find(command.getName());
    if (cmdIter != m_commands.end())
    {
        errorCode = cmdIter->second->execute(command, result);
    }

    return errorCode;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
