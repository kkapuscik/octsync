//-----------------------------------------------------------------------------
/// \file
/// DataEncoder - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_DataEncoder
#define INCLUDED_OCTsync_DataEncoder

//-----------------------------------------------------------------------------

// local includes
#include "../Variant.hpp"

// library includes
#include <bop/ByteArrayWriter.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class DataEncoder
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Bop::ByteArrayWriter& m_writer;

// Methods (public, protected, private)
public:
    DataEncoder(
            Bop::ByteArrayWriter& writer);

    void append(
            const Variant& variant);
    void append(
            const qint8& value);
    void append(
            const qint16& value);
    void append(
            const qint32& value);
    void append(
            const qint64& value);
    void append(
            const quint8& value);
    void append(
            const quint16& value);
    void append(
            const quint32& value);
    void append(
            const quint64& value);
    void append(
            const bool& value);
    void append(
            const QString& value);
    void append(
            const Commons::Uuid& value);
    void append(
            const QByteArray& value);
    void append(
            const VariantArray& value);
    void append(
            const VariantMap& value);

private:
    void appendType(
            const VariantType type);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_DataEncoder*/
