//-----------------------------------------------------------------------------
/// \file
/// RpcService - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_RpcService
#define INCLUDED_OCTsync_RpcService

//-----------------------------------------------------------------------------

// local includes
#include "../Service.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
class Device;

/// TODO: Class documentation
///
class RpcService : public Service
{
Q_OBJECT

// Static constants (public, protected, private)
private:
    static const QString SERVICE_NAME;

// Types (public, protected, private)
private:
    typedef TypedServiceCommand<RpcService> SvcCmd;

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    Device* m_device;

// Methods (public, protected, private)
public:
    RpcService(
            Device* parentDevice);

private:
    void addCommand(
            const QString& name,
            SvcCmd::Callback callback);

    quint32 cmdGetDeviceInfo(
            const Command& command,
            Result& result);

    quint32 cmdListServices(
            const Command& command,
            Result& result);

    quint32 cmdListCommands(
            const Command& command,
            Result& result);
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_RpcService*/
