//-----------------------------------------------------------------------------
/// \file
/// DataEncoder - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "DataEncoder.hpp"
#include "EncodeErrorException.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

DataEncoder::DataEncoder(
        Bop::ByteArrayWriter& writer) :
                m_writer(writer)
{
    // nothing to do
}

void DataEncoder::append(
        const Variant& variant)
{
    switch (variant.getType())
    {
        case VariantType::INT8:
            append(variant.getInt8());
            break;
        case VariantType::INT16:
            append(variant.getInt16());
            break;
        case VariantType::INT32:
            append(variant.getInt32());
            break;
        case VariantType::INT64:
            append(variant.getInt64());
            break;
        case VariantType::UINT8:
            append(variant.getUint8());
            break;
        case VariantType::UINT16:
            append(variant.getUint16());
            break;
        case VariantType::UINT32:
            append(variant.getUint32());
            break;
        case VariantType::UINT64:
            append(variant.getUint64());
            break;
        case VariantType::BOOL:
            append(variant.getBool());
            break;
        case VariantType::STRING:
            append(variant.getString());
            break;
        case VariantType::UUID:
            append(variant.getUuid());
            break;
        case VariantType::BYTEARRAY:
            append(variant.getByteArray());
            break;
        case VariantType::VARIANTARRAY:
            append(variant.getVariantArray());
            break;
        case VariantType::VARIANTMAP:
            append(variant.getVariantMap());
            break;
        default:
            throw EncodeErrorException("Invalid variant type");
    }
}

void DataEncoder::append(
        const qint8& value)
{
    appendType(VariantType::INT8);
    m_writer.appendInt8(value);
}

void DataEncoder::append(
        const qint16& value)
{
    appendType(VariantType::INT16);
    m_writer.appendInt16(value);
}

void DataEncoder::append(
        const qint32& value)
{
    appendType(VariantType::INT32);
    m_writer.appendInt32(value);
}

void DataEncoder::append(
        const qint64& value)
{
    appendType(VariantType::INT64);
    m_writer.appendInt64(value);
}

void DataEncoder::append(
        const quint8& value)
{
    appendType(VariantType::UINT8);
    m_writer.appendUint8(value);
}

void DataEncoder::append(
        const quint16& value)
{
    appendType(VariantType::UINT16);
    m_writer.appendUint16(value);
}

void DataEncoder::append(
        const quint32& value)
{
    appendType(VariantType::UINT32);
    m_writer.appendUint32(value);
}

void DataEncoder::append(
        const quint64& value)
{
    appendType(VariantType::UINT64);
    m_writer.appendUint64(value);
}

void DataEncoder::append(
        const bool& value)
{
    appendType(VariantType::BOOL);
    m_writer.appendUint8(value ? 1 : 0);
}

void DataEncoder::append(
        const QString& value)
{
    const QByteArray chars = value.toUtf8();
    appendType(VariantType::STRING);
    m_writer.appendUint32(chars.size());
    m_writer.append(chars);
}

void DataEncoder::append(
        const Commons::Uuid& value)
{
    const QByteArray chars = value.toString().toUtf8();
    appendType(VariantType::UUID);
    m_writer.appendUint32(chars.size());
    m_writer.append(chars);
}

void DataEncoder::append(
        const QByteArray& value)
{
    appendType(VariantType::BYTEARRAY);
    m_writer.appendUint32(value.size());
    m_writer.append(value);
}

void DataEncoder::append(
        const VariantArray& value)
{
    appendType(VariantType::VARIANTARRAY);
    m_writer.appendUint32(value.size());
    for (int i = 0; i < value.size(); ++i)
    {
        append(value.at(i));
    }
}

void DataEncoder::append(
        const VariantMap& value)
{
    appendType(VariantType::VARIANTMAP);
    m_writer.appendUint32(value.size());

    auto curIter = value.constBegin();
    auto endIter = value.constEnd();
    for (; curIter != endIter; ++curIter)
    {
        append(curIter.key());
        append(curIter.value());
    }
}

void DataEncoder::appendType(
        const VariantType type)
{
    m_writer.appendUint8(static_cast<quint8>(type));
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
