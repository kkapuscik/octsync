//-----------------------------------------------------------------------------
/// \file
/// Response - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Response.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
using OCTsync::Bop::DataObject;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const Bop::BoxId Response::BOX_ID('B', 'R', 'R', 'S');

Response::Response() :
                DataObject(BOX_ID),
                m_protocolMajor(CURRENT_PROTOCOL_MAJOR),
                m_protocolMinor(CURRENT_PROTOCOL_MINOR),
                m_context(0),
                m_errorCode(0)
{
    // nothing to do
}

void Response::setContext(
        const quint32 context)
{
    m_context = context;
}

void Response::setErrorCode(
        const quint32 errorCode)
{
    m_errorCode = errorCode;
}

bool Response::isValid() const
{
    return (m_protocolMajor == CURRENT_PROTOCOL_MAJOR)
            && (m_protocolMinor == CURRENT_PROTOCOL_MINOR);
}

DataObject::DecodeResult Response::decodePayload(
        Bop::ByteArrayReader& payloadReader)
{
    m_protocolMajor = payloadReader.decodeUint16();
    m_protocolMinor = payloadReader.decodeUint16();
    m_context = payloadReader.decodeUint32();
    m_errorCode = payloadReader.decodeUint32();

    return DataObject::DecodeResult::SUCCESS;
}

DataObject::EncodeResult Response::encodePayload(
        Bop::ByteArrayWriter& writer) const
{
    writer.appendUint16(m_protocolMajor);
    writer.appendUint16(m_protocolMinor);
    writer.appendUint32(m_context);
    writer.appendUint32(m_errorCode);

    return DataObject::EncodeResult::SUCCESS;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
