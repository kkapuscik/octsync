//-----------------------------------------------------------------------------
/// \file
/// Executor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Executor.hpp"
#include "../Request.hpp"
#include "../Response.hpp"
#include "../Client.hpp"

// library includes
#include <QtCore/QMutexLocker>
#include <QtCore/QTimer>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

QMutex Executor::sm_contextMutex;
quint32 Executor::sm_nextContext(0);

quint32 Executor::getNextContext()
{
    QMutexLocker locker(&sm_contextMutex);

    return sm_nextContext++;
}

Executor::Executor(
        QObject* parent) :
                QObject(parent),
                m_state(State::PENDING),
                m_failureReason(FailureReason::UNKNOWN),
                m_context(getNextContext())
{
    std::unique_ptr<Request> requestObject(new Request());
    std::unique_ptr<Command> commandObject(new Command());
    std::unique_ptr<Protocol> protocolObject(new Protocol());

    protocolObject->setRequest(std::move(requestObject), std::move(commandObject));

    m_requestObject = std::move(protocolObject);
}

void Executor::setClient(
        QPointer<Client> client)
{
    m_client = client;
}

QPointer<Client> Executor::getClient() const
{
    return m_client;
}

void Executor::setUrl(
        const QUrl& url)
{
    m_url = url;
}

const QUrl& Executor::getUrl() const
{
    return m_url;
}

void Executor::setServiceId(
        const quint32 serviceId)
{
    getRequestProtocolObject()->getRequest()->setServiceId(serviceId);
}

quint32 Executor::getServiceId() const
{
    return getRequestProtocolObject()->getRequest()->getServiceId();
}

void Executor::setCommandName(
        const QString& commandName)
{
    getCommand().setName(commandName);
}

QString Executor::getCommandName() const
{
    return getCommand().getName();
}

quint32 Executor::getContext() const
{
    return m_context;
}

void Executor::execute()
{
    Q_ASSERT(m_state == State::PENDING);

    if (!m_client)
    {
        setState(State::FAILED, FailureReason::INVALID_CLIENT);
        return;
    }

    if (!m_url.isValid())
    {
        setState(State::FAILED, FailureReason::INVALID_URL);
        return;
    }

    // that includes chcks of command name and service id
    if (!getRequestObject()->isValid())
    {
        setState(State::FAILED, FailureReason::INVALID_REQUEST);
        return;
    }

    // enforce context!
    getRequestProtocolObject()->getRequest()->setContext(m_context);

    setState(State::EXECUTING);

    m_client->execute(this);
}

Executor::State Executor::getState() const
{
    return m_state;
}

Executor::FailureReason Executor::getFailureReason() const
{
    return m_failureReason;
}

quint32 Executor::getErrorCode() const
{
    Q_ASSERT(m_state == State::SUCCEEDED);

    return getResponseProtocolObject()->getResponse()->getErrorCode();
}

Command& Executor::getCommand()
{
    Q_ASSERT(m_state == State::PENDING);

    return *getRequestProtocolObject()->getCommand();
}

const Command& Executor::getCommand() const
{
    return *getRequestProtocolObject()->getCommand();
}

const Result& Executor::getResult() const
{
    Q_ASSERT(m_state == State::SUCCEEDED);

    return *getResponseProtocolObject()->getResult();
}

void Executor::setState(
        const State newState,
        const FailureReason failureReason)
{
    bool notifyFinished = false;

    // TODO: more safety checks

    switch (newState)
    {
        case State::EXECUTING:
            m_state = newState;
            break;

        case State::FAILED:
            m_state = newState;
            m_failureReason = failureReason;
            notifyFinished = true;
            break;

        case State::SUCCEEDED:
            m_state = newState;
            notifyFinished = true;
            break;

        case State::PENDING:
        default:
            Q_ASSERT(0);
    }

    // make sure the notification is asynchronous
    if (notifyFinished)
    {
        QTimer::singleShot(0, this, SLOT( notifyFinished() ));
    }
}

void Executor::notifyFinished()
{
    emit finished(QPointer<Executor>(this));
}

Protocol* Executor::getRequestProtocolObject()
{
    Q_ASSERT(m_requestObject);
    Q_ASSERT(m_requestObject->getId() == Protocol::BOX_ID);

    Protocol* protocol = static_cast<Protocol*>(m_requestObject.get());

    Q_ASSERT(protocol->isRequest());

    return protocol;
}

const Protocol* Executor::getRequestProtocolObject() const
{
    Q_ASSERT(m_requestObject);
    Q_ASSERT(m_requestObject->getId() == Protocol::BOX_ID);

    const Protocol* protocol = static_cast<const Protocol*>(m_requestObject.get());

    Q_ASSERT(protocol->isRequest());

    return protocol;
}

const Protocol* Executor::getResponseProtocolObject() const
{
    Q_ASSERT(m_responseObject);
    Q_ASSERT(m_responseObject->getId() == Protocol::BOX_ID);

    const Protocol* protocol = static_cast<const Protocol*>(m_responseObject.get());

    Q_ASSERT(protocol->isResponse());

    return protocol;
}

Bop::ConstObjectSharedPtr Executor::getRequestObject() const
{
    Q_ASSERT(m_requestObject);
    Q_ASSERT(m_requestObject->getId() == Protocol::BOX_ID);

    return m_requestObject;
}

void Executor::processSendFinished(
        Bop::Error error)
{
    if (m_state == State::EXECUTING)
    {
        if (error == Bop::Error::NONE)
        {
            QTimer::singleShot(DEFAULT_TIMEOUT, this, SLOT( onTimeout() ));
        }
        else
        {
            setState(State::FAILED);
        }
    }
}

void Executor::processResponse(
        Bop::ConstObjectSharedPtr responseObject)
{
    if (m_state == State::EXECUTING)
    {
        m_responseObject = responseObject;

        setState(State::SUCCEEDED);
    }
}

void Executor::onTimeout()
{
    if (m_state == State::EXECUTING)
    {
        setState(State::FAILED, FailureReason::TIMEOUT);
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
