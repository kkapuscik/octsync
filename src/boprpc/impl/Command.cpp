//-----------------------------------------------------------------------------
/// \file
/// Command - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "Command.hpp"
#include "DataEncoder.hpp"
#include "EncodeErrorException.hpp"
#include "DataDecoder.hpp"
#include "DecodeErrorException.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
using OCTsync::Bop::DataObject;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const Bop::BoxId Command::BOX_ID('B', 'R', 'C', 'O');

Command::Command() :
                Bop::DataObject(BOX_ID)
{
    // nothing to do
}

bool Command::isValid() const
{
    if (!isToken(m_name))
    {
        return false;
    }

    for (auto curIter = m_args.begin(), endIter = m_args.end(); curIter != endIter; ++curIter)
    {
        if (!isToken(curIter.key()))
        {
            return false;
        }

        if (!curIter.value().isValid())
        {
            return false;
        }
    }

    return true;
}

bool Command::isToken(
        const QString& key) const
{
    // TODO: extend to check characters
    return key.length() > 0;
}

DataObject::DecodeResult Command::decodePayload(
        Bop::ByteArrayReader& reader)
{
    try
    {
        DataDecoder decoder(reader);

        m_name = decoder.decodeString();
        m_args = decoder.decodeVariantMap();

        if (reader.hasMoreData())
        {
            return DataObject::DecodeResult::INVALID_DATA;
        }

        return DataObject::DecodeResult::SUCCESS;
    }
    catch (DecodeErrorException& e)
    {
        return DataObject::DecodeResult::INVALID_DATA;
    }
}

DataObject::EncodeResult Command::encodePayload(
        Bop::ByteArrayWriter& writer) const
{
    try
    {
        DataEncoder encoder(writer);

        encoder.append(m_name);
        encoder.append(m_args);

        return DataObject::EncodeResult::SUCCESS;
    }
    catch (EncodeErrorException& e)
    {
        return DataObject::EncodeResult::INVALID_DATA;
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
