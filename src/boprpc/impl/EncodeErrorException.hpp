//-----------------------------------------------------------------------------
/// \file
/// EncodeErrorException - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_EncodeErrorException
#define INCLUDED_OCTsync_EncodeErrorException

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <commons/OctException.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class EncodeErrorException : public Commons::OctException
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
public:
    EncodeErrorException(
            const QString& message) :
                    Commons::OctException(message)
    {
        // nothing to do
    }

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_EncodeErrorException*/
