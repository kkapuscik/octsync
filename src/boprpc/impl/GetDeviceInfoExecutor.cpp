//-----------------------------------------------------------------------------
/// \file
/// GetDeviceInfoExecutor - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "GetDeviceInfoExecutor.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const QString GetDeviceInfoExecutor::COMMAND_NAME("GetDeviceInfo");

GetDeviceInfoExecutor::GetDeviceInfoExecutor(
        QObject* parent) :
                Executor(parent)
{
    setServiceId(BopRpc::Executor::RPC_SERVICE_ID);
    setCommandName(COMMAND_NAME);
}

QString GetDeviceInfoExecutor::getDeviceName() const
        throw (BadArgumentException)
{
    try
    {
        return getResult().getArgument("Name").getString();
    }
    catch (TypeMismatchException& e)
    {
        throw BadArgumentException("Invalid argument: Commands");
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
