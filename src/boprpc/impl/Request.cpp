//-----------------------------------------------------------------------------
/// \file
/// Request - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../Request.hpp"
#include "../Service.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
using OCTsync::Bop::DataObject;

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const Bop::BoxId Request::BOX_ID('B', 'R', 'R', 'Q');

Request::Request() :
                DataObject(BOX_ID),
                m_protocolMajor(CURRENT_PROTOCOL_MAJOR),
                m_protocolMinor(CURRENT_PROTOCOL_MINOR),
                m_serviceId(Service::INVALID_SERVICE_ID),
                m_context(0)
{
    // nothing to do
}

void Request::setServiceId(
        const quint32 serviceId)
{
    m_serviceId = serviceId;
}

void Request::setContext(
        const quint32 context)
{
    m_context = context;
}

bool Request::isValid() const
{
    return (m_protocolMajor == CURRENT_PROTOCOL_MAJOR)
            && (m_protocolMinor == CURRENT_PROTOCOL_MINOR)
            && (m_serviceId != Service::INVALID_SERVICE_ID);
}

DataObject::DecodeResult Request::decodePayload(
        Bop::ByteArrayReader& reader)
{
    m_protocolMajor = reader.decodeUint16();
    m_protocolMinor = reader.decodeUint16();
    m_serviceId = reader.decodeUint32();
    m_context = reader.decodeUint32();

    return DataObject::DecodeResult::SUCCESS;
}

DataObject::EncodeResult Request::encodePayload(
        Bop::ByteArrayWriter& writer) const
{
    writer.appendUint16(m_protocolMajor);
    writer.appendUint16(m_protocolMinor);
    writer.appendUint32(m_serviceId);
    writer.appendUint32(m_context);

    return DataObject::EncodeResult::SUCCESS;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
