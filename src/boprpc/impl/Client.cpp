//-----------------------------------------------------------------------------
/// \file
/// Client - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "../Client.hpp"
#include "../Executor.hpp"
#include "../RpcObjectFactory.hpp"
#include "../Protocol.hpp"
#include "../Response.hpp"
#include "../Request.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

Client::Client(
        QObject* parent) :
                QObject(parent)
{
    m_objectFactoryManager = new Bop::ObjectFactoryManager();
    m_objectFactoryManager->addObjectFactory(new RpcObjectFactory(m_objectFactoryManager));

    m_bopClient = new Bop::Client(m_objectFactoryManager, parent);
    connect(m_bopClient, &Bop::Client::objectReceived, this, &Client::onObjectReceived);
    connect(m_bopClient, &Bop::Client::objectSendFinished, this, &Client::onObjectSendFinished);

    // TODO
}

void Client::execute(
        Executor* executor)
{
    Q_ASSERT(executor);

    // store information that will allow to match the response
    m_executors.insert(executor->getContext(), QPointer<Executor>(executor));

    // send the object
    m_bopClient->sendObject(executor->getUrl(), true, executor->getRequestObject());
}

void Client::onObjectReceived(
        QUrl url,
        Bop::ConstObjectSharedPtr object)
{
    if (object->getId() == Protocol::BOX_ID)
    {
        const Protocol* const protocolObject = static_cast<const Protocol*>(object.get());
        if (protocolObject->isResponse())
        {
            Response* const responseObject = protocolObject->getResponse();

            const quint32 responseContext = responseObject->getContext();

            QPointer<Executor> executor = m_executors.value(responseContext);
            if (executor)
            {
                if (executor->getUrl() == url)
                {
                    // we have a match - notify executor about response
                    executor->processResponse(object);
                }
            }
        }
    }
}

void Client::onObjectSendFinished(
        QUrl url,
        Bop::ConstObjectSharedPtr object,
        Bop::Error error)
{
    if (object->getId() == Protocol::BOX_ID)
    {
        const Protocol* const protocolObject = static_cast<const Protocol*>(object.get());
        if (protocolObject->isRequest())
        {
            Request* const requestObject = protocolObject->getRequest();

            const quint32 requestContext = requestObject->getContext();

            QPointer<Executor> executor = m_executors.value(requestContext);
            if (executor)
            {
                if (executor->getUrl() == url)
                {
                    executor->processSendFinished(error);
                }
            }
        }
    }
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
