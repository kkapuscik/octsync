//-----------------------------------------------------------------------------
/// \file
/// RpcService - class implementation.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

// local includes
#include "RpcService.hpp"
#include "../Device.hpp"
#include "../RpcError.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

const QString RpcService::SERVICE_NAME("RPC");

RpcService::RpcService(
        Device* parentDevice) :
                Service(SERVICE_NAME, parentDevice),
                m_device(parentDevice)
{
    addCommand("GetDeviceInfo", &RpcService::cmdGetDeviceInfo);
    addCommand("ListServices", &RpcService::cmdListServices);
    addCommand("ListCommands", &RpcService::cmdListCommands);
}

void RpcService::addCommand(
        const QString& name,
        SvcCmd::Callback callback)
{
    std::unique_ptr<SvcCmd> commandGetDeviceInfo(new SvcCmd());
    commandGetDeviceInfo->setName(name);
    commandGetDeviceInfo->setCallback(this, callback);
    Service::addCommand(std::move(commandGetDeviceInfo));
}

quint32 RpcService::cmdGetDeviceInfo(
        const Command& command,
        Result& result)
{
    try
    {
        if (command.getArgumentCount() != 0)
        {
            return RpcError::INVALID_ARGUMENTS;
        }
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    result.setArgument("Name", m_device->getName());

    return RpcError::SUCCESS;
}

quint32 RpcService::cmdListServices(
        const Command& command,
        Result& result)
{
    try
    {
        if (command.getArgumentCount() != 0)
        {
            return RpcError::INVALID_ARGUMENTS;
        }
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    VariantArray serviceArray;

    auto curIter = m_device->m_services.constBegin();
    auto endIter = m_device->m_services.constEnd();
    for (; curIter != endIter; ++curIter)
    {
        if (curIter.value())
        {
            VariantMap serviceDescriptor;

            serviceDescriptor.insert("Id", curIter.key());
            serviceDescriptor.insert("Name", curIter.value()->getName());

            serviceArray.append(serviceDescriptor);
        }
    }

    result.setArgument("Services", serviceArray);

    return RpcError::SUCCESS;
}

quint32 RpcService::cmdListCommands(
        const Command& command,
        Result& result)
{
    quint32 serviceId;

    try
    {
        if (command.getArgumentCount() != 1)
        {
            return RpcError::INVALID_ARGUMENTS;
        }

        serviceId = command.getArgument("ServiceId").getUint32();
    }
    catch (TypeMismatchException& e)
    {
        return RpcError::INVALID_ARGUMENTS;
    }

    auto service = m_device->m_services.value(serviceId);
    if (!service)
    {
        return RpcError::UNKNOWN_SERVICE;
    }

    VariantArray commandArray;

    auto curIter = service->m_commands.cbegin();
    auto endIter = service->m_commands.cend();
    for (; curIter != endIter; ++curIter)
    {
        commandArray.append(curIter->first);
    }

    result.setArgument("Commands", commandArray);

    return RpcError::SUCCESS;
}

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------
