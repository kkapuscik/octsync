//-----------------------------------------------------------------------------
/// \file
/// Executor - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Executor
#define INCLUDED_OCTsync_Executor

//-----------------------------------------------------------------------------

// local includes
#include "Protocol.hpp"
#include "Command.hpp"
#include "Result.hpp"

// library includes
#include <QtCore/QObject>
#include <QtCore/QMutex>
#include <QtCore/QPointer>
#include <QtCore/QUrl>
#include <bop/ObjectBase.hpp>
#include <bop/Error.hpp>

// system includes
#include <memory>

using OCTsync::Bop::ObjectBase;

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
class Client;

/// TODO: Class documentation
///
class Executor : public QObject
{
Q_OBJECT

    friend class Client;

// Static constants (public, protected, private)
public:
    static const quint32 RPC_SERVICE_ID = 0;

private:
    static const int DEFAULT_TIMEOUT = 15 * 1000;

// Types (public, protected, private)
public:
    enum class FailureReason
    {
        UNKNOWN,
        INVALID_CLIENT,
        INVALID_URL,
        INVALID_REQUEST,
        TIMEOUT,
    };

    enum class State
    {
        PENDING,
        EXECUTING,
        FAILED,
        SUCCEEDED
    };

// Static members (public, protected, private)
private:
    static QMutex sm_contextMutex;
    static quint32 sm_nextContext;

// Static methods (public, protected, private)
private:
    static quint32 getNextContext();

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    State m_state;
    FailureReason m_failureReason;
    QPointer<Client> m_client;
    QUrl m_url;
    quint32 m_context;

    Bop::ObjectSharedPtr m_requestObject;
    Bop::ConstObjectSharedPtr m_responseObject;

// Methods (public, protected, private)
public:
    Executor(
            QObject* parent = nullptr);

    void setClient(
            QPointer<Client> client);

    QPointer<Client> getClient() const;

    void setUrl(
            const QUrl& url);

    const QUrl& getUrl() const;

    void setServiceId(
            const quint32 serviceId);

    quint32 getServiceId() const;

    void setCommandName(
            const QString& commandName);

    QString getCommandName() const;

    quint32 getContext() const;

    void execute();

    State getState() const;

    FailureReason getFailureReason() const;

    quint32 getErrorCode() const;

    Command& getCommand();

    const Command& getCommand() const;

    const Result& getResult() const;

private:
    void setState(
            const State newState,
            const FailureReason failureReason = FailureReason::UNKNOWN);

    Protocol* getRequestProtocolObject();

    const Protocol* getRequestProtocolObject() const;

    const Protocol* getResponseProtocolObject() const;

    Bop::ConstObjectSharedPtr getRequestObject() const;

    void processSendFinished(
            Bop::Error error);

    void processResponse(
            Bop::ConstObjectSharedPtr responseObject);

private slots:
    void notifyFinished();

    void onTimeout();

signals:
    void finished(
            QPointer<Executor> executor);

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Executor*/
