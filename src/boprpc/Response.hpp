//-----------------------------------------------------------------------------
/// \file
/// Response - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Response
#define INCLUDED_OCTsync_Response

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <bop/DataObject.hpp>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Response : public Bop::DataObject
{
// Static constants (public, protected, private)
public:
    static const Bop::BoxId BOX_ID;

private:
    static const quint16 CURRENT_PROTOCOL_MAJOR = 1;
    static const quint16 CURRENT_PROTOCOL_MINOR = 0;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
// (none)

// Methods (public, protected, private)
private:
    quint16 m_protocolMajor;
    quint16 m_protocolMinor;
    quint32 m_context;
    quint32 m_errorCode;

// Methods (public, protected, private)
public:
    Response();

    quint32 getContext() const
    {
        return m_context;
    }

    void setContext(
            const quint32 context);

    quint32 getErrorCode() const
    {
        return m_errorCode;
    }

    void setErrorCode(
            const quint32 errorCode);

    virtual bool isValid() const;

protected:
    virtual DecodeResult decodePayload(
            Bop::ByteArrayReader& payloadReader);

    virtual EncodeResult encodePayload(
            Bop::ByteArrayWriter& bytes) const;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Response*/
