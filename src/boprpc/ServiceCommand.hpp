//-----------------------------------------------------------------------------
/// \file
/// ServiceCommand - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_ServiceCommand
#define INCLUDED_OCTsync_ServiceCommand

//-----------------------------------------------------------------------------

// local includes
#include "Command.hpp"
#include "Result.hpp"

// library includes
// (none)

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
class Service;

/// TODO: Class documentation
///
class ServiceCommand
{
// Static constants (public, protected, private)
// (none)

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QString m_name;

// Methods (public, protected, private)
public:
    ServiceCommand()
    {
        // nothing to do
    }

    virtual ~ServiceCommand()
    {
        // nothing to do
    }

    virtual quint32 execute(
            const Command& command,
            Result& result) = 0;

    void setName(
            const QString& name)
    {
        m_name = name;
    }

    const QString& getName() const
    {
        return m_name;
    }

};

template<class ServiceType>
class TypedServiceCommand : public ServiceCommand
{
    // Static constants (public, protected, private)
    // (none)

    // Types (public, protected, private)
public:
    typedef quint32 (ServiceType::*Callback)(
            const Command& command,
            Result& result);

    // Static members (public, protected, private)
    // (none)

    // Static methods (public, protected, private)
    // (none)

    // Constants (public, protected, private)
    // (none)

    // Members (public, protected, private)
private:
    ServiceType* m_service;
    Callback m_callback;

    // Methods (public, protected, private)
public:
    TypedServiceCommand() :
                    m_service(nullptr),
                    m_callback(nullptr)
    {
        // nothing to do
    }

    void setCallback(
            ServiceType* service,
            const Callback callback)
    {
        Q_ASSERT(service);
        Q_ASSERT(callback);

        m_service = service;
        m_callback = callback;
    }

    Callback getCallback() const
    {
        return m_callback;
    }

    quint32 execute(
            const Command& command,
            Result& result)
    {
        quint32 retval = 1; // TODO

        if (m_callback && m_service)
        {
            retval = (m_service->*m_callback)(command, result);
        }

        return retval;
    }

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_ServiceCommand*/
