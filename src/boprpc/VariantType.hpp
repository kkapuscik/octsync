//-----------------------------------------------------------------------------
/// \file
/// VariantType - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_VariantType
#define INCLUDED_OCTsync_VariantType

//-----------------------------------------------------------------------------

// local includes
// (none)

// library includes
#include <QtCore/QtGlobal>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// TODO: documentation
///
/// Variant data type.
enum class VariantType : quint8
{
    /// Invalid type, used for unset variants.
    INVALID = 0x00,
    /// 8-bit signed integer.
    INT8 = 0x10,
    /// 16-bit signed integer.
    INT16 = 0x11,
    /// 32-bit signed integer.
    INT32 = 0x12,
    /// 64-bit signed integer.
    INT64 = 0x13,
    /// 8-bit unsigned integer.
    UINT8 = 0x20,
    /// 16-bit unsigned integer.
    UINT16 = 0x21,
    /// 32-bit unsigned integer.
    UINT32 = 0x22,
    /// 64-bit unsigned integer.
    UINT64 = 0x23,
    /// Boolean.
    BOOL = 0x40,
    /// String.
    STRING = 0x80,
    /// UUID,
    UUID = 0x90,
    /// Byte array.
    BYTEARRAY = 0xA0,
    /// Variant array.
    VARIANTARRAY = 0xB0,
    /// Variant map.
    VARIANTMAP = 0xB1
};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_VariantType*/
