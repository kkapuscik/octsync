//-----------------------------------------------------------------------------
/// \file
/// Command - class definition.
///
//-----------------------------------------------------------------------------
// This file is a part of OCTsync project.
//
// Copyright (C) 2013 OCTaedr Software
// All Rights Reserved
//-----------------------------------------------------------------------------

#ifndef INCLUDED_OCTsync_Command
#define INCLUDED_OCTsync_Command

//-----------------------------------------------------------------------------

// local includes
#include "Variant.hpp"

// library includes
#include <bop/DataObject.hpp>
#include <QtCore/QMap>

// system includes
// (none)

// using clauses
// (none)

// forward references
// (none)

//-----------------------------------------------------------------------------

namespace OCTsync
{
namespace BopRpc
{

// forward references in namespace
// (none)

/// TODO: Class documentation
///
class Command : public Bop::DataObject
{
// Static constants (public, protected, private)
public:
    static const Bop::BoxId BOX_ID;

// Types (public, protected, private)
// (none)

// Static members (public, protected, private)
// (none)

// Static methods (public, protected, private)
// (none)

// Constants (public, protected, private)
// (none)

// Members (public, protected, private)
private:
    QString m_name;
    VariantMap m_args;

// Methods (public, protected, private)
public:
    Command();

    const QString& getName() const
    {
        return m_name;
    }

    void setName(
            const QString& name)
    {
        m_name = name;
    }

    int getArgumentCount() const
    {
        return m_args.size();
    }

    void setArgument(
            const QString& name,
            const Variant& value)
    {
        m_args.insert(name, value);
    }

    void unsetArgument(
            const QString& name)
    {
        m_args.remove(name);
    }

    bool hasArgument(
            const QString& name) const
    {
        return m_args.contains(name);
    }

    Variant getArgument(
            const QString& name) const
    {
        return m_args.value(name);
    }

    virtual bool isValid() const;

protected:
    virtual DecodeResult decodePayload(
            Bop::ByteArrayReader& reader);

    virtual EncodeResult encodePayload(
            Bop::ByteArrayWriter& writer) const;

private:
    virtual bool isToken(
            const QString& key) const;

};

} // end of namespace
} // end of namespace

//-----------------------------------------------------------------------------

#endif /*INCLUDED_OCTsync_Command*/
