#!/bin/bash

INSTALL_DIRECTORY=$(readlink -f "../../install")

mkdir -p build

pushd build

cmake -D OCT_INSTALL_DIRECTORY=$INSTALL_DIRECTORY ..
make -j 8 $*

popd
